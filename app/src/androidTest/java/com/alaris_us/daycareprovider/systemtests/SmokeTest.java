package com.alaris_us.daycareprovider.systemtests;

import androidx.test.filters.LargeTest;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;

import com.alaris_us.daycareprovider.activities.DaycareProviderKiosk;
import com.alaris_us.daycareprovider_dev.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.*;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4ClassRunner.class)
@LargeTest
public class SmokeTest {

    @Rule
    public ActivityTestRule<DaycareProviderKiosk> kiosk =
            new ActivityTestRule<>(DaycareProviderKiosk.class);

    @Test
    public void dashboardDisplaysOnStartup() {
        // check the progress bar displays
        onView(withId(R.id.initializeProgressBar)).check(matches(isDisplayed()));
    }
}
