package com.alaris_us.daycareprovider.fragments;

import android.app.Activity;
import android.app.Fragment;

import com.alaris_us.daycareprovider.adapters.TabsAdapter.TabFragment;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;

public abstract class ProviderFragment extends Fragment implements TabFragment {

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);

		if (!(activity instanceof PersistenceLayerHost)) {
			throw new ClassCastException(activity.toString() + " must implement PersistentInterfaceHost");
		}

	}

	public interface PersistenceLayerHost {

		PersistenceLayer getPersistenceLayer();

	}

}
