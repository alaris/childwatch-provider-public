package com.alaris_us.daycareprovider.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.view.GuestMemberListItem;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class ConfirmationGuestCreator extends BaseGuestCreator {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_createguest_confirmation, container, false);
		UiBinder.bind(rootView, R.id.ListView_DisplayMembers, "Adapter", this, "ViewModel.Created",
				new AdapterConverter(GuestMemberListItem.class));
		return rootView;
	}
}
