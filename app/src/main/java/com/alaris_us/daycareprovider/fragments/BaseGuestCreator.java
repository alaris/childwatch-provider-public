package com.alaris_us.daycareprovider.fragments;

import android.app.Activity;
import android.app.Fragment;

import com.alaris_us.daycareprovider.models.GuestCreatorViewModel;
import com.alaris_us.daycareprovider.models.ViewModel.ViewModelHost;

public abstract class BaseGuestCreator extends Fragment {

	private GuestCreatorViewModel mViewModel;

	public GuestCreatorViewModel getViewModel() {

		return mViewModel;
	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		if (!(activity instanceof ViewModelHost)) {
			throw new ClassCastException(activity.toString() + " must implement ViewModelHost");
		}
		Object viewModel = ((ViewModelHost<?>) activity).getModel();
		if (viewModel instanceof GuestCreatorViewModel) {
			mViewModel = (GuestCreatorViewModel) viewModel;
		}
	}
}
