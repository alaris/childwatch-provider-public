package com.alaris_us.daycareprovider.fragments;

import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;

import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.ui.EditTextTextProperty;
import com.bindroid.ui.SeekBarProgressProperty;
import com.bindroid.ui.UiBinder;

public class PinEntryGuestCreator extends BaseGuestCreator {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_createguest_pin, container, false);
		EditText phoneNumber = (EditText) rootView.findViewById(R.id.EditText_PhoneNumber);
		phoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		UiBinder.bind(new EditTextTextProperty(phoneNumber), this, "ViewModel.PhoneNumber", BindingMode.TWO_WAY);
		UiBinder.bind(new SeekBarProgressProperty((SeekBar) rootView.findViewById(R.id.SeekBar_DaysAllowed)), this,
				"ViewModel.DaysAllowed", BindingMode.TWO_WAY);
		UiBinder.bind(rootView, R.id.TextView_DaysAllowed, "Text", this, "ViewModel.DaysAllowed", BindingMode.ONE_WAY,
				new ToStringConverter("%s Days"));
		return rootView;
	}
}
