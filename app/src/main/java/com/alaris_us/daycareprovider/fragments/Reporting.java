package com.alaris_us.daycareprovider.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.models.ReportingViewModel;
import com.alaris_us.daycareprovider.view.ReportingListItem;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class Reporting extends ProviderFragment {

	ReportingViewModel m_model;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		return inflater.inflate(R.layout.fragment_reporting, container, false);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);

		m_model = new ReportingViewModel(((PersistenceLayerHost) getActivity()).getPersistenceLayer());

		UiBinder.bind(this.getActivity(), R.id.reporting_startButton, "OnClickListener", m_model,
				"StartTimeButtonListener", BindingMode.ONE_WAY);
		UiBinder.bind(this.getActivity(), R.id.reporting_endButton, "OnClickListener", m_model, "EndTimeButtonListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(this.getActivity(), R.id.reporting_query, "OnClickListener", m_model, "UpdateButtonListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(this.getActivity(), R.id.reporting_startButton, "Text", m_model, "StartDateString",
				BindingMode.ONE_WAY);
		UiBinder.bind(this.getActivity(), R.id.reporting_endButton, "Text", m_model, "EndDateString",
				BindingMode.ONE_WAY);
		UiBinder.bind(this.getActivity(), R.id.reporting_list, "Adapter", m_model, "ReportingHistory",
				new AdapterConverter(ReportingListItem.class));
	}

	@Override
	public String getTitle() {

		return "Check-Ins";

	}

}
