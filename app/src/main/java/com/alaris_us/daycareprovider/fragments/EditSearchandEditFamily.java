package com.alaris_us.daycareprovider.fragments;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.data.AuthorizedMember;
import com.alaris_us.daycareprovider.models.SearchAndEditFamilyViewModel;
import com.alaris_us.daycareprovider.models.SearchAndEditFamilyViewModel.SEARCH_PARAMETER_DELEGATE;
import com.alaris_us.daycareprovider.popups.AuthorizationPopup.AuthorizationPopupListener;
import com.alaris_us.daycareprovider.utils.ImageFunctions;
import com.alaris_us.daycareprovider.utils.ImageFunctions.NullBitmapConverter;
import com.alaris_us.daycareprovider.view.AuthorizationMemberItem;
import com.alaris_us.daycareprovider.view.LinearListView;
import com.alaris_us.daycareprovider.view.MemberEditResultItem;
import com.alaris_us.daycareprovider.view.ViewHolder;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.ui.EditTextTextProperty;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class EditSearchandEditFamily extends BaseSearchAndEditFamily implements AuthorizationPopupListener{
	private View mMemberInfo, mMemberForm;
	private ViewHolder mViewHolder;
	private View mActionBarView;
	private Bitmap mBlankAvatar;
	/*
	 * private View unsavedChangesDialogView; private AlertDialog
	 * unsavedChangesDialog;
	 */
	private EditText notesEdittext;
	private InputMethodManager Imm;

	private void bindActionBar() {
		if (getViewModel() == null || mActionBarView == null)
			return;
		UiBinder.bind(mActionBarView, R.id.searchbar_familyname, "Text", getViewModel(), "FamilyName",
				BindingMode.ONE_WAY);
		UiBinder.bind(mActionBarView, R.id.editbutton_image, "OnClickListener", getViewModel(), "EditOnClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(mActionBarView, R.id.editbutton_image, "ImageResource", getViewModel(), "SaveOrEditButtonRes",
				BindingMode.ONE_WAY);
		UiBinder.bind(mActionBarView, R.id.discardbutton_image, "OnClickListener", getViewModel(),
				"DiscardChangesOnClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(mActionBarView, R.id.discardbutton_image, "Visibility", getViewModel(), "DiscardButtonVisibility",
				BindingMode.ONE_WAY);
		UiBinder.bind(mActionBarView, R.id.guestconversionbutton_image, "Visibility", getViewModel(),
				"GuestConversionButtonVisibility", BindingMode.ONE_WAY);
		UiBinder.bind(mActionBarView, R.id.guestconversionbutton_image, "OnClickListener", getViewModel(),
				"GuestConversionOnClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(mActionBarView, R.id.editfamily_backbutton_container, "OnClickListener", getViewModel(),
				"EditFamilyBackButtonListener", BindingMode.ONE_WAY);
		UiBinder.bind(mActionBarView, R.id.familysettingsbutton_image, "OnClickListener", getViewModel(),
				"FamilySettingsOnClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(mActionBarView, R.id.familysettingsbutton_image, "Visibility", getViewModel(), "FamilySettingsVisibility");
	}

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.editfamily_layout, container, false);

		mMemberInfo = inflater.inflate(R.layout.member_editinfo, null);
		mMemberForm = inflater.inflate(R.layout.member_editform, null);

		mViewHolder = (ViewHolder) rootView.findViewById(R.id.editfamily_holder);

		mBlankAvatar = ImageFunctions.drawableToBitmap(getResources().getDrawable(R.drawable.photo_not_available));

		// Binding
		UiBinder.bind(rootView, R.id.listview_parents, "Adapter", getViewModel(), "AdultResults",
				new AdapterConverter(MemberEditResultItem.class));
		UiBinder.bind(rootView, R.id.listview_children, "Adapter", getViewModel(), "ChildResults",
				new AdapterConverter(MemberEditResultItem.class));
		UiBinder.bind(new ReflectedProperty(this, "IsEditable"), getViewModel(), "IsEditable", BindingMode.ONE_WAY);
		UiBinder.bind(rootView, R.id.listview_parents, "OnItemClickListener", getViewModel(),
				"EditMemberOnItemClickListener");
		UiBinder.bind(rootView, R.id.listview_children, "OnItemClickListener", getViewModel(),
				"EditMemberOnItemClickListener");
		UiBinder.bind(rootView, R.id.update_button, "OnClickListener", getViewModel(), "UpdateButtonOnClickListener");

		/*
		 * UiBinder.bind(rootView, R.id.listview_parents, "Alpha",
		 * getViewModel(), "ParentAlpha"); UiBinder.bind(rootView,
		 * R.id.listview_children, "Alpha", getViewModel(), "ChildrenAlpha");
		 */

		//UiBinder.bind(new ReflectedProperty(this, "MembershipLabel"), getViewModel(), "MembershipLabel",
				//BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "StatusLabel"), getViewModel(), "StatusLabel", BindingMode.ONE_WAY);

		// Binding info pane
		UiBinder.bind(mMemberInfo, R.id.firstname, "Text", getViewModel(), "SelectedMember.FirstName",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.lastname, "Text", getViewModel(), "SelectedMember.LastName",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.dateofbirth, "Text", getViewModel(), "DateOfBirth", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.gender, "Text", getViewModel(), "SelectedMember.Gender", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.email, "Text", getViewModel(), "SelectedMember.Email", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.membership_type, "Text", getViewModel(), "SelectedMember.MembershipType",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.membership, "Text", getViewModel(), "Membership", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.status, "Text", getViewModel(), "Status", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.home, "Text", getViewModel(), "HomeNumber", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.work, "Text", getViewModel(), "WorkNumber", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.barcode, "Text", getViewModel(), "SelectedMember.Barcode", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.lastcheckin, "Text", getViewModel(), "LastCheckIn", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.minutesleft, "Text", getViewModel(), "MinutesLeft", BindingMode.ONE_WAY,
				new ToStringConverter("%d min remaining"));
		UiBinder.bind(mMemberInfo, R.id.weeklyminutesleft, "Text", getViewModel(), "WeeklyMinutesLeft",
				BindingMode.ONE_WAY, new ToStringConverter("%d min remaining"));
		UiBinder.bind(mMemberInfo, R.id.service, "Text", getViewModel(), "SelectedMember.Service",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.medical, "Text", getViewModel(), "SelectedMember.MedicalConcerns",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.sms, "Text", getViewModel(), "SMSNumber", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.notes, "Text", getViewModel(), "SelectedMember.Notes", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.heading_authorizations, "Visibility", getViewModel(), "AuthHeadingVisibility", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "Authorizations"), getViewModel(), "Authorizations",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencyname1, "Text", getViewModel(), "EmergencyContacts[0].Name",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencyname2, "Text", getViewModel(), "EmergencyContacts[1].Name",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencyname3, "Text", getViewModel(), "EmergencyContacts[2].Name",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencyname4, "Text", getViewModel(), "EmergencyContacts[3].Name",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencyname5, "Text", getViewModel(), "EmergencyContacts[4].Name",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencyname6, "Text", getViewModel(), "EmergencyContacts[5].Name",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencynum1, "Text", getViewModel(), "EmergencyContacts[0].Phone",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencynum2, "Text", getViewModel(), "EmergencyContacts[1].Phone",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencynum3, "Text", getViewModel(), "EmergencyContacts[2].Phone",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencynum4, "Text", getViewModel(), "EmergencyContacts[3].Phone",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencynum5, "Text", getViewModel(), "EmergencyContacts[4].Phone",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.emergencynum6, "Text", getViewModel(), "EmergencyContacts[5].Phone",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.photo, "ImageBitmap", getViewModel(), "SelectedMember.Image",
				BindingMode.ONE_WAY, new NullBitmapConverter(mBlankAvatar));
		UiBinder.bind(mMemberInfo, R.id.pottyTrained, "IsChecked", getViewModel(), "PottyTrained", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.usesChildWatch, "IsChecked", getViewModel(), "UsesChildWatch",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.diaper, "IsChecked", getViewModel(), "Diaper", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.feeding, "IsChecked", getViewModel(), "Feeding",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.enrichment, "IsChecked", getViewModel(), "Enrichment",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.form, "IsChecked", getViewModel(), "HasForm",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberInfo, R.id.usesSMS, "IsChecked", getViewModel(), "UsesSMS", BindingMode.ONE_WAY);
        UiBinder.bind(mMemberInfo, R.id.canSelfCheckIn, "IsChecked", getViewModel(), "CanSelfCheckIn",
                BindingMode.ONE_WAY);
        UiBinder.bind(mMemberInfo, R.id.canSelfCheckIn, "Visibility", getViewModel(), "SelfCheckInVisibility",
                BindingMode.ONE_WAY);

		// Binding form pane
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.firstname)), getViewModel(),
				"MemberForm.FirstName", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.lastname)), getViewModel(),
				"MemberForm.LastName", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.dateofbirth)), getViewModel(),
				"DateOfBirth", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.gender)), getViewModel(),
				"MemberForm.Gender", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.email)), getViewModel(),
				"MemberForm.Email", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.membership_type)), getViewModel(),
				"MembershipType", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.membership)), getViewModel(),
				"Membership", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.status)), getViewModel(),
				"Status", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.home)), getViewModel(),
				"HomeNumber", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.work)), getViewModel(),
				"WorkNumber", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.barcode)), getViewModel(),
				"MemberForm.Barcode", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.lastcheckin)), getViewModel(),
				"LastCheckIn", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.minutesleft)), getViewModel(),
				"MinutesLeft", BindingMode.TWO_WAY, new ToStringConverter("%d min remaining"));
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.weeklyminutesleft)),
				getViewModel(), "WeeklyMinutesLeft", BindingMode.TWO_WAY, new ToStringConverter("%d min remaining"));
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.service)), getViewModel(),
				"Service", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.medical)), getViewModel(),
				"MemberForm.MedicalConcerns", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.sms)), getViewModel(),
				"SMSNumber", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.notes)), getViewModel(),
				"MemberForm.Notes", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencyname1)),
				getViewModel(), "EmergencyContacts[0].Name", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencyname2)),
				getViewModel(), "EmergencyContacts[1].Name", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencyname3)),
				getViewModel(), "EmergencyContacts[2].Name", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencyname4)),
				getViewModel(), "EmergencyContacts[3].Name", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencyname5)),
				getViewModel(), "EmergencyContacts[4].Name", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencyname6)),
				getViewModel(), "EmergencyContacts[5].Name", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencynum1)), getViewModel(),
				"EmergencyContacts[0].Phone", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencynum2)), getViewModel(),
				"EmergencyContacts[1].Phone", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencynum3)), getViewModel(),
				"EmergencyContacts[2].Phone", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencynum4)), getViewModel(),
				"EmergencyContacts[3].Phone", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencynum5)), getViewModel(),
				"EmergencyContacts[4].Phone", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) mMemberForm.findViewById(R.id.emergencynum6)), getViewModel(),
				"EmergencyContacts[5].Phone", BindingMode.TWO_WAY);
		UiBinder.bind(mMemberForm, R.id.status, "Enabled", getViewModel(), "IsStatusEnabled", BindingMode.ONE_WAY);
		UiBinder.bind(mMemberForm, R.id.photo2, "ImageBitmap", getViewModel(), "MemberForm.Image", BindingMode.ONE_WAY,
				new NullBitmapConverter(mBlankAvatar));
		UiBinder.bind(mMemberForm, R.id.photo2, "OnClickListener", getViewModel(), "MemberPhotoOnClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberForm, R.id.button_setforall, "OnClickListener", getViewModel(), "SetForAllOnClickListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(mMemberForm, R.id.pottyTrained, "IsChecked", getViewModel(), "PottyTrained", BindingMode.TWO_WAY);
		UiBinder.bind(mMemberForm, R.id.usesChildWatch, "IsChecked", getViewModel(), "UsesChildWatch",
				BindingMode.TWO_WAY);
		UiBinder.bind(mMemberForm, R.id.diaper, "IsChecked", getViewModel(), "Diaper", BindingMode.TWO_WAY);
		UiBinder.bind(mMemberForm, R.id.feeding, "IsChecked", getViewModel(), "Feeding",
				BindingMode.TWO_WAY);
		UiBinder.bind(mMemberForm, R.id.enrichment, "IsChecked", getViewModel(), "Enrichment",
				BindingMode.TWO_WAY);
		UiBinder.bind(mMemberForm, R.id.form, "IsChecked", getViewModel(), "HasForm",
				BindingMode.TWO_WAY);
		UiBinder.bind(mMemberForm, R.id.usesSMS, "IsChecked", getViewModel(), "UsesSMS", BindingMode.TWO_WAY);
        UiBinder.bind(mMemberForm, R.id.canSelfCheckIn, "IsChecked", getViewModel(), "CanSelfCheckIn",
                BindingMode.TWO_WAY);
        UiBinder.bind(mMemberForm, R.id.canSelfCheckIn, "Visibility", getViewModel(), "SelfCheckInVisibility",
                BindingMode.ONE_WAY);

		((EditText) mMemberForm.findViewById(R.id.emergencynum1))
				.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		((EditText) mMemberForm.findViewById(R.id.emergencynum2))
				.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		((EditText) mMemberForm.findViewById(R.id.emergencynum3))
				.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		((EditText) mMemberForm.findViewById(R.id.emergencynum4))
				.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		((EditText) mMemberForm.findViewById(R.id.emergencynum5))
				.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		((EditText) mMemberForm.findViewById(R.id.emergencynum6))
				.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		

		mMemberForm.findViewById(R.id.membership).setEnabled(false);
		mMemberForm.findViewById(R.id.status).setEnabled(true);
		mMemberForm.findViewById(R.id.home).setEnabled(true);
		mMemberForm.findViewById(R.id.work).setEnabled(true);
		// mMemberForm.findViewById(R.id.barcode).setEnabled(false);
		mMemberForm.findViewById(R.id.lastcheckin).setEnabled(false);
		mMemberForm.findViewById(R.id.dateofbirth).setEnabled(false);
		mMemberForm.findViewById(R.id.membership_type).setEnabled(false);
		mMemberInfo.findViewById(R.id.usesSMS).setEnabled(false);
		mMemberInfo.findViewById(R.id.pottyTrained).setEnabled(false);
		mMemberInfo.findViewById(R.id.usesChildWatch).setEnabled(false);
		mMemberInfo.findViewById(R.id.diaper).setEnabled(false);
		mMemberInfo.findViewById(R.id.feeding).setEnabled(false);
		mMemberInfo.findViewById(R.id.enrichment).setEnabled(false);
		mMemberInfo.findViewById(R.id.form).setEnabled(false);
		mMemberForm.findViewById(R.id.service).setEnabled(false);
        mMemberInfo.findViewById(R.id.canSelfCheckIn).setEnabled(false);

		mMemberForm.findViewById(R.id.membership).setFocusable(false);
		mMemberForm.findViewById(R.id.status).setFocusable(true);
		// mMemberForm.findViewById(R.id.userpin).setFocusable(false);
		// mMemberForm.findViewById(R.id.barcode).setFocusable(false);
		mMemberForm.findViewById(R.id.lastcheckin).setFocusable(false);
		mMemberForm.findViewById(R.id.dateofbirth).setFocusable(false);
		mMemberForm.findViewById(R.id.service).setFocusable(false);

		notesEdittext = (EditText) mMemberForm.findViewById(R.id.notes);
		Imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		Imm.showSoftInput(notesEdittext, InputMethodManager.SHOW_IMPLICIT);
		notesEdittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus)
					Imm.showSoftInput(notesEdittext, InputMethodManager.SHOW_IMPLICIT);
				else
					Imm.hideSoftInputFromWindow(notesEdittext.getWindowToken(), 0);
			}
		});

		mMemberForm.findViewById(R.id.button_addcontact).setVisibility(View.GONE);

		bindActionBar();
		return rootView;
	}

	public void setAuthorizations(List<AuthorizedMember> auths) {
		if (auths != null) {
			LinearListView layout = (LinearListView) mMemberInfo.findViewById(R.id.authorized_listview);
			List<AuthorizationMemberItem> views = new ArrayList<AuthorizationMemberItem>();
			for (AuthorizedMember auth : auths) {
				AuthorizationMemberItem view = new AuthorizationMemberItem(layout.getContext(), EditSearchandEditFamily.this);
				view.bind(auth);
				views.add(view);
			}
			layout.setChildrenViews(views);
		}
	}

	public void setIsEditable(boolean isEditable) {
		if (isEditable)
			mViewHolder.setView(mMemberForm);
		else
			mViewHolder.setView(mMemberInfo);
	}

	@Override
	public int getActionBarLayout() {

		return R.layout.view_editfamily_navigation;
	}

	@Override
	public void setActionBarView(View bar) {

		mActionBarView = bar;
		bindActionBar();
	}

	public void setStatusLabel(int text) {
		((TextView) mMemberInfo.findViewById(R.id.label_status)).setText(text);
		((TextView) mMemberForm.findViewById(R.id.label_status)).setText(text);
	}

	@Override
	public void onAuthFamilyButtonClicked(Member authMember) {
		if (!authMember.getMemberID().isEmpty())
			getViewModel().query(authMember.getMemberID(), SEARCH_PARAMETER_DELEGATE.MEMBER_ID);
		else {
			getViewModel().query(authMember.getFirstName() + " " + authMember.getLastName(), SEARCH_PARAMETER_DELEGATE.FULL_NAME);
			getViewModel().setCurrentPage(SearchAndEditFamilyViewModel.SEARCHRESULTPAGE);
		}
	}
}
