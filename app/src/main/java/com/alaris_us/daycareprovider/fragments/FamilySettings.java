package com.alaris_us.daycareprovider.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.AdmissionBalance;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycareprovider.data.AdmissionAndFacility;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.view.SelectFacilityDropDown;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

public class FamilySettings extends BaseSearchAndEditFamily {
	private View mActionBarView;
	private TrackableField<String> mFamilyName = new TrackableField<String>();
	private LinearLayout mAdmissionLayout;
	private LayoutInflater mInflater;
	private List<AdmissionAndFacility> mFamilyAdmissions;
	private List<Facility> mAssocFacilities;
	private AdmissionAndFacility mAdmit;
	private List<Admission> mOrigFamilyAdmissions;
	private View mRootView;
	
	private void bindActionBar() {
		if (getViewModel() == null || mActionBarView == null)
			return;
		mActionBarView.findViewById(R.id.familysettings_backbutton_container)
				.setOnClickListener(new BackButtonOnClickListener());
		UiBinder.bind(mActionBarView, R.id.familysettings_header, "Text", this, "FamilyName",
				BindingMode.ONE_WAY);
		UiBinder.bind(mActionBarView, R.id.familysettings_savebutton, "OnClickListener", this, "SaveOnClickListener",
				BindingMode.ONE_WAY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mInflater = inflater;
		mRootView = inflater.inflate(R.layout.fragment_familysettings, container, false);
		mAdmissionLayout = (LinearLayout) mRootView.findViewById(R.id.admissions_container);
		mFamilyAdmissions = new ArrayList<>();
		mOrigFamilyAdmissions = new ArrayList<>();
		
		UiBinder.bind(new ReflectedProperty(this, "FamilyAdmissions"), getViewModel(), "FamilyAdmissions", BindingMode.ONE_WAY);
		UiBinder.bind(mRootView, R.id.familysettings_add, "OnClickListener", this, "FamilySettingsAddAdmissionOnClickListener", BindingMode.ONE_WAY);
		UiBinder.bind(mRootView, R.id.familysettings_add, "Visibility", this, "AddButtonVisibility");
		
		bindActionBar();
		return mRootView;
	}

	@Override
	public int getActionBarLayout() {
		return R.layout.view_familysettings_navigation;
	}

	@Override
	public void setActionBarView(View bar) {
		mActionBarView = bar;
		bindActionBar();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	private class BackButtonOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mAdmissionLayout.removeAllViewsInLayout();
			getViewModel().exit();
		}
	}
	
	public String getFamilyName() {
		mFamilyName.set(getViewModel().getFamily().getName());
		return mFamilyName.get() + " FAMILY SETTINGS";
	}
	
	public void setFamilyAdmissions(List<AdmissionAndFacility> familyAdmissions) {
		mAssocFacilities = getViewModel().getAssocFacilities();
		/*for (int i = 0; i < familyAdmissions.size(); i++) {
			mFamilyAdmissions.add(familyAdmissions.get(i).getAdmission());
			mOrigFamilyAdmissions.add(familyAdmissions.get(i).getAdmission());
		}*/
		mFamilyAdmissions = familyAdmissions;
		for (AdmissionAndFacility admit : familyAdmissions) {
			mAdmit = admit;
			View v = mInflater.inflate(R.layout.familysettings_admission_item, null);
			EditText et = (EditText) v.findViewById(R.id.familysettings_numOfAdmissions);
			et.setText(admit.getAdmission());
			UiBinder.bind(v, R.id.familysettings_facility_spinner, "Adapter", this, "FacilitySelectionList",
					new AdapterConverter(SelectFacilityDropDown.class));
			UiBinder.bind(v, R.id.familysettings_facility_spinner, "Selection", this, "DefaultSelection");
			ImageButton deleteButton = (ImageButton) v.findViewById(R.id.familysettings_delete);
			deleteButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int indexOfAdmit = mAdmissionLayout.indexOfChild((View) v.getParent());
					if (!(indexOfAdmit > getViewModel().getFamilyAdmissions().size())) {
						getViewModel().getFamilyAdmissions().remove(indexOfAdmit);
						mFamilyAdmissions.remove(indexOfAdmit);
					}
					if (getViewModel().getFacility().getUsesAssocWideAdmissions() && mFamilyAdmissions.size() < 1) {
						ImageButton addButton = (ImageButton) mRootView.findViewById(R.id.familysettings_add);
						addButton.setVisibility(View.VISIBLE);
					}
					mAdmissionLayout.removeView((View) v.getParent());
				}
			});
			mAdmissionLayout.addView(v);
		}
	}
	
	public OnClickListener getFamilySettingsAddAdmissionOnClickListener() {
		return new FamilySettingsAddAdmissionOnClickListener();
	}

	public class FamilySettingsAddAdmissionOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			Admission admission = getViewModel().createFamilyAdmission();
			admission.setAdmissions(0);
			admission.setFamily(getViewModel().getFamily());
			admission.setFacility(mAssocFacilities.get(0));
			//mFamilyAdmissions.add(admission);
			if (getViewModel().getFacility().getUsesAssocWideAdmissions() && mFamilyAdmissions.size() >= 1) {
				ImageButton addButton = (ImageButton) mRootView.findViewById(R.id.familysettings_add);
				addButton.setVisibility(View.GONE);
			}
			View newAdmission = mInflater.inflate(R.layout.familysettings_admission_item, null);
			EditText et = (EditText) newAdmission.findViewById(R.id.familysettings_numOfAdmissions);
			et.setText(admission.getAdmissions().toString());
			Spinner spinner = (Spinner) newAdmission.findViewById(R.id.familysettings_facility_spinner);
			List<String> list = new ArrayList<String>();
			for (Facility fac : mAssocFacilities) {
				list.add(fac.getYMCAName());
			}
			ArrayAdapter<String> adp = new ArrayAdapter<String>(v.getContext(), R.layout.spinner_item, list);
			spinner.setAdapter(adp);
			spinner.setSelection(0);
			ImageButton deleteButton = (ImageButton) newAdmission.findViewById(R.id.familysettings_delete);
			deleteButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int indexOfAdmit = mAdmissionLayout.indexOfChild((View) v.getParent());
					if (!(indexOfAdmit > getViewModel().getFamilyAdmissions().size())) {
						getViewModel().getFamilyAdmissions().remove(indexOfAdmit);
						mFamilyAdmissions.remove(indexOfAdmit);
					}
					mAdmissionLayout.removeView((View) v.getParent());
					if (getViewModel().getFacility().getUsesAssocWideAdmissions() && mFamilyAdmissions.size() < 1) {
						ImageButton addButton = (ImageButton) mRootView.findViewById(R.id.familysettings_add);
						addButton.setVisibility(View.VISIBLE);
					}
				}
			});
			mAdmissionLayout.addView(newAdmission);
			//getViewModel().getFamilyAdmissions().add(new AdmissionAndFacilities(admission, false, mAssocFacilitties));
		}
	}
	
	public String getNumOfAdmissions() {
		return mAdmit.getAdmission();
	}
	
	public List<String> getFacilitySelectionList() {
		List<String> facilityNames = new ArrayList<>();
		for (Facility fac : mAssocFacilities) {
			facilityNames.add(fac.getYMCAName());
		}
		return facilityNames;
	}
	
	public int getDefaultSelection() {
		AdmissionAndFacility admit = mAdmit;
		int position = 0;

		if (admit.getFacility() != null) {
			for (Facility fac : mAssocFacilities)
				if (fac.getObjectId().equals(admit.getFacility().getObjectId())) {
					position = mAssocFacilities.indexOf(fac);
					break;
				}
		}
		return position;
	}
	
	public OnClickListener getSaveOnClickListener() {
		return new OnSaveClickListener();
	}
	
	public class OnSaveClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			List<Admission> admissionsToDelete = new ArrayList<>();
			int numOfAdmits = mAdmissionLayout.getChildCount();
			for (int i = 0; i < numOfAdmits; i++) {
				View admitsView = mAdmissionLayout.getChildAt(i);
				EditText et = (EditText) admitsView.findViewById(R.id.familysettings_numOfAdmissions);
				int admitsToCreditTo = Integer.parseInt(et.getText().toString());
				/*Spinner s = (Spinner) admitsView.findViewById(R.id.familysettings_facility_spinner);
				String admitFacilityName = s.getSelectedItem().toString();
				Facility admissionFacility = null;
				for (Facility fac : mAssocFacilitties) {
					if (fac.getYMCAName().equals(admitFacilityName))
						admissionFacility = fac;
				}
				mFamilyAdmissions.get(i).setAdmissions(numOfAdmitsToSave);
				mFamilyAdmissions.get(i).setFacility(admissionFacility);*/
				int admissionsToCredit = admitsToCreditTo - Integer.parseInt(mFamilyAdmissions.get(i).getAdmission());
				mFamilyAdmissions.get(i).setAdmission(String.valueOf(admissionsToCredit));
				getViewModel().creditAdmissions(getViewModel().getSelectedMember(), getViewModel().getFacility(), admissionsToCredit,
						new PersistenceLayer.PersistenceLayerCallback<AdmissionBalance>() {
					@Override
					public void done(AdmissionBalance data, Exception e) {
						AdmissionBalance result = data;
					}
				});
			}
			/*for (Admission admit : mOrigFamilyAdmissions) {
				boolean exists = false;
				for (Admission newAdmit : mFamilyAdmissions) {
					if (admit.equals(newAdmit))
						exists = true;	
				}
				if (!exists)
					admissionsToDelete.add(admit);
			}*/

			//getViewModel().deleteAdmissions(admissionsToDelete);
		}
	}
	
	public int getAddButtonVisibility() {
		if (getViewModel().getFacility().getUsesAssocWideAdmissions()) {
			if (mFamilyAdmissions.size() >= 1)
				return View.GONE;
			else
				return View.VISIBLE;
		} else
			return View.VISIBLE;
	}
}
