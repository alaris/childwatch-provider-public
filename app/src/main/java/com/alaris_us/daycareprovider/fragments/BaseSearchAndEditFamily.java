package com.alaris_us.daycareprovider.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.view.View;

import com.alaris_us.daycareprovider.models.SearchAndEditFamilyViewModel;
import com.alaris_us.daycareprovider.models.ViewModel.ViewModelHost;

public abstract class BaseSearchAndEditFamily extends Fragment {

	private SearchAndEditFamilyViewModel mViewModel;

	public SearchAndEditFamilyViewModel getViewModel() {

		return mViewModel;
	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		if (!(activity instanceof ViewModelHost)) {
			throw new ClassCastException(activity.toString() + " must implement ViewModelHost");
		}
		Object viewModel = ((ViewModelHost<?>) activity).getModel();
		if (viewModel instanceof SearchAndEditFamilyViewModel) {
			mViewModel = (SearchAndEditFamilyViewModel) viewModel;
		}
	}

	public abstract int getActionBarLayout();

	public abstract void setActionBarView(View bar);
}
