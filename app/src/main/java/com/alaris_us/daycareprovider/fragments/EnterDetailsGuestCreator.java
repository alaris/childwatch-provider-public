package com.alaris_us.daycareprovider.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.view.GuestMemberListItem;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.EditTextTextProperty;
import com.bindroid.ui.UiBinder;

public class EnterDetailsGuestCreator extends BaseGuestCreator {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_createguest_enterdetails, container, false);
		UiBinder.bind(rootView, R.id.ListView_ImportMembers, "Adapter", this, "ViewModel.ToCreate",
				new AdapterConverter(GuestMemberListItem.class));
		UiBinder.bind(new EditTextTextProperty((EditText) rootView.findViewById(R.id.EditText_FirstName)), this,
				"ViewModel.FirstName", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) rootView.findViewById(R.id.EditText_LastName)), this,
				"ViewModel.LastName", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty((EditText) rootView.findViewById(R.id.EditText_EMail)), this,
				"ViewModel.Email", BindingMode.TWO_WAY);
		UiBinder.bind(rootView, R.id.Button_BirthDate, "Text", this, "ViewModel.BirthDateString");
		UiBinder.bind(rootView, R.id.Button_BirthDate, "OnClickListener", this, "ViewModel.ButtonDateOnClickListener");
		UiBinder.bind(rootView, R.id.Button_AddToList, "OnClickListener", this, "ViewModel.ButtonAddOnClickListener");
		UiBinder.bind(rootView, R.id.RadioGroup_Gender, "OnCheckedChangeListener", this,
				"ViewModel.GenderOnCheckedChangeListener");
		return rootView;
	}
}
