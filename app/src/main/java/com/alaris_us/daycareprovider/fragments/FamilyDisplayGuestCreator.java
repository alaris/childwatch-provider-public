package com.alaris_us.daycareprovider.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.view.DaycareMemberListItem;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class FamilyDisplayGuestCreator extends BaseGuestCreator {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_createguest_familydisplay, container, false);
		UiBinder.bind(rootView, R.id.ListView_DisplayMembers, "Adapter", this, "ViewModel.Existing",
				new AdapterConverter(DaycareMemberListItem.class));
		return rootView;
	}
}
