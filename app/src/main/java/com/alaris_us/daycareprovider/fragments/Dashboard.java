package com.alaris_us.daycareprovider.fragments;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.alaris_us.daycareprovider.models.DashboardViewModel;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.popups.AdmissionsUsedPopup;
import com.alaris_us.daycareprovider.popups.AdmissionsUsedPopup.OnConfirmAdmissionUsageListener;
import com.alaris_us.daycareprovider.popups.MinutesOveragePopup;
import com.alaris_us.daycareprovider.popups.MinutesOveragePopup.OnConfirmMintuesOverageListener;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider.utils.DaycareHelpers.YesNoDialogCallback;
import com.alaris_us.daycareprovider.view.ChildListTabHost;
import com.alaris_us.daycareprovider.view.ChildListTabHost.ChildListTab;
import com.alaris_us.daycareprovider.view.ChildStatusListItem;
import com.alaris_us.daycareprovider.view.FilterTabs;
import com.alaris_us.daycareprovider.view.FilterTabs.FilterTab;
import com.alaris_us.daycareprovider.view.FilterTabs.OnFilterTabClick;
import com.alaris_us.daycareprovider.view.StackedTextView;
import com.alaris_us.daycareprovider_dev.R;
import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.FillDirection;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYRegionFormatter;
import com.androidplot.xy.XYSeriesFormatter;
//import com.androidplot.xy.XYStepMode;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;

public class Dashboard extends ProviderFragment {

	public enum ChildListMode {
		SINGLE, GROUP
	}

	DashboardViewModel m_model;

	private TrackableField<XYPlot> m_xyPlot = new TrackableField<XYPlot>();
	private TrackableField<XYSeriesFormatter<XYRegionFormatter>> mSeries1Format = new TrackableField<XYSeriesFormatter<XYRegionFormatter>>();
	private ChildListTabHost mTabHost;
	private View rv;
	private ChildListMode mChildListMode;
	private ListView mChildListView;
	private Menu mMenu;
	private ChildListTab mNameTab;
	private ChildListTab mFamilyTab;
	private ChildListTab mAgeTab;
	private ChildListTab mTimeTab;
	private FilterTabs mFilterTabs;
	private PersistenceLayer mPersistenceLayer;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rv = inflater.inflate(R.layout.fragment_dashboard, container, false);
		mFilterTabs = (FilterTabs) rv.findViewById(R.id.filter_tabs);
		mPersistenceLayer = ((PersistenceLayerHost) getActivity()).getPersistenceLayer();

		// Get Dimensions
		float hOffset = getResources().getDimensionPixelSize(R.dimen.XYPlot_hOffset);
		float vOffset = getResources().getDimensionPixelSize(R.dimen.XYPlot_vOffset);

		// Format plot with stuff that cannot be done with configurator

		XYPlot plot = (XYPlot) rv.findViewById(R.id.dashboardcurrentstats_graph);
		plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new PlotDomainValueFormat());
		plot.setDomainStep(StepMode.INCREMENT_BY_VAL, 86400);

		BarFormatter series1Format = new BarFormatter();
		series1Format.setPointLabelFormatter(new PointLabelFormatter(0, hOffset, vOffset));
		series1Format.configure(getActivity(), R.xml.bar_formatter_plf1);
		mSeries1Format.set(series1Format);

		m_xyPlot.set(plot);

		mTabHost = (ChildListTabHost) rv.findViewById(R.id.tabhost);
		mTabHost.setup();

		mNameTab = mTabHost.newTab(getResources().getString(R.string.childlist_tab_fullname));
		mFamilyTab = mTabHost.newTab("FAMILY");
		mAgeTab = mTabHost.newTab("AGE");
		mTimeTab = mTabHost.newTab("TIME");

		// Set the Tab name and Activity
		// that will be opened when particular Tab will be selected
		mNameTab.setContent(R.id.dashboardchildlist_childList);
		mFamilyTab.setContent(R.id.dashboardchildlist_childList);
		mAgeTab.setContent(R.id.dashboardchildlist_childList);
		mTimeTab.setContent(R.id.dashboardchildlist_childList);

		/* Add the tabs to the TabHost to display. */
		mTabHost.addTab(mNameTab);
		mTabHost.addTab(mFamilyTab);
		mTabHost.addTab(mAgeTab);
		mTabHost.addTab(mTimeTab);

		return rv;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		m_model = new DashboardViewModel(((PersistenceLayerHost) getActivity()).getPersistenceLayer(), this);

		UiBinder.bind(this.getActivity(), R.id.dashboardcurrentstats_textInCenter, "BottomText", m_model, "InCenter",
				BindingMode.ONE_WAY, new ToStringConverter("%d"));
		UiBinder.bind(this.getActivity(), R.id.dashboardcurrentstats_textUnderAge, "BottomText", m_model, "UnderAge",
				BindingMode.ONE_WAY, new ToStringConverter("%d"));
		UiBinder.bind(this.getActivity(), R.id.dashboardcurrentstats_textOverAge, "BottomText", m_model, "OverAge",
				BindingMode.ONE_WAY, new ToStringConverter("%d"));
		UiBinder.bind(this.getActivity(), R.id.dashboardcurrentstats_Count, "BottomText", m_model, "WeightedCount",
				BindingMode.ONE_WAY, new ToStringConverter("%d"));
		UiBinder.bind(this.getActivity(), R.id.dashboardcurrentstats_Count, "TopText", m_model, "WeightedCountText",
				BindingMode.ONE_WAY);
		UiBinder.bind(this.getActivity(), R.id.dashboardcurrentstats_TotalCheckIns, "BottomText", m_model,
				"TotalCheckIns", BindingMode.ONE_WAY, new ToStringConverter("%d"));
		UiBinder.bind(this.getActivity(), R.id.dashboardcurrentstats_AMcheckIns, "BottomText", m_model, "AMCheckIns",
				BindingMode.ONE_WAY, new ToStringConverter("%d"));
		UiBinder.bind(this.getActivity(), R.id.dashboardcurrentstats_PMcheckIns, "BottomText", m_model, "PMCheckIns",
				BindingMode.ONE_WAY, new ToStringConverter("%d"));
		UiBinder.bind(this.getActivity(), R.id.dashboardcurrentstats_StaffCount, "BottomText", m_model, "StaffCount",
				BindingMode.ONE_WAY, new ToStringConverter("%d"));
		UiBinder.bind(this.getActivity(), R.id.dashboardchildlist_childList, "Adapter", m_model, "CheckedInMembers",
				new AdapterConverter(ChildStatusListItem.class));
		UiBinder.bind(this.getActivity(), R.id.dashboardchildlist_childList, "OnItemClickListener", m_model,
				"ChildDetailsOnItemClickListener");
		UiBinder.bind(new ReflectedProperty(this, "FilterTabs"), m_model, "FilterTabs", BindingMode.ONE_WAY_TO_SOURCE);
		StackedTextView staffCountTV = (StackedTextView) this.getActivity().findViewById(R.id.dashboardcurrentstats_StaffCount);
		staffCountTV.setOnClickListener(m_model.getStaffOnClickListener());

		UiBinder.bind(new ReflectedProperty(this, "XYPlot"), m_model, "XYPlot", BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(new ReflectedProperty(this, "XYPlotFormatter"), m_model, "XYPlotFormatter",
				BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(new ReflectedProperty(this, "TabHost"), m_model, "TabHost", BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(new ReflectedProperty(this, "FilterTabs"), m_model, "FilterTabs", BindingMode.ONE_WAY_TO_SOURCE);

		mNameTab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mNameTab.getView().toggleSortIcon();
				m_model.sortChildListByFullName(mNameTab.getView().getSortDirection());
			}
		});

		mFamilyTab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mFamilyTab.getView().toggleSortIcon();
				m_model.sortChildListByFamily(mFamilyTab.getView().getSortDirection());

			}
		});
		mAgeTab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mAgeTab.getView().toggleSortIcon();
				m_model.sortChildListByAge(mAgeTab.getView().getSortDirection());
			}
		});
		mAgeTab.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				mFilterTabs.show();
				mTabHost.findViewById(R.id.filter_tabs_divider).setVisibility(View.VISIBLE);
				mTabHost.findViewById(android.R.id.tabcontent)
						.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, 0, 8.5f));
				FilterTab allAgesTab = mFilterTabs.getNewTab("ALL AGES");
				FilterTab lessThanOneTab = mFilterTabs.getNewTab("< 1");
				FilterTab oneToThreeTab = mFilterTabs.getNewTab("1 - 3");
				FilterTab fourToSixTab = mFilterTabs.getNewTab("4 - 6");
				FilterTab cancelTab = mFilterTabs.getNewTab("CANCEL");

				allAgesTab.setOnFilterTabClick(new OnFilterTabClick() {
					@Override
					public void onClick(FilterTab tab) {
						m_model.resetAgeFilters();
						m_model.sortChildListByAge(mAgeTab.getView().getSortDirection());
					}
				});
				lessThanOneTab.setOnFilterTabClick(new OnFilterTabClick() {
					@Override
					public void onClick(FilterTab tab) {
						m_model.filterChildListByAge(0, 0.99);
					}
				});
				oneToThreeTab.setOnFilterTabClick(new OnFilterTabClick() {
					@Override
					public void onClick(FilterTab tab) {
						m_model.filterChildListByAge(1, 3);
					}
				});
				fourToSixTab.setOnFilterTabClick(new OnFilterTabClick() {
					@Override
					public void onClick(FilterTab tab) {
						m_model.filterChildListByAge(4, 6);
					}
				});
				cancelTab.setOnFilterTabClick(new OnFilterTabClick() {
					@Override
					public void onClick(FilterTab tab) {
						m_model.resetAgeFilters();
						m_model.sortChildListByAge(mAgeTab.getView().getSortDirection());
						mFilterTabs.hide();
						mFilterTabs.clearTabs();
						mTabHost.findViewById(R.id.filter_tabs_divider).setVisibility(View.GONE);
						mTabHost.findViewById(android.R.id.tabcontent).setLayoutParams(
								new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, 0, 9.25f));
					}
				});
				mFilterTabs.clearTabs();
				mFilterTabs.addTab(allAgesTab);
				mFilterTabs.addTab(lessThanOneTab);
				mFilterTabs.addTab(oneToThreeTab);
				mFilterTabs.addTab(fourToSixTab);
				mFilterTabs.addTab(cancelTab);
				return true;
			}
		});

		mTimeTab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mTimeTab.getView().toggleSortIcon();
				m_model.sortChildListByTimeLeft(mTimeTab.getView().getSortDirection());

			}
		});

		mTabHost.setOnTabChangedListener(new OnTabChangedListener());
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		this.mMenu = menu;
		m_model.setMenu(mMenu);
		super.onCreateOptionsMenu(menu, inflater);
		UiBinder.bind(new ReflectedProperty(mMenu.findItem(R.id.action_selectall), "Title"), m_model,
				"SelectAllButtonText", BindingMode.ONE_WAY);
	}

	@Override
	public String getTitle() {

		return "Overview";
	}

	public XYPlot getXYPlot() {
		return m_xyPlot.get();
	}

	public XYSeriesFormatter<XYRegionFormatter> getXYPlotFormatter() {
		return mSeries1Format.get();
	}

	private class PlotDomainValueFormat extends Format {

		private static final long serialVersionUID = 1L;
		@SuppressLint("SimpleDateFormat")
		private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd");

		@Override
		public StringBuffer format(Object object, StringBuffer buffer, FieldPosition field) {
			// because our timestamps are in seconds and SimpleDateFormat
			// expects milliseconds
			// we multiply our timestamp by 1000:
			long timestamp = ((Number) object).longValue() * 1000;
			Date date = new Date(timestamp);
			return dateFormat.format(date, buffer, field);
		}

		@Override
		public Object parseObject(String string, ParsePosition position) {
			return null;
		}
	}

	public void filterChildrenByRoom(String roomName) {
		if (m_model != null) {
			m_model.setRoomName(roomName);
		}
	}

	public TabHost getTabHost() {
		return mTabHost;
	}

	public FilterTabs getFilterTabs() {
		return mFilterTabs;
	}

	public ChildListMode getMode() {
		return m_model.getChildListMode();
	}

	public void setMode(ChildListMode mode) {
		switch (mode) {
		case SINGLE:
			mChildListMode = ChildListMode.SINGLE;
			break;
		case GROUP:
			mChildListMode = ChildListMode.GROUP;
			break;
		}
		m_model.setChildListMode(mChildListMode);
	}

	public void setListViewAlpha(ChildListMode mode) {
		mChildListView = (ListView) rv.findViewById(R.id.dashboardchildlist_childList);
		ListAdapter adapter = mChildListView.getAdapter();
		for (int i = 0; i < adapter.getCount(); i++) {
			if (mode.equals(ChildListMode.GROUP))
				((MemberAndCubbies) adapter.getItem(i)).setDimmed(true);
			else
				((MemberAndCubbies) adapter.getItem(i)).setDimmed(false);
		}
	}

	public List<Member> getSelectedMembers() {
		List<Member> selectedMembers = new ArrayList<Member>();
		for (MemberAndCubbies mAndC : m_model.getSelectedMembersAndCubbies()) {
			selectedMembers.add(mAndC.getMember());
		}
		return selectedMembers;
	}

	public List<MemberAndCubbies> getSelectedMembersAndCubbies() {
		return m_model.getSelectedMembersAndCubbies();
	}

	public boolean areMembersSelected() {
		List<MemberAndCubbies> selectedMembers = getSelectedMembersAndCubbies();
		if (selectedMembers.isEmpty())
			return false;
		else
			return true;
	}

	public void selectAll() {
		m_model.onSelectAllClick();
		if (areMembersSelected()) {
			mMenu.findItem(R.id.action_move).setEnabled(true);
			mMenu.findItem(R.id.action_checkout).setEnabled(true);
		} else {
			mMenu.findItem(R.id.action_move).setEnabled(false);
			mMenu.findItem(R.id.action_checkout).setEnabled(false);
		}
	}

	public void resetAll() {
		ListAdapter adapter = mChildListView.getAdapter();
		for (int i = 0; i < adapter.getCount(); i++) {
			((MemberAndCubbies) adapter.getItem(i)).setSelected(false);
			((MemberAndCubbies) adapter.getItem(i)).setDimmed(false);
		}
		mMenu.findItem(R.id.action_selectall).setTitle("Select All");
		setMode(ChildListMode.SINGLE);
		m_model.getSelectedMembersAndCubbies().clear();
	}

	public void checkOut() {
		boolean usesOverageAlerting = getResources().getBoolean(R.bool.IS_OVERAGE_ALERTING_ENABLED);
		if (usesOverageAlerting) {
			HashMap <Member, Number> payableChildren = new HashMap<>();
			payableChildren = DaycareHelpers.getChildrenWithOverageMinutes(getSelectedMembers());
			if (!payableChildren.isEmpty()) {
				new MinutesOveragePopup(payableChildren, rv.getContext(), new ConfirmMinutesOverage()).show(rv);
			} else {
				showConfirmGroupCheckoutDialog();
			}
		} else if (mPersistenceLayer.getPersistenceData().getFacilityUsesAdmissions()) {
			mPersistenceLayer.chargeAdmissions(getSelectedMembers(), mPersistenceLayer.getPersistenceData().getFacility(), false, null,
					new PersistenceLayerCallback<List<HashMap<String, Object>>>() {
						@Override
						public void done(List<HashMap<String, Object>> data, Exception e) {
							new AdmissionsUsedPopup(data, getSelectedMembers(), mPersistenceLayer.getPersistenceData().getFacility(), null, rv.getContext(), mPersistenceLayer,
									new ConfirmAdmissionUsageOverage()).show(rv);
						}
					});
		} else {
			showConfirmGroupCheckoutDialog();
		}
	}

	public DashboardViewModel getModel() {
		return m_model;
	}

	private class OnTabChangedListener implements OnTabChangeListener {
		@Override
		public void onTabChanged(String tabId) {
			if (tabId.equals("FULL NAME Tab"))
				m_model.sortChildListByFullName();
			else if (tabId.equals("FAMILY Tab"))
				m_model.sortChildListByFamily();
			else if (tabId.equals("AGE Tab"))
				m_model.sortChildListByAge();
			else if (tabId.equals("TIME Tab"))
				m_model.sortChildListByTimeLeft();
		}
	}

	public void hideFilterTabsOnRefresh() {
		mFilterTabs.hide();
		mFilterTabs.clearTabs();
		mTabHost.findViewById(R.id.filter_tabs_divider).setVisibility(View.GONE);
		mTabHost.findViewById(android.R.id.tabcontent)
				.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, 0, 9.25f));
		m_model.resetAgeFilters();
	}

	public void showConfirmGroupCheckoutDialog(){
		String message = "Are you sure you want to check out the selected children?";
		DaycareHelpers.createYesNoDialog(getActivity(), message, new YesNoDialogCallback() {

			@Override
			public void yesClicked() {
				if (mPersistenceLayer.getPersistenceData().getFacilityUsesAdmissions()) {
					mPersistenceLayer.chargeAdmissions(getSelectedMembers(), mPersistenceLayer.getPersistenceData().getFacility(), true, null,
							new PersistenceLayerCallback<List<HashMap<String, Object>>>() {
								@Override
								public void done(List<HashMap<String, Object>> data, Exception e) {

								}
							});
				}
				m_model.checkOut(getSelectedMembers());
			}

			@Override
			public void noClicked() {
			}
		});
	}

	public class ConfirmMinutesOverage implements OnConfirmMintuesOverageListener{

		@Override
		public void onCheckOut() {
			showConfirmGroupCheckoutDialog();
		}

		@Override
		public void onCancel() {
		}

	}

	public class ConfirmAdmissionUsageOverage implements OnConfirmAdmissionUsageListener{

		@Override
		public void onCheckOut() {
			showConfirmGroupCheckoutDialog();
		}

		@Override
		public void onCancel() {
		}

	}
}
