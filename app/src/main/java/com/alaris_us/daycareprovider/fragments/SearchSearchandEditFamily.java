package com.alaris_us.daycareprovider.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.view.MemberSearchResultItem;
import com.alaris_us.daycareprovider.view.SearchParameterDropDown;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.EditTextTextProperty;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

public class SearchSearchandEditFamily extends BaseSearchAndEditFamily {
	private void bindActionBar() {

		if (getViewModel() == null || mActionBarView == null)
			return;
		UiBinder.bind(mActionBarView, R.id.searchbar_filter, "OnItemSelectedListener", getViewModel(),
				"SearchParameterOnItemSelectedListener");
		UiBinder.bind(mActionBarView, R.id.searchbar_filter, "Adapter", getViewModel(), "SearchParameterList",
				new AdapterConverter(SearchParameterDropDown.class));
		UiBinder.bind(new EditTextTextProperty((EditText) mActionBarView.findViewById(R.id.searchbar_searchbox)),
				getViewModel(), "SearchBoxText", BindingMode.ONE_WAY_TO_SOURCE);
		UiBinder.bind(mActionBarView, R.id.searchbar_searchbutton, "OnClickListener", getViewModel(),
				"SearchOnClickListener");
		UiBinder.bind(mActionBarView, R.id.searchbar_searchbox, "OnEditorActionListener", getViewModel(),
				"SearchOnActionListener");
		UiBinder.bind(mActionBarView, R.id.searchbar_searchbox, "InputType", getViewModel(), "SearchBoxInputType");
		UiBinder.bind(new ReflectedProperty(this, "SearchBoxHint"), getViewModel(),
				"SearchParameter.SearchBoxHintReference", BindingMode.ONE_WAY);
	}

	public void setSearchBoxHint(int hint) {
		((EditText) mActionBarView.findViewById(R.id.searchbar_searchbox)).setHint(hint);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_searchandeditfamily_search, container, false);
		UiBinder.bind(rootView, R.id.searchresults, "Adapter", getViewModel(), "SearchResults",
				new AdapterConverter(MemberSearchResultItem.class));
		UiBinder.bind(rootView, R.id.searchresults, "OnItemClickListener", getViewModel(),
				"FamilyListOnItemClickListener");
		bindActionBar();
		return rootView;
	}

	@Override
	public int getActionBarLayout() {

		return R.layout.view_search_navigation;
	}

	@Override
	public void setActionBarView(View bar) {

		this.mActionBarView = bar;
		bindActionBar();
	}

	private View mActionBarView;
}
