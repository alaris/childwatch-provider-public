package com.alaris_us.daycareprovider.fragments;

import com.alaris_us.daycareprovider.camera.CameraManager;
import com.alaris_us.daycareprovider.camera.MemberPhotoCameraPreview;
import com.alaris_us.daycareprovider.utils.ImageFunctions;
import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.camera.MemberPhotoCameraPreview.FaceInViewFinder;
import com.bindroid.BindingMode;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.ui.UiBinder;

import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MemberPhotoScreenFragment extends BaseSearchAndEditFamily implements FaceInViewFinder {
	private TextView mInstructions;
	private String mHeaderText;
	private String mHeaderConfirmText;
	private String mHeaderTakeText;
	private View mActionBarView;
	private FrameLayout mCameraDisplay;
	private View mViewFinder;
	private MemberPhotoCameraPreview mPreview;

	private static int PHOTOWIDTH = 320;
	private static int PHOTOHEIGHT = 200;

	private void bindActionBar() {
		if (getViewModel() == null || mActionBarView == null)
			return;
		UiBinder.bind(mActionBarView, R.id.searchbar_header, "Text", getViewModel(), "MemberName", BindingMode.ONE_WAY,
				new ToStringConverter(mHeaderText));
		mActionBarView.findViewById(R.id.searchbar_photo_backbutton_container)
				.setOnClickListener(new BackButtonOnClickListener());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.memberphotoscreen_layout, container, false);

		mHeaderText = mHeaderTakeText = getString(R.string.memberphotoscreen_tile_header_take);
		mHeaderConfirmText = getString(R.string.memberphotoscreen_tile_header_confirm);
		mInstructions = (TextView) rootView.findViewById(R.id.memberphotoscreen_instructions);

		UiBinder.bind(rootView, R.id.memberphotoscreen_takephoto_button, "Visibility", getViewModel(),
				"TakePhotoVisibility");
		UiBinder.bind(rootView, R.id.memberphotoscreen_switchcamera_button, "Visibility", getViewModel(),
				"SwitchCameraVisibility");
		UiBinder.bind(rootView, R.id.memberphotoscreen_tryagain_button, "Visibility", getViewModel(),
				"TryAgainVisibility");
		UiBinder.bind(rootView, R.id.memberphotoscreen_usephoto_button, "Visibility", getViewModel(),
				"UsePhotoVisibility");
		UiBinder.bind(rootView, R.id.memberphotoscreen_instructions, "Visibility", getViewModel(),
				"InstructionsVisibility");
		UiBinder.bind(rootView, R.id.memberphotoscreen_instructions, "Text", getViewModel(), "InstructionsText");
		UiBinder.bind(rootView, R.id.memberphotoscreen_takenphoto, "ImageBitmap", getViewModel(), "NewMemberPhoto");

		UiBinder.bind(rootView, R.id.memberphotoscreen_dim_container, "Visibility", getViewModel(), "DimVisibility",
				BindingMode.ONE_WAY);
		UiBinder.bind(rootView, R.id.memberphotoscreen_photo_name, "Visibility", getViewModel(), "PhotoNameVisibility",
				BindingMode.ONE_WAY);
		UiBinder.bind(rootView, R.id.memberphotoscreen_photo_name, "Text", getViewModel(), "MemberName",
				BindingMode.ONE_WAY);

		// Button Listeners
		rootView.findViewById(R.id.memberphotoscreen_takephoto_button)
				.setOnClickListener(new TakePhotoOnClickListener());
		rootView.findViewById(R.id.memberphotoscreen_tryagain_button).setOnClickListener(new TryAgainOnClickListener());
		rootView.findViewById(R.id.memberphotoscreen_switchcamera_button)
				.setOnClickListener(new SwitchCameraOnClickListener());
		rootView.findViewById(R.id.memberphotoscreen_usephoto_button).setOnClickListener(new UsePhotoOnClickListener());

		// Setup Camera
		mCameraDisplay = (FrameLayout) rootView.findViewById(R.id.memberphotoscreen_camera_container);
		mViewFinder = (View) rootView.findViewById(R.id.memberphotoscreen_camera_photoframe);
		mPreview = new MemberPhotoCameraPreview(getActivity(), new CameraManager());
		mPreview.setViewFinderRect(mViewFinder);
		mPreview.setBackground(getResources().getDrawable(R.color.invis));

		bindActionBar();
		return rootView;
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (mPreview != null)
			if (isVisibleToUser)
				mPreview.startPreview();
			else
				mPreview.stopPreview();
	}

	public void setInstructions(int res) {
		mInstructions.setText(res);
	}

	public void setIsConfirmPhotoState(boolean isConfirm) {
		mHeaderText = isConfirm ? mHeaderConfirmText : mHeaderTakeText;
	}

	public float getScreenDensity() {
		return getResources().getDisplayMetrics().density;
	}

	public MemberPhotoScreenFragment getFragment() {
		return this;
	}

	@Override
	public int getActionBarLayout() {
		return R.layout.view_memberphoto_navigation;
	}

	@Override
	public void setActionBarView(View bar) {
		mActionBarView = bar;
		bindActionBar();
	}

	@Override
	public void onResume() {
		super.onResume();
		mCameraDisplay.removeAllViews();
		mCameraDisplay.addView(mPreview);
		mPreview.setFaceInViewFinderListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		mPreview.setFaceInViewFinderListener(null);
		mCameraDisplay.removeAllViews();
	}

	private class SwitchCameraOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getViewModel().faceNotInFocus();
			mPreview.switchCamera();
		}
	}

	private class TakePhotoOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mPreview.takePicture(new MemberPhotoPictureCallback());
		}
	}

	private class UsePhotoOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mPreview.stopPreview();
			getViewModel().usePhoto();
		}
	}

	private class TryAgainOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mPreview.startPreview();
			getViewModel().tryAgain();
		}
	}

	private class MemberPhotoPictureCallback implements PictureCallback {
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			Bitmap photo = ImageFunctions.byteArrayToBitmap(data);
			if (mPreview.isFrontCamera())
				photo = ImageFunctions.mirrorBitmap(photo, ImageFunctions.HORIZONTAL);
			photo = ImageFunctions.cropBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight());
			photo = ImageFunctions.rotateBitmap(photo, 180f);
			photo = ImageFunctions.scaleBitmap(photo, PHOTOWIDTH, PHOTOHEIGHT);

			getViewModel().photoTaken(photo, mPreview.isFrontCamera());
			mPreview.stopPreview();
		}
	}

	private class BackButtonOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getViewModel().exit();
			//mPreview.startPreview();
		}
	}

	@Override
	public void faceAccepted() {
		getViewModel().faceInFocus();
	}

	@Override
	public void faceNotAccepted() {
		getViewModel().faceNotInFocus();
	}
}
