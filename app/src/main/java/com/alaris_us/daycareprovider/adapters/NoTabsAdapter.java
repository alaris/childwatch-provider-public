package com.alaris_us.daycareprovider.adapters;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;


import androidx.legacy.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;


public class NoTabsAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener, Iterable<Fragment> {

	private final ViewPager m_viewPager;

	private final List<Fragment> m_fragments;

	public NoTabsAdapter(Activity activity, ViewPager pager) {

		super(activity.getFragmentManager());

		m_fragments = new ArrayList<Fragment>();
		m_viewPager = pager;
		m_viewPager.setAdapter(this);
		m_viewPager.setOnPageChangeListener(this);

	}

	public void addFragment(Fragment fragment) {

		m_fragments.add(fragment);
		notifyDataSetChanged();

	}

	@Override
	public Fragment getItem(int position) {

		return (Fragment) m_fragments.get(position);

	}

	@Override
	public int getCount() {

		return m_fragments.size();

	}

	@Override
	public CharSequence getPageTitle(int position) {

		return "";

	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int arg0) {

	}

	@Override
	public Iterator<Fragment> iterator() {

		return m_fragments.iterator();
	}

}
