package com.alaris_us.daycareprovider.adapters;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;


import androidx.legacy.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.alaris_us.daycareprovider.adapters.TabsAdapter.TabFragment;

public class TabsAdapter extends FragmentPagerAdapter
		implements ActionBar.TabListener, ViewPager.OnPageChangeListener, Iterable<TabFragment> {

	private final ActionBar m_actionBar;
	private final ViewPager m_viewPager;

	private final List<TabFragment> m_fragments;

	public interface TabFragment {

		public String getTitle();

	}

	public TabsAdapter(Activity activity, ViewPager pager) {

		super(activity.getFragmentManager());

		m_fragments = new ArrayList<TabFragment>();
		m_actionBar = activity.getActionBar();

		m_viewPager = pager;
		m_viewPager.setAdapter(this);
		m_viewPager.setOnPageChangeListener(this);

	}

	public void addFragment(TabFragment fragment) {

		m_fragments.add(fragment);
		m_actionBar.addTab(m_actionBar.newTab().setText(fragment.getTitle()).setTabListener(this));
		notifyDataSetChanged();

	}

	@Override
	public Fragment getItem(int position) {

		return (Fragment) m_fragments.get(position);

	}

	@Override
	public int getCount() {

		return m_fragments.size();

	}

	@Override
	public CharSequence getPageTitle(int position) {

		return m_fragments.get(position).getTitle();

	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int arg0) {

	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {

		m_viewPager.setCurrentItem(tab.getPosition());

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public Iterator<TabFragment> iterator() {

		return m_fragments.iterator();
	}

}
