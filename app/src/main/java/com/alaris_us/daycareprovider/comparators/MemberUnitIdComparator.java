package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;

import com.alaris_us.daycaredata.to.Member;

public class MemberUnitIdComparator implements Comparator<Member> {

	@Override
	public int compare(Member lhs, Member rhs) {
		if (lhs.getUnitID().length() != rhs.getUnitID().length())
			return lhs.getUnitID().length() - rhs.getUnitID().length();
		else
			return lhs.getUnitID().compareTo(rhs.getUnitID());
	}

}
