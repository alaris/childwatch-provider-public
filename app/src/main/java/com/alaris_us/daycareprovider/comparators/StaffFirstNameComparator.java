package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;
import com.alaris_us.daycaredata.to.Staff;

public class StaffFirstNameComparator implements Comparator<Staff> {

    @Override
    public int compare(Staff lhs, Staff rhs) {
        return lhs.getFirstName().compareTo(rhs.getFirstName());
    }
}
