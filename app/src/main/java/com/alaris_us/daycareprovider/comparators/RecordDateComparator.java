package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;

import com.alaris_us.daycaredata.to.Record;

public class RecordDateComparator implements Comparator<Record> {

	@Override
	public int compare(Record lhs, Record rhs) {
		return lhs.getCreatedAt().compareTo(rhs.getCreatedAt());
	}

}
