package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;

import com.alaris_us.daycaredata.to.Facility;

public class FacilityNameComparator implements Comparator<Facility> {

	@Override
	public int compare(Facility lhs, Facility rhs) {
		return lhs.getYMCAName().compareTo(rhs.getYMCAName());
	}

}
