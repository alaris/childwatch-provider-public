package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;

import com.alaris_us.daycaredata.to.Pager;

public class PagerNameComparator implements Comparator<Pager> {

	@Override
	public int compare(Pager lhs, Pager rhs) {
		return lhs.getName().compareTo(rhs.getName());
	}

}