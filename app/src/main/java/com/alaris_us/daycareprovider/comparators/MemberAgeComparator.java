package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;
import org.joda.time.LocalDate;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.view.TabWithIcons.SortDirection;

public class MemberAgeComparator implements Comparator<Member> {
	private SortDirection sortDirection = SortDirection.ASCENDING;

	public MemberAgeComparator() {

	}

	public MemberAgeComparator(SortDirection sortDirection) {
		this.sortDirection = sortDirection;
	}

	@Override
	public int compare(Member lhs, Member rhs) {

		LocalDate lhsBDate = LocalDate.fromDateFields(lhs.getBirthDate());
		LocalDate rhsBDate = LocalDate.fromDateFields(rhs.getBirthDate());

		if (sortDirection.equals(SortDirection.DESCENDING)) {
			if (lhsBDate.isBefore(rhsBDate))
				return -1;
			else if (lhsBDate.isAfter(rhsBDate))
				return 1;
			else
				return 0;
		} else {
			if (lhsBDate.isBefore(rhsBDate))
				return 1;
			else if (lhsBDate.isAfter(rhsBDate))
				return -1;
			else
				return 0;
		}
	}
}
