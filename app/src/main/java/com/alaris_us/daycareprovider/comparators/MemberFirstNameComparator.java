package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.view.TabWithIcons.SortDirection;

public class MemberFirstNameComparator implements Comparator<Member> {
	private SortDirection sortDirection = SortDirection.ASCENDING;

	public MemberFirstNameComparator() {

	}

	public MemberFirstNameComparator(SortDirection sortDirection) {
		this.sortDirection = sortDirection;
	}

	@Override
	public int compare(Member lhs, Member rhs) {
		int result = lhs.getFirstName().compareTo(rhs.getFirstName());
		if (sortDirection == SortDirection.DESCENDING) {
			result *= -1;
		}
		return result;
	}

}
