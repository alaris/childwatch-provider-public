package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.view.TabWithIcons.SortDirection;

public class MemberTimeLeftComparator implements Comparator<Member> {
	private SortDirection sortDirection = SortDirection.ASCENDING;

	public MemberTimeLeftComparator() {

	}

	public MemberTimeLeftComparator(SortDirection sortDirection) {
		this.sortDirection = sortDirection;
	}

	@Override
	public int compare(Member lhs, Member rhs) {

		DateTime now = new DateTime();
		int minLHS = Minutes.minutesBetween(new DateTime(lhs.getLastCheckIn()), now).getMinutes();
		// minLHS -= lhs.getMinutesLeft().intValue();
		int minRHS = Minutes.minutesBetween(new DateTime(rhs.getLastCheckIn()), now).getMinutes();
		// minRHS -= rhs.getMinutesLeft().intValue();

		if (sortDirection.equals(SortDirection.DESCENDING)) {
			if (minRHS > minLHS)
				return -1;
			else if (minRHS < minLHS)
				return 1;
			else
				return 0;
		} else {
			if (minRHS > minLHS)
				return 1;
			else if (minRHS < minLHS)
				return -1;
			else
				return 0;
		}
	}

}
