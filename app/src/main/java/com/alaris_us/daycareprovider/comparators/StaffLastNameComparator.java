package com.alaris_us.daycareprovider.comparators;

import com.alaris_us.daycaredata.to.Staff;
import java.util.Comparator;

public class StaffLastNameComparator implements Comparator<Staff> {

    @Override
    public int compare(Staff lhs, Staff rhs) {
        return lhs.getLastName().compareTo(rhs.getLastName());
    }
}
