package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.view.TabWithIcons.SortDirection;

public class MemberLastNameComparator implements Comparator<Member> {
	private SortDirection sortDirection = SortDirection.ASCENDING;

	public MemberLastNameComparator() {

	}

	public MemberLastNameComparator(SortDirection sortDirection) {
		this.sortDirection = sortDirection;
	}

	@Override
	public int compare(Member lhs, Member rhs) {
		int result = lhs.getLastName().compareTo(rhs.getLastName());
		if (sortDirection == SortDirection.DESCENDING) {
			result *= -1;
		}
		return result;
	}
}
