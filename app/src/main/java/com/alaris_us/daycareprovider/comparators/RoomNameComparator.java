package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;

import com.alaris_us.daycaredata.to.Room;

public class RoomNameComparator implements Comparator<Room> {

	@Override
	public int compare(Room lhs, Room rhs) {
		return lhs.getName().compareTo(rhs.getName());
	}

}
