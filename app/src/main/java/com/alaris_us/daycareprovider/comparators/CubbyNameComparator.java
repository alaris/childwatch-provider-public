package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;

import com.alaris_us.daycaredata.to.Cubby;

public class CubbyNameComparator implements Comparator<Cubby> {

	@Override
	public int compare(Cubby lhs, Cubby rhs) {
		return lhs.getName().compareTo(rhs.getName());
	}

}
