package com.alaris_us.daycareprovider.comparators;

import java.util.Comparator;

import com.alaris_us.daycaredata.to.Incident;

public class IncidentDateComparator implements Comparator<Incident> {

	@Override
	public int compare(Incident lhs, Incident rhs) {

		// DateTime now = new DateTime();
		return rhs.getCreatedAt().compareTo(lhs.getCreatedAt());
	}

}
