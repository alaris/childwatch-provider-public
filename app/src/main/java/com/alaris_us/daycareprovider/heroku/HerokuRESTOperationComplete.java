package com.alaris_us.daycareprovider.heroku;

public interface HerokuRESTOperationComplete<T> {
	
	public void operationComplete(T data, HerokuRESTException e);
	
}
