package com.alaris_us.daycareprovider.heroku;

import com.alaris_us.daycaredata.util.ConnectivityUtility;

public class HerokuData {
	private final String mHerokuBaseUrl;
	private final String mAssociationCode;
	private ConnectivityUtility mConnectivityUtility;
	private final String mClubNumber;

	public HerokuData(String herokuBaseUrl, String associationCode, ConnectivityUtility connectivityUtility) {
		mHerokuBaseUrl = herokuBaseUrl;
		mConnectivityUtility = connectivityUtility;
		mAssociationCode = associationCode;
		mClubNumber = "";
	}
	
	public HerokuData(String herokuBaseUrl, String associationCode, String clubNumber, ConnectivityUtility connectivityUtility) {
		mHerokuBaseUrl = herokuBaseUrl;
		mConnectivityUtility = connectivityUtility;
		mAssociationCode = associationCode;
		mClubNumber = clubNumber;
	}
	
	public HerokuCommands getHerokuCommands(){
		return new HerokuCommands(mHerokuBaseUrl, mAssociationCode, mClubNumber, mConnectivityUtility);
	}
}
