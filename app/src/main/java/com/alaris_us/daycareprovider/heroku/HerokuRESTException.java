package com.alaris_us.daycareprovider.heroku;

public class HerokuRESTException extends Exception {
	private static final long serialVersionUID = 1L;
	private int mCode;

	public HerokuRESTException(int code, String message) {
		super(message);
		mCode = code;
	}

	public HerokuRESTException(Throwable throwable) {
		super(throwable);
		mCode = -1;
	}

	public HerokuRESTException(int code, String message, Throwable throwable) {
		super(message, throwable);
		mCode = code;
	}

	public int getCode() {
		return mCode;
	}

}
