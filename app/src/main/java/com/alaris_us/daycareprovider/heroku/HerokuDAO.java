package com.alaris_us.daycareprovider.heroku;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.alaris_us.daycaredata.util.ConnectivityUtility;
import com.alaris_us.daycareprovider.heroku.HerokuREST.RequestType;

import android.os.Handler;

public class HerokuDAO {
	protected final String mHerokuBaseUrl;
	protected final String mAssociationCode;
	protected ConnectivityUtility mConnectivityUtility;
	protected final String mClubNumber;

	/*public HerokuDAO(String herokuBaseUrl, String associationCode, ConnectivityUtility connectivityUtility) {
		mHerokuBaseUrl = herokuBaseUrl;
		mConnectivityUtility = connectivityUtility;
		mAssociationCode = associationCode;
		mClubNumber = "";
	}*/
	
	public HerokuDAO(String herokuBaseUrl, String associationCode, String clubNumber, ConnectivityUtility connectivityUtility) {
		mHerokuBaseUrl = herokuBaseUrl;
		mConnectivityUtility = connectivityUtility;
		mAssociationCode = associationCode;
		mClubNumber = clubNumber;
	}

	protected void runHerokuRESTTask(RequestType reqType, String urlPath,
			HerokuRESTOperationComplete<HerokuDataObject> callback) {
		runHerokuRESTTask(reqType, urlPath, null, callback);
	}

	protected void runHerokuRESTTask(RequestType reqType, String urlPath, Map<String, Object> body,
			HerokuRESTOperationComplete<HerokuDataObject> callback) {
		if (mConnectivityUtility.isInternetConnected()) {
			String url = mHerokuBaseUrl + "/" + urlPath;
			HerokuREST herokuRest = new HerokuREST(reqType, url, body);
			HerokuRESTTaskRunner runner = new HerokuRESTTaskRunner(herokuRest, callback);
			runner.start();
		} else {
			callback.operationComplete(null, new HerokuRESTException(599, "No Internet Connected"));
		}
	}

	private class HerokuRESTTaskRunner implements Runnable {
		private final static long DEFAULT_DELAY = 1000;
		private final static int MAX_RETRY = 15;
		private long mDelay;
		private Handler mHandler;
		private AtomicInteger mCount;
		private HerokuREST mTask;
		private HerokuRESTOperationComplete<HerokuDataObject> mCallback;

		public HerokuRESTTaskRunner(HerokuREST herokuREST, HerokuRESTOperationComplete<HerokuDataObject> callback) {
			this(herokuREST, DEFAULT_DELAY, callback);
		}

		public HerokuRESTTaskRunner(HerokuREST herokuREST, long delay,
				HerokuRESTOperationComplete<HerokuDataObject> callback) {
			mHandler = new Handler();
			mCount = new AtomicInteger(0);
			mTask = herokuREST;
			mDelay = delay;
			mCallback = callback;
		}

		public void start() {
			long delay = mCount.get() == 0 ? 0 : mDelay;
			mHandler = new Handler(mHandler.getLooper());
			mHandler.postDelayed(this, delay);
		}

		@Override
		public void run() {
			mTask = mCount.get() == 0 ? mTask : mTask.clone();
			mTask.execute(new RetryCallback());
		}

		private class RetryCallback implements HerokuRESTOperationComplete<HerokuDataObject> {
			@Override
			public void operationComplete(HerokuDataObject data, HerokuRESTException e) {
				if (e != null) {
					if (e.getCode() == 404 && mCount.incrementAndGet() <= MAX_RETRY)
						start();
					else
						mCallback.operationComplete(null, e);
				} else {
					mCallback.operationComplete(data, null);
				}
			}
		}
	}
}
