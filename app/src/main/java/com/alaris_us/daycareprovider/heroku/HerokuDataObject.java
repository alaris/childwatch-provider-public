package com.alaris_us.daycareprovider.heroku;

import org.json.JSONObject;

public class HerokuDataObject{
	private JSONObject mJSONObject;
	
	public HerokuDataObject(JSONObject json){
		mJSONObject = json;
	}
	
	public HerokuDataObject(HerokuDataObject obj){
		this(obj.getData());
	}
	
	protected JSONObject getData(){
		return mJSONObject;
	}
}
