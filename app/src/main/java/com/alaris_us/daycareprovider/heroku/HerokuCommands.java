package com.alaris_us.daycareprovider.heroku;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alaris_us.daycaredata.util.ConnectivityUtility;
import com.alaris_us.daycareprovider.data.CheckInCount;
import com.alaris_us.daycareprovider.data.HerokuJob;
import com.alaris_us.daycareprovider.heroku.HerokuREST.RequestType;

import android.util.Log;

public class HerokuCommands extends HerokuDAO{
	
	public HerokuCommands(String herokuBaseUrl, String associationCode, String clubNumber, ConnectivityUtility connectivityUtility) {
		super(herokuBaseUrl, associationCode, clubNumber, connectivityUtility);
	}

	protected void createJob(String urlPath, Map<String, Object> body,
			final HerokuRESTOperationComplete<HerokuJob> callback) {
		runHerokuRESTTask(RequestType.POST, urlPath, body, new HerokuRESTOperationComplete<HerokuDataObject>() {
			@Override
			public void operationComplete(HerokuDataObject data, HerokuRESTException e) {
				if (e != null)
					callback.operationComplete(null, e);
				else
					callback.operationComplete(new HerokuJob(data), null);
			}
		});
	}

	public void fetchCheckInCounts(String facilityId, List<String> roomNames,
			final HerokuRESTOperationComplete<CheckInCount> callback) {

		Map<String, Object> body = new HashMap<String, Object>();
		body.put("facilityId", facilityId);
		body.put("roomNames", roomNames);
		body.put("alarisAssociationCode", mAssociationCode);

		createJob("fetchCheckinCounts", body, new HerokuRESTOperationComplete<HerokuJob>() {
			@Override
			public void operationComplete(HerokuJob data, HerokuRESTException e) {
				if (data == null)
					callback.operationComplete(null, e);
				else {
				Map<String, Object> body = new HashMap<String, Object>();
				body.put("jobName", "fetchCheckinCounts");
				body.put("jobId", data.getJobId());
				runHerokuRESTTask(RequestType.POST, "fetchJobData", body,
						new HerokuRESTOperationComplete<HerokuDataObject>() {
							@Override
							public void operationComplete(HerokuDataObject data, HerokuRESTException e) {
								if (e != null)
									callback.operationComplete(null, e);
								else
									callback.operationComplete(new CheckInCount(data), null);
							}
						});
				}
			}
		});
	}

	public void syncFamilyData(List<String> memberIds, final HerokuRESTOperationComplete<HerokuJob> callback) {

		Map<String, Object> body = new HashMap<String, Object>();
		body.put("alarisMemberIds", memberIds);
		body.put("alarisAssociationCode", mAssociationCode);
		if (mAssociationCode.equals("MSF") || mAssociationCode.equals("CFF") || mAssociationCode.equals("EOS"))
			body.put("alarisClubNumber", mClubNumber);

		createJob("updateMembers", body, callback);
	}
}
