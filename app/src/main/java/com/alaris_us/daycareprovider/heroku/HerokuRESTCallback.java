package com.alaris_us.daycareprovider.heroku;

import org.json.JSONObject;

public interface HerokuRESTCallback {

	public void success(JSONObject result);

	public void failure(HerokuRESTException error);

}