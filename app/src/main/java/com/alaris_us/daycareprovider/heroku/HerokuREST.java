package com.alaris_us.daycareprovider.heroku;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class HerokuREST extends AsyncTask<HttpUriRequest, Void, String> {

	private HerokuRESTOperationComplete<HerokuDataObject> cb;
	private HttpUriRequest mRequest;

	public enum RequestType {
		GET, POST
	}

	public HerokuREST(RequestType reqType, String url) {
		createRequest(reqType, url);
		setHeaders(getDefaultHeaders());
	}

	public HerokuREST(RequestType reqType, String url, Map<String, Object> body) {
		this(reqType, url);
		setBody(body);
	}
	
	public HerokuREST(HttpUriRequest request){
		mRequest = request;
	}
	
	public HerokuREST clone(){
		return new HerokuREST(mRequest);
	}
	
	private JSONObject getDefaultHeaders() {
		JSONObject headers = new JSONObject();
		try {
			headers.put("Accept", "application/json");
			headers.put("Content-Type", "application/json");
		} catch (JSONException e) {
			Log.e(this.getClass().getSimpleName(), e.getMessage());
		}
		return headers;
	}

	private void createRequest(RequestType requestType, String url) {
		switch (requestType) {
		case GET:
			mRequest = new HttpGet(url);
			break;
		case POST:
			mRequest = new HttpPost(url);
			break;
		}
	}

	@SuppressWarnings("unchecked")
	private void setHeaders(JSONObject headers) {
		Iterator<String> itr = headers.keys();
		while (itr.hasNext()) {
			try {
				String key = itr.next();
				mRequest.setHeader(key, headers.getString(key));
			} catch (JSONException e) {
				Log.e(this.getClass().getSimpleName(), e.getMessage());
			}
		}
	}

	public void setBody(JSONObject body) {
		if (mRequest instanceof HttpPost) {
			String bodyStr = "";
			if (body != null)
				bodyStr = body.toString();
			try {
				StringEntity entity = new StringEntity(bodyStr, "UTF-8");
				((HttpPost) mRequest).setEntity(entity);
			} catch (Exception e) {
				Log.e(this.getClass().getSimpleName(), e.getMessage());
			}
		}
	}
	
	public void setBody(Map<String, Object> body){
		setBody(new JSONObject(body));
	}

	public void execute(HerokuRESTOperationComplete<HerokuDataObject> callback) {
		cb = callback;
		super.execute(mRequest);
	}
	
	@Override
	protected String doInBackground(HttpUriRequest... params) {
		try {
			HttpUriRequest request = params[0];
			HttpClient mClient = new DefaultHttpClient();
			HttpResponse serverResponse = mClient.execute(request);
			BasicResponseHandler handler = new BasicResponseHandler();
			return handler.handleResponse(serverResponse);
		} catch (Exception e) {
			Log.e(e.getClass().getSimpleName(), e.getMessage());
			cb.operationComplete(null, new HerokuRESTException(500, e.getMessage()));
		}
		return "";
	}

	@Override
	protected void onPostExecute(String result) {
		try {
			JSONObject obj = new JSONObject(result);
			if (!obj.isNull("data")) {
				cb.operationComplete(new HerokuDataObject(obj.getJSONObject("data")), null);
			} else if (!obj.isNull("error")) {
				JSONObject error = obj.getJSONObject("error");
				cb.operationComplete(null, new HerokuRESTException(error.getInt("code"), error.getString("message")));
			} else {
				throw new JSONException("Empty Data");
			}
		} catch (JSONException e) {
			Log.e("Error Response", result);
			Log.e("JSONException", e.getMessage());
			cb.operationComplete(null, new HerokuRESTException(500, e.getMessage()));
		}
	}
}
