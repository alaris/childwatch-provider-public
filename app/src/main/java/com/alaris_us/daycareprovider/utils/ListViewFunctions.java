package com.alaris_us.daycareprovider.utils;

import android.view.ViewGroup;

public class ListViewFunctions {
	private final static double TILEDIV = 3.5;

	public static int calculateTileHeight(int height, int numTiles) {
		double div = (numTiles > 0 && numTiles < TILEDIV) ? numTiles : TILEDIV;
		double tileHeightMod;
		if (numTiles == 1)
			tileHeightMod = height * (.333) + 10;
		else
			tileHeightMod = (1 - (1 / div));
		return (int) ((height / div) - tileHeightMod);
	}

	public static int calculateTileHeight(int height, int numTiles, int numTilesPerScreen) {
		double div = (numTiles > 0 && numTiles < numTilesPerScreen) ? numTiles : numTilesPerScreen;
		double tileHeightMod;
		if (numTiles == 1)
			tileHeightMod = height * (.333) + 10;
		else
			tileHeightMod = (1 - (1 / div));
		return (int) ((height / div) - tileHeightMod);
	}

	public static int calculateTileWidth(int width, int numTiles) {
		double div = (numTiles > 0 && numTiles < TILEDIV) ? numTiles : TILEDIV;
		double tileHeightMod = (1 - (1 / div));
		return (int) ((width / div) - tileHeightMod);
	}

	public static int calculateTileWidth(int width, int numTiles, int numTilesPerScreen) {
		double div = (numTiles > 0 && numTiles < numTilesPerScreen) ? numTiles : numTilesPerScreen;

		double tileHeightMod = (1 - (1 / div));
		return (int) ((width / div) - tileHeightMod);
	}

	public static void setTileHeight(ViewGroup view, int height) {
		ViewGroup.LayoutParams lp = view.getLayoutParams();
		if (lp == null)
			lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
		else
			lp.height = height;
		view.setLayoutParams(lp);
	}

	public static void setTileWidth(ViewGroup view, int width) {
		ViewGroup.LayoutParams lp = view.getLayoutParams();
		if (lp == null)
			lp = new ViewGroup.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT);
		else
			lp.width = width;
		view.setLayoutParams(lp);
	}
}
