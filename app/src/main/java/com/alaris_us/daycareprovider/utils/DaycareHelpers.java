package com.alaris_us.daycareprovider.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections4.comparators.ComparatorChain;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Minutes;
import org.joda.time.Period;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.daycareprovider.comparators.CubbyNameComparator;
import com.alaris_us.daycareprovider.comparators.FacilityNameComparator;
import com.alaris_us.daycareprovider.comparators.PagerNameComparator;
import com.alaris_us.daycareprovider.comparators.RecordDateComparator;
import com.alaris_us.daycareprovider.comparators.RoomNameComparator;
import com.alaris_us.daycareprovider.comparators.StaffFirstNameComparator;
import com.alaris_us.daycareprovider.comparators.StaffLastNameComparator;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.webkit.GeolocationPermissions;

public class DaycareHelpers {

	public static boolean isAdult(Date ofBirth, int adultAge) {

		return (getAgeInYears(ofBirth) > adultAge);
	}

	public static int getAgeInYears(Date birth) {

		LocalDate localBirth = LocalDate.fromDateFields(birth);
		Period fromNow = new Period(localBirth, LocalDate.now());
		return fromNow.getYears();
	}

	public static int getAgeInMonths(Date birth) {

		LocalDate localBirth = LocalDate.fromDateFields(birth);
		Period fromNow = new Period(localBirth, LocalDate.now());
		return fromNow.getMonths();
	}

	public static int getAgeInYears(DateTime birth) {

		LocalDate localBirth = birth.toLocalDate();
		Period fromNow = new Period(localBirth, LocalDate.now());
		return fromNow.getYears();
	}

	public static int getAgeInMonths(DateTime birth) {

		LocalDate localBirth = birth.toLocalDate();
		Period fromNow = new Period(localBirth, LocalDate.now());
		return fromNow.getMonths();
	}

	public static int getMinutesToNow(Date beforeNow) {

		DateTime beforeNowDT = new DateTime(beforeNow);
		return getMinutesToNow(beforeNowDT);
	}

	public static int getMinutesToNow(DateTime beforeNowDT) {

		DateTime now = new DateTime();
		return Minutes.minutesBetween(beforeNowDT, now).getMinutes();
	}

	public static void sortCubbyList(List<Cubby> needsSorting) {

		ComparatorChain<Cubby> chain = new ComparatorChain<Cubby>();
		chain.addComparator(new CubbyNameComparator());
		Collections.sort(needsSorting, chain);
	}

	public static void sortPagerList(List<Pager> needsSorting) {

		ComparatorChain<Pager> chain = new ComparatorChain<Pager>();
		chain.addComparator(new PagerNameComparator());
		Collections.sort(needsSorting, chain);
	}

	public static void sortRoomsList(List<Room> needsSorting) {

		ComparatorChain<Room> chain = new ComparatorChain<Room>();
		chain.addComparator(new RoomNameComparator());
		Collections.sort(needsSorting, chain);
	}

	public static void sortRecordsList(List<Record> needsSorting) {

		ComparatorChain<Record> chain = new ComparatorChain<Record>();
		chain.addComparator(new RecordDateComparator());
		Collections.sort(needsSorting, chain);
	}

	public static void sortStaffList(List<Staff> needsSorting) {

		ComparatorChain<Staff> chain = new ComparatorChain<Staff>();
		chain.addComparator(new StaffFirstNameComparator());
		chain.addComparator(new StaffLastNameComparator());
		Collections.sort(needsSorting, chain);
	}

	public static void sortFacilitiesList(List<Facility> needsSorting) {

		ComparatorChain<Facility> chain = new ComparatorChain<Facility>();
		chain.addComparator(new FacilityNameComparator());
		Collections.sort(needsSorting, chain);
	}

	public interface YesNoDialogCallback {

		public void yesClicked();

		public void noClicked();
	}

	public static void createYesNoDialog(Context context, String message, final YesNoDialogCallback callback) {
		createYesNoDialog(context, message, "Yes", "No", callback);
	}

	public static void createYesNoDialog(Context context, String message, String posText, String negText,
			final YesNoDialogCallback callback) {

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (callback != null) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						callback.yesClicked();
						return;
					case DialogInterface.BUTTON_NEGATIVE:
						callback.noClicked();
						return;
					}
				}
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setCancelable(false).setPositiveButton(posText, dialogClickListener)
				.setNegativeButton(negText, dialogClickListener).show();
	}

	public interface OKDialogCallback {

		public void okClicked();
	}

	public static void createOKDialog(Context context, String message) {

		createOKDialog(context, message, null);
	}

	public static void createOKDialog(Context context, String message, final OKDialogCallback callback) {

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				if (callback != null) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE:
						callback.okClicked();
						return;
					}
				}
			}
		};
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setCancelable(false).setPositiveButton("OK", dialogClickListener).show();
	}

	public static HashMap<Member, Number> getChildrenWithOverageMinutes(List<Member> children) {
		HashMap<Member, Number> toReturn = new HashMap<>();

		for (Member child : children) {
			int minutesUsed = DaycareHelpers.getMinutesToNow(child.getLastCheckIn());
			int minutesLeft = child.getMinutesLeft().intValue();
			int newMinutesLeft = child.getMinutesLeft().intValue() - minutesUsed;
			int payableMinutes = 0;
			if (Math.signum(minutesLeft) == 0 || Math.signum(minutesLeft) == -1) {
				payableMinutes = Math.abs(minutesUsed);
			} else if ((Math.signum(minutesLeft) == 0 || Math.signum(minutesLeft) == 1)
					&& Math.signum(newMinutesLeft) == -1) {
				payableMinutes = Math.abs(newMinutesLeft);
			}
			if (payableMinutes > 0) {
				toReturn.put(child, payableMinutes);
			}
		}
		return toReturn;
	}

	public static HashMap<Admission, Number> calculateAdmissionsUsed(List<Admission> admissions, Member child, Facility facility) {
		HashMap<Admission, Number> toReturn = new HashMap<>();
		Number admitsUsed = 0;
		
		for (Admission admit : admissions) {
			if (facility.getUsesAssocWideAdmissions()) {
				int minutesUsed = DaycareHelpers.getMinutesToNow(child.getLastCheckIn());
				admitsUsed = admitsUsed.intValue() + (int) Math.ceil(((minutesUsed - facility.getAdmissionGracePeriod().doubleValue())
								/ facility.getAdmissionLength().doubleValue()));
				toReturn.put(admit, admitsUsed);
			} else {
				if (admit.getFacility().equals(facility)) {
					int minutesUsed = DaycareHelpers.getMinutesToNow(child.getLastCheckIn());
					admitsUsed = admitsUsed.intValue() + (int) Math.ceil(((minutesUsed - facility.getAdmissionGracePeriod().doubleValue())
									/ facility.getAdmissionLength().doubleValue()));
					toReturn.put(admit, admitsUsed);
				}
			}
		}
		return toReturn;
	}
	
	public static HashMap<Admission, Number> calculateAdmissionsUsed(List<Admission> admissions, List<Member> children, Facility facility) {
		HashMap<Admission, Number> toReturn = new HashMap<>();
		
		for (Admission admit : admissions) {
			Number admitsUsed = 0;
			for (Member child : children) {
				if (admit.getFamily().equals(child.getPFamily())) {
					double minutesUsed = DaycareHelpers.getMinutesToNow(child.getLastCheckIn());
					admitsUsed = admitsUsed.intValue() + (int) Math.ceil(((minutesUsed - facility.getAdmissionGracePeriod().doubleValue())
									/ facility.getAdmissionLength().doubleValue()));
				}
			}
			toReturn.put(admit, admitsUsed);
		}
		return toReturn;
	}

	public static String getCommonLastName(List<Member> members) {
		String name = "";
		int maxFrequency = 0;
		List<String> lastNames = new ArrayList<>();
		for (Member m : members) {
			lastNames.add(m.getLastName());
		}
		for (Member mem : members) {
			int showsUp = Collections.frequency(lastNames, mem.getLastName());
			if (showsUp > maxFrequency) {
				maxFrequency = showsUp;
				name = mem.getLastName();
			}
		}
		return name;
	}
}
