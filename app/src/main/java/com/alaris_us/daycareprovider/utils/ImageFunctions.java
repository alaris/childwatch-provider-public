package com.alaris_us.daycareprovider.utils;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;

import com.bindroid.ValueConverter;

public class ImageFunctions {

	public static int HORIZONTAL = 0;
	public static int VERTICAL = 1;

	public static Bitmap roundImage(Bitmap bmp, Float radius) {

		Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());
		Canvas canvas = new Canvas(ret);

		BitmapShader shader;
		shader = new BitmapShader(bmp, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);

		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setShader(shader);

		RectF rect = new RectF(0.0f, 0.0f, bmp.getWidth(), bmp.getHeight());

		canvas.drawRoundRect(rect, radius, radius, paint);

		canvas.drawBitmap(bmp, 0, 0, paint);

		return ret;
	}

	public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness) {
		ColorMatrix cm = new ColorMatrix(new float[] { contrast, 0, 0, 0, brightness, 0, contrast, 0, 0, brightness, 0,
				0, contrast, 0, brightness, 0, 0, 0, 1, 0 });

		Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

		Canvas canvas = new Canvas(ret);

		Paint paint = new Paint();
		paint.setColorFilter(new ColorMatrixColorFilter(cm));
		canvas.drawBitmap(bmp, 0, 0, paint);

		return ret;
	}

	public static Bitmap changeBitmapSaturation(Bitmap source, float value) {

		ColorMatrix grayMatrix = new ColorMatrix();
		grayMatrix.setSaturation(value);

		ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(grayMatrix);
		Paint paint = new Paint();
		paint.setColorFilter(colorFilter);

		Canvas canvas = new Canvas(source);
		canvas.drawBitmap(source, 0, 0, paint);

		return source;

	}

	public static Bitmap colorizeBitmap(Bitmap source, int value, float mix, float saturation) {

		Bitmap grayscaleBitmap = changeBitmapSaturation(source, mix);

		Paint paint = new Paint();
		paint.setColorFilter(new PorterDuffColorFilter(value, PorterDuff.Mode.MULTIPLY));

		Canvas canvas = new Canvas(grayscaleBitmap);
		canvas.drawBitmap(grayscaleBitmap, 0, 0, paint);

		grayscaleBitmap = changeBitmapSaturation(grayscaleBitmap, saturation);

		return grayscaleBitmap;

	}

	public static Bitmap loadBitmapFromURI(Uri uri, Context context) {

		try {
			return MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		}

	}

	public static Bitmap compressAsJPG(Bitmap src, int quality) {

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		src.compress(Bitmap.CompressFormat.JPEG, quality, os);

		byte[] array = os.toByteArray();
		return BitmapFactory.decodeByteArray(array, 0, array.length);

	}

	public static Bitmap squareCropBitmap(Bitmap source) {

		int height = source.getHeight();
		int width = source.getWidth();

		if (width >= source.getHeight()) {

			return Bitmap.createBitmap(source, width / 2 - height / 2, 0, height, height);

		} else {

			return Bitmap.createBitmap(source, 0, height / 2 - width / 2, width, width);
		}

	}

	public static Bitmap cropBitmap(Bitmap source, int x, int y, int width, int height) {
		Bitmap bitmap;
		try {
			bitmap = Bitmap.createBitmap(source, x, y, width, height);

		} catch (IllegalArgumentException e) {
			bitmap = source;
		}
		return bitmap;
	}

	public static Bitmap cropBitmap(Bitmap source, Rect cropRect) {

		return cropBitmap(source, cropRect.left, cropRect.top, cropRect.width(), cropRect.height());

	}

	public static Bitmap scaleBitmap(Bitmap source, int newWidth, int newHeight) {

		return Bitmap.createScaledBitmap(source, newWidth, newHeight, false);

	}

	public static Bitmap drawableToBitmap(Drawable drawable) {

		return ((BitmapDrawable) drawable).getBitmap();

	}

	public static Drawable bitmapToDrawable(Bitmap bitmap) {

		return new BitmapDrawable(Resources.getSystem(), bitmap);

	}

	public static byte[] bitmapToByteArray(Bitmap bitmap) {

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

		return stream.toByteArray();

	}

	public static Bitmap byteArrayToBitmap(byte[] byteArray) {

		return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

	}

	public static void adjustHue(ColorMatrix cm, float value) {
		value = cleanValue(value, 180f) / 180f * (float) Math.PI;
		if (value == 0) {
			return;
		}
		float cosVal = (float) Math.cos(value);
		float sinVal = (float) Math.sin(value);
		float lumR = 0.213f;
		float lumG = 0.715f;
		float lumB = 0.072f;
		float[] mat = new float[] { lumR + cosVal * (1 - lumR) + sinVal * (-lumR),
				lumG + cosVal * (-lumG) + sinVal * (-lumG), lumB + cosVal * (-lumB) + sinVal * (1 - lumB), 0, 0,
				lumR + cosVal * (-lumR) + sinVal * (0.143f), lumG + cosVal * (1 - lumG) + sinVal * (0.140f),
				lumB + cosVal * (-lumB) + sinVal * (-0.283f), 0, 0, lumR + cosVal * (-lumR) + sinVal * (-(1 - lumR)),
				lumG + cosVal * (-lumG) + sinVal * (lumG), lumB + cosVal * (1 - lumB) + sinVal * (lumB), 0, 0, 0f, 0f,
				0f, 1f, 0f, 0f, 0f, 0f, 0f, 1f };

		cm.postConcat(new ColorMatrix(mat));
	}

	public static Bitmap mirrorBitmap(Bitmap source, int axis) {
		Matrix m = new Matrix();
		if (axis == HORIZONTAL)
			m.preScale(-1, 1);
		else
			m.preScale(-1, -1);
		Bitmap dst = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), m, false);
		dst.setDensity(DisplayMetrics.DENSITY_DEFAULT);
		return dst;
	}

	public static Bitmap rotateBitmap(Bitmap source, float degrees) {
		Matrix m = new Matrix();
		m.preRotate(degrees);
		Bitmap dst = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), m, false);
		dst.setDensity(DisplayMetrics.DENSITY_DEFAULT);
		return dst;
	}

	private static float cleanValue(float p_val, float p_limit) {
		return Math.min(p_limit, Math.max(-p_limit, p_val));
	}

	public static class BitmapCropConverter extends ValueConverter {
		private int mWidth, mHeight;
		private View mView;
		private final boolean isUsingView;

		public BitmapCropConverter(int width, int height) {
			mWidth = width;
			mHeight = height;
			isUsingView = false;
		}

		public BitmapCropConverter(View view) {
			mView = view;
			isUsingView = true;
		}

		@Override
		public Object convertToSource(Object targetValue, Class<?> sourceType) {
			return super.convertToSource(isUsingView ? cropUsingView(targetValue) : cropUsingDim(targetValue),
					sourceType);
		}

		@Override
		public Object convertToTarget(Object sourceValue, Class<?> targetType) {
			return super.convertToSource(isUsingView ? cropUsingView(sourceValue) : cropUsingDim(sourceValue),
					targetType);
		}

		public Object cropUsingDim(Object obj) {
			if (obj instanceof Bitmap) {
				Bitmap targetBitmap = (Bitmap) obj;
				int objWidth = targetBitmap.getWidth();
				int objHeight = targetBitmap.getHeight();
				if (mWidth <= objWidth && mHeight <= objHeight) {
					Bitmap sourceBitmap = Bitmap.createBitmap(targetBitmap, objWidth - mWidth, objHeight - mHeight,
							mWidth, mHeight);
					return sourceBitmap;
				}
			}
			return obj;
		}

		public Object cropUsingView(Object obj) {
			if (obj instanceof Bitmap) {
				Bitmap targetBitmap = (Bitmap) obj;
				int objWidth = targetBitmap.getWidth();
				int objHeight = targetBitmap.getHeight();
				double ratio = ((double) mView.getWidth() / (double) mView.getHeight());
				int width = (int) (objHeight * ratio);
				if (width <= objWidth) {
					Bitmap sourceBitmap = Bitmap.createBitmap(targetBitmap, objWidth - width, 0, width, objHeight);
					return scaleBitmap(sourceBitmap, mView.getWidth(), mView.getHeight());
				}
			}
			return obj;
		}

	}

	public static class NullBitmapConverter extends ValueConverter {
		private Bitmap mBitmap;

		public NullBitmapConverter(Bitmap bitmap) {
			mBitmap = bitmap;
		}

		@Override
		public Object convertToSource(Object targetValue, Class<?> sourceType) {
			if (targetValue == null)
				targetValue = mBitmap;
			return super.convertToSource(targetValue, sourceType);
		}

		@Override
		public Object convertToTarget(Object sourceValue, Class<?> targetType) {
			if (sourceValue == null)
				sourceValue = mBitmap;
			return super.convertToTarget(sourceValue, targetType);
		}

	}
}
