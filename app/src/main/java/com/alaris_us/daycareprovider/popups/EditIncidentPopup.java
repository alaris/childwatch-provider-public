package com.alaris_us.daycareprovider.popups;

import org.apache.commons.lang3.text.WordUtils;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.ui.EditTextTextProperty;
import com.bindroid.ui.UiBinder;

public class EditIncidentPopup extends Popup {

	private final Incident mIncident;
	private String mHeaderText;
	private EditText mEditText;
	private final EditIncidentPopupCallback mEditIncidentPopupCallback;

	public interface EditIncidentPopupCallback {

		void updateIncident(Incident incident);
	}

	public Incident getIncident() {

		return mIncident;
	}

	public String getChildName() {

		return getFormattedMemberName(getIncident());
	}

	private String getFormattedMemberName(Incident incident) {

		return WordUtils.capitalizeFully(incident.getFirstName() + " " + incident.getLastName());
	}

	public OnClickListener getSaveOnClickListener() {

		return new SaveOnClickListener();
	}

	public OnClickListener getCancelOnClickListener() {

		return new CancelOnClickListener();
	}

	class SaveOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			EditIncidentPopup.this.dismiss();
			mIncident.setIncident(getIncidentText());
			mEditIncidentPopupCallback.updateIncident(mIncident);
		}
	}

	class CancelOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			EditIncidentPopup.this.dismiss();
		}
	}

	public String getIncidentMessage() {
		return mIncident.getIncident().toString();
	}

	public String getIncidentText() {
		return mEditText.getText().toString();
	}

	public EditIncidentPopup(Context context, View view, Incident incident,
			EditIncidentPopupCallback editIncidentPopupCallback) {

		super(context, R.layout.popup_editincident, PopupMode.CENTER);
		View v = super.getView();
		mIncident = incident;
		mEditIncidentPopupCallback = editIncidentPopupCallback;
		mHeaderText = context.getResources().getString(R.string.edit_incident_popup_tile_header);
		mView = view;
		mEditText = (EditText) v.findViewById(R.id.EditText_EditIncident);
		// Incident Information
		UiBinder.bind(v, R.id.TextView_EditIncident, "Text", this, "ChildName", BindingMode.ONE_WAY,
				new ToStringConverter(mHeaderText));
		UiBinder.bind(new EditTextTextProperty(mEditText), this, "IncidentMessage", BindingMode.TWO_WAY);
		// Button Actions
		UiBinder.bind(v, R.id.Button_Save, "OnClickListener", this, "SaveOnClickListener");
		UiBinder.bind(v, R.id.Button_Cancel, "OnClickListener", this, "CancelOnClickListener");
	}
}
