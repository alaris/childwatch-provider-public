package com.alaris_us.daycareprovider.popups;

import org.joda.time.DateTime;

import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;

import com.alaris_us.daycareprovider_dev.R;

public class DatePopup extends Popup {

	public interface DatePickerDialogListener {

		public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth);
	}

	private DatePicker mDP;
	private DatePickerDialogListener m_listener;

	public DatePopup(Context context) {

		super(context, R.layout.popup_date, PopupMode.CENTER);
		mDP = (DatePicker) super.getView().findViewById(R.id.datePicker);
	}

	public void setDateTimePickerDialogListener(DatePickerDialogListener listener) {

		m_listener = listener;
	}

	public void show(View anchor, DateTime initial) {

		super.show(anchor);
		if (initial == null)
			initial = new DateTime();
		mDP.init(initial.getYear(), initial.getMonthOfYear() - 1, initial.getDayOfMonth(), new OnDateChangedListener() {

			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

				m_listener.onDateChanged(view, year, monthOfYear + 1, dayOfMonth);
			}
		});
	}
}
