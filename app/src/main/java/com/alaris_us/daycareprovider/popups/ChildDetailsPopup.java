package com.alaris_us.daycareprovider.popups;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycaredata.util.ConnectivityUtility.ConnectivityListener;
import com.alaris_us.daycareprovider.activities.SearchAndEditFamily;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.popups.AdmissionsUsedPopup.OnConfirmAdmissionUsageListener;
import com.alaris_us.daycareprovider.popups.MinutesOveragePopup.OnConfirmMintuesOverageListener;
import com.alaris_us.daycareprovider.popups.SelectRoomPopup.SelectRoomPopupListener;
import com.alaris_us.daycareprovider.printing.TemplatePrint;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider.utils.DaycareHelpers.YesNoDialogCallback;
import com.alaris_us.daycareprovider.view.ChildPopupExpectedAreas;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.UiBinder;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.View.OnClickListener;

public class ChildDetailsPopup extends Popup implements ConnectivityListener {
	private PersistenceLayer mPersistenceLayer;

	private final TrackableBoolean mInternetConnected;
	private final TrackableCollection<Room> mRoomList;
	private final TrackableField<MemberAndCubbies> mChild;
	private final TrackableCollection<Member> mCheckedInChildren;
	private final ChildDetailsPopupCallback mChildDetailsPopupCallback;
	private final SelectRoomPopupListener mRoomSelectListener;
	private static String YEAR_UNIT = "yr";
	private static String MONTH_UNIT = "mo";
	private View mView;
	private TemplatePrint mPrinter;

	public interface ChildDetailsPopupCallback {

		void checkOut(List<Member> children);

		void addCubby(Member child);

		void addIncident(Member child, String message);

		void sendSMS(Member member, String message);

		void assignPager(Pager pager, Member child, Family family, Member proxy);

	}

	public List<WorkoutArea> getExpected() {
		return mChild.get().getExpectedAreas();
	}

	public Member getChild() {

		return mChild.get().getMember();
	}

	public Member getCheckedInBy() {

		return mChild.get().getMember().getPCheckInBy();
	}

	public String getUsage() {

		String usageLabel = "";

		if (mChild.get().getUsing().size() > 0 && mChild.get().getMember().getEstimatedPickupTime() != null) {
			for (Cubby c : mChild.get().getUsing()) {
				usageLabel += c.getName() + " ";
			}
			usageLabel += "/ " + mChild.get().getMember().getEstimatedPickupTime();
		} else if (mChild.get().getUsing().size() > 0 && mChild.get().getPagers().size() > 0) { 
			for (Cubby c : mChild.get().getUsing()) {
				usageLabel += c.getName() + " ";
			}
			usageLabel += "and ";
			for (Pager p : mChild.get().getPagers()) {
				usageLabel += p.getName() + " ";
			}
		} else if (mChild.get().getUsing().size() > 0) {
			for (Cubby c : mChild.get().getUsing()) {
				usageLabel += c.getName() + " ";
			}
		} else if (mChild.get().getPagers().size() > 0) {
			for (Pager p : mChild.get().getPagers()) {
				usageLabel += p.getName() + " ";
			}
		} else if (mChild.get().getMember().getEstimatedPickupTime() != null) {
			if (!mChild.get().getMember().getEstimatedPickupTime().isEmpty())
				usageLabel = mChild.get().getMember().getEstimatedPickupTime();
		}

		return usageLabel;
	}

	public String getUsageTitle() {

		String usageTitle = "";
		if (mChild.get().getUsing().size() > 0 && mChild.get().getMember().getEstimatedPickupTime() != null)
			usageTitle = "Cubby/Pickup:";
		else if (mChild.get().getUsing().size() > 0 && mChild.get().getPagers().size() > 0)
			usageTitle = "Cubby and Pager:";
		else if (mChild.get().getUsing().size() > 0)
			usageTitle = "Cubbies:";
		else if (mChild.get().getPagers().size() > 0)
			usageTitle = "Pagers:";
		else if (!mChild.get().getMember().getEstimatedPickupTime().isEmpty())
			usageTitle = "Est. Pickup:";
		return usageTitle;
	}

	public String getCubbys() {

		String cubbyLabel = "";
		for (Cubby c : mChild.get().getUsing()) {
			cubbyLabel += c.getName() + " ";
		}
		return cubbyLabel;
	}

	public String getPagers() {

		String pagerLabel = "";
		for (Pager p : mChild.get().getPagers()) {
			pagerLabel += p.getName() + " ";
		}
		return pagerLabel;
	}

	public String getIncidents() {
		String incidentsLabel = "";
		for (Incident i : mChild.get().getIncidents())
			incidentsLabel += i.getIncident() + " ";
		return incidentsLabel;
	}

	public String getProgram() {
		String programLabel = "";
		if (mChild.get().getMember().getInProgram() != null && mChild.get().getMember().getPRoom() != null)
			programLabel = mChild.get().getMember().getInProgram().getName() + " - "
					+ mChild.get().getMember().getPRoom().getName();
		return programLabel;
	}

	public String getCheckedInByName() {

		return getFormattedMemberName(getCheckedInBy());
	}

	public String getChildName() {
		return getFormattedMemberName(getChild());
	}

	public String getAge() {

		LocalDate bDate = LocalDate.fromDateFields(getChild().getBirthDate());
		Period age = new Period(bDate, LocalDate.now());
		int difference = age.getYears();
		String unit = YEAR_UNIT;
		if (difference < 1) {
			difference = age.getMonths();
			unit = MONTH_UNIT;
		}
		return String.format(Locale.getDefault(), "Age: %d%s", difference, unit);
	}

	private String getFormattedMemberName(Member member) {

		return WordUtils.capitalizeFully(member.getFirstName() + " " + member.getLastName());
	}

	public Bitmap getChildImage() {

		Bitmap image = getChild().getImage();
		return (image == null) ? BitmapFactory.decodeResource(mView.getResources(), R.drawable.photo_not_available)
				: image;
	}

	public Bitmap getCheckedInByImage() {

		Bitmap image = getCheckedInBy().getImage();
		if (image == null)
			image = BitmapFactory.decodeResource(mView.getResources(), R.drawable.photo_not_available);
		return image;
	}

	public OnClickListener getCheckOutOnClickListener() {

		return new CheckOutOnClickListener();
	}

	public OnClickListener getAddCubbyOnClickListener() {

		return new AddCubbyOnClickListener();
	}

	public OnClickListener getGotoFamilyOnClickListener() {

		return new GotoFamilyOnClickListener();
	}

	public OnClickListener getIncidentOnClickListener() {

		return new IncidentOnClickListener();
	}

	public ChangeRoomOnClickListener getChangeRoomOnClickListener() {
		return new ChangeRoomOnClickListener();
	}

	class GotoFamilyOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			Intent toSearch = new Intent(v.getContext(), SearchAndEditFamily.class);
			toSearch.putExtra("OBJECT_ID", getChild().getObjectId());
			v.getContext().startActivity(toSearch);
			ChildDetailsPopup.this.dismiss();
		}
	}

	class CheckOutOnClickListener implements OnClickListener, OnConfirmMintuesOverageListener, OnConfirmAdmissionUsageListener {
		@Override
		public void onClick(final View v) {
			final Facility facility = mPersistenceLayer.getPersistenceData().getFacility();
			boolean usesOverageAlerting = mView.getResources().getBoolean(R.bool.IS_OVERAGE_ALERTING_ENABLED);
			if (usesOverageAlerting) {
				HashMap <Member, Number> payableChildren = new HashMap<>();
				payableChildren = DaycareHelpers.getChildrenWithOverageMinutes(Arrays.asList(getChild()));
				if (!payableChildren.isEmpty()) {
					ChildDetailsPopup.this.dismiss();
					new MinutesOveragePopup(payableChildren, v.getContext(), this).show(mView);
				} else {
					this.onCheckOut();
				}
			} else if (mPersistenceLayer.getPersistenceData().getFacilityUsesAdmissions()) {
				mPersistenceLayer.chargeAdmissions(Arrays.asList(getChild()), facility, false, getCheckedInBy(),
						new PersistenceLayerCallback<List<HashMap<String, Object>>>() {
					@Override
					public void done(List<HashMap<String, Object>> data, Exception e) {
						new AdmissionsUsedPopup(data, Arrays.asList(getChild()), facility, getCheckedInBy(), v.getContext(), mPersistenceLayer,
								CheckOutOnClickListener.this).show(mView);
					}
				});
			} else {
				this.onCheckOut();
			}
		}

		@Override
		public void onCheckOut() {
			String message = "Do you want to checkout user " + getChildName() + "?";
			DaycareHelpers.createYesNoDialog(mView.getContext(), message, new YesNoDialogCallback() {

				@Override
				public void yesClicked() {

					if (mChildDetailsPopupCallback != null) {
						if (mPersistenceLayer.getPersistenceData().getFacilityUsesAdmissions()) {
							mPersistenceLayer.chargeAdmissions(Arrays.asList(getChild()), mPersistenceLayer.getPersistenceData().getFacility(), true, getCheckedInBy(),
									new PersistenceLayerCallback<List<HashMap<String, Object>>>() {
										@Override
										public void done(List<HashMap<String, Object>> data, Exception e) {
										}
									});
						}
						mChildDetailsPopupCallback.checkOut(Arrays.asList(getChild()));
					}
					ChildDetailsPopup.this.dismiss();
				}

				@Override
				public void noClicked() {

				}
			});

		}

		@Override
		public void onCancel() {

		}
	}

	class AddCubbyOnClickListener implements OnClickListener {

		@Override
		public void onClick(final View v) {
			mPersistenceLayer.fetchAvailableCubbies(new PersistenceLayerCallback<List<Cubby>>() {
				@Override
				public void done(final List<Cubby> availableCubbies, Exception e) {
					String message = "Do you want to add additional cubby for " + getChildName() + "?";
					DaycareHelpers.createYesNoDialog(v.getContext(), message, new YesNoDialogCallback() {

						@Override
						public void yesClicked() {

							if (mChildDetailsPopupCallback != null)
								mChildDetailsPopupCallback.addCubby(getChild());
							ChildDetailsPopup.this.dismiss();
						}

						@Override
						public void noClicked() {

						}
					});
				}
			});
		}
	}

	class IncidentOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			ChildDetailsPopup.this.dismiss();
			(new IncidentPopup(mView.getContext(), mView, getChild(), ChildDetailsPopup.this,
					mChildDetailsPopupCallback)).show(mView);
		}
	}

	class ChangeRoomOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			ChildDetailsPopup.this.dismiss();
			List<Member> childrenToChangeRoom = new ArrayList<Member>();
			childrenToChangeRoom.add(getChild());
			(new SelectRoomPopup(mView.getContext(), mRoomList, childrenToChangeRoom, mRoomSelectListener)).show(mView);

		}

	}

	public SendSMSOnClickListener getSendSMSOnClickListener() {
		return mInternetConnected.get() ? new SendSMSOnClickListener() : null;
	}

	class SendSMSOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			ChildDetailsPopup.this.dismiss();
			(new SendSMSPopup(mView.getContext(), mView, getChild(), getCheckedInBy(), mPersistenceLayer.getPersistenceData().getSMSMessages(),
					ChildDetailsPopup.this, mChildDetailsPopupCallback)).show(mView);
		}
	}

	public int getSyncSpinnerVisibility() {
		return mChild.get().getFetching() ? View.VISIBLE : View.GONE;
	}

	public int getSendSMSVisibility() {
		return getCheckedInBy().getUsesSMS() ? View.VISIBLE : View.GONE;
	}

	public float getSendSMSAlpha() {
		return mInternetConnected.get() ? 1f : 0.5f;
	}

	public AssignPagerOnClickListener getAssignPagerOnClickListener() {
		return new AssignPagerOnClickListener();
	}

	class AssignPagerOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			ChildDetailsPopup.this.dismiss();
			mPersistenceLayer.fetchAvailablePagers(new PersistenceLayerCallback<List<Pager>>() {
				@Override
				public void done(final List<Pager> availablePagers, Exception e) {
					(new AssignPagerPopup(mView.getContext(), mView, getChild(), getCheckedInBy(), availablePagers,
							ChildDetailsPopup.this, mChildDetailsPopupCallback)).show(mView);
				}
			});
		}
	}

	public ReprintLabelOnClickListener getReprintLabelOnClickListener() {
		return new ReprintLabelOnClickListener();
	}

	class ReprintLabelOnClickListener implements OnClickListener, PersistenceLayerCallback<List<Member>> {
		@Override
		public void onClick(View v) {
			ChildDetailsPopup.this.dismiss();

			if (mCheckedInChildren.isEmpty())
				mPersistenceLayer.FetchMembersCheckedInBy(getCheckedInBy(), this);
			else
				showReprintLabelPopup();
		}

		public void showReprintLabelPopup() {
			Facility facility = mPersistenceLayer.getPersistenceData().getFacility();
			(new ReprintLabelPopup(mView.getContext(), mView, getChild(), getCheckedInBy(), getExpected(),
					mCheckedInChildren, ChildDetailsPopup.this, mChildDetailsPopupCallback, mPrinter, facility)).show(mView);
		}

		@Override
		public void done(List<Member> checkedInChildren, Exception e) {
			mCheckedInChildren.addAll(checkedInChildren);
			showReprintLabelPopup();
		}
	}

	public int getReprintLabelVisibility() {
		String ip = mPersistenceLayer.getPersistenceData().getPrinterIP();
		String macAddress = mPersistenceLayer.getPersistenceData().getPrinterMacAddress();
		if (ip != null && macAddress != null) {
			if (!ip.isEmpty() || !macAddress.isEmpty())
				return View.VISIBLE;
			else
				return View.GONE;
		} else if (ip != null) {
			if (!ip.isEmpty())
				return View.VISIBLE;
			else
				return View.GONE;
		} else if (macAddress != null) {
			if (!macAddress.isEmpty())
				return View.VISIBLE;
			else
				return View.GONE;
		} else
			return View.GONE;
	}

	public int getAddCubbyVisibility() {
		List<Cubby> facilityCubbies = mPersistenceLayer.getPersistenceData().getAllFacilityCubbies();
		if (facilityCubbies != null) {
			if (!facilityCubbies.isEmpty())
				return View.VISIBLE;
			else
				return View.GONE;
		} else
			return View.GONE;
	}

	public int getAssignPagerVisibility() {
		List<Pager> facilityPagers = mPersistenceLayer.getPersistenceData().getAllFacilityPagers();
		if (facilityPagers != null) {
			if (!facilityPagers.isEmpty())
				return View.VISIBLE;
			else
				return View.GONE;
		} else
			return View.GONE;
	}

	public int getProgramVisibility() {
		if (mChild.get().getMember().getInProgram() != null)
			return View.VISIBLE;
		else
			return View.GONE;
	}

	public ChildDetailsPopup(Context context, View view, PersistenceLayer persistenceLayer, MemberAndCubbies child,
			ChildDetailsPopupCallback childDetailsPopupCallback, SelectRoomPopupListener roomChangedCallback,
			TemplatePrint printer) {
		super(context, R.layout.popup_childdetails, PopupMode.CENTER);

		mPersistenceLayer = persistenceLayer;
		mView = view;
		mChild = new TrackableField<MemberAndCubbies>(child);
		mRoomList = new TrackableCollection<Room>(mPersistenceLayer.getPersistenceData().getAllFacilityRooms());
		mCheckedInChildren = new TrackableCollection<Member>();
		mChildDetailsPopupCallback = childDetailsPopupCallback;
		mRoomSelectListener = roomChangedCallback;
		mPrinter = printer;
		mInternetConnected = new TrackableBoolean(mPersistenceLayer.getConnectivityUtility().isInternetConnected());
		mPersistenceLayer.getConnectivityUtility().addConnectivityListener(this);

		View v = super.getView();
		// Child Information
		UiBinder.bind(v, R.id.ImageView_Photo, "ImageBitmap", this, "ChildImage");
		UiBinder.bind(v, R.id.TextView_Name, "Text", this, "ChildName");
		UiBinder.bind(v, R.id.TextView_Age, "Text", this, "Age");
		UiBinder.bind(v, R.id.TextView_MedicalConcerns, "Text", this, "Child.MedicalConcerns");
		UiBinder.bind(v, R.id.TextView_Incidents, "Text", this, "Incidents");
		UiBinder.bind(v, R.id.TextView_Program, "Text", this, "Program");
		UiBinder.bind(v, R.id.TextView_Notes, "Text", this, "Child.Notes");
		// Checked-In By information
		UiBinder.bind(v, R.id.ImageView_PhotoAdult, "ImageBitmap", this, "CheckedInByImage");
		UiBinder.bind(v, R.id.TextView_CheckedInBy, "Text", this, "CheckedInByName");
		UiBinder.bind(v, R.id.TextView_UsageTitle, "Text", this, "UsageTitle");
		UiBinder.bind(v, R.id.TextView_UsageList, "Text", this, "Usage");
		UiBinder.bind(v, R.id.ListView_ExpectedAreas, "Adapter", this, "Expected",
				new AdapterConverter(ChildPopupExpectedAreas.class));
		// Button Actions
		UiBinder.bind(v, R.id.Button_CheckOut, "OnClickListener", this, "CheckOutOnClickListener");
		UiBinder.bind(v, R.id.Button_AddCubby, "OnClickListener", this, "AddCubbyOnClickListener");
		UiBinder.bind(v, R.id.Button_GoFamily, "OnClickListener", this, "GotoFamilyOnClickListener");
		UiBinder.bind(v, R.id.Button_Incident, "OnClickListener", this, "IncidentOnClickListener");
		UiBinder.bind(v, R.id.Button_ChangeRoom, "OnClickListener", this, "ChangeRoomOnClickListener");
		UiBinder.bind(v, R.id.Button_SendSMS, "OnClickListener", this, "SendSMSOnClickListener");
		UiBinder.bind(v, R.id.Button_SendSMS, "Visibility", this, "SendSMSVisibility");
		UiBinder.bind(v, R.id.Button_SendSMS, "Alpha", this, "SendSMSAlpha");
		UiBinder.bind(v, R.id.Button_AssignPager, "OnClickListener", this, "AssignPagerOnClickListener");
		UiBinder.bind(v, R.id.Button_ReprintLabel, "OnClickListener", this, "ReprintLabelOnClickListener");
		UiBinder.bind(v, R.id.Button_ReprintLabel, "Visibility", this, "ReprintLabelVisibility");
		UiBinder.bind(v, R.id.Button_AddCubby, "Visibility", this, "AddCubbyVisibility");
		UiBinder.bind(v, R.id.Button_AssignPager, "Visibility", this, "AssignPagerVisibility");

		UiBinder.bind(v, R.id.sync_spinner, "Visibility", this, "SyncSpinnerVisibility");

		UiBinder.bind(v, R.id.TextView_Program, "Visibility", this, "ProgramVisibility");
	}

	@Override
	public void onConnectivityChanged(boolean isConnected) {
		mInternetConnected.set(isConnected);
	}
}
