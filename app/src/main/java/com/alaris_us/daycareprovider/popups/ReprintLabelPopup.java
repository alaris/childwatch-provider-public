package com.alaris_us.daycareprovider.popups;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycareprovider.popups.ChildDetailsPopup.ChildDetailsPopupCallback;
import com.alaris_us.daycareprovider.printing.TemplatePrint;
import com.alaris_us.daycareprovider.view.ReprintLabelItem;
import com.alaris_us.daycareprovider.view.ReprintLabelItemModel;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;

public class ReprintLabelPopup extends Popup {

	private final Member mGuardian;
	private final Member mChild;
	private List<ReprintLabelItemModel> mLabelList;
	private ChildDetailsPopup mDetailsPopup;
	private View mView;
	private Button mPrintButton;
	private ChildDetailsPopupCallback mChildDetailsPopupCallback;
	private List<Member> mLabelsToReprint;
	private TemplatePrint mPrinter;
	private final static String YEAR_UNIT = "yr";
	private final static String MONTH_UNIT = "mo";
	private List<WorkoutArea> mAreas;
	private List<Member> mCheckedInChildren;
	private Context mContext;
	private final int mSelectAllTextRes = R.string.reprintlabel_popup_button_selectall_text;
	private final int mSelectNoneTextRes = R.string.reprintlabel_popup_button_selectnone_text;
	private Button mSelectButton;
	private Facility m_facility;

	public Member getGuardian() {

		return mGuardian;
	}

	public List<ReprintLabelItemModel> getLabelList() {
		// Add Child ReprintLabelItem Models
		for (Member child : mCheckedInChildren) {
			ReprintLabelItemModel childModel = new ReprintLabelItemModel(mContext, child, false);
			mLabelList.add(childModel);
		}
		// Add Guardian ReprintLabelItem Model
		ReprintLabelItemModel guardianModel = new ReprintLabelItemModel(mContext, mGuardian, false);
		mLabelList.add(guardianModel);

		return mLabelList;
	}

	public String getGuardianName() {

		return getFormattedMemberName(getGuardian());
	}

	private String getFormattedMemberName(Member member) {

		return WordUtils.capitalizeFully(member.getFirstName() + " " + member.getLastName());
	}

	public OnClickListener getPrintOnClickListener() {

		return new PrintOnClickListener();
	}

	public OnClickListener getCancelOnClickListener() {

		return new CancelOnClickListener();
	}

	public OnClickListener getSelectAllOnClickListener() {

		return new SelectAllOnClickListener();
	}

	class PrintOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			ReprintLabelPopup.this.dismiss();
			printLabels();
		}
	}

	class CancelOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			ReprintLabelPopup.this.dismiss();
			mDetailsPopup.show(mView);
		}
	}

	class SelectAllOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			if (mSelectButton.getText().equals("Select All")) {
				mLabelsToReprint.clear();
				for (int i = 0; i < mLabelList.size(); i++) {
					mLabelsToReprint.add(mLabelList.get(i).getMember());
					mLabelList.get(i).setSelected(true);
				}
				mSelectButton.setText(mSelectNoneTextRes);
			} else {
				for (int i = 0; i < mLabelList.size(); i++) {
					mLabelsToReprint.remove(mLabelList.get(i).getMember());
					mLabelList.get(i).setSelected(false);
				}
				mSelectButton.setText(mSelectAllTextRes);
			}
		}
	}

	public android.widget.AdapterView.OnItemClickListener getReprintLabelOnItemClickListener() {
		return new ReprintLabelOnItemClickListener();
	}

	public class ReprintLabelOnItemClickListener implements android.widget.AdapterView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			ReprintLabelItemModel model = ((ReprintLabelItem) view).getReprintLabelItemModel();
			model.toggleSelected();
			if (model.getSelected())
				mLabelsToReprint.add(model.getMember());
			else
				mLabelsToReprint.remove(model.getMember());
		}
	}

	public ReprintLabelPopup(Context context, View view, Member childMember, Member guardianMember,
			List<WorkoutArea> areas, List<Member> checkedInChildren, ChildDetailsPopup detailsPopup,
			ChildDetailsPopupCallback cb, TemplatePrint printer, Facility facility) {
		super(context, R.layout.popup_reprintlabel, PopupMode.CENTER);
		View v = super.getView();
		mGuardian = guardianMember;
		mChild = childMember;
		mLabelList = new ArrayList<ReprintLabelItemModel>();
		mDetailsPopup = detailsPopup;
		mView = view;
		mPrintButton = (Button) v.findViewById(R.id.Button_Print);
		mChildDetailsPopupCallback = cb;
		mLabelsToReprint = new ArrayList<Member>();
		mPrinter = printer;
		mAreas = areas;
		mCheckedInChildren = checkedInChildren;
		mContext = context;
		m_facility = facility;

		mSelectButton = (Button) v.findViewById(R.id.Button_SelectAll);

		// Bind the listview
		UiBinder.bind(v, R.id.ListView_LabelList, "Adapter", this, "LabelList", BindingMode.TWO_WAY,
				new AdapterConverter(ReprintLabelItem.class, true, true));
		UiBinder.bind(v, R.id.ListView_LabelList, "OnItemClickListener", this, "ReprintLabelOnItemClickListener");

		// Button Actions
		UiBinder.bind(v, R.id.Button_Print, "OnClickListener", this, "PrintOnClickListener");
		UiBinder.bind(v, R.id.Button_Cancel, "OnClickListener", this, "CancelOnClickListener");
		UiBinder.bind(v, R.id.Button_SelectAll, "OnClickListener", this, "SelectAllOnClickListener");
	}

	private void printLabels() {

		if (null == mPrinter)
			return;

		// Print labels
		mPrinter.clearPrintData();
		Member guardianToReprint = null;
		List<Member> childrenToReprint = new ArrayList<Member>();
		List<String> guardianAreas = new ArrayList<String>();
		List<String> childNames = new ArrayList<String>();
		List<String> childAges = new ArrayList<String>();
		String guardianBarcode = "";
		if (mGuardian.getBarcode() != null)
			guardianBarcode = mGuardian.getBarcode();
		else
			guardianBarcode = " ";
		
		String guardianName = "";
		if (m_facility.getLabelPrintingOpts() != null) {
			try {
				if (m_facility.getLabelPrintingOpts().getBoolean("usesFirstNameLastInitial"))
					guardianName = mGuardian.getFirstName() + " " + mGuardian.getLastName().substring(0,1) + ".";
				else
					guardianName = mGuardian.getFirstName() + " " + mGuardian.getLastName();
			} catch (JSONException e) {
				guardianName = mGuardian.getFirstName() + " " + mGuardian.getLastName();
			}
		} else
			guardianName = mGuardian.getFirstName() + " " + mGuardian.getLastName();
		String guardianPhone = "";
		String returnTime = "";
		if (mGuardian.getEstimatedPickupTime() != null)
			returnTime = mGuardian.getEstimatedPickupTime();
		else
			returnTime = " ";

		if (mGuardian.getPhones() != null) {
			JSONArray guardianPhones = mGuardian.getPhones();
			for (int i = 0; i < guardianPhones.length(); i++) {
				try {
					JSONObject phone = guardianPhones.getJSONObject(i);
					if (phone.get("type").toString().contains("SMS") || phone.get("type").toString().contains("CELL")) {
						guardianPhone = phone.getString("number");
						break;
					}
				} catch (JSONException e) {
					e.printStackTrace();
					guardianPhone = " ";
				}
			}
		} else
			guardianPhone = " ";

		for (WorkoutArea w : mAreas) {
			guardianAreas.add(w.getWorkoutName());
		}

		for (Member m : mLabelsToReprint) {
			if (m.getObjectId().equals(mGuardian.getObjectId()))
				guardianToReprint = m;
			else
				childrenToReprint.add(m);
		}

		// Print Children Labels
		if (!childrenToReprint.isEmpty()) {
			for (Member m : childrenToReprint) {
				String childName = "";
				if (m_facility.getLabelPrintingOpts() != null) {
					try {
						if (m_facility.getLabelPrintingOpts().getBoolean("usesFirstNameLastInitial"))
							 childName = m.getFirstName() + " " + m.getLastName().substring(0,1) + ".";
						else
							 childName = m.getFirstName() + " " + m.getLastName();
					} catch (JSONException e) {
						 childName = m.getFirstName() + " " + m.getLastName();
					}
				} else
					 childName = m.getFirstName() + " " + m.getLastName();
				
				String diaper = " "; String feeding = " "; String potty = " "; String enrichment = " "; String bag = " ";
				if (m.getAllowsDiaperChange() != null)
					if (m.getAllowsDiaperChange())
						diaper = "D";
				if (m.getAllowsFeeding() != null)
					if (m.getAllowsFeeding())
						feeding = "F";
				if (m.getIsPottyTrained())
					potty = "P";
				if (m.getAllowsEnrichment() != null)
					if (m.getAllowsEnrichment())
						enrichment = "B";
				if (m.getHasBag() != null)
					if (m.getHasBag())
						bag = "BAG";
				String programName = " ";
				if (m.getInProgram() != null)
					programName = m.getInProgram().getName();
				else
					programName = " ";
				mPrinter.addChildPrint(childName, m.getBirthDate(), guardianBarcode, guardianName, guardianAreas,
						guardianPhone, m.getMedicalConcerns(), m.getLastCheckIn(), returnTime, mDetailsPopup.getCubbys(),
						diaper, feeding, potty, enrichment, bag, programName);
			}
		}

		// Print Guardian Label
		if (guardianToReprint != null) {
			for (Member children : mCheckedInChildren) {
				String childName = "";
				if (m_facility.getLabelPrintingOpts() != null) {
					try {
						if (m_facility.getLabelPrintingOpts().getBoolean("usesFirstNameLastInitial"))
							 childName = children.getFirstName() + " " + children.getLastName().substring(0,1) + ".";
						else
							 childName = children.getFirstName() + " " + children.getLastName();
					} catch (JSONException e) {
						 childName = children.getFirstName() + " " + children.getLastName();
					}
				} else
					 childName = children.getFirstName() + " " + children.getLastName();
				childNames.add(childName);
				childAges.add(getAge(children));
			}

			mPrinter.addGuardianPrint(childNames, childAges, guardianName, guardianToReprint.getUpdatedAt(),
					guardianBarcode, returnTime, mDetailsPopup.getCubbys());
		}

		// call function to print
		mPrinter.print();
	}

	public String getAge(Member child) {

		LocalDate bDate = LocalDate.fromDateFields(child.getBirthDate());
		Period age = new Period(bDate, LocalDate.now());

		int difference = age.getYears();
		String unit = YEAR_UNIT;

		if (difference < 1) {
			difference = age.getMonths();
			unit = MONTH_UNIT;
		}

		return String.format(Locale.getDefault(), "%d%s", difference, unit);
	}
}
