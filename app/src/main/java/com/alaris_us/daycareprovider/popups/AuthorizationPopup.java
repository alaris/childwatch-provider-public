package com.alaris_us.daycareprovider.popups;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.text.WordUtils;

import com.alaris_us.daycaredata.PhoneNumber;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.ui.UiBinder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.View.OnClickListener;

public class AuthorizationPopup extends Popup {

	public interface AuthorizationPopupListener {

		public void onAuthFamilyButtonClicked(Member authMember);
	}
	
	private final Member mAuthMember;
	private final AuthorizationPopupListener mListener;
	private final SimpleDateFormat DATEFORMAT = new SimpleDateFormat("MM-dd-yyyy", Locale.US);

	public AuthorizationPopup(Context context, Member authMember, AuthorizationPopupListener listener) {
		super(context, R.layout.popup_authorization, PopupMode.CENTER);

		View v = super.getView();
		mAuthMember = authMember;
		mListener = listener;

		UiBinder.bind(v, R.id.popup_authorization_imageView_photo, "ImageBitmap", this, "Image");
		UiBinder.bind(v, R.id.popup_authorization_textView_name, "Text", this, "Name");
		UiBinder.bind(v, R.id.popup_authorization_memberid, "Text", this, "MemberID");
		UiBinder.bind(v, R.id.popup_authorization_membertype, "Text", this, "MembershipType");
		UiBinder.bind(v, R.id.popup_authorization_status, "Text", this, "Status");
		UiBinder.bind(v, R.id.popup_authorization_barcode, "Text", this, "Barcode");
		UiBinder.bind(v, R.id.popup_authorization_birthdate, "Text", this, "BirthDate");
		UiBinder.bind(v, R.id.popup_authorization_home, "Text", this, "HomeNumber");
		UiBinder.bind(v, R.id.popup_authorization_work, "Text", this, "WorkNumber");
		UiBinder.bind(v, R.id.popup_authorization_cell, "Text", this, "CellNumber");
		UiBinder.bind(v, R.id.popup_authorization_family_button, "OnClickListener", this, "OnClickListener");
	}

	public void show(View anchor) {
		super.show(anchor);
	}
	
	public Bitmap getImage() {

		Bitmap image = mAuthMember.getImage();
		return (image == null) ? BitmapFactory.decodeResource(mView.getResources(), R.drawable.photo_not_available)
				: image;
	}
	
	public String getName() {
		return getFormattedMemberName(mAuthMember);
	}
	
	private String getFormattedMemberName(Member member) {
		return WordUtils.capitalizeFully(member.getFirstName() + " " + member.getLastName());
	}
	
	public String getMemberID() {
		return mAuthMember.getMemberID();
	}

	public String getMembershipType() {
		return mAuthMember.getMembershipType();
	}
	
	public String getStatus() {
		String status = "";
		if (!mAuthMember.isGuest())
			if (mAuthMember.isActive())
				status = "Active";
			else
				status = "Inactive";
		else {
			status = mAuthMember.getGuestDaysLeft().toString() + " days left";
		}
		return status;
	}
	
	public String getBarcode() {
		return mAuthMember.getBarcode();
	}
	
	public String getBirthDate() {
		return DATEFORMAT.format(mAuthMember.getBirthDate());
	}
	
	public String getHomeNumber() {
		List<PhoneNumber> phones = new ArrayList<>(mAuthMember.listPhoneNumbers());
		String homeNumber = "";
		for (PhoneNumber p : phones){
			if (p.getType().equals("HOME")){
				homeNumber = p.getNumber();
				break;
			}
		}
		return homeNumber;
	}
	
	public String getWorkNumber() {
		List<PhoneNumber> phones = new ArrayList<>(mAuthMember.listPhoneNumbers());
		String workNumber = "";
		for (PhoneNumber p : phones){
			if (p.getType().equals("WORK")){
				workNumber = p.getNumber();
				break;
			}
		}
		return workNumber;
	}
	
	public String getCellNumber() {
		List<PhoneNumber> phones = new ArrayList<>(mAuthMember.listPhoneNumbers());
		String cellNumber = "";
		for (PhoneNumber p : phones){
			if (p.getType().equals("CELL") || p.getType().equals("SMS")){
				cellNumber = p.getNumber();
				break;
			}
		}
		return cellNumber;
	}
	
	public OnClickListener getOnClickListener(){
		return new FamilySearchOnClickListener();
	}
	
	public class FamilySearchOnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			AuthorizationPopup.this.dismiss();
			mListener.onAuthFamilyButtonClicked(mAuthMember);
		}
		
	}
}
