package com.alaris_us.daycareprovider.popups;

import com.alaris_us.daycareprovider_dev.R;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.PopupWindow;

public abstract class Popup {

	final private PopupWindow mWindow;
	final private WindowManager mWindowManager;

	private PopupMode mMode;

	protected View mView;

	protected Popup(Context context, int viewResource, PopupMode mode) {

		mMode = mode;

		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mView = layoutInflater.inflate(viewResource, null);
		mView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics displayMetrics = new DisplayMetrics();
		mWindowManager.getDefaultDisplay().getMetrics(displayMetrics);
		int height = displayMetrics.heightPixels;
		int width = displayMetrics.widthPixels;
		
		mView.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

		mWindow = new PopupWindow(mView, (int) (width/2.5), LayoutParams.WRAP_CONTENT , true);
		// Funny android bug with dismiss listener. Required to set background.
		mWindow.setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(android.R.color.transparent)));

	}

	protected View getView() {
		return mView;
	}

	public boolean isShowing() {
		return mWindow.isShowing();
	}

	public void show(View anchor) {

		if (mWindow.isShowing() || (mWindow.getContentView() == null))
			return;

		int[] anchorLoc = { 0, 0 };

		// Measure Rect Regions
		anchor.getLocationOnScreen(anchorLoc);
		Rect anchorRect = new Rect(0, 0, anchor.getMeasuredWidth(), anchor.getMeasuredHeight());
		anchorRect.offset(anchorLoc[0], anchorLoc[1]);

		Rect screenRect = new Rect();
		mWindowManager.getDefaultDisplay().getRectSize(screenRect);

		Rect rootRect = new Rect(0, 0, mView.getMeasuredWidth(), mView.getMeasuredHeight());

		// Perform drawing
		mMode.draw(mWindow, anchor, rootRect, anchorRect, screenRect);

	}

	public void dismiss() {
		mWindow.dismiss();
	}

	public void setOnDismissListener(PopupWindow.OnDismissListener listener) {
		mWindow.setOnDismissListener(listener);
	}

	public void setFocusable(boolean focusable) {
		mWindow.setFocusable(focusable);
		mWindow.update();
	}
	
	public enum PopupMode {

		VERTICAL {

			@Override
			public void draw(PopupWindow window, View anchor, Rect rootRect, Rect anchorRect, Rect screenRect) {

				if (anchorRect.top < screenRect.centerY()) {
					// Located at top half of screen
					rootRect.offset(0, anchorRect.centerY());
					window.setAnimationStyle(R.style.PopupFromTop);

				} else {
					// Located at bottom half of screen
					rootRect.offset(0, anchorRect.centerY() - rootRect.height());
					window.setAnimationStyle(R.style.PopupFromTop);
				}

				// Right Screen
				if (anchorRect.left + rootRect.width() > screenRect.width()) {
					rootRect.offset(screenRect.right - rootRect.width(), 0);
				}
				// Left Screen
				else if (anchorRect.left - rootRect.centerX() < 0) {
					rootRect.offset(anchorRect.left, 0);
				}

				// Middle
				else {
					rootRect.offset(anchorRect.centerX() - rootRect.centerX(), 0);
				}

				window.showAtLocation(anchor, Gravity.NO_GRAVITY, rootRect.left, rootRect.top);

			}

		},
		HORIZONTAL {

			@Override
			public void draw(PopupWindow window, View anchor, Rect rootRect, Rect anchorRect, Rect screenRect) {

				if (anchorRect.left < screenRect.centerX()) {
					// Located at left half of screen
					rootRect.offset(anchorRect.centerX(), 0);
					window.setAnimationStyle(R.style.PopupFromRight);

				} else {
					// Located at right half of screen
					rootRect.offset(Math.abs(anchorRect.centerX() - rootRect.width()), 0);
					window.setAnimationStyle(R.style.PopupFromRight);
				}

				// Bottom
				if (anchorRect.top + rootRect.height() > screenRect.height()) {
					rootRect.offset(0, screenRect.bottom - rootRect.height());
				}

				// Top
				else if (anchorRect.top - rootRect.centerY() < 0) {
					rootRect.offset(0, anchorRect.top);
				}

				// Middle
				else {
					rootRect.offset(0, anchorRect.centerY() - rootRect.centerY());
				}

				window.showAtLocation(anchor, Gravity.NO_GRAVITY, rootRect.left, rootRect.top);

			}

		},
		TOP {

			@Override
			public void draw(PopupWindow window, View anchor, Rect rootRect, Rect anchorRect, Rect screenRect) {
				int xCoord = (int) (screenRect.centerX() - rootRect.centerX());
				int yCoord = (int) (screenRect.top + rootRect.height() / 4);
				window.showAtLocation(anchor, Gravity.NO_GRAVITY, xCoord, yCoord);
			}
		},
		CENTER {
			
			@Override
			public void draw(PopupWindow window, View anchor, Rect rootRect, Rect anchorRect, Rect screenRect) {
				window.setAnimationStyle(R.style.PopupFromRight);
				window.showAtLocation(anchor, Gravity.CENTER, 0, 0);
			}
		};

		public abstract void draw(PopupWindow window, View anchor, Rect rootRect, Rect anchorRect, Rect screenRect);

	}

}
