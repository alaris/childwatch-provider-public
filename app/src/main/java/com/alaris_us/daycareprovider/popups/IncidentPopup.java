package com.alaris_us.daycareprovider.popups;

import org.apache.commons.lang3.text.WordUtils;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.popups.ChildDetailsPopup.ChildDetailsPopupCallback;
import com.bindroid.BindingMode;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.ui.UiBinder;

public class IncidentPopup extends Popup {

	private final Member mChild;
	private String mHeaderText;
	private ChildDetailsPopup mDetailsPopup;
	private View mView;
	private EditText mEditText;
	private ChildDetailsPopupCallback mChildDetailsPopupCallback;

	public interface IncidentPopupCallback {

		void addIncident(Member child, String message);

	}

	public Member getChild() {

		return mChild;
	}

	public String getChildName() {

		return getFormattedMemberName(getChild());
	}

	private String getFormattedMemberName(Member member) {

		return WordUtils.capitalizeFully(member.getFirstName() + " " + member.getLastName());
	}

	public OnClickListener getSaveOnClickListener() {

		return new SaveOnClickListener();
	}

	public OnClickListener getCancelOnClickListener() {

		return new CancelOnClickListener();
	}

	class SaveOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			IncidentPopup.this.dismiss();
			mChildDetailsPopupCallback.addIncident(mChild, getIncidentMessage());
		}
	}

	class CancelOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			IncidentPopup.this.dismiss();
			mDetailsPopup.show(mView);
		}
	}

	public String getIncidentMessage() {
		return mEditText.getText().toString();
	}

	public IncidentPopup(Context context, View view, Member childMember, ChildDetailsPopup detailsPopup,
			ChildDetailsPopupCallback cb) {

		super(context, R.layout.popup_incident, PopupMode.CENTER);
		View v = super.getView();
		mChild = childMember;
		mHeaderText = context.getResources().getString(R.string.incident_popup_tile_header);
		mDetailsPopup = detailsPopup;
		mView = view;
		mChildDetailsPopupCallback = cb;
		mEditText = (EditText) v.findViewById(R.id.EditText_Incident);
		// Child Information
		UiBinder.bind(v, R.id.TextView_Title, "Text", this, "ChildName", BindingMode.ONE_WAY,
				new ToStringConverter(mHeaderText));
		// Button Actions
		UiBinder.bind(v, R.id.Button_Save, "OnClickListener", this, "SaveOnClickListener");
		UiBinder.bind(v, R.id.Button_Cancel, "OnClickListener", this, "CancelOnClickListener");
	}
}
