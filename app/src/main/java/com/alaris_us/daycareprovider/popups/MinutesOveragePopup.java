package com.alaris_us.daycareprovider.popups;

import java.util.HashMap;
import java.util.Map.Entry;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.UiBinder;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

public class MinutesOveragePopup extends Popup {
	private OnConfirmMintuesOverageListener mListener;
	private HashMap<Member, Number> mChildrenMap;
	private String mBreakdownText;

	public MinutesOveragePopup(HashMap<Member, Number> children, Context context, OnConfirmMintuesOverageListener listener) {
		super(context, R.layout.popup_minutesoverage, PopupMode.CENTER);
		mListener = listener;
		mChildrenMap = children;
		View v = super.getView();
		mBreakdownText = v.getResources().getString(R.string.mintuesoverage_popup_body_text);
		
		//Bindings
		UiBinder.bind(v, R.id.TextView_Body, "Text", this, "OverageMinutesBreakdownText", BindingMode.ONE_WAY);
		UiBinder.bind(v, R.id.Button_OK, "OnClickListener", this, "OkOnClickListener");
	}
	
	public OnClickListener getOkOnClickListener() {
		return new OkOnClickListener();
	}
	
	class OkOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mListener.onCheckOut();
			MinutesOveragePopup.this.dismiss();
		}
	}
	
	public String getOverageMinutesBreakdownText(){
		String toReturn = "";
		
		for (Entry<Member, Number> entry : mChildrenMap.entrySet()){
			Member child = entry.getKey();
			String overageMinutes = entry.getValue().toString();
			String name = child.getFirstName() + " " + child.getLastName() + ": ";
			String breakdownText = mBreakdownText.replace("[MIN]", overageMinutes);
			toReturn = toReturn + name + breakdownText + "\n";
		}
		return toReturn;
	}

	public interface OnConfirmMintuesOverageListener {
		public void onCheckOut();

		public void onCancel();
	}

}
