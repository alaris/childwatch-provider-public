package com.alaris_us.daycareprovider.popups;

import org.joda.time.DateTime;

import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import com.alaris_us.daycareprovider_dev.R;

public class DateTimePopup extends Popup {

	public interface DateTimePickerDialogListener {

		public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth);

		public void onTimeChanged(TimePicker view, int hourOfDay, int minute);

	}

	private DatePicker mDP;
	private TimePicker mTP;
	private DateTimePickerDialogListener m_listener;

	public DateTimePopup(Context context) {
		super(context, R.layout.popup_datetime, PopupMode.CENTER);

		mDP = (DatePicker) super.getView().findViewById(R.id.datePicker);
		mTP = (TimePicker) super.getView().findViewById(R.id.timePicker);

	}

	public void setDateTimePickerDialogListener(DateTimePickerDialogListener listener) {
		m_listener = listener;
	}

	public void show(View anchor, DateTime initial) {
		super.show(anchor);

		if (initial == null)
			initial = new DateTime();

		mDP.init(initial.getYear(), initial.getMonthOfYear() - 1, initial.getDayOfMonth(), new OnDateChangedListener() {

			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				m_listener.onDateChanged(view, year, monthOfYear + 1, dayOfMonth);

			}
		});

		mTP.setCurrentHour(initial.getHourOfDay());
		mTP.setCurrentMinute(initial.getMinuteOfHour());

		mTP.setOnTimeChangedListener(new OnTimeChangedListener() {

			@Override
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				m_listener.onTimeChanged(view, hourOfDay, minute);

			}
		});

	}

}
