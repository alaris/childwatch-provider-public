package com.alaris_us.daycareprovider.popups;

import org.apache.commons.lang3.text.WordUtils;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.popups.ChildDetailsPopup.ChildDetailsPopupCallback;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.ui.UiBinder;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;

import java.util.List;

public class SendSMSPopup extends Popup {

	private final Member mChild;
	private final Member mParent;
	private String mHeaderText;
	private ChildDetailsPopup mDetailsPopup;
	private View mView;
	private EditText mEditText;
	private ChildDetailsPopupCallback mChildDetailsPopupCallback;
	private String mMessage;
	private ScrollView mScroller;
	private List<String> mDefaultMessages;

	public interface IncidentPopupCallback {

		void addIncident(Member child, String message);

	}

	public Member getChild() {

		return mChild;
	}

	public Member getParent() {

		return mParent;
	}

	public String getChildName() {

		return getFormattedMemberName(getChild());
	}

	public String getParentName() {

		return getFormattedMemberName(getParent());
	}

	private String getFormattedMemberName(Member member) {

		return WordUtils.capitalizeFully(member.getFirstName() + " " + member.getLastName());
	}

	public OnClickListener getSendOnClickListener() {

		return new SendOnClickListener();
	}

	public OnClickListener getCancelOnClickListener() {

		return new CancelOnClickListener();
	}

	class SendOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			SendSMSPopup.this.dismiss();
			mChildDetailsPopupCallback.sendSMS(mParent, getSMSMessage());
		}
	}

	class CancelOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			SendSMSPopup.this.dismiss();
			mDetailsPopup.show(mView);
		}
	}

	public android.widget.RadioGroup.OnCheckedChangeListener getRadioGroupCheckedChangeListener() {

		return new RadioGroupCheckedChangeListener();
	}

	class RadioGroupCheckedChangeListener implements android.widget.RadioGroup.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			RadioButton rb = (RadioButton) group.findViewById(checkedId);
			if (rb.getText().equals("Other")) {
				mEditText.setVisibility(View.VISIBLE);
				mScroller.post(new Runnable() {
				    @Override
				    public void run() {
				    	mScroller.fullScroll(ScrollView.FOCUS_DOWN);
				    }
				});
			} else {
				mMessage = rb.getText().toString();
				if (mEditText.getVisibility() == View.VISIBLE){
					mEditText.setVisibility(View.GONE);
				}
			}
		}
	}

	public String getSMSMessage() {
		if (!mEditText.getText().toString().isEmpty())
			return mEditText.getText().toString();
		else
			return mMessage;
	}

	public SendSMSPopup(Context context, View view, Member childMember, Member checkedInBy, List<String> messages,
			ChildDetailsPopup detailsPopup, ChildDetailsPopupCallback cb) {

		super(context, R.layout.popup_sendsms, PopupMode.CENTER);
		View v = super.getView();
		mChild = childMember;
		mParent = checkedInBy;
		mHeaderText = context.getResources().getString(R.string.sendSMS_popup_tile_header);
		mDetailsPopup = detailsPopup;
		mView = view;
		mChildDetailsPopupCallback = cb;
		mEditText = (EditText) v.findViewById(R.id.EditText_SendSMS);
		mMessage = "";
		mDefaultMessages = messages;

		mScroller = (ScrollView) v.findViewById(R.id.Radio_Group_Scroller);

		final RadioGroup radioGrp = (RadioGroup) v.findViewById(R.id.Radio_Group);

		String smsKey = mView.getResources().getString(R.string.ASSOCIATION_CODE);
		String[] smsArray = new String[0];

		// get string array from source
		switch (smsKey) {
		case "LOU":
			smsArray = mView.getResources().getStringArray(R.array.lou_sms_array);
			break;
		case "HOU":
			smsArray = mView.getResources().getStringArray(R.array.hou_sms_array);
			break;
		case "SHR":
			smsArray = mView.getResources().getStringArray(R.array.shr_sms_array);
			break;
		case "PK":
			smsArray = mView.getResources().getStringArray(R.array.pk_sms_array);
			break;
		case "PP":
			smsArray = mView.getResources().getStringArray(R.array.pp_sms_array);
			break;
		case "BR":
			smsArray = mView.getResources().getStringArray(R.array.br_sms_array);
			break;
		case "SF":
			smsArray = mView.getResources().getStringArray(R.array.sf_sms_array);
			break;
		case "MID":
			smsArray = mView.getResources().getStringArray(R.array.mid_sms_array);
			break;
		case "NWF":
			smsArray = mView.getResources().getStringArray(R.array.nwf_sms_array);
			break;
		case "TC":
			smsArray = mView.getResources().getStringArray(R.array.tc_sms_array);
			break;
		case "DAY":
			smsArray = mView.getResources().getStringArray(R.array.day_sms_array);
			break;
		case "SAT":
			smsArray = mView.getResources().getStringArray(R.array.sat_sms_array);
			break;
		case "CFL":
			smsArray = mView.getResources().getStringArray(R.array.cfl_sms_array);
			break;
		case "CW":
			smsArray = mView.getResources().getStringArray(R.array.cw_sms_array);
			break;
		case "SC":
			smsArray = mView.getResources().getStringArray(R.array.sc_sms_array);
			break;
		case "FFC":
			smsArray = mView.getResources().getStringArray(R.array.ffc_sms_array);
			break;
		case "VOS":
			smsArray = mView.getResources().getStringArray(R.array.vos_sms_array);
			break;
		case "IND":
			smsArray = mView.getResources().getStringArray(R.array.ind_sms_array);
			break;
		case "SAY":
			smsArray = mView.getResources().getStringArray(R.array.say_sms_array);
			break;
		case "ROC":
			smsArray = mView.getResources().getStringArray(R.array.roc_sms_array);
			break;
		case "DLH":
			smsArray = mView.getResources().getStringArray(R.array.dlh_sms_array);
			break;
		case "TRI":
			smsArray = mView.getResources().getStringArray(R.array.tri_sms_array);
			break;
		case "FC":
			smsArray = mView.getResources().getStringArray(R.array.fc_sms_array);
			break;
		case "MSF":
			smsArray = mView.getResources().getStringArray(R.array.msf_sms_array);
			break;
		case "EPP":
			smsArray = mView.getResources().getStringArray(R.array.epp_sms_array);
			break;
		case "OLY":
			smsArray = mView.getResources().getStringArray(R.array.oly_sms_array);
			break;
		case "TR":
			smsArray = mView.getResources().getStringArray(R.array.tr_sms_array);
			break;
		case "CKY":
			smsArray = mView.getResources().getStringArray(R.array.cky_sms_array);
			break;
		case "ATL":
			smsArray = mView.getResources().getStringArray(R.array.atl_sms_array);
			break;
		case "CFF":
			smsArray = mView.getResources().getStringArray(R.array.cff_sms_array);
			break;
		case "SD":
			smsArray = mView.getResources().getStringArray(R.array.sd_sms_array);
			break;
		case "WCF":
			smsArray = mView.getResources().getStringArray(R.array.wcf_sms_array);
			break;
		case "WIC":
			smsArray = mView.getResources().getStringArray(R.array.wic_sms_array);
			break;
		case "HNL":
			smsArray = mView.getResources().getStringArray(R.array.hnl_sms_array);
			break;
		case "TCY":
			smsArray = mView.getResources().getStringArray(R.array.tcy_sms_array);
			break;
        case "SCC":
            smsArray = mView.getResources().getStringArray(R.array.scc_sms_array);
            break;
        case "MEM":
            smsArray = mView.getResources().getStringArray(R.array.mem_sms_array);
            break;
        case "MTN":
            smsArray = mView.getResources().getStringArray(R.array.mtn_sms_array);
            break;
        case "CVA":
           	smsArray = mView.getResources().getStringArray(R.array.cva_sms_array);
        	break;
		case "CNY":
			smsArray = mView.getResources().getStringArray(R.array.cny_sms_array);
			break;
		case "EOS":
			smsArray = mView.getResources().getStringArray(R.array.eos_sms_array);
			break;
		case "SEV":
			smsArray = mView.getResources().getStringArray(R.array.sev_sms_array);
			break;
		case "NAC":
			smsArray = mView.getResources().getStringArray(R.array.nac_sms_array);
			break;
		case "CHA":
			smsArray = mView.getResources().getStringArray(R.array.cha_sms_array);
			break;
		case "TPA":
			smsArray = mView.getResources().getStringArray(R.array.tpa_sms_array);
			break;
		case "LIV":
			smsArray = mView.getResources().getStringArray(R.array.liv_sms_array);
			break;
		}

		// create radio buttons
		if (mDefaultMessages != null) {
			for (int i = 0; i < mDefaultMessages.size(); i++) {
				RadioButton radioButton = new RadioButton(v.getContext());
				radioButton.setText(mDefaultMessages.get(i));
				radioButton.setId(i);
				radioGrp.addView(radioButton);
			}
		} else {
			for (int i = 0; i < smsArray.length; i++) {
				RadioButton radioButton = new RadioButton(v.getContext());
				radioButton.setText(smsArray[i]);
				radioButton.setId(i);
				radioGrp.addView(radioButton);
			}
		}

		// Child Information
		UiBinder.bind(v, R.id.TextView_Title, "Text", this, "ParentName", BindingMode.ONE_WAY,
				new ToStringConverter(mHeaderText));
		// Button Actions
		UiBinder.bind(v, R.id.Button_Send, "OnClickListener", this, "SendOnClickListener");
		UiBinder.bind(v, R.id.Button_Cancel, "OnClickListener", this, "CancelOnClickListener");
		// Radio Group
		UiBinder.bind(v, R.id.Radio_Group, "OnCheckedChangeListener", this, "RadioGroupCheckedChangeListener");
	}
}
