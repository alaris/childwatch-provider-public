package com.alaris_us.daycareprovider.popups;

import com.alaris_us.daycareprovider_dev.R;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

public class ConfirmCheckOutPopup extends Popup {
	private OnConfirmCheckOutListener mListener;

	public ConfirmCheckOutPopup(Context context, OnConfirmCheckOutListener listener) {
		super(context, R.layout.popup_fastcheckout, PopupMode.CENTER);
		mListener = listener;
		View v = super.getView();
		v.findViewById(R.id.Button_CheckOut).setOnClickListener(new CheckOutOnClickListener());
		v.findViewById(R.id.Button_Cancel).setOnClickListener(new CancelOnClickListener());
	}

	class CheckOutOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mListener.onConfirmCheckOut();
			ConfirmCheckOutPopup.this.dismiss();
		}
	}

	class CancelOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mListener.onCancel();
			ConfirmCheckOutPopup.this.dismiss();
		}
	}

	public interface OnConfirmCheckOutListener {
		public void onConfirmCheckOut();

		public void onCancel();
	}

}
