package com.alaris_us.daycareprovider.popups;

import java.util.List;

import org.apache.commons.lang3.text.WordUtils;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.ValidMembership;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider.utils.DaycareHelpers.YesNoDialogCallback;
import com.alaris_us.daycareprovider.view.GuestConversionPopupList;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.ui.UiBinder;

public class GuestConversionPopup extends Popup {

	public interface SelectGuestTypePopupListener {

		public void onGuestTypeSelectedListener(ValidMembership selectedMembershipType, Member member);
	}

	private ListView mLV;
	private final TrackableCollection<ValidMembership> mGuestTypeList;
	private SelectGuestTypePopupListener mListener;
	private final Member mMember;

	public GuestConversionPopup(Context context, List<ValidMembership> guestTypeList, Member member) {
		super(context, R.layout.popup_guestconversion, PopupMode.CENTER);

		View v = super.getView();
		mLV = (ListView) v.findViewById(R.id.popup_guesttype_listview);

		mMember = member;

		mGuestTypeList = new TrackableCollection<ValidMembership>(guestTypeList);

		UiBinder.bind(v, R.id.popup_guesttype_listview, "Adapter", this, "GuestTypeList",
				new AdapterConverter(GuestConversionPopupList.class));

	}

	public void setGuestTypeSelectedListener(SelectGuestTypePopupListener listener) {
		mListener = listener;
	}

	public TrackableCollection<ValidMembership> getGuestTypeList() {

		return mGuestTypeList;

	}

	public String getFormattedName(Member m) {

		return getFormattedMemberName(m);
	}

	private String getFormattedMemberName(Member member) {

		return WordUtils.capitalizeFully(member.getFirstName() + " " + member.getLastName());
	}

	public void show(View anchor) {

		super.show(anchor);

		mLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
				String message = "Are you sure you want to convert " + getFormattedName(mMember) + " to "
						+ getGuestTypeList().get(arg2).getDescriptor() + "?";
				DaycareHelpers.createYesNoDialog(mView.getContext(), message, new YesNoDialogCallback() {

					@Override
					public void yesClicked() {

						mListener.onGuestTypeSelectedListener(getGuestTypeList().get(arg2), mMember);
						GuestConversionPopup.this.dismiss();
					}

					@Override
					public void noClicked() {
					}
				});
			}
		});

	}
}
