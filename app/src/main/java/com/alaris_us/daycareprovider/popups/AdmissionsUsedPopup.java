package com.alaris_us.daycareprovider.popups;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.UiBinder;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

public class AdmissionsUsedPopup extends Popup {
	private OnConfirmAdmissionUsageListener mListener;
	private HashMap<Admission, Number> mAdmissionsMap;
	private List<HashMap<String, Object>> mAdmissionsArray;
	private String mBreakdownText;
	private List<Member> mChildren;
	private Facility mFacility;
	private Member mMemberToCharge;
	private PersistenceLayer mPersistenceLayer;

	public AdmissionsUsedPopup(HashMap<Admission, Number> admissions, Context context, OnConfirmAdmissionUsageListener listener) {
		super(context, R.layout.popup_admissionusage, PopupMode.CENTER);
		mListener = listener;
		mAdmissionsMap = admissions;
		View v = super.getView();
		mBreakdownText = v.getResources().getString(R.string.admissionusage_popup_body_text);
		
		//Bindings
		UiBinder.bind(v, R.id.TextView_Body, "Text", this, "AdmissionsUsedBreakdownText", BindingMode.ONE_WAY);
		UiBinder.bind(v, R.id.Button_OK, "OnClickListener", this, "OkOnClickListener");
	}

	public AdmissionsUsedPopup(List<HashMap<String, Object>> admissions, List<Member> children, Facility facility, Member memberToCharge,
							   Context context, PersistenceLayer persistenceLayer, OnConfirmAdmissionUsageListener listener) {
		super(context, R.layout.popup_admissionusage, PopupMode.CENTER);
		mListener = listener;
		mAdmissionsArray = admissions;
		mChildren = children;
		mFacility = facility;
		mMemberToCharge = memberToCharge;
		mPersistenceLayer = persistenceLayer;
		View v = super.getView();
		mBreakdownText = v.getResources().getString(R.string.admissionusage_popup_body_text);

		//Bindings
		UiBinder.bind(v, R.id.TextView_Body, "Text", this, "AdmissionsUsedBreakdownText", BindingMode.ONE_WAY);
		UiBinder.bind(v, R.id.Button_OK, "OnClickListener", this, "OkOnClickListener");
	}

	public OnClickListener getOkOnClickListener() {
		return new OkOnClickListener();
	}
	
	class OkOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (mAdmissionsArray.isEmpty()) {
				AdmissionsUsedPopup.this.dismiss();
			} else {
				mListener.onCheckOut();
				AdmissionsUsedPopup.this.dismiss();
			}
		}
	}
	
	public String getAdmissionsUsedBreakdownText() {
		String toReturn = "";
		
		if (mAdmissionsArray.isEmpty())
			toReturn = "Please set Admissions before checking out!";
		else {
			/*for (Entry<Admission, Number> entry : mAdmissionsMap.entrySet()) {
				Admission admit = entry.getKey();
				String usedAdmits = entry.getValue().toString();
				String familyName = admit.getFamily().getName();
				String breakdownText = mBreakdownText.replace("[FAMILY]", familyName);
				breakdownText = breakdownText.replace("[USED]", usedAdmits);
				Number remaining = admit.getAdmissions().intValue() - entry.getValue().intValue();
				breakdownText = breakdownText.replace("[LEFT]", remaining.toString());
				toReturn = toReturn + breakdownText + "\n";
			}*/
			for (HashMap<String, Object> admission : mAdmissionsArray) {
				String familyName = admission.get("familyName").toString();
				String usedAdmits = admission.get("admissionsUsed").toString();
				String admissionsLeft = admission.get("admissionsLeft").toString();
				String breakdownText = mBreakdownText.replace("[FAMILY]", familyName);
				breakdownText = breakdownText.replace("[USED]", usedAdmits);
				breakdownText = breakdownText.replace("[LEFT]", admissionsLeft);
				toReturn = toReturn + breakdownText + "\n";
			}
		}
		return toReturn;
	}

	public interface OnConfirmAdmissionUsageListener {
		public void onCheckOut();

		public void onCancel();
	}

}
