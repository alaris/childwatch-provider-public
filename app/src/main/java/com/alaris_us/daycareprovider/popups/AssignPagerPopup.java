package com.alaris_us.daycareprovider.popups;

import java.util.List;

import org.apache.commons.lang3.text.WordUtils;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycareprovider.popups.ChildDetailsPopup.ChildDetailsPopupCallback;
import com.alaris_us.daycareprovider.view.ValuePicker;
import com.alaris_us.daycareprovider.view.ValuePicker.GetNameConverter;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class AssignPagerPopup extends Popup {

	private final Member mGuardian;
	private final Member mChild;
	private final List<Pager> mAvailablePagers;
	private String mHeaderText;
	private ChildDetailsPopup mDetailsPopup;
	private View mView;
	private Button mSaveButton;
	private ValuePicker<Pager> mNumberPicker;
	private ChildDetailsPopupCallback mChildDetailsPopupCallback;

	public interface IncidentPopupCallback {

		void addIncident(Member child, String message);

	}

	public Member getGuardian() {

		return mGuardian;
	}

	public List<Pager> getAvailablePagers() {
		return mAvailablePagers;
	}

	public String getGuardianName() {

		return getFormattedMemberName(getGuardian());
	}

	private String getFormattedMemberName(Member member) {

		return WordUtils.capitalizeFully(member.getFirstName() + " " + member.getLastName());
	}

	public OnClickListener getSaveOnClickListener() {

		return new SaveOnClickListener();
	}

	public OnClickListener getCancelOnClickListener() {

		return new CancelOnClickListener();
	}

	class SaveOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			AssignPagerPopup.this.dismiss();
			mChildDetailsPopupCallback.assignPager(getPager(), mChild, mGuardian.getPFamily(), mGuardian);
		}
	}

	class CancelOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			AssignPagerPopup.this.dismiss();
			mDetailsPopup.show(mView);
		}
	}

	public void setPickerValues(List<Pager> pagers) {
		mNumberPicker.setDisplayedValues(pagers);
		if (pagers.size() < 1)
			mSaveButton.setEnabled(false);
	}

	public Pager getPager() {
		return mAvailablePagers.get(mNumberPicker.getValue());
	}

	@SuppressWarnings("unchecked")
	public AssignPagerPopup(Context context, View view, Member childMember, Member guardianMember,
			List<Pager> availablePagers, ChildDetailsPopup detailsPopup, ChildDetailsPopupCallback cb) {
		super(context, R.layout.popup_assignpager, PopupMode.CENTER);
		View v = super.getView();
		mGuardian = guardianMember;
		mChild = childMember;
		mAvailablePagers = availablePagers;
		mHeaderText = context.getResources().getString(R.string.assignpager_popup_tile_header);
		mDetailsPopup = detailsPopup;
		mView = view;
		mSaveButton = (Button) v.findViewById(R.id.Button_Save);
		mChildDetailsPopupCallback = cb;
		mNumberPicker = (ValuePicker<Pager>) v.findViewById(R.id.NumberPicker_AssignPager);
		mNumberPicker.setNoValuesMessage("No Pagers Available");
		mNumberPicker.setGetNameConverter(new GetNameConverter<Pager>() {
			@Override
			public String toString(Pager pager) {
				return pager.getName();
			}
		});

		// Guardian Information
		UiBinder.bind(v, R.id.TextView_Title, "Text", this, "GuardianName", BindingMode.ONE_WAY,
				new ToStringConverter(mHeaderText));
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "PickerValues")), this, "AvailablePagers",
				BindingMode.ONE_WAY);

		// Button Actions
		UiBinder.bind(v, R.id.Button_Save, "OnClickListener", this, "SaveOnClickListener");
		UiBinder.bind(v, R.id.Button_Cancel, "OnClickListener", this, "CancelOnClickListener");
	}
}
