package com.alaris_us.daycareprovider.popups;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.daycareprovider.data.StaffMember;
import com.alaris_us.daycareprovider.models.DashboardViewModel;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.view.StaffPopupListItem;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.ui.UiBinder;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

public class StaffPopup extends Popup {
	
	public interface SelectStaffPopupListener {

		public void onStaffSelected(Staff selectedItem, List<Member> child);
	}

	private ListView mLV;
	private Button mSaveButton;
	private Button mCancelButton;
	private TrackableCollection<Staff> mStaffList;
	private TrackableCollection<StaffMember> mStaffListItem;
	private final List<Boolean> mOrigStaffList;
	private final List<StaffMember> mOrigStaffListItem;
	private final ChildDetailsPopup.ChildDetailsPopupCallback mChildDetailsPopupCallback;
	private PersistenceLayer mPersistenceLayer;
	private DashboardViewModel m_dashboardViewModel;
	private List<Room> mRoomList;

	public StaffPopup(Context context, PersistenceLayer persistenceLayer, DashboardViewModel model, List<Staff> staff, List<Room> roomList) {
		super(context, R.layout.popup_staffselect, PopupMode.CENTER);
		this.setFocusable(false);
		
		View v = super.getView();
		mLV = (ListView) v.findViewById(R.id.popup_staffselect_listview);
		mSaveButton = (Button) v.findViewById(R.id.popup_staffselect_save);
		mCancelButton = (Button) v.findViewById(R.id.popup_staffselect_cancel);
		mPersistenceLayer = persistenceLayer;
		m_dashboardViewModel = model;
		mStaffListItem = new TrackableCollection<StaffMember>();
		mOrigStaffListItem = new ArrayList<StaffMember>();
		mRoomList = roomList;
		for (Staff s : staff) {
			mStaffListItem.add(new StaffMember(s, s.getIsClockedIn(), roomList, mPersistenceLayer.getPersistenceData().getFacility()));
			mOrigStaffListItem.add(new StaffMember(s, s.getIsClockedIn(), roomList, mPersistenceLayer.getPersistenceData().getFacility()));
		}
		
		mChildDetailsPopupCallback = null;
		mStaffList = new TrackableCollection<Staff>(staff);
		mOrigStaffList = new ArrayList<Boolean>();
		for (Staff s : staff)
			mOrigStaffList.add(s.getIsClockedIn());

		UiBinder.bind(v, R.id.popup_staffselect_listview, "Adapter", this, "StaffMemberList",
				new AdapterConverter(StaffPopupListItem.class));

	}

	/*public StaffPopup(Context context, List<Staff> staff, ChildDetailsPopup.ChildDetailsPopupCallback listener) {
		super(context, R.layout.popup_staffselect, PopupMode.VERTICAL);

		View v = super.getView();
		mLV = (ListView) v.findViewById(R.id.popup_staffselect_listview);
		mStaffListItem = new TrackableCollection<StaffMember>();
		for (Staff s : staff)
			mStaffListItem.add(new StaffMember(s, s.getIsClockedIn()));
		
		mChildDetailsPopupCallback = listener;
		mStaffList = new TrackableCollection<Staff>(staff);

		UiBinder.bind(v, R.id.popup_staffselect_listview, "Adapter", this, "StaffList",
				new AdapterConverter(StaffPopupListItem.class));

	}*/

	public List<Staff> getStaffList() {
		return mStaffList;
	}
	
	public List<StaffMember> getStaffMemberList() {
		return mStaffListItem;
	}

	public void show(final View anchor) {

		super.show(anchor);

		mLV.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Staff staff = getStaffList().get(arg2);
				StaffMember staffItem = mStaffListItem.get(arg2);
				staffItem.toggleSelected();
				staff.setIsClockedIn(staffItem.getIsSelected());
				if (!staffItem.getIsSelected())
					staffItem.getStaff().setPRoom(null);
				else {
					Spinner spinner = (Spinner) arg1.findViewById(R.id.iconcheckbox_stackedtext_spinner);
					staffItem.getStaff().setPRoom(mRoomList.get(spinner.getSelectedItemPosition()));
				}
			}
		});

		mSaveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				List<Staff> clockedInStaff = new ArrayList<>();
				for (Staff s : mStaffList) {
					if (s.getIsClockedIn())
						clockedInStaff.add(s);
				}
				mPersistenceLayer.updateStaff(mStaffList);
				if (m_dashboardViewModel.getRoomName().equals("OVERVIEW"))
					m_dashboardViewModel.setStaffCount(clockedInStaff.size());
				else {
					int staff = 0;
					if (mPersistenceLayer.getPersistenceData().getFacility().getRatioType() != null) {
						if (mPersistenceLayer.getPersistenceData().getFacility().getRatioType().equals("FACILITYANDROOM") ||
								mPersistenceLayer.getPersistenceData().getFacility().getRatioType().equals("FACILITY")) {
							staff = clockedInStaff.size();
						}
					} else {
						for (Staff s : mPersistenceLayer.getPersistenceData().getClockedInStaff()) {
							if (s.getPRoom().getName().equals(m_dashboardViewModel.getRoomName()))
								staff++;
						}
					}
					m_dashboardViewModel.setStaffCount(staff);
				}
				
				m_dashboardViewModel.filterChildrenByRoom(m_dashboardViewModel.getRoomName());
				StaffPopup.this.dismiss();
			}
		});
		
		mCancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				for (int j = 0; j < mStaffList.size(); j++) {
					if (mStaffList.get(j).getIsClockedIn() != mOrigStaffList.get(j)) {
						mStaffList.get(j).setIsClockedIn(!mStaffList.get(j).getIsClockedIn());
					}
				}
				StaffPopup.this.dismiss();
			}
		});
		
	}
}
