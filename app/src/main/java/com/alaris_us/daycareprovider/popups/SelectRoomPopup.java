package com.alaris_us.daycareprovider.popups;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.view.SelectRoomPopupList;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.ui.UiBinder;

public class SelectRoomPopup extends Popup {

	public interface SelectRoomPopupListener {

		public void onRoomSelected(Room selectedItem, List<Member> child);
	}

	private ListView mLV;
	private final TrackableCollection<Room> mRoomList;
	private final SelectRoomPopupListener mListener;
	private final List<Member> mForChild;

	public SelectRoomPopup(Context context, List<Room> roomList, List<Member> forChild,
			SelectRoomPopupListener listener) {
		super(context, R.layout.popup_roomselect, PopupMode.CENTER);

		View v = super.getView();
		mLV = (ListView) v.findViewById(R.id.popup_roomselect_listview);

		mListener = listener;
		mForChild = forChild;

		mRoomList = new TrackableCollection<Room>(roomList);

		UiBinder.bind(v, R.id.popup_roomselect_listview, "Adapter", this, "RoomList",
				new AdapterConverter(SelectRoomPopupList.class));

	}

	public TrackableCollection<Room> getRoomList() {

		return mRoomList;

	}

	public void show(View anchor) {

		super.show(anchor);

		mLV.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				SelectRoomPopup.this.dismiss();
				mListener.onRoomSelected(getRoomList().get(arg2), mForChild);
			}
		});

	}
}
