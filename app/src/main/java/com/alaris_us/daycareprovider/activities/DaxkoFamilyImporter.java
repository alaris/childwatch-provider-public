package com.alaris_us.daycareprovider.activities;

import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.models.DaxkoFamilyImporterViewModel;
import com.alaris_us.daycareprovider.view.DaxkoMemberListItem;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.EditTextTextProperty;
import com.bindroid.ui.UiBinder;

public class DaxkoFamilyImporter extends Persistence<DaxkoFamilyImporterViewModel> {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_importfamily);
		EditText phoneNumber = (EditText) findViewById(R.id.EditText_PhoneNumber);
		phoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
		UiBinder.bind(this, R.id.Button_Search, "OnClickListener", getModel(), "SearchButtonOnClickListener");
		UiBinder.bind(this, R.id.ListView_ImportMembers, "OnItemClickListener", getModel(),
				"MemberListOnItemClickListener");
		UiBinder.bind(new EditTextTextProperty(phoneNumber), getModel(), "PhoneNumber", BindingMode.TWO_WAY);
		UiBinder.bind(this, R.id.ListView_ImportMembers, "Adapter", getModel(), "RegisterableMembers",
				new AdapterConverter(DaxkoMemberListItem.class));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_previous:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.import_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
}
