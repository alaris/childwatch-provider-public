package com.alaris_us.daycareprovider.activities;

import java.util.List;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.adapters.NoTabsAdapter;
import com.alaris_us.daycareprovider.fragments.ConfirmationGuestCreator;
import com.alaris_us.daycareprovider.fragments.EnterDetailsGuestCreator;
import com.alaris_us.daycareprovider.fragments.FamilyDisplayGuestCreator;
import com.alaris_us.daycareprovider.fragments.PinEntryGuestCreator;
import com.alaris_us.daycareprovider.models.GuestCreatorViewModel;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.view.CustomToast;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.ui.UiBinder;

import android.app.ActionBar;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

public class GuestCreator extends Persistence<GuestCreatorViewModel> {

	private ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// Android Boilerplate
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guestcreator);
		// Action Bar
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		View v = getLayoutInflater().inflate(R.layout.view_navigation, new RelativeLayout(this), false);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM, ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(v, new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
				ActionBar.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM | Gravity.END));
		// ViewPager
		mViewPager = (ViewPager) findViewById(R.id.pager);
		// Setup Fragments
		NoTabsAdapter noTabsAdapter = new NoTabsAdapter(this, mViewPager);
		noTabsAdapter.addFragment(new PinEntryGuestCreator());
		noTabsAdapter.addFragment(new FamilyDisplayGuestCreator());
		noTabsAdapter.addFragment(new EnterDetailsGuestCreator());
		noTabsAdapter.addFragment(new ConfirmationGuestCreator());
		// Bind listeners and data
		UiBinder.bind(this, R.id.pager, "CurrentItem", getModel(), "CurrentPage");
		UiBinder.bind(actionBar.getCustomView(), R.id.view_navigation_back_container, "OnClickListener", this,
				"BackOnClickListener");
		UiBinder.bind(actionBar.getCustomView(), R.id.view_navigation_next_container, "OnClickListener", this,
				"NextOnClickListener");
	}

	public OnClickListener getBackOnClickListener() {

		return new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (getModel().getCurrentPage() == 0) {
					GuestCreator.this.finish();
				} else {
					getModel().setCurrentPage(0);
					getModel().setPhoneNumber("");
				}
			}
		};
	}

	public OnClickListener getNextOnClickListener() {

		return new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (getModel().getPIN() == null) {
					CustomToast.makeToast(GuestCreator.this, "Invalid Phone Number Format", CustomToast.DURATION_LONG,
							CustomToast.STYLE_ERROR).show();
					getModel().setPhoneNumber("");
					return;
				}

				int currentItem = getModel().getCurrentPage();
				if (currentItem == 0) {
					getPersistenceLayer().fetchFamilyByPin(getModel().getPIN(),
							new PersistenceLayerCallback<List<Member>>() {

						@Override
						public void done(List<Member> data, Exception e) {

							getModel().getExisting().clear();
							if (data.size() <= 0) {
								getModel().setCurrentPage(getModel().getCurrentPage() + 2);
								return;
							} else {
								getModel().getExisting().addAll(data);
								getModel().setCurrentPage(getModel().getCurrentPage() + 1);
								return;
							}
						}
					});
				} else if (currentItem == 1) {
					getModel().setCurrentPage(getModel().getCurrentPage() + 1);
				} else if (currentItem == 2) {
					if (getModel().registerGuests() == false) {
						CustomToast.makeToast(v.getContext(), "Must have at least one parent",
								CustomToast.DURATION_LONG, CustomToast.STYLE_ERROR).show();
					}
				} else if (currentItem == 3) {
					GuestCreator.this.finish();
				}
			}
		};
	}
}
