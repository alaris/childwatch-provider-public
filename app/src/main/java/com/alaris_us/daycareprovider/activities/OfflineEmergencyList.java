package com.alaris_us.daycareprovider.activities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.alaris_us.daycareprovider.models.FastCheckoutViewModel;
import com.alaris_us.daycareprovider_dev.R;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

public class OfflineEmergencyList extends Persistence<FastCheckoutViewModel>  {
	
	private boolean oldConnection = true;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// Android Boilerplate
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_offlineemergencylist);
		TextView tv = (TextView) findViewById(R.id.offlineText);
		
		String ret = "";

	    try {
	        InputStream inputStream = getApplicationContext().openFileInput("offline_emergency_contacts.txt");

	        if ( inputStream != null ) {
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ((receiveString = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(receiveString);
	                stringBuilder.append(System.lineSeparator());
	            }

	            inputStream.close();
	            ret = stringBuilder.toString();
	            tv.setText(ret);
	        }
	    }
	    catch (FileNotFoundException e) {
	        Log.e("login activity", "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e("login activity", "Can not read file: " + e.toString());
	    }


	}

	@Override
	public void onAttachedToWindow() {
		// // Use full-screen mode to hide the status bar
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// // Show our window instead of regular lock screen
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		// // Hide keyboard on showing activity
		// this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		super.onAttachedToWindow();
	}

	@Override
	public void onConnectivityChanged(boolean isConnected) {
		//super.onConnectivityChanged(isConnected);
		if (isConnected && isConnected != oldConnection) {
			OfflineEmergencyList.this.finish();
			Intent intent = new Intent(this, DaycareProviderKiosk.class);
			startActivity(intent);
		}
		oldConnection = isConnected;
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}
}

