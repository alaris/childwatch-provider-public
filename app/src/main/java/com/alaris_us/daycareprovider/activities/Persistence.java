package com.alaris_us.daycareprovider.activities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;

import com.alaris_us.daycaredata.util.ConnectivityUtility.ConnectivityListener;
import com.alaris_us.daycareprovider.app.App;
import com.alaris_us.daycareprovider.fragments.ProviderFragment.PersistenceLayerHost;
import com.alaris_us.daycareprovider.models.ViewModel;
import com.alaris_us.daycareprovider.models.ViewModel.ViewModelHost;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;

public abstract class Persistence<T extends ViewModel> extends Activity implements PersistenceLayerHost,
		PersistenceLayer.PersistenceLayerListener, ViewModelHost<T>, ConnectivityListener {

	private ProgressDialog mProgressDialog;
	private PersistenceLayer mPersistenceLayer;
	private T mViewModel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		// Data configuration
		App ourapp = (App) getApplication();
		mPersistenceLayer = ourapp.getPersistenceLayer();
		// ViewModel
		try {
			ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
			@SuppressWarnings("unchecked")
			Class<T> type = (Class<T>) superClass.getActualTypeArguments()[0];
			mViewModel = type.getDeclaredConstructor(PersistenceLayer.class).newInstance(mPersistenceLayer);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {

		super.onResume();
		mPersistenceLayer.addPersistanceLayerListener(this);
		mPersistenceLayer.getConnectivityUtility().register();
		mPersistenceLayer.getConnectivityUtility().addConnectivityListener(this);
	}

	@Override
	protected void onPause() {

		super.onPause();
		mPersistenceLayer.removePersistanceLayerListener(this);
		mPersistenceLayer.getConnectivityUtility().unregister();
	}

	@Override
	public void updating() {

		if (mProgressDialog == null || !mProgressDialog.isShowing()) {
			mProgressDialog = ProgressDialog.show(Persistence.this, "Please Wait...", "Communicating With Server", true,
					false);
		}
	}

	@Override
	public void updated() {

		if (mProgressDialog != null)
			mProgressDialog.dismiss();
	}

	@Override
	public void exception(Exception e) {

		// TODO Handle Exceptions
	}

	@Override
	public PersistenceLayer getPersistenceLayer() {

		return mPersistenceLayer;
	}

	@Override
	public T getModel() {

		return mViewModel;
	}

	@Override
	public void onConnectivityChanged(boolean isConnected) {

	}
}
