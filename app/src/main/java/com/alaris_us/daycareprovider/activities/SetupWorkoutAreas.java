package com.alaris_us.daycareprovider.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.models.SetupWorkoutAreasViewModel;
import com.alaris_us.daycareprovider.view.WorkoutAreaCheckBox;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class SetupWorkoutAreas extends Persistence<SetupWorkoutAreasViewModel> {

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_previous:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.setupworkoutareas_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_selectworkoutlocations);
		UiBinder.bind(this, R.id.selectworkoutlocations_lvLocations, "Adapter", getModel(), "WorkoutAreas",
				BindingMode.ONE_WAY, new AdapterConverter(WorkoutAreaCheckBox.class));
		UiBinder.bind(this, R.id.selectworkoutlocations_lvLocations, "OnItemClickListener", getModel(),
				"ListviewListener");
	}

	@Override
	protected void onResume() {

		super.onResume();
		getPersistenceLayer().fetchSelectedWorkoutLocations();
		getPersistenceLayer().fetchAllWorkoutLocations();
	}
}
