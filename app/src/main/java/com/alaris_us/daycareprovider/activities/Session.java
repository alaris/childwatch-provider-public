package com.alaris_us.daycareprovider.activities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.comparators.ComparatorChain;

import com.alaris_us.daycaredata.EmergencyNumber;
import com.alaris_us.daycaredata.PhoneNumber;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycareprovider.adapters.TabsAdapter;
import com.alaris_us.daycareprovider.app.App;
import com.alaris_us.daycareprovider.comparators.MemberFirstNameComparator;
import com.alaris_us.daycareprovider.comparators.MemberLastNameComparator;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.alaris_us.daycareprovider.fragments.Dashboard;
import com.alaris_us.daycareprovider.fragments.Dashboard.ChildListMode;
import com.alaris_us.daycareprovider.models.SessionViewModel;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.popups.SelectRoomPopup;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider.view.ChildListTabHost;
import com.alaris_us.daycareprovider.view.TabWithIcons.SortDirection;
import com.alaris_us.daycareprovider_dev.R;

import android.Manifest;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import androidx.viewpager.widget.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;

public class Session extends Persistence<SessionViewModel> implements TabListener {

	private static final int FETCH_DAYS_COUNT = 7;
	private static final int OFFSCREEN_LIMIT = 5;
	private TabsAdapter m_tabsAdapter;
	private ViewPager m_viewPager;
	private Drawable mClientAppIcon;
	private Intent mClientLaunchIntent;
	private String mClientPackage;
	private Menu mMenu;
	private Dashboard mFrag;
	private Dashboard providerFrag;
	private ChildListTabHost mTabHost;
	private View rv;
	private SortDirection sortDirection;
	private View mBanner;
	private BroadcastReceiver daydreamReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_session);
		// Get application variables
		mClientPackage = ((App) getApplication()).getClientPackage();
		// Get client app info
		try {
			mClientAppIcon = this.getPackageManager().getApplicationIcon(mClientPackage);
			mClientLaunchIntent = getPackageManager().getLaunchIntentForPackage(mClientPackage);
			mClientLaunchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		} catch (PackageManager.NameNotFoundException ne) {
		}
		mFrag = (Dashboard) getFragmentManager().findFragmentById(R.id.mainFragment);
		mBanner = findViewById(R.id.banner);
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
		actionBar.addTab(actionBar.newTab().setText("OVERVIEW").setTabListener(this));
		for (Room room : getPersistenceLayer().getPersistenceData().getAllFacilityRooms()) {
			actionBar.addTab(actionBar.newTab().setText(room.getName()).setTabListener(this));
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
				requestPermissions(new String[] {Manifest.permission.CAMERA}, 1);
			}
		}

		// Call onResume() when exiting daydream mode
		daydreamReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				onResume();
			}
		};

		/*
		 * // ViewPager m_viewPager = (ViewPager) findViewById(R.id.pager);
		 * m_viewPager.setOffscreenPageLimit(OFFSCREEN_LIMIT); // Setup
		 * Fragments m_tabsAdapter = new TabsAdapter(this, m_viewPager);
		 * m_tabsAdapter.addFragment(new Dashboard());
		 * m_tabsAdapter.addFragment(new Reporting());
		 * m_tabsAdapter.addFragment(new Incidents()); if
		 * (!getPersistenceLayer().getPersistenceData().getAllFacilityRooms().
		 * isEmpty()) for (Room room :
		 * getPersistenceLayer().getPersistenceData().getAllFacilityRooms()){
		 * Bundle args = new Bundle(); args.putString("roomName",
		 * room.getName()); args.putString("objectId", room.getObjectId());
		 * Rooms r = new Rooms(); r.setArguments(args);
		 * m_tabsAdapter.addFragment(r); } // Add page change listener. Must be
		 * executed after pages added m_viewPager.setOnPageChangeListener(new
		 * ViewPager.SimpleOnPageChangeListener() {
		 * 
		 * @Override public void onPageSelected(int position) {
		 * 
		 * getActionBar().setSelectedNavigationItem(position); } });
		 */
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		mMenu = menu;
		getMenuInflater().inflate(R.menu.session_menu, menu);
		if (mClientLaunchIntent != null) {
			SubMenu submenu = menu.findItem(R.id.submenu_settings).getSubMenu();
			submenu.add(Menu.NONE, R.id.action_launch_client, Menu.NONE, "Launch Client").setIcon(mClientAppIcon)
					.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		Intent intent;
		switch (item.getItemId()) {
		case R.id.action_setuplocation:
			intent = new Intent(this, SetupLocation.class);
			startActivity(intent);
			return true;
		case R.id.action_setupareas:
			intent = new Intent(this, SetupWorkoutAreas.class);
			startActivity(intent);
			return true;
		case R.id.action_launch_client:
			startActivity(mClientLaunchIntent);
			return true;
		case R.id.action_import:
			intent = new Intent(this, DaxkoFamilyImporter.class);
			startActivity(intent);
			return true;
		case R.id.action_addguest:
			intent = new Intent(this, GuestCreator.class);
			startActivity(intent);
			return true;
		case R.id.action_refresh:
			mTabHost = (ChildListTabHost) mFrag.getTabHost();
			sortDirection = mTabHost.getTab(mFrag.getTabHost().getCurrentTab()).getView().getSortDirection();

			mFrag.hideFilterTabsOnRefresh();
			if (mFrag.getTabHost().getCurrentTab() == 0)
				getPersistenceLayer().fetchCheckedInMembersAndCubbies(sortDirection, "Name", new WriteEmergencyContacts());
			else if (mFrag.getTabHost().getCurrentTab() == 1)
				getPersistenceLayer().fetchCheckedInMembersAndCubbies(sortDirection, "Family", new WriteEmergencyContacts());
			else if (mFrag.getTabHost().getCurrentTab() == 2)
				getPersistenceLayer().fetchCheckedInMembersAndCubbies(sortDirection, "Age", new WriteEmergencyContacts());
			else if (mFrag.getTabHost().getCurrentTab() == 3)
				getPersistenceLayer().fetchCheckedInMembersAndCubbies(sortDirection, "Time", new WriteEmergencyContacts());
			else 
				getPersistenceLayer().fetchCheckedInMembersAndCubbies(new WriteEmergencyContacts());
			//getPersistenceLayer().fetchSignInHistory();
			getPersistenceLayer().fetchFacilityStaff();
			getPersistenceLayer().fetchTodaysSignInRecords();
			return true;
		case R.id.action_fastcheckout:
			intent = new Intent(this, FastCheckout.class);
			startActivity(intent);
			return true;
		case R.id.action_setupcubbies:
			intent = new Intent(this, SetupCubbies.class);
			startActivity(intent);
			return true;
		case R.id.action_searchedit:
			intent = new Intent(this, SearchAndEditFamily.class);
			startActivity(intent);
			return true;
		case R.id.action_checkins:
			intent = new Intent(this, Reporting.class);
			startActivity(intent);
			return true;
		case R.id.action_incident:
			intent = new Intent(this, Incidents.class);
			startActivity(intent);
			return true;
		case R.id.action_groupaction:
			mMenu.findItem(R.id.action_cancel).setVisible(true);
			mMenu.findItem(R.id.action_selectall).setVisible(true);
			mMenu.findItem(R.id.action_move).setVisible(true);
			mMenu.findItem(R.id.action_move).setEnabled(false);
			mMenu.findItem(R.id.action_checkout).setVisible(true);
			mMenu.findItem(R.id.action_checkout).setEnabled(false);
			mMenu.findItem(R.id.action_groupaction).setVisible(false);
			mMenu.findItem(R.id.action_refresh).setVisible(false);
			mMenu.findItem(R.id.action_fastcheckout).setVisible(false);

			mFrag.setMode(ChildListMode.GROUP);
			mFrag.setListViewAlpha(ChildListMode.GROUP);
			return true;
		case R.id.action_cancel:
			mMenu.findItem(R.id.action_cancel).setVisible(false);
			mMenu.findItem(R.id.action_selectall).setVisible(false);
			mMenu.findItem(R.id.action_move).setVisible(false);
			mMenu.findItem(R.id.action_checkout).setVisible(false);
			mMenu.findItem(R.id.action_groupaction).setVisible(true);
			mMenu.findItem(R.id.action_refresh).setVisible(true);
			mMenu.findItem(R.id.action_fastcheckout).setVisible(true);

			mFrag.setListViewAlpha(ChildListMode.SINGLE);
			mFrag.resetAll();
			return true;
		case R.id.action_selectall:
			mFrag.selectAll();
			return true;
		case R.id.action_move:
			View moveView = findViewById(R.id.action_move);
			new SelectRoomPopup(moveView.getContext(), getPersistenceLayer().getPersistenceData().getAllFacilityRooms(),
					mFrag.getSelectedMembers(), mFrag.getModel()).show(moveView);
			return true;
		case R.id.action_checkout:
			mFrag.checkOut();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(daydreamReceiver, new IntentFilter(Intent.ACTION_DREAMING_STOPPED));
		onConnectivityChanged(getPersistenceLayer().getConnectivityUtility().isInternetConnected());
		getPersistenceLayer().fetchTodaysSignInRecords();
		getPersistenceLayer().fetchSignInHistory();
		getPersistenceLayer().fetchFacilityStaff();
		getPersistenceLayer().fetchCheckedInMembersAndCubbies(new WriteEmergencyContacts());
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(daydreamReceiver);
	}

	@Override
	public void onAttachedToWindow() {
		// // Use full-screen mode to hide the status bar
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// // Show our window instead of regular lock screen
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		//
		super.onAttachedToWindow();
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// mFrag = (Dashboard)
		// getFragmentManager().findFragmentById(R.id.mainFragment);
		mFrag.filterChildrenByRoom(tab.getText().toString());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		mFrag.filterChildrenByRoom(tab.getText().toString());
	}

	@Override
	public void onConnectivityChanged(boolean isConnected) {
		if (getResources().getBoolean(R.bool.IS_OFFLINE_MODE_ENABLED) && !isConnected) {
			Intent intent = new Intent(this, OfflineEmergencyList.class);
			startActivity(intent);
		}
			//mBanner.setVisibility(isConnected ? View.GONE : View.VISIBLE);
	}
	
	private class WriteEmergencyContacts implements PersistenceLayerCallback<List<MemberAndCubbies>>{

		@Override
		public void done(List<MemberAndCubbies> data, Exception e) {
			String FILENAME = "offline_emergency_contacts.txt";
			String separator = System.getProperty("line.separator");
			List<Member> sortedData = new ArrayList<Member>();
			for (MemberAndCubbies mc: data){
				sortedData.add(mc.getMember());
			}
			ComparatorChain<Member> chain = new ComparatorChain<Member>();
			chain.addComparator(new MemberFirstNameComparator(sortDirection));
			chain.addComparator(new MemberLastNameComparator(sortDirection));
			Collections.sort(sortedData, chain);
			
			FileOutputStream fos;
			try {
				fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
				OutputStreamWriter osw = new OutputStreamWriter(fos);
				for (int i = 0; i < sortedData.size(); i++) {
					List<EmergencyNumber> emergencyList = sortedData.get(i).listEmergencyNumbers();
					String emergencyContacts = "";
					for (int j = 0; j < emergencyList.size(); j++){
						emergencyContacts = emergencyContacts + emergencyList.get(j).getName() + ": " + emergencyList.get(j).getPhone();
						if (j != emergencyList.size()-1)
							emergencyContacts = emergencyContacts + ", ";
					}
					List<PhoneNumber> checkedInByPhones = sortedData.get(i).getPCheckInBy().listPhoneNumbers();
					String checkedInByPhonesText = "";
					for (int k = 0; k < checkedInByPhones.size(); k++){
						checkedInByPhonesText = checkedInByPhonesText + checkedInByPhones.get(k).getNumber();
						if (k != checkedInByPhones.size() - 1)
							checkedInByPhonesText = checkedInByPhonesText +  ", ";
					}
					osw.append(sortedData.get(i).getFirstName() + " " + sortedData.get(i).getLastName() + " (Age: " + DaycareHelpers.getAgeInYears(sortedData.get(i).getBirthDate()) 
					+ ", Checked In By: " + sortedData.get(i).getPCheckInBy().getFirstName() + " " + sortedData.get(i).getPCheckInBy().getLastName() 
					+ " " + checkedInByPhonesText + ")");
					osw.append(separator);
					osw.append("Emergency Contacts: " + emergencyContacts);
					osw.append(separator);
					osw.append(separator);
				}
				osw.close();
				fos.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
