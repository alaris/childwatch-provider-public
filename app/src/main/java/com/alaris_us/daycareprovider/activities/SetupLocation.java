package com.alaris_us.daycareprovider.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.models.SetupLocationViewModel;
import com.alaris_us.daycareprovider.utils.ImageFunctions;
import com.bindroid.BindingMode;
import com.bindroid.ui.EditTextTextProperty;
import com.bindroid.ui.UiBinder;

public class SetupLocation extends Persistence<SetupLocationViewModel> {

	private static int RESULT_LOAD_IMAGE = 1;
	private EditText m_editCity, m_editState, m_editName;
	private Button m_buttonUpdateImage;
	private static final float IMAGE_SATURATION = 0.95f;
	private static final float IMAGE_MIX = 0.3f;
	private static final float IMAGE_BRIGHTNESS = 80f;
	private static final float IMAGE_CONTRAST = 0.5f;
	private static int IMAGE_LENGTH = 300;
	private static int IMAGE_QUALITY = 90;
	private static int IMAGE_COLOR = 0xff66F781;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setuplocation);
		m_editCity = (EditText) findViewById(R.id.setuplocation_editCity);
		m_editState = (EditText) findViewById(R.id.setuplocation_editState);
		m_editName = (EditText) findViewById(R.id.setuplocation_editName);
		m_buttonUpdateImage = (Button) findViewById(R.id.setuplocation_buttonUpdateHomescreen);
		m_buttonUpdateImage.setOnClickListener(new UpdateImage());
		UiBinder.bind(new EditTextTextProperty(m_editCity), getModel(), "City", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty(m_editState), getModel(), "State", BindingMode.TWO_WAY);
		UiBinder.bind(new EditTextTextProperty(m_editName), getModel(), "YmcaName", BindingMode.TWO_WAY);
		UiBinder.bind(this, R.id.setuplocation_fonttextCityState, "Text", getModel(), "CityState", BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.setuplocation_fonttextFacility, "Text", getModel(), "YmcaName", BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.setuplocation_imageHomescreen, "ImageBitmap", getModel(), "BitmapHomeScreen",
				BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.setuplocation_buttonSaveInformation, "OnClickListener", getModel(),
				"SaveInformationOnClickListener");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_previous:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.setuplocation_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	private class UpdateImage implements OnClickListener {

		@Override
		public void onClick(View v) {

			Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(i, RESULT_LOAD_IMAGE);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);
		// Called when save information is run
		if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
			Uri selectedImage = data.getData();
			Bitmap bitmap = ImageFunctions.loadBitmapFromURI(selectedImage, this);
			// Square crop and scale image
			bitmap = ImageFunctions.squareCropBitmap(bitmap);
			bitmap = ImageFunctions.scaleBitmap(bitmap, IMAGE_LENGTH, IMAGE_LENGTH);
			bitmap = ImageFunctions.colorizeBitmap(bitmap, IMAGE_COLOR, IMAGE_MIX, IMAGE_SATURATION);
			bitmap = ImageFunctions.changeBitmapContrastBrightness(bitmap, IMAGE_CONTRAST, IMAGE_BRIGHTNESS);
			bitmap = ImageFunctions.compressAsJPG(bitmap, IMAGE_QUALITY);
			getModel().setBitmapHomeScreen(bitmap);
		}
	}
}
