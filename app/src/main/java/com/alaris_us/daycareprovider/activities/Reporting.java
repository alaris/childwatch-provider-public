package com.alaris_us.daycareprovider.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.alaris_us.daycareprovider.models.ReportingViewModel;
import com.alaris_us.daycareprovider.view.ReportingListItem;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class Reporting extends Persistence<ReportingViewModel> {

	ReportingViewModel m_model;

	/*
	 * @Override public View onCreateView(LayoutInflater inflater, ViewGroup
	 * container, Bundle savedInstanceState) {
	 * 
	 * return inflater.inflate(R.layout.fragment_reporting, container, false);
	 * 
	 * }
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_reporting);

		/*
		 * m_model = new ReportingViewModel( ((PersistenceLayerHost)
		 * getActivity()).getPersistenceLayer());
		 */

		UiBinder.bind(this, R.id.reporting_startButton, "OnClickListener", getModel(), "StartTimeButtonListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.reporting_endButton, "OnClickListener", getModel(), "EndTimeButtonListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.reporting_query, "OnClickListener", getModel(), "UpdateButtonListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.reporting_startButton, "Text", getModel(), "StartDateString", BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.reporting_endButton, "Text", getModel(), "EndDateString", BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.reporting_list, "Adapter", getModel(), "ReportingHistory",
				new AdapterConverter(ReportingListItem.class));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_previous:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.viewcheckins_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/*
	 * @Override public String getTitle() {
	 * 
	 * return "Check-Ins";
	 * 
	 * }
	 */

}
