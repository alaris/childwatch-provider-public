package com.alaris_us.daycareprovider.activities;

import com.alaris_us.daycaredata.exceptions.DaycareDataExceptionConnection;
import com.alaris_us.daycaredata.exceptions.DaycareDataExceptionNoObject;
import com.alaris_us.daycareprovider.app.App;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerListener;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.converters.BoolConverter;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.UiBinder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.TextView;

public class DaycareProviderKiosk extends Activity {

	private TrackableField<String> mStatus = new TrackableField<String>();
	private TrackableField<String> mErrorStatus = new TrackableField<String>();
	private TrackableField<String> mAndroidId = new TrackableField<String>();
	private TrackableBoolean mCanRegister = new TrackableBoolean(false);
	private TrackableBoolean mIsBusy = new TrackableBoolean(false);
	private String mAppPassword;
	private State mCurrentState;
	private PersistenceLayer mPersistenceLayer;
	private LayerListener mLayerListener;
	private TextView m_versionText;
	private String versionName;
	private int versionCode;

	public String getAndroidId() {

		return mAndroidId.get();
	}

	public boolean getIsProduction() {
		return ((App) getApplication()).isStartupUpdateEnabled() && ((App) getApplication()).isUpdateEnabled();
	}

	public boolean getIsBusy() {

		return mIsBusy.get();
	}

	public void setIsBusy(boolean isBusy) {

		mIsBusy.set(isBusy);
	}

	public String getErrorStatus() {

		return mErrorStatus.get();
	}

	public String getStatus() {

		return mStatus.get();
	}

	public boolean getCanRegister() {

		return mCanRegister.get();
	}

	private void setStatus(String status) {

		mStatus.set(status);
		mErrorStatus.set("");
	}

	private void setStatus(String status, Exception e) {

		mStatus.set(status);
		mErrorStatus.set((e == null) ? "" : e.getMessage());
	}

	public OnClickListener getRegisterOnClickListener() {

		return new RegistrationOnClickListener();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_daycareproviderkiosk);
		// Data configuration and updates
		App app = (App) getApplication();
		app.checkForUpdates();
		mAppPassword = app.getAppPassword();
		mPersistenceLayer = app.getPersistenceLayer();
		mAndroidId.set(mPersistenceLayer.getAndroidId());
		m_versionText = (TextView) findViewById(R.id.initialize_versionText);
		// Version Text
		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			versionName = pInfo.versionName;
			versionCode = pInfo.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		m_versionText.setText("Version: " + versionName + " Rev " + versionCode);
		UiBinder.bind(this, R.id.initialize_textstatus, "Text", this, "Status");
		UiBinder.bind(this, R.id.initialize_texterrordetail, "Text", this, "ErrorStatus");
		UiBinder.bind(this, R.id.initialize_textDeviceID, "Text", this, "AndroidId",
				new ToStringConverter("Device ID: %s"));
		UiBinder.bind(this, R.id.initialize_buttonRegister, "Visibility", this, "CanRegister", new BoolConverter());
		UiBinder.bind(this, R.id.initialize_buttonRegister, "OnClickListener", this, "RegisterOnClickListener");
		UiBinder.bind(this, R.id.initializeProgressBar, "Visibility", this, "IsBusy", new BoolConverter());
		UiBinder.bind(this, R.id.TextView_DevRelease, "Visibility", this, "IsProduction", new BoolConverter(true));
	}

	@Override
	protected void onResume() {

		super.onResume();
		if (isNetworkAvailable(ConnectivityManager.TYPE_WIFI) || isNetworkAvailable(ConnectivityManager.TYPE_MOBILE))
			initialize();
		else {
			Intent intent = new Intent(this, OfflineEmergencyList.class);
			startActivity(intent);
		}
	}

	@Override
	protected void onPause() {

		super.onPause();
		mPersistenceLayer.removePersistanceLayerListener(mLayerListener);
	}

	private void initialize() {

		mLayerListener = new LayerListener();
		mPersistenceLayer.addPersistanceLayerListener(mLayerListener);
		mCurrentState = new SigninState();
		mCurrentState.enterState();
	}

	abstract class State {

		public abstract void enterState();

		public void exitState() {

			mCurrentState = null;
		}

		public void exception(Exception e) {

			mCurrentState = null;
		}
	}

	private class SigninState extends State {

		@Override
		public void enterState() {

			setStatus("Signing In");
			mPersistenceLayer.login(mAppPassword);
		}

		@Override
		public void exitState() {

			mCurrentState = new FetchFacilityState();
		}

		@Override
		public void exception(Exception e) {

			setStatus("Could not login", e);
			// Allow registration if user not found
			if (e instanceof DaycareDataExceptionNoObject) {
				mCanRegister.set(true);
			}
			mCurrentState = null;
		}
	}

	private class RegistrationState extends State {

		@Override
		public void enterState() {

			mCanRegister.set(false);
			setStatus("Registering User");
			mPersistenceLayer.registerUser(mAppPassword);
		}

		@Override
		public void exitState() {

			mCanRegister.set(false);
			mCurrentState = new FetchFacilityState();
		}

		@Override
		public void exception(Exception e) {

			setStatus("Could not register device", e);
			// Don't allow user to re-register
			mCanRegister.set(false);
			mCurrentState = null;
		}
	}

	private class FetchFacilityState extends State {

		@Override
		public void enterState() {

			setStatus("Fetching Facility Information");
			mPersistenceLayer.fetchFacility();
		}

		@Override
		public void exitState() {
			
			mPersistenceLayer.initializeHerokuDataSource();
			mCurrentState = new FetchFacilityCubbies();
		}

		@Override
		public void exception(Exception e) {
			if (!(e instanceof DaycareDataExceptionConnection)) {
				setStatus("Could not fetch facility information");
				mCurrentState = null;
			}
		}
	}

	private class FetchFacilityCubbies extends State {

		@Override
		public void enterState() {

			setStatus("Fetching Facility Cubbies");
			mPersistenceLayer.fetchFacilityCubbies();
		}

		@Override
		public void exitState() {

			mCurrentState = new FetchFacilityPagers();
		}

		@Override
		public void exception(Exception e) {
			if (!(e instanceof DaycareDataExceptionConnection)) {
				setStatus("Could not fetch cubby information");
				mCurrentState = null;
			}
		}
	}

	private class FetchFacilityPagers extends State {

		@Override
		public void enterState() {

			setStatus("Fetching Facility Pagers");
			mPersistenceLayer.fetchFacilityPagers();
		}

		@Override
		public void exitState() {

			mCurrentState = new FetchFacilityRoomsState();
		}

		@Override
		public void exception(Exception e) {
			if (!(e instanceof DaycareDataExceptionConnection)) {
				setStatus("Could not fetch pager information");
				mCurrentState = null;
			}
		}
	}

	private class FetchFacilityRoomsState extends State {

		@Override
		public void enterState() {

			setStatus("Fetching Facility Rooms");
			mPersistenceLayer.fetchFacilityRooms();
		}

		@Override
		public void exitState() {

			mCurrentState = new FetchWorkoutLocationsState();
		}

		@Override
		public void exception(Exception e) {
			if (!(e instanceof DaycareDataExceptionConnection)) {
				setStatus("Could not fetch cubby information");
				mCurrentState = null;
			}
		}
	}

	private class FetchWorkoutLocationsState extends State {

		@Override
		public void enterState() {

			setStatus("Fetching All Workout Locations");
			mPersistenceLayer.fetchAllWorkoutLocations();
		}

		@Override
		public void exitState() {

			mCurrentState = new FetchSatffState();
		}

		@Override
		public void exception(Exception e) {
			if (!(e instanceof DaycareDataExceptionConnection)) {
				setStatus("Could not fetch workout locations");
				mCurrentState = null;
			}
		}
	}

	private class FetchSatffState extends State {

		@Override
		public void enterState() {

			setStatus("Fetching Staff");
			mPersistenceLayer.fetchFacilityStaff();
		}

		@Override
		public void exitState() {

			mCurrentState = new FetchSMSMessageState();
		}

		@Override
		public void exception(Exception e) {
			if (!(e instanceof DaycareDataExceptionConnection)) {
				setStatus("Could not fetch staff members");
				mCurrentState = null;
			}
		}
	}

	private class FetchSMSMessageState extends State {

		@Override
		public void enterState() {

			setStatus("Fetching SMS Messages");
			mPersistenceLayer.fetchSMSMessages();
		}

		@Override
		public void exitState() {

			mCurrentState = new FetchAdmissionSettingsState();
		}

		@Override
		public void exception(Exception e) {
			if (!(e instanceof DaycareDataExceptionConnection)) {
				setStatus("Could not fetch SMS Messages");
				//mCurrentState = null;
				mCurrentState = new FetchSMSMessageState();
			}
		}
	}

	private class FetchAdmissionSettingsState extends State {

		@Override
		public void enterState() {

			setStatus("Fetching Admission Settings");
			mPersistenceLayer.fetchAdmissionSettingsForFacility();
		}

		@Override
		public void exitState() {

			mCurrentState = new InitalizationCompleteState();
		}

		@Override
		public void exception(Exception e) {
			if (!(e instanceof DaycareDataExceptionConnection)) {
				setStatus("Could not fetch Admission Settings");
				//mCurrentState = null;
				mCurrentState = new FetchAdmissionSettingsState();
			}
		}
	}

	private class InitalizationCompleteState extends State {

		@Override
		public void enterState() {

			Intent intent = new Intent(DaycareProviderKiosk.this, Session.class);
			startActivity(intent);
		}
	}

	private class LayerListener implements PersistenceLayerListener {

		@Override
		public void updating() {

			mIsBusy.set(true);
		}

		@Override
		public void updated() {

			mIsBusy.set(false);
			if (mCurrentState == null)
				return;
			mCurrentState.exitState();
			mCurrentState.enterState();
		}

		@Override
		public void exception(Exception e) {
			mCurrentState.exception(e);
		}
	}

	private class RegistrationOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			mCurrentState = new RegistrationState();
			mCurrentState.enterState();
		}
	}

	@Override
	public void onAttachedToWindow() {

		// Use full-screen mode to hide the status bar
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// Show our window instead of regular lock screen
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		super.onAttachedToWindow();
	}
	
	public boolean isNetworkAvailable(int networkType) {
	    try {
	        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	            NetworkInfo netInfo = cm.getNetworkInfo(networkType);
	            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
	                return true;
	            }
	    } catch (Exception e) {
	        return false;
	    }
	    return false;
	}
}
