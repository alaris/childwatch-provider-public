package com.alaris_us.daycareprovider.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.alaris_us.daycareprovider.models.IncidentsViewModel;
import com.alaris_us.daycareprovider.view.IncidentItem;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

public class Incidents extends Persistence<IncidentsViewModel> {

	IncidentsViewModel m_model;

	/*
	 * @Override public View onCreateView(LayoutInflater inflater, ViewGroup
	 * container, Bundle savedInstanceState) {
	 * 
	 * return inflater.inflate(R.layout.fragment_incidents, container, false);
	 * 
	 * }
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_incidents);
		/*
		 * m_model = new IncidentsViewModel( ((PersistenceLayerHost)
		 * getActivity()).getPersistenceLayer());
		 */

		UiBinder.bind(this, R.id.incident_startButton, "OnClickListener", getModel(), "StartTimeButtonListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.incident_endButton, "OnClickListener", getModel(), "EndTimeButtonListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.incident_query, "OnClickListener", getModel(), "UpdateButtonListener",
				BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.incident_startButton, "Text", getModel(), "StartDateString", BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.incident_endButton, "Text", getModel(), "EndDateString", BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.ListView_Incidents, "Adapter", getModel(), "IncidentHistory",
				new AdapterConverter(IncidentItem.class));
		UiBinder.bind(this, R.id.ListView_Incidents, "OnItemClickListener", getModel(),
				"EditIncidentOnItemClickListener");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_previous:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.findincidents_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/*
	 * @Override public String getTitle() {
	 * 
	 * return "Incidents";
	 * 
	 * }
	 */

}
