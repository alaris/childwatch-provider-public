package com.alaris_us.daycareprovider.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;

import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.models.SetupCubbiesViewModel;
import com.alaris_us.daycareprovider.view.CubbyListItem;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.ui.SeekBarProgressProperty;
import com.bindroid.ui.UiBinder;

public class SetupCubbies extends Persistence<SetupCubbiesViewModel> {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// Android
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setupcubbies);
		// Binding
		UiBinder.bind(this, R.id.GridView_Cubby, "NumColumns", getModel(), "CubbyColumns");
		UiBinder.bind(this, R.id.GridView_Cubby, "Adapter", getModel(), "CubbyList",
				new AdapterConverter(CubbyListItem.class));
		UiBinder.bind(this, R.id.Button_SaveCubbyNames, "OnClickListener", getModel(), "SaveCubbiesOnClickListener");
		UiBinder.bind(new SeekBarProgressProperty((SeekBar) findViewById(R.id.Seekbar_CubbyColumns)), getModel(),
				"SeekbarCubbyColumns", BindingMode.TWO_WAY);
		UiBinder.bind(new SeekBarProgressProperty((SeekBar) findViewById(R.id.SeekBar_CubbyCount)), getModel(),
				"CubbyCount", BindingMode.TWO_WAY);
		UiBinder.bind(this, R.id.TextView_CubbyCount, "Text", getModel(), "CubbyCount", new ToStringConverter("%d"));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_previous:
			this.finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.setupcubbies_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}
}
