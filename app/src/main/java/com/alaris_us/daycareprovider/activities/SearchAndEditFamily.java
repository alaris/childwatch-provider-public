package com.alaris_us.daycareprovider.activities;

import com.alaris_us.daycareprovider.adapters.NoTabsAdapter;
import com.alaris_us.daycareprovider.fragments.BaseSearchAndEditFamily;
import com.alaris_us.daycareprovider.fragments.EditSearchandEditFamily;
import com.alaris_us.daycareprovider.fragments.FamilySettings;
import com.alaris_us.daycareprovider.fragments.MemberPhotoScreenFragment;
import com.alaris_us.daycareprovider.fragments.SearchSearchandEditFamily;
import com.alaris_us.daycareprovider.models.SearchAndEditFamilyViewModel;
import com.alaris_us.daycareprovider.models.SearchAndEditFamilyViewModel.OnKeyboardCallback;
import com.alaris_us.daycareprovider.models.SearchAndEditFamilyViewModel.SEARCH_PARAMETER_DELEGATE;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.UiBinder;
import com.bindroid.utils.ReflectedProperty;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class SearchAndEditFamily extends Persistence<SearchAndEditFamilyViewModel> implements OnKeyboardCallback {


	private ViewPager mViewPager;
	private NoTabsAdapter mViewPagerAdapter;
	public static String ACTION_SHOWFAMILY_WITH_MEMBERID = "ACTION_SHOWFAMILY_WITH_MEMBERID";
	public boolean mBackExits;
	private View mBanner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM, ActionBar.DISPLAY_SHOW_CUSTOM);
		// Android Boilerplate
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guestcreator);
		mBanner = findViewById(R.id.banner);
		// ViewPager
		mViewPager = (ViewPager) findViewById(R.id.pager);
		// Setup Fragments
		mViewPagerAdapter = new NoTabsAdapter(this, mViewPager);
		mViewPagerAdapter.addFragment(new SearchSearchandEditFamily());
		mViewPagerAdapter.addFragment(new EditSearchandEditFamily());
		mViewPagerAdapter.addFragment(new MemberPhotoScreenFragment());
		mViewPagerAdapter.addFragment(new FamilySettings());
		// Bindings
		UiBinder.bind(new ReflectedProperty(this, "ActionBarItem"), getModel(), "CurrentPage", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "CurrentItem"), getModel(), "CurrentPage", BindingMode.ONE_WAY);
		UiBinder.bind(new ReflectedProperty(this, "KeyboardCallback"), getModel(), "KeyboardCallback",
				BindingMode.ONE_WAY_TO_SOURCE);

		// Handle Intent
		handleSearchIntent(getIntent());
	}

	@Override
	public void onAttachedToWindow() {

		// // Use full-screen mode to hide the status bar
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// // Show our window instead of regular lock screen
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		// // Hide keyboard on showing activity
		// this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		super.onAttachedToWindow();
	}

	public void setCurrentItem(int i) {
		mViewPager.setCurrentItem(i, false);
	}

	public ViewPager getViewPager() {

		return mViewPager;
	}

	public OnKeyboardCallback getKeyboardCallback() {
		return this;
	}

	private void hideKeyboard() {
		InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (getCurrentFocus() != null)
			in.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
	}

	private void showKeyboard() {
		InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		in.showSoftInputFromInputMethod(getCurrentFocus().getWindowToken(), InputMethodManager.SHOW_FORCED);
	}

	public void setActionBarItem(int atPage) {

		BaseSearchAndEditFamily frag = ((BaseSearchAndEditFamily) mViewPagerAdapter.getItem(atPage));
		int actionBarLayout = frag.getActionBarLayout();
		View inflated = getLayoutInflater().inflate(actionBarLayout, null);
		getActionBar().setCustomView(inflated, new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
				ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER | Gravity.END));
		frag.setActionBarView(inflated);
		// Bind navigation buttons
		UiBinder.bind(inflated, R.id.searchbar_backbutton_container, "OnClickListener", this,
				"BackButtonOnClickListener");
	}

	@Override
	public void onBackPressed() {

		/*
		 * int currentItem=mViewPager.getCurrentItem()-1; if(currentItem>=0)
		 * mViewPager.setCurrentItem(currentItem, false); else
		 * super.onBackPressed();
		 */
	}

	public OnClickListener getBackButtonOnClickListener() {

		return new BackButtonOnClickListener();
	}

	class BackButtonOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			int currentPage = getModel().getCurrentPage();
			if (mBackExits) {
				SearchAndEditFamily.this.finish();
			} else if (currentPage == 0) {
				SearchAndEditFamily.this.finish();
			} else {
				getModel().setCurrentPage(--currentPage);
			}
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {

		super.onNewIntent(intent);
		handleSearchIntent(intent);
	}

	private void handleSearchIntent(Intent intent) {
		mBackExits = false;
		String q = intent.getStringExtra("OBJECT_ID");
		if (q == null)
			return;
		mBackExits = true;
		setCurrentItem(1);
		getModel().query(q, SEARCH_PARAMETER_DELEGATE.OBJECT_ID);
	}

	@Override
	public void show(long delay) {
		if (delay > 0)
			getViewPager().postDelayed(new Runnable() {
				@Override
				public void run() {
					showKeyboard();
				}
			}, delay);
		else
			showKeyboard();
	}

	@Override
	public void hide(long delay) {
		if (delay > 0)
			getViewPager().postDelayed(new Runnable() {
				@Override
				public void run() {
					hideKeyboard();
				}
			}, delay);
		else
			hideKeyboard();
	}

	@Override
	public void onConnectivityChanged(boolean isConnected) {
		mBanner.setVisibility(isConnected ? View.GONE : View.VISIBLE);
	}
}
