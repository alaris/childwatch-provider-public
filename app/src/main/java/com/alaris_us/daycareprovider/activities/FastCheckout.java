package com.alaris_us.daycareprovider.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.camera.BarcodeCameraPreview;
import com.alaris_us.daycareprovider.camera.BarcodeCameraPreview.BarcodeCameraPreviewListener;
import com.alaris_us.daycareprovider.fragments.Dashboard.ConfirmAdmissionUsageOverage;
import com.alaris_us.daycareprovider.fragments.ProviderFragment.PersistenceLayerHost;
import com.alaris_us.daycareprovider.camera.CameraManager;
import com.alaris_us.daycareprovider.models.FastCheckoutViewModel;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.popups.AdmissionsUsedPopup;
import com.alaris_us.daycareprovider.popups.ChildDetailsPopup;
import com.alaris_us.daycareprovider.popups.ConfirmCheckOutPopup;
import com.alaris_us.daycareprovider.popups.MinutesOveragePopup;
import com.alaris_us.daycareprovider.popups.AdmissionsUsedPopup.OnConfirmAdmissionUsageListener;
import com.alaris_us.daycareprovider.popups.ConfirmCheckOutPopup.OnConfirmCheckOutListener;
import com.alaris_us.daycareprovider.popups.MinutesOveragePopup.OnConfirmMintuesOverageListener;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider.view.CustomToast;
import com.alaris_us.daycareprovider.view.FastCheckOutMemberItem;
import com.alaris_us.daycareprovider.view.FastCheckOutGuardianItem;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.ValueConverter;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.ui.UiBinder;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

public class FastCheckout extends Persistence<FastCheckoutViewModel> implements BarcodeCameraPreviewListener {
	private FrameLayout mCameraDisplay;
	private View mBusyView;
	private BarcodeCameraPreview mPreview;
	private TextView mSelectAllButton;
	private PersistenceLayer mPersistenceLayer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM, ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.view_fastcheckout_actionbar);
		actionBar.getCustomView().findViewById(R.id.fastcheckout_backcontainer)
				.setOnClickListener(new OnBackClickListener());

		// Android Boilerplate
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fastcheckout);

		mCameraDisplay = (FrameLayout) findViewById(R.id.camerapreview);
		mPreview = new BarcodeCameraPreview(this, new CameraManager());
		mPreview.setBackground(getResources().getDrawable(R.color.invis));

		mBusyView = findViewById(R.id.camerabusy);
		mPersistenceLayer = ((PersistenceLayerHost) FastCheckout.this).getPersistenceLayer();

		mSelectAllButton = (TextView) findViewById(R.id.fastcheckout_selectall_button);

		UiBinder.bind(this, R.id.listview_member, "Adapter", getModel(), "AuthenticatedMember",
				new AdapterConverter(FastCheckOutGuardianItem.class));
		UiBinder.bind(this, R.id.listview_children, "Adapter", getModel(), "Children",
				new AdapterConverter(FastCheckOutMemberItem.class));
		UiBinder.bind(this, R.id.fastcheckout_instructions_left, "Visibility", getModel(),
				"LeftInstructionsVisibility");
		UiBinder.bind(this, R.id.fastcheckout_instructions_right, "Visibility", getModel(),
				"RightInstructionsVisibility");
		UiBinder.bind(this, R.id.fastcheckout_checkout_button, "Enabled", getModel(), "CheckOutButtonEnabled");
		UiBinder.bind(this, R.id.fastcheckout_checkout_button, "Alpha", getModel(), "CheckOutButtonEnabled",
				new BooleanToAlphaConverter());
		UiBinder.bind(this, R.id.fastcheckout_selectall_button, "Text", getModel(), "SelectAllButtonText");
		UiBinder.bind(this, R.id.fastcheckout_selectall_button, "Enabled", getModel(), "SelectAllButtonEnabled");
		UiBinder.bind(this, R.id.fastcheckout_selectall_button, "Alpha", getModel(), "SelectAllButtonEnabled",
				new BooleanToAlphaConverter());

		((ListView) findViewById(R.id.listview_children)).setOnItemClickListener(new OnChildClickListener());

		findViewById(R.id.fastcheckout_cancel_button).setOnClickListener(new OnCancelClickListener());
		findViewById(R.id.fastcheckout_selectall_button).setOnClickListener(new OnSelectAllClickListener());
		findViewById(R.id.fastcheckout_checkout_button).setOnClickListener(new OnCheckOutClickListener());

		// Handle Intent
		handleSearchIntent(getIntent());
	}

	@Override
	public void onAttachedToWindow() {
		// // Use full-screen mode to hide the status bar
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// // Show our window instead of regular lock screen
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		// // Hide keyboard on showing activity
		// this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		super.onAttachedToWindow();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		handleSearchIntent(intent);
	}

	private void handleSearchIntent(Intent intent) {
		String q = intent.getStringExtra("OBJECT_ID");
		if (q == null)
			return;
	}

	@Override
	public void onReady() {
		if (null != mBusyView) {
			mBusyView.setVisibility(View.GONE);
		}
	}

	@Override
	public void onStopped() {
		if (null != mBusyView) {
			mBusyView.setVisibility(View.VISIBLE);
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		mCameraDisplay.removeAllViews();
		if (mPreview != null) {
			ViewGroup parent = (ViewGroup) mPreview.getParent();
			if (parent != null)
				parent.removeView(mPreview);
		}
		mCameraDisplay.addView(mPreview);

		mPreview.setDecodeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		mPreview.setDecodeListener(null);
		mCameraDisplay.removeAllViews();
	}

	@Override
	public void read(String barcodeText) {
		getModel().lookupBarcode(barcodeText);
	}

	private class OnChildClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			getModel().onChildClick((int) id);
		}
	}

	private class OnCancelClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getModel().onCancelClick();
		}
	}

	private class OnSelectAllClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			getModel().onSelectAllClick();
		}
	}

	private class OnCheckOutClickListener
			implements OnClickListener, PersistenceLayerCallback<List<Member>>, OnConfirmCheckOutListener, OnConfirmMintuesOverageListener,
			OnConfirmAdmissionUsageListener{
		@Override
		public void onClick(final View v) {
			//final PersistenceLayer persistenceLayer = ((PersistenceLayerHost) FastCheckout.this).getPersistenceLayer();
			boolean usesOverageAlerting = getResources().getBoolean(R.bool.IS_OVERAGE_ALERTING_ENABLED);
			if (usesOverageAlerting) {
				HashMap <Member, Number> payableChildren = new HashMap<>();
				payableChildren = DaycareHelpers.getChildrenWithOverageMinutes(getModel().getChildrenToCheckOut());
				if (!payableChildren.isEmpty()) {
					new MinutesOveragePopup(payableChildren, getBaseContext(), this).show(v);
				} else {
					this.onCheckOut();
				}
			} else if (mPersistenceLayer.getPersistenceData().getFacilityUsesAdmissions()) {
				final Facility facility = mPersistenceLayer.getPersistenceData().getFacility();
				mPersistenceLayer.chargeAdmissions(getModel().getChildrenToCheckOut(), facility,
						false, getModel().getAuthenticatedMember().get(0).getMember(), new PersistenceLayerCallback<List<HashMap<String, Object>>>() {
					@Override
					public void done(List<HashMap<String, Object>> data, Exception e) {
						new AdmissionsUsedPopup(data, getModel().getChildrenToCheckOut(), facility, getModel().getAuthenticatedMember().get(0).getMember(),
								v.getContext(), mPersistenceLayer, OnCheckOutClickListener.this).show(v);
					}
				});
			} else {
				this.onCheckOut();
			}
		}

		@Override
		public void done(List<Member> data, Exception e) {
			CustomToast.makeToast(getBaseContext(), "Check Out Successful", CustomToast.DURATION_LONG,
					CustomToast.STYLE_SUCCESS).show();
			getModel().reset();
		}

		@Override
		public void onConfirmCheckOut() {
			if (mPersistenceLayer.getPersistenceData().getFacilityUsesAdmissions()) {
				mPersistenceLayer.chargeAdmissions(getModel().getChildrenToCheckOut(), mPersistenceLayer.getPersistenceData().getFacility(), true,
						getModel().getAuthenticatedMember().get(0).getMember(), new PersistenceLayerCallback<List<HashMap<String, Object>>>() {
							@Override
							public void done(List<HashMap<String, Object>> data, Exception e) {
							}
						});
			}
			getModel().onCheckOutClick(this);
		}

		@Override
		public void onCancel() {

		}

		@Override
		public void onCheckOut() {
			new ConfirmCheckOutPopup(getBaseContext(), this).show(getWindow().getDecorView());
		}
	}

	private class OnBackClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			onBackPressed();
		}
	}

	private class BooleanToAlphaConverter extends ValueConverter {
		@Override
		public Object convertToTarget(Object sourceValue, Class<?> targetType) {
			return (boolean) sourceValue ? 1f : 0.6f;
		}
	}
}
