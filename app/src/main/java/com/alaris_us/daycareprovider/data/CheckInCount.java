package com.alaris_us.daycareprovider.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.alaris_us.daycareprovider.heroku.HerokuDataObject;

import android.util.Log;

public class CheckInCount extends HerokuDataObject {
	private Map<String, List<Integer>> mRoomCounts;

	public CheckInCount(JSONObject json) {
		super(json);
		populateRoomCounts();
	}
	
	public CheckInCount(HerokuDataObject obj) {
		super(obj);
		populateRoomCounts();
	}

	private void populateRoomCounts() {
		mRoomCounts = new HashMap<String, List<Integer>>();
		try {
			JSONObject obj = getData().getJSONObject("roomCounts");
			@SuppressWarnings("unchecked")
			Iterator<String> itr = obj.keys();
			while (itr.hasNext()) {
				String key = itr.next();
				List<Integer> counts = new ArrayList<Integer>();
				try {
					JSONArray arr = obj.getJSONArray(key);
					for (int i = 0; i < arr.length(); i++) {
						counts.add(arr.getInt(i));
					}
				} catch (JSONException e) {
					Log.e("Error getting counts for: " + key, e.getMessage());
				}
				mRoomCounts.put(key, counts);
			}
		} catch (JSONException e) {
			Log.e(this.getClass().getSimpleName(), e.getMessage());
		}
	}
	
	public List<Integer> getRoomCount(String roomName) {
		if (mRoomCounts.containsKey(roomName))
			return mRoomCounts.get(roomName);
		else
			return new ArrayList<Integer>();
	}

	public List<Integer> getTotalCount() {
		Integer[] totalCount = new Integer[] { 0, 0, 0, 0, 0, 0, 0 };
		for (List<Integer> count : mRoomCounts.values()) {
			for (int i = 0; i < count.size(); i++)
				totalCount[i] += count.get(i);
		}
		return Arrays.asList(totalCount);
	}
}
