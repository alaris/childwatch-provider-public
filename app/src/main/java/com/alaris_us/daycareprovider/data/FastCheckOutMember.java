package com.alaris_us.daycareprovider.data;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.util.Cancelable;
import com.alaris_us.daycareprovider.data.MemberAndCubbies.DoneCb;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class FastCheckOutMember {

	private final TrackableField<Member> mMember;
	private final TrackableBoolean mIsSelected;
	private DaycareData mDatasource;
	private TrackableField<Facility> mFacility;
	private final TrackableBoolean mShowCheckmarkOnSelection;
	private AtomicBoolean isCanceled;
	private Cancelable task;
	private boolean mHasCubbies;
	private boolean mHasPagers;
	private TrackableCollection<Cubby> mUsing;
	private TrackableCollection<Pager> mPagers;
	private TrackableCollection<Incident> mIncidents;
	private TrackableBoolean mFetching;

	public FastCheckOutMember(DaycareData datasource, Member member, Facility facility, boolean hasCubbies,
			boolean hasPagers) {
		this(datasource, member, false, true, facility, hasCubbies, hasPagers);
	}

	public FastCheckOutMember(DaycareData datasource, Member member, boolean isSelected, boolean showCheckmarkOnSelection, 
			Facility facility, boolean hasCubbies, boolean hasPagers) {
		mUsing = new TrackableCollection<Cubby>();
		mPagers = new TrackableCollection<Pager>();
		mFetching = new TrackableBoolean(false);
		isCanceled = new AtomicBoolean(false);
		mIncidents = new TrackableCollection<Incident>();
		mDatasource = datasource;
		mMember = new TrackableField<Member>(member);
		mIsSelected = new TrackableBoolean(isSelected);
		mShowCheckmarkOnSelection = new TrackableBoolean(showCheckmarkOnSelection);
		mFacility =  new TrackableField<Facility>(facility);
		mHasCubbies = hasCubbies;
		mHasPagers = hasPagers;
		
		fetchAllData();
	}

	public Member getMember() {
		return mMember.get();
	}

	public void toggleSelected() {
		mIsSelected.set(!getIsSelected());
	}

	public void setIsSelected(boolean isSelected) {
		mIsSelected.set(isSelected);
	}

	public boolean getIsSelected() {
		return mIsSelected.get();
	}

	public boolean getShowCheckmarkOnSelection() {
		return mShowCheckmarkOnSelection.get();
	}
	
	public List<Incident> getIncidents() {
		return mIncidents;
	}

	public List<Cubby> getUsing() {
		DaycareHelpers.sortCubbyList(mUsing);
		return mUsing;
	}

	public List<Pager> getPagers() {
		DaycareHelpers.sortPagerList(mPagers);
		return mPagers;
	}
	
	public void fetchAllData() {
		fetchAllData(null);
	}

	public void fetchAllData(final DoneCb cb) {
		mFetching.set(true);

		fetchCubbies(new DaycareOperationComplete<List<Cubby>>() {
			@Override
			public void operationComplete(List<Cubby> cubbies, DaycareDataException e) {

				fetchPagers(new DaycareOperationComplete<List<Pager>>() {
					@Override
					public void operationComplete(List<Pager> pagers, DaycareDataException e) {

						fetchIncidents(new DaycareOperationComplete<List<Incident>>() {
							@Override
							public void operationComplete(List<Incident> incidents, DaycareDataException e) {
								
								mFetching.set(false);
								if (cb != null)
									cb.done();
							}
						});
					}
				});
			}
		});
	}

	private void fetchCubbies(final DaycareOperationComplete<List<Cubby>> cb) {
		if (isCanceled.get() || !mHasCubbies)
			cb.operationComplete(null, null);
		else
			task = mDatasource.getCubbiesDAO().listReservedCubbies(mFacility.get(), mMember.get(),
					new DaycareOperationComplete<List<Cubby>>() {
						@Override
						public void operationComplete(List<Cubby> data, DaycareDataException e) {
							task = null;
							mUsing.clear();
							mUsing.addAll(data);
							cb.operationComplete(data, e);
						}
					});
	}

	private void fetchPagers(final DaycareOperationComplete<List<Pager>> cb) {
		if (isCanceled.get() || !mHasPagers)
			cb.operationComplete(null, null);
		else
			task = mDatasource.getPagersDAO().listReservedPagers(mFacility.get(), mMember.get(),
					new DaycareOperationComplete<List<Pager>>() {
						@Override
						public void operationComplete(List<Pager> data, DaycareDataException e) {
							task = null;
							mPagers.clear();
							mPagers.addAll(data);
							cb.operationComplete(data, e);
						}
					});
	}

	private void fetchIncidents(final DaycareOperationComplete<List<Incident>> cb) {
		if (isCanceled.get())
			cb.operationComplete(null, null);
		else
			task = mDatasource.getIncidentsDAO().findIncidents(mMember.get(),
					new DaycareOperationComplete<List<Incident>>() {
						@Override
						public void operationComplete(List<Incident> data, DaycareDataException e) {
							task = null;
							mIncidents.clear();
							mIncidents.addAll(data);
							cb.operationComplete(data, e);
						}
					});
	}
}
