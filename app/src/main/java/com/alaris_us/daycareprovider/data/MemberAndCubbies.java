package com.alaris_us.daycareprovider.data;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Authorization;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.SelfCheckIn;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycaredata.util.Cancelable;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class MemberAndCubbies {
	private DaycareData mDatasource;
	private AtomicBoolean isCanceled;
	private Cancelable task;
	private boolean mHasCubbies;
	private boolean mHasPagers;
	private TrackableField<Member> mMember;
	private TrackableField<Facility> mFacility;
	private TrackableCollection<Cubby> mUsing;
	private TrackableCollection<Pager> mPagers;
	private TrackableCollection<Incident> mIncidents;
	private TrackableBoolean mSelected;
	private TrackableBoolean mDimmed;
	private TrackableCollection<WorkoutArea> mExpectedAreas;
	private TrackableBoolean mFetching;
	private TrackableCollection<Authorization> mAuthorizedMembers;
	private TrackableCollection<Authorization> mAuthorizedFors;
	private TrackableField<SelfCheckIn> mSelfCheckIn;

	public MemberAndCubbies(DaycareData datasource, Member member, Facility facility, boolean hasCubbies,
			boolean hasPagers) {
		isCanceled = new AtomicBoolean(false);
		mDatasource = datasource;
		mMember = new TrackableField<Member>(member);
		mFacility = new TrackableField<Facility>(facility);
		mUsing = new TrackableCollection<Cubby>();
		mPagers = new TrackableCollection<Pager>();
		mExpectedAreas = new TrackableCollection<WorkoutArea>();
		mSelected = new TrackableBoolean(false);
		mDimmed = new TrackableBoolean(false);
		mIncidents = new TrackableCollection<Incident>();
		mFetching = new TrackableBoolean(false);
		mHasCubbies = hasCubbies;
		mHasPagers = hasPagers;
		mAuthorizedMembers = new TrackableCollection<Authorization>();
		mAuthorizedFors = new TrackableCollection<Authorization>();
		mSelfCheckIn = new TrackableField<SelfCheckIn>();

		fetchAllData();
	}

	public interface DoneCb {
		public void done();
	}

	public void fetchAllData() {
		fetchAllData(null);
	}

	public void fetchAllData(final DoneCb cb) {
		mFetching.set(true);

		fetchCubbies(new DaycareOperationComplete<List<Cubby>>() {
			@Override
			public void operationComplete(List<Cubby> cubbies, DaycareDataException e) {

				fetchPagers(new DaycareOperationComplete<List<Pager>>() {
					@Override
					public void operationComplete(List<Pager> pagers, DaycareDataException e) {

						fetchIncidents(new DaycareOperationComplete<List<Incident>>() {
							@Override
							public void operationComplete(List<Incident> incidents, DaycareDataException e) {
								
								fetchAuthorizations(new DaycareOperationComplete<List<Authorization>>() {
									@Override
									public void operationComplete(List<Authorization> authorizations, DaycareDataException e) {
										
										fetchExpectedAreas(new DaycareOperationComplete<List<WorkoutArea>>() {
											@Override
											public void operationComplete(List<WorkoutArea> areas, DaycareDataException e) {

												fetchSelfCheckIns(new DaycareOperationComplete<SelfCheckIn>() {
													@Override
													public void operationComplete(SelfCheckIn selfCheckIn, DaycareDataException e) {
														mFetching.set(false);
														if (cb != null)
															cb.done();
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});
	}

	private void fetchCubbies(final DaycareOperationComplete<List<Cubby>> cb) {
		if (isCanceled.get() || !mHasCubbies)
			cb.operationComplete(null, null);
		else
			task = mDatasource.getCubbiesDAO().listReservedCubbies(mFacility.get(), mMember.get(),
					new DaycareOperationComplete<List<Cubby>>() {
						@Override
						public void operationComplete(List<Cubby> data, DaycareDataException e) {
							task = null;
							mUsing.clear();
							mUsing.addAll(data);
							cb.operationComplete(data, e);
						}
					});
	}

	private void fetchPagers(final DaycareOperationComplete<List<Pager>> cb) {
		if (isCanceled.get() || !mHasPagers)
			cb.operationComplete(null, null);
		else
			task = mDatasource.getPagersDAO().listReservedPagers(mFacility.get(), mMember.get(),
					new DaycareOperationComplete<List<Pager>>() {
						@Override
						public void operationComplete(List<Pager> data, DaycareDataException e) {
							task = null;
							mPagers.clear();
							mPagers.addAll(data);
							cb.operationComplete(data, e);
						}
					});
	}

	private void fetchIncidents(final DaycareOperationComplete<List<Incident>> cb) {
		if (isCanceled.get())
			cb.operationComplete(null, null);
		else
			task = mDatasource.getIncidentsDAO().findIncidents(mMember.get(),
					new DaycareOperationComplete<List<Incident>>() {
						@Override
						public void operationComplete(List<Incident> data, DaycareDataException e) {
							task = null;
							mIncidents.clear();
							mIncidents.addAll(data);
							cb.operationComplete(data, e);
						}
					});
	}

	private void fetchExpectedAreas(final DaycareOperationComplete<List<WorkoutArea>> cb) {
		Member checkedInBy = mMember.get().getPCheckInBy();
		if (isCanceled.get() || checkedInBy == null)
			cb.operationComplete(null, null);
		else
			task = mDatasource.getMembersDAO().listExpectedAreas(checkedInBy,
					new DaycareOperationComplete<List<WorkoutArea>>() {
						@Override
						public void operationComplete(List<WorkoutArea> data, DaycareDataException e) {
							task = null;
							mExpectedAreas.clear();
							mExpectedAreas.addAll(data);
							cb.operationComplete(data, e);
						}
					});
	}
	
	private void fetchAuthorizations(final DaycareOperationComplete<List<Authorization>> cb) {
		if (DaycareHelpers.isAdult(mMember.get().getBirthDate(), 12)) {
			task = mDatasource.getAuthorizationsDAO().fetchAuthorizedChildren(mMember.get(),
					new DaycareOperationComplete<List<Authorization>>() {
						@Override
						public void operationComplete(List<Authorization> data, DaycareDataException e) {
							task = null;
							mAuthorizedFors.clear();
							mAuthorizedFors.addAll(data);
							cb.operationComplete(data, e);
						}
					});
		} else {
			task = mDatasource.getAuthorizationsDAO().fetchAuthorizedMember(mMember.get(),
					new DaycareOperationComplete<List<Authorization>>() {
						@Override
						public void operationComplete(List<Authorization> data, DaycareDataException e) {
							task = null;
							mAuthorizedMembers.clear();
							mAuthorizedMembers.addAll(data);
							cb.operationComplete(data, e);
						}
					});
		}
	}

	private void fetchSelfCheckIns(final DaycareOperationComplete<SelfCheckIn> cb) {
		task = mDatasource.getSelfCheckInsDAO().getSelfCheckInByMember(mMember.get(),
				new DaycareOperationComplete<SelfCheckIn>() {
					@Override
					public void operationComplete(SelfCheckIn data, DaycareDataException e) {
						task = null;
						mSelfCheckIn.set(data);
						cb.operationComplete(data, e);
					}
				});
	}

	public void cancel() {
		isCanceled.set(true);
		if (task != null) {
			task.cancel();
			task = null;
		}
	}

	public Member getMember() {
		return mMember.get();
	}

	public List<Incident> getIncidents() {
		return mIncidents;
	}

	public List<Cubby> getUsing() {
		DaycareHelpers.sortCubbyList(mUsing);
		return mUsing;
	}

	public List<Pager> getPagers() {
		DaycareHelpers.sortPagerList(mPagers);
		return mPagers;
	}

	public List<WorkoutArea> getExpectedAreas() {
		return mExpectedAreas;
	}
	
	public List<Authorization> getAuthorizedMembers() {
		return mAuthorizedMembers;
	}
	
	public List<Authorization> getAuthorizedFors() {
		return mAuthorizedFors;
	}

	public SelfCheckIn getSelfCheckIn() {
		return mSelfCheckIn.get();
	}

	public void setSelfCheckIn(SelfCheckIn selfCheckIn) {
		mSelfCheckIn.set(selfCheckIn);
	}

	public void setSelected(boolean isSelected) {
		mSelected.set(isSelected);
	}

	public boolean getSelected() {
		return mSelected.get();
	}

	public void setDimmed(boolean isDimmed) {
		mDimmed.set(isDimmed);
	}

	public boolean getDimmed() {
		return mDimmed.get();
	}

	public boolean getFetching() {
		return mFetching.get();
	}
}
