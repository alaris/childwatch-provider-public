package com.alaris_us.daycareprovider.data;

import android.telephony.PhoneNumberUtils;
import android.util.Patterns;

public final class PhoneNumber {

	private final String mAreaCode;
	private final String mExchange;
	private final String mSubscriber;

	public static PhoneNumber getNewInstance(String phoneNumber) {

		if (phoneNumber == null)
			return null;
		// Check phone number pattern
		if (!Patterns.PHONE.matcher(phoneNumber).matches())
			return null;
		// Strip phone number
		phoneNumber = PhoneNumberUtils.stripSeparators(phoneNumber);
		// Check number length
		if (phoneNumber.length() != 10)
			return null;
		// Valid format. Populate fields
		return new PhoneNumber(phoneNumber.substring(0, 3), phoneNumber.substring(3, 6), phoneNumber.substring(6, 10));
	}

	public String getAreaCode() {

		return mAreaCode;
	}

	public String getExchange() {

		return mExchange;
	}

	public String getSubscriber() {

		return mSubscriber;
	}

	public String getDashSeparatedNumber() {

		StringBuilder sb = new StringBuilder();
		return sb.append(getExchange()).append('-').append(getSubscriber()).toString();
	}

	public String getDashedSeparatedNumberWithAreaCode() {

		StringBuilder sb = new StringBuilder(getAreaCode());
		return sb.append('-').append(getDashSeparatedNumber()).toString();
	}

	private PhoneNumber(String areaCode, String exchange, String subscriber) {

		mAreaCode = areaCode;
		mExchange = exchange;
		mSubscriber = subscriber;
	}
}
