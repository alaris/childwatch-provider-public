package com.alaris_us.daycareprovider.data;

import java.util.List;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Facility;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

public class AdmissionAndFacility {

	private String mAdmissionBalance;
	private final TrackableBoolean mIsSelected;
	private TrackableField<Facility> mFacility;

	public AdmissionAndFacility(String admissionBalance, boolean isSelected, Facility facility) {
		mAdmissionBalance = admissionBalance;
		mIsSelected = new TrackableBoolean(isSelected);
		mFacility = new TrackableField<Facility>(facility);
	}

	public String getAdmission() {
		return mAdmissionBalance;
	}

	public void setAdmission(String balance) {
		mAdmissionBalance = balance;
	}

	public void toggleSelected() {
		mIsSelected.set(!getIsSelected());
	}

	public void setIsSelected(boolean isSelected) {
		mIsSelected.set(isSelected);
	}

	public boolean getIsSelected() {
		return mIsSelected.get();
	}
	
	public Facility getFacility() {
		return mFacility.get();
	}
	
	public void setFacility(Facility facility) {
		mFacility.set(facility);
	}
}
