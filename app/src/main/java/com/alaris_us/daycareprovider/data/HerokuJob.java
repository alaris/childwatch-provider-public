package com.alaris_us.daycareprovider.data;

import org.json.JSONException;
import org.json.JSONObject;

import com.alaris_us.daycareprovider.heroku.HerokuDataObject;

import android.util.Log;

public class HerokuJob extends HerokuDataObject {

	public HerokuJob(JSONObject json) {
		super(json);
	}
	
	public HerokuJob(HerokuDataObject obj) {
		super(obj);
	}

	public int getJobId() {
		int id = -1;
		try {
			id = getData().getInt("id");
		} catch (JSONException e) {
			Log.e(this.getClass().getSimpleName(), e.getMessage());
		}
		return id;
	}

	public String getSyncType() {
		String syncType = "";
		try {
			syncType = getData().getString("syncType");
		} catch (JSONException e) {
			Log.e(this.getClass().getSimpleName(), e.getMessage());
		}
		return syncType;
	}

}
