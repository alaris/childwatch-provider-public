package com.alaris_us.daycareprovider.data;

import com.alaris_us.daycaredata.to.Member;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;

public class AuthorizedMember {

	private final TrackableField<Member> mMember;
	private final TrackableBoolean mIsSelected;

	public AuthorizedMember(Member member, boolean isSelected) {
		mMember = new TrackableField<Member>(member);
		mIsSelected = new TrackableBoolean(isSelected);
	}

	public Member getMember() {
		return mMember.get();
	}

	public void toggleSelected() {
		mIsSelected.set(!getIsSelected());
	}

	public void setIsSelected(boolean isSelected) {
		mIsSelected.set(isSelected);
	}

	public boolean getIsSelected() {
		return mIsSelected.get();
	}
}
