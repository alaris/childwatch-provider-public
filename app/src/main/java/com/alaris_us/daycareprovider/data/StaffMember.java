package com.alaris_us.daycareprovider.data;

import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Staff;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class StaffMember {

	private TrackableField<Staff> mStaff;
	private final TrackableBoolean mIsSelected;
	private TrackableCollection<Room> mFacilityRooms;
	private TrackableField<Facility> mFacility;

	public StaffMember(Staff staff) {
		this(staff, false, null, null);
	}

	public StaffMember(Staff staff, boolean isSelected, List<Room> rooms, Facility facility) {
		mStaff = new TrackableField<Staff>(staff);
		mIsSelected = new TrackableBoolean(isSelected);
		mFacilityRooms = new TrackableCollection<Room>(rooms);
		mFacility = new TrackableField<Facility>(facility);
	}

	public Staff getStaff() {
		return mStaff.get();
	}

	public void toggleSelected() {
		mIsSelected.set(!getIsSelected());
	}

	public void setIsSelected(boolean isSelected) {
		mIsSelected.set(isSelected);
	}

	public boolean getIsSelected() {
		return mIsSelected.get();
	}
	
	public List<Room> getFacilityRooms() {
		return mFacilityRooms; 
	}
	
	public void setFacilityRooms(List<Room> rooms) {
		for (Room r : rooms)
			mFacilityRooms.add(r);
	}
	
	public Facility getFacility() {
		return mFacility.get();
	}
}
