package com.alaris_us.daycareprovider.view;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.daycareprovider.data.StaffMember;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;

public class StaffPopupListItem extends LinearLayout implements BoundUi<StaffMember> {

	private TrackableField<StaffMember> mData;

	public StaffPopupListItem(Context context) {
		super(context);
		View.inflate(getContext(), R.layout.list_iconcheckbox_stackedtext, this);
	}
	
	public Bitmap getImageIconImageBitmap() {
		Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(),
				R.drawable.photo_not_available);
		if (getData().getImage() == null)
			return icon ;
		else
			return getData().getImage();
	}

	public String getTextFirstLineText() {
		return getData().getFirstName();
	}

	public String getTextSecondLineText() {
		return getData().getLastName();
	}

	public int getCheckmarkVisibility() {
		return mData.get().getIsSelected() ? View.VISIBLE : View.INVISIBLE;
	}
	
	public void setIsClockedIn(boolean isClockedIn) {
		//mIsClockedIn.set(isClockedIn);
		mData.get().setIsSelected(isClockedIn);
	}

	public boolean getIsClockedIn() {
		//return mIsClockedIn.get();
		return mData.get().getIsSelected();
	}
	
	public Staff getData() {
		return mData.get().getStaff();
	}
	
	public List<String> getSelectedRoomNameList() {
		List<String> roomNameList = new ArrayList<String>();
		for (Room r : mData.get().getFacilityRooms()) {
			roomNameList.add(r.getName());
		}
		return roomNameList;
	}
	
	public OnItemSelectedListener getSelectRoomNameListOnItemSelectedListener() {
		return new SelectRoomNameListOnItemSelectedListener();
	}

	public class SelectRoomNameListOnItemSelectedListener implements OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			mData.get().getStaff().setPRoom(mData.get().getFacilityRooms().get(position));
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
		}
	}

	public int getRoomSpinnerVisibility() {
		boolean usesRoomRatios = false;
		if (mData.get().getFacility().getRatioType() != null)
			if (mData.get().getFacility().getRatioType().equals("FACILITYANDROOM") || mData.get().getFacility().getRatioType().equals("FACILITY")) {
				usesRoomRatios = false;
			}
		else {
			for (Room r : mData.get().getFacilityRooms()) {
				if (r.getRatio().intValue() != 0) {
					usesRoomRatios = true;
					break;
				}
			}
		}
		if (!usesRoomRatios)
			return View.GONE;
		else {
			if (mData.get().getIsSelected())
				return View.VISIBLE;
			else
				return View.GONE;
		}
	}
	
	public int getRoomSpinnerDefaultSelection() {
		int position = 0;
		if (mData.get().getStaff().getPRoom() != null) {
			String roomName = mData.get().getStaff().getPRoom().getName();
			for (int i = 0; i < mData.get().getFacilityRooms().size(); i++) {
				if (roomName.equals(mData.get().getFacilityRooms().get(i).getName()))
					position = i;
			}
		}
		return position;
	}
	
	@Override
	public void bind(StaffMember dataSource) {
		mData = new TrackableField<StaffMember>(dataSource);

		// Bind Views
		UiBinder.bind(this, R.id.iconcheckbox_stackedtext_imageIcon, "ImageBitmap", this, "ImageIconImageBitmap");
		UiBinder.bind(this, R.id.iconcheckbox_stackedtext_textFirstLine, "Text", this, "TextFirstLineText");
		UiBinder.bind(this, R.id.iconcheckbox_stackedtext_textSecondLine, "Text", this, "TextSecondLineText");
		UiBinder.bind(this, R.id.iconcheckbox_stackedtext_checkbox, "Visibility", this, "CheckmarkVisibility");
		UiBinder.bind(this, R.id.iconcheckbox_stackedtext_spinner, "Adapter", this, "SelectedRoomNameList", new AdapterConverter(SelectRoomDropDown.class));
		UiBinder.bind(this, R.id.iconcheckbox_stackedtext_spinner, "OnItemSelectedListener", this, "SelectRoomNameListOnItemSelectedListener");
		UiBinder.bind(this, R.id.iconcheckbox_stackedtext_spinner, "Visibility", this, "RoomSpinnerVisibility");
		UiBinder.bind(this, R.id.iconcheckbox_stackedtext_spinner, "Selection", this, "RoomSpinnerDefaultSelection");
		
	}

}
