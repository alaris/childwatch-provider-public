package com.alaris_us.daycareprovider.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.bindroid.BindingMode;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class StringList extends TextView implements BoundUi<String> {

	private TrackableField<String> mData;

	public String getData() {
		return mData.get();
	}

	public StringList(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public StringList(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public StringList(Context context) {
		super(context);
	}

	@Override
	public void bind(String dataSource) {
		mData = new TrackableField<String>(dataSource);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Text")), this, "Data", BindingMode.ONE_WAY);

	}

}
