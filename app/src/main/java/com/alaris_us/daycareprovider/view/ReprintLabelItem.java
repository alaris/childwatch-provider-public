package com.alaris_us.daycareprovider.view;

import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

public class ReprintLabelItem extends LinearLayout implements BoundUi<ReprintLabelItemModel> {

	private ReprintLabelItemModel mMemberData;

	public ReprintLabelItem(Context context) {

		// Android Boilerplate
		super(context);
		View.inflate(getContext(), R.layout.membernameage_item, this);

		// Init Fields
		mMemberData = new ReprintLabelItemModel();

	}

	public ReprintLabelItemModel getReprintLabelItemModel() {
		return mMemberData;
	}

	@Override
	public void bind(ReprintLabelItemModel dataSource) {
		mMemberData = dataSource;

		// Bind Views
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Photo), "ImageBitmap")),
				mMemberData, "MemberImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Name), "Text")), mMemberData,
				"MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Age), "Text")), mMemberData,
				"MemberAge", BindingMode.ONE_WAY);
		UiBinder.bind(
				UiProperty.make(new ReflectedProperty(findViewById(R.id.MemberNameAge_ListItem), "BackgroundResource")),
				mMemberData, "BackgroundDrawable", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Checkmark), "Visibility")),
				mMemberData, "CheckMarkVisibility", BindingMode.ONE_WAY);
	}
}
