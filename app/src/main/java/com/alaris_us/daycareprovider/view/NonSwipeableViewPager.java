package com.alaris_us.daycareprovider.view;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class NonSwipeableViewPager extends ViewPager {

	public NonSwipeableViewPager(Context context) {

		super(context);
	}

	public NonSwipeableViewPager(Context context, AttributeSet attrs) {

		super(context, attrs);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {

		// Never allow swiping to switch between pages
		return false;
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		// No click or swipe
		return false;
	}

}
