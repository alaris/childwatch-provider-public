package com.alaris_us.daycareprovider.view;

import java.util.Locale;

import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.BoolConverter;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class DaycareMemberListItem extends LinearLayout implements BoundUi<Member> {

	private final TrackableField<Member> mMemberData;
	private final TrackableBoolean mIsParent;
	private final static String YEAR_UNIT = "yr";
	private final static String MONTH_UNIT = "mo";

	public DaycareMemberListItem(Context context) {

		// Android Boilerplate
		super(context);
		View.inflate(getContext(), R.layout.list_daxkomember, this);
		// Init Fields
		mMemberData = new TrackableField<Member>();
		mIsParent = new TrackableBoolean();
		// Bind Views
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Name), "Text")), this,
				"MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Age), "Text")), this, "Age",
				BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Gender), "Text")), this,
				"Member.Gender", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Parent), "Visibility")), this,
				"IsParent", BindingMode.ONE_WAY, new BoolConverter());
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Child), "Visibility")), this,
				"IsParent", BindingMode.ONE_WAY, new BoolConverter(true));
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_MemberID), "Text")), this,
				"Member.MemberId", BindingMode.ONE_WAY);
	}

	public boolean getIsParent() {

		return mIsParent.get();
	}

	public String getMemberName() {

		Member d = getMember();
		return WordUtils.capitalizeFully(d.getFirstName() + " " + d.getLastName());
	}

	public String getMemberId() {

		Member d = getMember();
		return d.getMemberID();
	}

	public String getAge() {

		LocalDate bDate = LocalDate.fromDateFields(getMember().getBirthDate());
		Period age = new Period(bDate, LocalDate.now());
		int difference = age.getYears();
		String unit = YEAR_UNIT;
		if (difference > 12) {
			mIsParent.set(true);
		} else {
			mIsParent.set(false);
		}
		if (difference < 1) {
			difference = age.getMonths();
			unit = MONTH_UNIT;
		}
		return String.format(Locale.getDefault(), "%d%s", difference, unit);
	}

	public Member getMember() {

		return mMemberData.get();
	}

	@Override
	public void bind(Member dataSource) {

		mMemberData.set(dataSource);
	}
}
