package com.alaris_us.daycareprovider.view;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareprovider_dev.R;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FilterTabs extends LinearLayout {
	List<FilterTab> tabsList = new ArrayList<FilterTab>();
	FilterTab currentTab;
	TabClickListener listener;

	public FilterTabs(Context context) {
		super(context);
		init();
	}

	public void show() {
		this.setVisibility(View.VISIBLE);
	}

	public void hide() {
		this.setVisibility(View.GONE);
	}

	public FilterTabs(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public FilterTabs(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public void init() {
		listener = new TabClickListener();
	}

	private View getDivider() {
		return LayoutInflater.from(getContext()).inflate(R.layout.view_divider_line, this, false);
	}

	public FilterTab getNewTab(String text) {
		FilterTab tab = new FilterTab(text);
		return tab;
	}

	public void addTab(FilterTab tab) {
		tab.getView().setOnClickListener(listener);
		if (tabsList.size() > 0)
			this.addView(getDivider());
		else
			setCurrentTab(tab);
		tabsList.add(tab);
		this.addView(tab.getView());
	}

	public void clearTabs() {
		this.removeAllViews();
		currentTab = null;
		tabsList.clear();
	}

	public void setCurrentTab(FilterTab tab) {
		if (currentTab != null)
			currentTab.getView().setBackgroundResource(0);
		currentTab = tab;
		currentTab.getView().setBackgroundColor(Color.parseColor("#6db2e1"));
	}

	public FilterTab getCurrentTab() {
		return currentTab;
	}

	public List<FilterTab> getTabsList() {
		return tabsList;
	}

	private class TabClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			for (FilterTab tab : tabsList) {
				if (tab.getView().equals(v)) {
					tab.click();
					setCurrentTab(tab);
				}
			}
		}
	}

	public interface OnFilterTabClick {
		public void onClick(FilterTab tab);
	}

	public class FilterTab {
		private TextView view;
		private OnFilterTabClick clickListener;

		public FilterTab(String text) {
			view = new TextView(getContext());
			view.setText(text);
			view.setTextSize(13);
			view.setGravity(Gravity.CENTER);
			LayoutParams lp = new LayoutParams(0, LayoutParams.MATCH_PARENT, 1);
			view.setLayoutParams(lp);
		}

		public View getView() {
			return view;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((clickListener == null) ? 0 : clickListener.hashCode());
			result = prime * result + ((view == null) ? 0 : view.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FilterTab other = (FilterTab) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (clickListener == null) {
				if (other.clickListener != null)
					return false;
			} else if (!clickListener.equals(other.clickListener))
				return false;
			if (view == null) {
				if (other.view != null)
					return false;
			} else if (!view.equals(other.view))
				return false;
			return true;
		}

		public void click() {
			if (clickListener != null)
				clickListener.onClick(this);
		}

		public void setOnFilterTabClick(OnFilterTabClick listener) {
			clickListener = listener;
		}

		private FilterTabs getOuterType() {
			return FilterTabs.this;
		}
	}
}
