package com.alaris_us.daycareprovider.view;

import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class LinearListView extends LinearLayout {
	public LinearListView(Context context) {
		super(context);
		init();
	}

	public LinearListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public LinearListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	private void init(){
		/*this.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
		this.setDividerDrawable(getContext().getResources().getDrawable(R.drawable.shape_circle));
		this.setDividerPadding();*/
	}
	
	public void setChildrenViews(List<? extends View> views){
		this.removeAllViews();
		addChildrenViews(views);
	}
	
	public void addChildrenViews(List<? extends View> views){
		for(View view : views)
			this.addView(view);
	}

}
