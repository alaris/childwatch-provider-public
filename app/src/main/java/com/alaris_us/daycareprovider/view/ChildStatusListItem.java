package com.alaris_us.daycareprovider.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.Minutes;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

public class ChildStatusListItem extends LinearLayout implements BoundUi<MemberAndCubbies>, Runnable {

	private final TrackableField<MemberAndCubbies> mMemberData;
	private final TrackableField<DateTime> mNow;

	private final static String YEAR_UNIT = "yr";
	private final static String MONTH_UNIT = "mo";
	private final static int REFRESH_RATE = 60000;

	public ChildStatusListItem(Context context) {

		// Android Boilerplate
		super(context);
		View.inflate(getContext(), R.layout.list_childstatus, this);

		// Init Fields
		mMemberData = new TrackableField<MemberAndCubbies>();
		mNow = new TrackableField<DateTime>();

		// Bind Views
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Photo), "ImageBitmap")), this,
				"MemberImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Name), "Text")), this,
				"MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Age), "Text")), this, "Age",
				BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Time), "Text")), this,
				"CheckinStats", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Usage), "Text")), this,
				"UsageString", BindingMode.ONE_WAY);
		/*UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Pager), "Text")), this,
				"PagerString", BindingMode.ONE_WAY);*/
		/*
		 * UiBinder.bind(UiProperty.make(new ReflectedProperty(
		 * findViewById(R.id.TextView_EstimatedPickup), "Text")), this,
		 * "EstimatedPickupString", BindingMode.ONE_WAY);
		 */
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Medical), "Visibility")), this,
				"HasMedicalCondition", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Alert), "Visibility")), this,
				"HasAlert", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Incident), "Visibility")), this,
				"HasIncident", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Override), "Visibility")), this,
				"HasOverride", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Notes), "Visibility")), this,
				"HasNotes", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Potty), "Visibility")), this,
				"IsPottyTrained", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Diaper), "Visibility")), this,
				"AllowsDiaperChange", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Feeding), "Visibility")), this,
				"AllowsFeeding", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Enrichment), "Visibility")), this,
				"AllowsEnrichment", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Bag), "Visibility")), this,
				"HasBag", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Form), "Visibility")), this,
				"HasForm", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_SelfCheckIn), "Visibility")), this,
				"CanSelfCheckIn", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Alpha")), this, "Alpha", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Checkmark), "Visibility")),
				this, "CheckMarkVisibility", BindingMode.ONE_WAY);
	}

	public float getAlpha() {
		return mMemberData.get().getDimmed() ? 0.5f : 1;
	}
	
	public int getCheckMarkVisibility() {
		return mMemberData.get().getSelected() ? View.VISIBLE : View.INVISIBLE;
	}

	public int getHasOverride() {
		return (getMember().getWasOverridden()) ? View.VISIBLE : View.GONE;

	}

	public int getHasIncident() {
		List<Incident> incidents = mMemberData.get().getIncidents();
		if (incidents != null) {
			boolean hasUnacknowleged = false;
			for (Incident incident : incidents)
				hasUnacknowleged = hasUnacknowleged
						|| (!incident.getIncident().isEmpty() && !incident.getAcknowledged());
			return hasUnacknowleged ? View.VISIBLE : View.GONE;
		} else {
			return View.GONE;
		}

	}

	public int getHasMedicalCondition() {

		String mc = getMember().getMedicalConcerns();

		return (mc == null || mc.isEmpty()) ? View.GONE : View.VISIBLE;

	}

	public int getHasNotes() {

		String note = getMember().getNotes();

		return (note == null || note.isEmpty()) ? View.GONE : View.VISIBLE;

	}

	public int getHasAlert() {

		if (getMinutesLeft() <= 0)
			return View.VISIBLE;
		else
			return View.GONE;
	}

	public int getIsPottyTrained() {

		boolean pottyTrained = getMember().getIsPottyTrained();
		return (pottyTrained) ? View.VISIBLE : View.GONE;
	}
	
	public int getAllowsDiaperChange() {

		if (getMember().getAllowsDiaperChange() == null)
			return View.GONE;
		else {
			boolean allowsDiaperChange = getMember().getAllowsDiaperChange();
			return (allowsDiaperChange) ? View.VISIBLE : View.GONE;
		}
	}
	
	public int getAllowsFeeding() {

		if (getMember().getAllowsFeeding() == null)
			return View.GONE;
		else {
			boolean allowsFeeding = getMember().getAllowsFeeding();
			return (allowsFeeding) ? View.VISIBLE : View.GONE;
		}
	}
	
	public int getAllowsEnrichment() {

		if (getMember().getAllowsEnrichment() == null)
			return View.GONE;
		else {
			boolean allowsEnrichment = getMember().getAllowsEnrichment();
			return (allowsEnrichment) ? View.VISIBLE : View.GONE;
		}
	}
	
	public int getHasBag() {

		if (getMember().getHasBag() == null)
			return View.GONE;
		else {
			boolean hasBag = getMember().getHasBag();
			return (hasBag) ? View.VISIBLE : View.GONE;
		}
	}

	public int getHasForm() {

		if (getMember().getHasForm() == null)
			return View.GONE;
		else {
			boolean hasForm = getMember().getHasForm();
			return (hasForm) ? View.VISIBLE : View.GONE;
		}
	}

	public int getCanSelfCheckIn() {

		if (mMemberData.get().getSelfCheckIn() == null)
			return View.GONE;
		else
			return View.VISIBLE;
	}

	public String getMemberName() {

		Member d = getMember();

		return WordUtils.capitalizeFully(d.getFirstName() + " " + d.getLastName());

	}

	public String getAge() {

		LocalDate bDate = LocalDate.fromDateFields(getMember().getBirthDate());
		Period age = new Period(bDate, LocalDate.now());

		int difference = age.getYears();
		String unit = YEAR_UNIT;

		if (difference < 1) {
			difference = age.getMonths();
			unit = MONTH_UNIT;
		}

		return String.format(Locale.getDefault(), "Age %d%s", difference, unit);

	}

	public int getMinutesLeft() {

		DateTime lastCheckIn = new DateTime(getMember().getLastCheckIn());

		int minutesPassed = Minutes.minutesBetween(lastCheckIn, getNow()).getMinutes();
		return getMember().getMinutesLeft().intValue() - minutesPassed;

	}

	public String getCheckinStats() {

		DateTime lastCheckIn = new DateTime(getMember().getLastCheckIn());
		Interval timeIn = new Interval(lastCheckIn, getNow());

		DateTimeFormatter dfmt = DateTimeFormat.forPattern("h:mm a");
		PeriodFormatter pfmt = new PeriodFormatterBuilder().appendDays().appendSuffix("d").appendHours()
				.appendSuffix("hr").appendMinutes().appendSuffix("min").toFormatter();

		return new StringBuilder().append(timeIn.toPeriod().toString(pfmt)).append(" (")
				.append(lastCheckIn.toString(dfmt)).append(")").toString();

	}

	public String getUsageString() {

		String returning = "";
		List<Cubby> cubbiesUsed = getUsing();
		List<Pager> pagersUsed =  getPagers();
		String estimatedPickup = getEstimatedPickup();

		if (cubbiesUsed.size() < 1 && pagersUsed.size() < 1 && estimatedPickup == null)
			returning = "";
		else if (cubbiesUsed.size() > 0 && pagersUsed.size() > 0) {
			StringBuilder toReturn = new StringBuilder("Cubby: ");
			for (Cubby c : cubbiesUsed) {
				toReturn.append(c.getName()).append(", ");
			}
			toReturn.append("Pager: ");
			for (Pager p : pagersUsed) {
				toReturn.append(p.getName()).append(", ");
			}
			returning = toReturn.toString().substring(0, toReturn.length() - 2);
		} else if (cubbiesUsed.size() > 0) {
			StringBuilder toReturn = new StringBuilder("Cubby: ");
			for (Cubby c : cubbiesUsed) {
				toReturn.append(c.getName()).append(", ");
			}
			returning = toReturn.toString().substring(0, toReturn.length() - 2);
		} else if (pagersUsed.size() > 0) {
			StringBuilder toReturn = new StringBuilder("Pager: ");
			for (Pager p : pagersUsed) {
				toReturn.append(p.getName()).append(", ");
			}
			returning = toReturn.toString().substring(0, toReturn.length() - 2);
		} else if (!estimatedPickup.isEmpty()) {
			StringBuilder toReturn = new StringBuilder("Est. Pickup: ");
			returning = toReturn.append(estimatedPickup).toString();
		}
		return returning;
	}

	public Bitmap getMemberImage() {

		Bitmap image = getMember().getImage();

		return (image == null) ? BitmapFactory.decodeResource(getResources(), R.drawable.photo_not_available) : image;

	}

	public void setNow(DateTime now) {
		mNow.set(now);
	}

	public DateTime getNow() {
		return mNow.get();
	}

	public Member getMember() {

		return mMemberData.get().getMember();
	}

	public List<Cubby> getUsing() {
		return mMemberData.get().getUsing();
	}

	public List<Pager> getPagers() {
		return mMemberData.get().getPagers();
	}

	public String getEstimatedPickup() {
		return mMemberData.get().getMember().getEstimatedPickupTime();
	}
	
	@Override
	public void bind(MemberAndCubbies dataSource) {

		mMemberData.set(dataSource);

	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		getHandler().post(this);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		getHandler().removeCallbacks(this);
	}

	@Override
	public void run() {
		setNow(new DateTime());
		postDelayed(this, REFRESH_RATE);
	}

}
