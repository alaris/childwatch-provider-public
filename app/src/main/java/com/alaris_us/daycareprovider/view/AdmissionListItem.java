/*
package com.alaris_us.daycareprovider.view;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycareprovider.data.AdmissionAndFacility;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.AdapterConverter;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class AdmissionListItem extends RelativeLayout implements BoundUi<AdmissionAndFacility>{

	private final TrackableField<AdmissionAndFacility> mAdmissionAndFacility;
	private View v;

	public AdmissionListItem(Context context) {

		// Android Boilerplate
		super(context);
		v = View.inflate(getContext(), R.layout.familysettings_admission_item, this);

		// Init Fields
		mAdmissionAndFacility = new TrackableField<AdmissionAndFacility>();

		// Bind Views
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.familysettings_numOfAdmissions), "Text")), this,
				"NumOfAdmissions", BindingMode.TWO_WAY);
		*/
/*UiBinder.bind(this, R.id.familysettings_numOfAdmissions, "Text", this,
				"NumOfAdmissions", BindingMode.ONE_WAY);*//*

		UiBinder.bind(this, R.id.familysettings_facility_spinner, "Adapter", this, "FacilitySelectionList",
				new AdapterConverter(SelectFacilityDropDown.class));
		UiBinder.bind(this, R.id.familysettings_facility_spinner, "OnItemSelectedListener", this,
				"FacilityOnItemSelectedListener");
		UiBinder.bind(this, R.id.familysettings_facility_spinner, "Selection", this, "DefaultSelection");
		UiBinder.bind(this, R.id.familysettings_delete, "OnClickListener", this, "OnDeleteClickListener");
	}

	public String getNumOfAdmissions() {
		return mAdmissionAndFacility.get().getAdmission();
	}
	
	public List<String> getFacilitySelectionList() {
		List<String> facilityNames = new ArrayList<>();
		for (Facility fac : mAdmissionAndFacility.get().getFacility()) {
			facilityNames.add(fac.getYMCAName());
		}
		return facilityNames;
	}
	
	public OnClickListener getOnDeleteClickListener() {
		return new OnDeleteClickListener();
	}
	
	public class OnDeleteClickListener implements OnClickListener{
		@Override
		public void onClick(View v) {
			Toast.makeText(v.getContext(), "Deleted", Toast.LENGTH_LONG ).show();
		}
	}
	
	public int getDefaultSelection() {
		//Admission admit = mAdmissionAndFacilities.get().getAdmission();
		Facility facilityForAdmission = null;
		int position = 0;

		*/
/*if (admit.getFacility() != null) {
			for (Facility fac : mAdmissionAndFacilities.get().getFacilities())
				if (fac.getObjectId().equals(admit.getFacility().getObjectId())) {
					position = mAdmissionAndFacilities.get().getFacilities().indexOf(fac);
					break;
				}
		}*//*

		return position;
	}
	
	@Override
	public void bind(AdmissionAndFacilities dataSource) {
		mAdmissionAndFacility.set(dataSource);
		*/
/*EditText et = (EditText) v.findViewById(R.id.familysettings_numOfAdmissions);
		et.setText(mAdmissionAndFacilities.get().getAdmission().getAdmissions().toString());*//*

	}
}
*/
