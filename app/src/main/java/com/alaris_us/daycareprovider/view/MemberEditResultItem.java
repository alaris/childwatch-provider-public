package com.alaris_us.daycareprovider.view;

import org.apache.commons.lang3.text.WordUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.LinearLayout;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.bindroid.BindingMode;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class MemberEditResultItem extends LinearLayout implements BoundUi<MemberAndCubbies>, Runnable {

	private final TrackableField<MemberAndCubbies> mMemberData;

	private final static int REFRESH_RATE = 60000;

	public MemberEditResultItem(Context context) {

		// Android Boilerplate
		super(context);
		View.inflate(getContext(), R.layout.membername_item, this);

		// Init Fields
		mMemberData = new TrackableField<MemberAndCubbies>();

		// Bind Views
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Photo), "ImageBitmap")), this,
				"MemberImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Name), "Text")), this,
				"MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Alpha")), this, "ItemAlpha", BindingMode.ONE_WAY);
	}

	public float getItemAlpha() {
		return getIsSelected() ? 1f : 0.5f;
	}

	public String getMemberName() {

		Member d = getMember();

		return WordUtils.capitalizeFully(d.getFirstName() + " " + d.getLastName());

	}

	public Bitmap getMemberImage() {

		Bitmap image = getMember().getImage();

		return (image == null) ? BitmapFactory.decodeResource(getResources(), R.drawable.photo_not_available) : image;

	}

	public Member getMember() {

		return mMemberData.get().getMember();
	}

	public boolean getIsSelected() {
		return mMemberData.get().getSelected();
	}

	@Override
	public void bind(MemberAndCubbies dataSource) {

		mMemberData.set(dataSource);

	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		getHandler().post(this);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		getHandler().removeCallbacks(this);
	}

	@Override
	public void run() {
		postDelayed(this, REFRESH_RATE);
	}

}
