package com.alaris_us.daycareprovider.view;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycareprovider.view.TabWithIcons.SortDirection;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TabHost;

public class ChildListTabHost extends TabHost {
	List<ChildListTab> tabs = new ArrayList<ChildListTab>();

	public ChildListTabHost(Context context) {
		super(context);
	}

	@Override
	public void setCurrentTab(int index) {
		int lastIndex = getCurrentTab();
		super.setCurrentTab(index);

		ChildListTab tab = tabs.get(index);
		if (lastIndex > -1 && lastIndex != index) {
			tabs.get(lastIndex).getView().showSortIcon(false);
		}

		if (lastIndex == index) {
			tab.click();
		} else {
			tab.getView().setSortDirection(SortDirection.ASCENDING);
			tab.getView().showSortIcon(true);
		}

	}

	public ChildListTabHost(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void addTab(ChildListTab tab) {
		tabs.add(tab);
		super.addTab(tab.getTabSpec());
	}

	public ChildListTab newTab(String name) {
		return new ChildListTab(name);
	}

	public ChildListTab getTab(int tabIndex) {
		return tabs.get(tabIndex);
	}

	public class ChildListTab {
		private TabSpec tabSpec;
		private TabWithIcons view;
		private OnClickListener listener;

		public ChildListTab(String name) {
			tabSpec = ChildListTabHost.this.newTabSpec(name + " Tab");
			view = new TabWithIcons(ChildListTabHost.this.getContext());
			view.setText(name);
			tabSpec.setIndicator(view);
		}

		public void click() {
			if (listener != null)
				listener.onClick(view);
		}

		public void setContent(int id) {
			tabSpec.setContent(id);
		}

		public TabWithIcons getView() {
			return view;
		}

		public TabSpec getTabSpec() {
			return tabSpec;
		}

		public String getTag() {
			return tabSpec.getTag();
		}

		public void setOnClickListener(OnClickListener listener) {
			this.listener = listener;
		}

		public void setOnLongClickListener(OnLongClickListener listener) {
			getView().setOnLongClickListener(listener);
		}
	}
}
