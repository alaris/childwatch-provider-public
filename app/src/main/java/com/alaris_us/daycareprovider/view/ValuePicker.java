package com.alaris_us.daycareprovider.view;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.NumberPicker;

public class ValuePicker<T> extends NumberPicker {
	private String noValuesMessage = "";
	private GetNameConverter<T> toStringConverter;

	public ValuePicker(Context context) {
		super(context);
		init();
	}

	public ValuePicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public ValuePicker(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	@SuppressWarnings("unchecked")
	private void init() {
		toStringConverter = (GetNameConverter<T>) new ToStringConverter();
	}

	public void setNoValuesMessage(String message) {
		noValuesMessage = message;
	}

	public void setDisplayedValues(List<T> values) {
		List<String> vals = new ArrayList<String>();
		for (T val : values) {
			vals.add(toStringConverter.toString(val));
		}
		setDisplayedValues((String[]) vals.toArray(new String[0]));
	}

	@Override
	public void setDisplayedValues(String[] displayedValues) {
		if (displayedValues.length > 0) {
			super.setDisplayedValues(displayedValues);
			setMaxValue(displayedValues.length - 1);
		} else {
			super.setDisplayedValues(new String[] { noValuesMessage });
			setMaxValue(0);
		}
		setMinValue(0);
		setWrapSelectorWheel(true);
	}

	public void setGetNameConverter(GetNameConverter<T> converter) {
		toStringConverter = converter;
	}

	public interface GetNameConverter<T> {
		public String toString(T value);
	}

	private class ToStringConverter implements GetNameConverter<String> {
		@Override
		public String toString(String value) {
			return value;
		}
	}
}
