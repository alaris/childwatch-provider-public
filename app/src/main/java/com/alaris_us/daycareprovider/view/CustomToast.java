package com.alaris_us.daycareprovider.view;

import com.alaris_us.daycareprovider_dev.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ShowToast")
public class CustomToast {
	public final static int DURATION_SHORT = Toast.LENGTH_SHORT;
	public final static int DURATION_LONG = Toast.LENGTH_LONG;
	public final static int STYLE_ERROR = 2;
	public final static int STYLE_SUCCESS = 3;
	private final static int COLOR_ERROR = R.color.toast_text_error;
	private final static int COLOR_SUCCESS = R.color.toast_text_success;

	private int mColorError;
	private int mColorSuccess;
	private Toast mToast;

	public CustomToast(Context context) {
		mColorError = context.getResources().getColor(COLOR_ERROR);
		mColorSuccess = context.getResources().getColor(COLOR_SUCCESS);
		mToast = Toast.makeText(context, "", DURATION_SHORT);
	}

	public CustomToast setText(String text) {
		mToast.setText(text);
		return this;
	}

	public CustomToast setDuration(int duration) {
		mToast.setDuration(duration);
		return this;
	}

	public void show() {
		mToast.show();
	}

	public CustomToast setStyle(int style) {
		TextView textView = (TextView) mToast.getView().findViewById(android.R.id.message);
		textView.setTextColor(style == STYLE_SUCCESS ? mColorSuccess : mColorError);
		return this;
	}

	public static CustomToast makeToast(Context context) {
		return new CustomToast(context);
	}

	public static CustomToast makeToast(Context context, String text) {
		return new CustomToast(context).setText(text);
	}

	public static CustomToast makeToast(Context context, String text, int duration) {
		return new CustomToast(context).setText(text).setDuration(duration);
	}

	public static CustomToast makeToast(Context context, String text, int duration, int style) {
		return new CustomToast(context).setText(text).setDuration(duration).setStyle(style);
	}
}
