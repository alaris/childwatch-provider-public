package com.alaris_us.daycareprovider.view;

import java.util.Locale;

import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;

public class ReprintLabelItemModel {

	private TrackableBoolean m_isSelected;
	private TrackableField<Member> mMember;
	private final static String YEAR_UNIT = "yr";
	private final static String MONTH_UNIT = "mo";
	private Context mContext;

	public ReprintLabelItemModel() {

		this(null, null, false);
	}

	public ReprintLabelItemModel(Context context, Member member, boolean isSelected) {

		mMember = new TrackableField<Member>(member);
		m_isSelected = new TrackableBoolean(isSelected);
		mContext = context;

	}

	public Member getMember() {

		return mMember.get();
	}

	public void setMember(Member member) {

		mMember.set(member);
	}

	public boolean getSelected() {

		return m_isSelected.get();
	}

	public void setSelected(boolean selected) {

		m_isSelected.set(selected);
	}

	public void toggleSelected() {
		m_isSelected.set(!m_isSelected.get());
	}

	public int getCheckMarkVisibility() {

		if (m_isSelected.get())
			return View.VISIBLE;
		else
			return View.INVISIBLE;
	}

	public String getMemberName() {

		Member d = mMember.get();

		return WordUtils.capitalizeFully(d.getFirstName() + " " + d.getLastName());

	}

	public String getMemberAge() {

		Member d = mMember.get();

		LocalDate bDate = LocalDate.fromDateFields(d.getBirthDate());
		Period age = new Period(bDate, LocalDate.now());

		int difference = age.getYears();
		String unit = YEAR_UNIT;

		if (difference < 1) {
			difference = age.getMonths();
			unit = MONTH_UNIT;
		}

		if (difference > 13)
			return "Guardian";

		return String.format(Locale.getDefault(), "Age %d%s", difference, unit);

	}

	public int getBackgroundDrawable() {
		if (DaycareHelpers.isAdult(mMember.get().getBirthDate(), 13)) {
			return R.drawable.background_list_item_guardian;
		} else {
			return R.drawable.background_list_item;
		}
	}

	public Bitmap getMemberImage() {

		Bitmap image = mMember.get().getImage();

		return (image == null) ? BitmapFactory.decodeResource(mContext.getResources(), R.drawable.photo_not_available)
				: image;

	}
}
