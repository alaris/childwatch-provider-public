package com.alaris_us.daycareprovider.view;

import com.alaris_us.daycareprovider_dev.R;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TabWithIcons extends LinearLayout {
	private ImageView leftIcon;
	private ImageView rightIcon;
	private TextView text;
	private SortDirection sortDirection = SortDirection.DESCENDING;
	private int sortDescendingRes = R.drawable.icon_arrow_down;
	private int sortAscendingRes = R.drawable.icon_arrow_up; // TODO change to
																// sort
	// up/down arrows

	public TabWithIcons(Context context) {
		super(context);
		init();
	}

	public TabWithIcons(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TabWithIcons(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	private void init() {
		inflate(getContext(), R.layout.layout_tab_text_with_icon, this);
		this.leftIcon = (ImageView) findViewById(R.id.tab_image_left);
		this.rightIcon = (ImageView) findViewById(R.id.tab_image_right);
		this.text = (TextView) findViewById(R.id.tab_text);
		showFilterIcon(false);
		showSortIcon(false);
	}

	public void showFilterIcon(Boolean isFiltered) {
		this.leftIcon.setVisibility(isFiltered ? View.VISIBLE : View.INVISIBLE);
	}

	public void setSortDirection(SortDirection direction) {
		sortDirection = direction;
		this.rightIcon
				.setImageResource(sortDirection == SortDirection.ASCENDING ? sortAscendingRes : sortDescendingRes);
	}

	public void showSortIcon(Boolean isFiltered) {
		this.rightIcon.setVisibility(isFiltered ? View.VISIBLE : View.INVISIBLE);
	}

	public void toggleSortIcon() {
		setSortDirection(sortDirection == SortDirection.ASCENDING ? SortDirection.DESCENDING : SortDirection.ASCENDING);
	}

	public SortDirection getSortDirection() {
		return sortDirection;
	}

	public void setText(String text) {
		this.text.setText(text);
	}

	public enum SortDirection {
		ASCENDING, DESCENDING
	}

}
