package com.alaris_us.daycareprovider.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

public class ViewHolder extends FrameLayout {

	public ViewHolder(Context context, AttributeSet attr) {
		super(context, attr);

	}

	public ViewHolder(Context context) {
		super(context);
	}

	public void setView(View v) {
		removeAllViews();
		addView(v);
	}

}
