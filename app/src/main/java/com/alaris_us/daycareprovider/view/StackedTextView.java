package com.alaris_us.daycareprovider.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alaris_us.daycareprovider_dev.R;

public class StackedTextView extends LinearLayout {

	TextView m_topTextView, m_bottomTextView;

	public StackedTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init(attrs);

	}

	public StackedTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

		init(attrs);

	}

	public StackedTextView(Context context) {
		super(context);

		init(null);
	}

	public void setTopText(CharSequence text) {
		m_topTextView.setText(text);
	}

	public void setBottomText(CharSequence text) {
		m_bottomTextView.setText(text);
	}

	public CharSequence getTopText() {
		return m_topTextView.getText();
	}

	public CharSequence getBottomText() {
		return m_bottomTextView.getText();
	}
	
	private void init(AttributeSet attrs) {

		if (attrs == null)
			return;

		View.inflate(getContext(), R.layout.view_stackedtextview, this);

		m_topTextView = (TextView) findViewById(R.id.topTextView);
		m_bottomTextView = (TextView) findViewById(R.id.BottomTextView);

		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.StackedTextView);

		String topText = a.getString(R.styleable.StackedTextView_topText);
		String bottomText = a.getString(R.styleable.StackedTextView_bottomText);

		if (topText != null)
			m_topTextView.setText(topText);
		if (bottomText != null)
			m_bottomTextView.setText(bottomText);

		a.recycle();

	}

}
