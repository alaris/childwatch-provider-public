package com.alaris_us.daycareprovider.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.LinearLayout;

import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;

public class SelectRoomPopupList extends LinearLayout implements BoundUi<Room> {

	private TrackableField<Room> mData;

	public SelectRoomPopupList(Context context) {
		super(context);
		View.inflate(getContext(), R.layout.list_icon_stackedtext, this);

	}

	public Bitmap getImageIconImageBitmap() {
		return getData().getImage();
	}

	public String getTextFirstLineText() {
		return getData().getName();
	}

	public String getTextSecondLineText() {
		return getData().getType().name();
	}

	public Room getData() {
		return mData.get();
	}

	@Override
	public void bind(Room dataSource) {
		mData = new TrackableField<Room>(dataSource);

		// Bind Views
		UiBinder.bind(this, R.id.icon_stackedtext_imageIcon, "ImageBitmap", this, "ImageIconImageBitmap");
		UiBinder.bind(this, R.id.icon_stackedtext_textFirstLine, "Text", this, "TextFirstLineText");
		UiBinder.bind(this, R.id.icon_stackedtext_textSecondLine, "Text", this, "TextSecondLineText");

	}

}
