package com.alaris_us.daycareprovider.view;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class WorkoutAreaCheckBox extends RelativeLayout implements BoundUi<WorkoutAreaCheckBoxModel> {

	private WorkoutAreaCheckBoxModel m_data;

	private CheckBox m_checkBox;
	private ImageView m_imageView;
	private FontTextView m_fontTextView;

	public WorkoutAreaCheckBox(Context context) {

		super(context);

		View.inflate(getContext(), R.layout.list_checkmark_image, this);

		m_imageView = (ImageView) findViewById(R.id.list_checkmark_image_image);
		m_checkBox = (CheckBox) findViewById(R.id.list_checkmark_image_checkmark);
		m_fontTextView = (FontTextView) findViewById(R.id.list_checkmark_image_text);

	}

	public WorkoutAreaCheckBoxModel getWorkoutAreaCheckBoxModel() {

		return m_data;
	}

	@Override
	public void bind(WorkoutAreaCheckBoxModel dataSource) {

		m_data = dataSource;

		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_checkBox, "Checked")), m_data, "Selected",
				BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_fontTextView, "Text")), m_data, "WorkoutAreaTitle",
				BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(m_imageView, "ImageBitmap")), m_data, "WorkoutAreaImage",
				BindingMode.ONE_WAY);

	}

}
