package com.alaris_us.daycareprovider.view;

import java.text.DateFormat;

import org.apache.commons.lang3.text.WordUtils;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.alaris_us.daycaredata.dao.Records.EventType;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;

public class ReportingListItem extends LinearLayout implements BoundUi<Record> {

	private TrackableField<Record> m_data;

	private static String SIGNIN_EVENT_TEXT = "IN";
	private static String SIGNOUT_EVENT_TEXT = "OUT";
	private static String NA_EVENT_TEXT = "";

	public ReportingListItem(Context context) {

		super(context);

		View.inflate(getContext(), R.layout.list_reportrow, this);

		m_data = new TrackableField<Record>();

		UiBinder.bind(this, R.id.TextView_Col1, "Text", this, "Col1Text");
		UiBinder.bind(this, R.id.TextView_Col2, "Text", this, "Col2Text");
		UiBinder.bind(this, R.id.TextView_Col3, "Text", this, "Col3Text");
		UiBinder.bind(this, R.id.TextView_Col4, "Text", this, "Col4Text");
		UiBinder.bind(this, R.id.TextView_Col5, "Text", this, "Col5Text");
		UiBinder.bind(this, R.id.TextView_Col6, "Text", this, "Col6Text");
		UiBinder.bind(this, R.id.TextView_Col7, "Text", this, "Col7Text");
	}

	public String getCol1Text() {
		return WordUtils.capitalizeFully(m_data.get().getFirstName());
	}

	public String getCol2Text() {
		return WordUtils.capitalizeFully(m_data.get().getLastName());

	}

	public String getCol3Text() {

		return DateFormat.getDateInstance().format(m_data.get().getBirthDate());

	}

	public String getCol4Text() {
		return WordUtils.capitalizeFully(m_data.get().getProxyFirstName());

	}

	public String getCol5Text() {
		return WordUtils.capitalizeFully(m_data.get().getProxyLastName());

	}

	public String getCol6Text() {
		return DateFormat.getDateTimeInstance().format(m_data.get().getCreatedAt());

	}

	public String getCol7Text() {

		EventType signInType = m_data.get().getEventType();

		if (signInType == EventType.CHECK_IN)
			return SIGNIN_EVENT_TEXT;
		else if (signInType == EventType.CHECK_OUT)
			return SIGNOUT_EVENT_TEXT;

		return NA_EVENT_TEXT;

	}

	@Override
	public void bind(Record dataSource) {

		m_data.set(dataSource);

	}

}
