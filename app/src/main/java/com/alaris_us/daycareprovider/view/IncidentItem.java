package com.alaris_us.daycareprovider.view;

import java.util.Date;

import org.apache.commons.lang3.text.WordUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.LinearLayout;

import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.converters.ToStringConverter;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class IncidentItem extends LinearLayout implements BoundUi<Incident>, Runnable {

	private TrackableField<Incident> m_data;
	private String mLabelText;

	private final static int REFRESH_RATE = 60000;

	public IncidentItem(Context context) {

		// Android Boilerplate
		super(context);
		View.inflate(getContext(), R.layout.incident_item, this);

		// Init Fields
		m_data = new TrackableField<Incident>();

		mLabelText = context.getResources().getString(R.string.incident_item_incident_text);

		// Bind Views
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Photo), "ImageBitmap")), this,
				"MemberImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Name), "Text")), this,
				"MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Incident_Message), "Text")),
				this, "IncidentMessage", BindingMode.ONE_WAY, new ToStringConverter(mLabelText));
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_DateTime), "Text")), this,
				"CreatedDate", BindingMode.ONE_WAY, new ToStringConverter("%1$tb-%1$td-%1$tY %1tr"));
	}

	public Date getCreatedDate() {
		return m_data.get().getCreatedAt();

	}

	public String getMemberName() {

		// Member d = getMember();

		return WordUtils.capitalizeFully(m_data.get().getFirstName() + " " + m_data.get().getLastName());

	}

	public Bitmap getMemberImage() {

		Bitmap image = getMember().getImage();

		return (image == null) ? BitmapFactory.decodeResource(getResources(), R.drawable.photo_not_available) : image;

	}

	public Member getMember() {

		return m_data.get().getPMember();
	}

	public String getIncidentMessage() {
		return m_data.get().getIncident();
	}

	@Override
	public void bind(Incident dataSource) {

		m_data.set(dataSource);

	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		getHandler().post(this);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		getHandler().removeCallbacks(this);
	}

	@Override
	public void run() {
		postDelayed(this, REFRESH_RATE);
	}

}
