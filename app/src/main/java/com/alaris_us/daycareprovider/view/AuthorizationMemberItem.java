package com.alaris_us.daycareprovider.view;

import org.apache.commons.lang3.text.WordUtils;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.data.AuthorizedMember;
import com.alaris_us.daycareprovider.popups.AuthorizationPopup;
import com.alaris_us.daycareprovider.popups.AuthorizationPopup.AuthorizationPopupListener;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class AuthorizationMemberItem extends LinearLayout implements BoundUi<AuthorizedMember>, Runnable {

	private final TrackableField<AuthorizedMember> mMemberData;
	private final AuthorizationPopupListener mListener;

	private final static int REFRESH_RATE = 60000;

	public AuthorizationMemberItem(Context context, AuthorizationPopupListener listener) {

		// Android Boilerplate
		super(context);
		View.inflate(getContext(), R.layout.membername_item, this);

		// Init Fields
		mMemberData = new TrackableField<AuthorizedMember>();
		mListener = listener;

		// Bind Views
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Photo), "ImageBitmap")), this,
				"MemberImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Name), "Text")), this,
				"MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Alpha")), this, "ItemAlpha", BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.ImageView_Checkmark, "Visibility", this, "CheckmarkVisibility");
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "OnClickListener")), this, "OnClickListener", BindingMode.ONE_WAY);
	}
	
	public String getMemberName() {

		Member d = getMember();

		return WordUtils.capitalizeFully(d.getFirstName() + " " + d.getLastName());

	}

	public Bitmap getMemberImage() {

		Bitmap image = getMember().getImage();

		return (image == null) ? BitmapFactory.decodeResource(getResources(), R.drawable.photo_not_available) : image;

	}

	public Member getMember() {
		return mMemberData.get().getMember();
	}

	public OnClickListener getOnClickListener(){
		return new AuthorizationOnClickListener();
	}
	
	public class AuthorizationOnClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			AuthorizationPopup popup = new AuthorizationPopup(v.getContext(), mMemberData.get().getMember(), mListener);
			popup.show(v);
		}
		
	}

	@Override
	public void bind(AuthorizedMember dataSource) {

		mMemberData.set(dataSource);

	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		getHandler().post(this);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		getHandler().removeCallbacks(this);
	}

	@Override
	public void run() {
		postDelayed(this, REFRESH_RATE);
	}

}
