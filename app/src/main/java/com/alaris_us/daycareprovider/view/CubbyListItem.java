package com.alaris_us.daycareprovider.view;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.EditTextTextProperty;
import com.bindroid.ui.UiBinder;

public class CubbyListItem extends LinearLayout implements BoundUi<Cubby> {

	private final TrackableField<Cubby> mData;

	public CubbyListItem(Context context) {

		// Android Boilerplate
		super(context);
		View.inflate(getContext(), R.layout.list_cubby, this);
		// Init Fields
		mData = new TrackableField<Cubby>();
		// Bind Views
		UiBinder.bind(new EditTextTextProperty((EditText) findViewById(R.id.EditText_Name)), this, "Data.Name",
				BindingMode.TWO_WAY);
	}

	public Cubby getData() {

		return mData.get();
	}

	public void setData(Cubby data) {

		mData.set(data);
	}

	@Override
	public void bind(Cubby dataSource) {

		setData(dataSource);
	}
}
