package com.alaris_us.daycareprovider.view;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.Minutes;
import org.joda.time.MonthDay;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class MemberSearchResultItem extends RelativeLayout implements BoundUi<MemberAndCubbies>, Runnable {

	private final TrackableField<MemberAndCubbies> mMemberData;
	private final TrackableField<DateTime> mNow;

	private final static String YEAR_UNIT = "yr";
	private final static String MONTH_UNIT = "mo";
	private final static int REFRESH_RATE = 60000;
	private static final int AGE_CUTOFF = 12;

	private final String lastCheckInLabel, minutesLeftLabel;
	private final int childImageRes = R.drawable.ic_list_child;
	private final int adultImageRes = R.drawable.ic_list_parents;

	private static SimpleDateFormat DATEFORMAT = new SimpleDateFormat("MM-dd-yyyy", Locale.US);

	public MemberSearchResultItem(Context context) {

		// Android Boilerplate
		super(context);
		ListView.inflate(context, R.layout.searchandeditfamily_item, this);

		// Init Fields
		mMemberData = new TrackableField<MemberAndCubbies>();
		mNow = new TrackableField<DateTime>();
		lastCheckInLabel = getResources().getString(R.string.searchresult_info_lastcheckin_label);
		minutesLeftLabel = getResources().getString(R.string.searchresult_info_minutesleft_label);

		// Bind Views
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_image), "ImageBitmap")),
				this, "MemberImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_header_name), "Text")), this,
				"MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_header_age), "Text")), this,
				"Age", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_info_membership), "Text")),
				this, "Membership", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_info_memberid), "Text")),
				this, "MemberID", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_info_birthdate), "Text")),
				this, "Birthdate", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_info_joindate), "Text")),
				this, "JoinDate", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_info_statusinfo), "Text")),
				this, "StatusInfo", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_info_status), "Text")), this,
				"Status", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_info_gender), "Text")), this,
				"Gender", BindingMode.ONE_WAY);
		UiBinder.bind(
				UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_info_statusinfo_label), "Text")),
				this, "StatusInfoLabel", BindingMode.ONE_WAY);
		UiBinder.bind(
				UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_info_birthday), "Visibility")),
				this, "BirthdayVisibility", BindingMode.ONE_WAY);
		UiBinder.bind(
				UiProperty.make(new ReflectedProperty(findViewById(R.id.searchresult_info_medical), "Visibility")),
				this, "MedicalVisibility", BindingMode.ONE_WAY);
		UiBinder.bind(
				UiProperty
						.make(new ReflectedProperty(findViewById(R.id.searchresult_header_ageimage), "ImageResource")),
				this, "IsAdult", BindingMode.ONE_WAY);
	}

	public boolean getHasMedicalCondition() {

		String mc = getMember().getMedicalConcerns();

		return (mc == null || mc.isEmpty()) ? false : true;

	}

	public boolean getHasAlert() {

		return (getMinutesLeft() <= 0);

	}

	public String getMemberName() {

		Member d = getMember();

		return WordUtils.capitalizeFully(d.getFirstName() + " " + d.getLastName());

	}

	public String getMemberID() {

		Member d = getMember();

		return d.getMemberID();

	}

	public String getGender() {

		Member d = getMember();

		return d.getGender();

	}

	public int getIsAdult() {
		Member d = getMember();
		return DaycareHelpers.isAdult(d.getBirthDate(), AGE_CUTOFF) ? adultImageRes : childImageRes;
	}

	public String getMembership() {
		Member d = getMember();
		String membership = d.isActive() ? "Active" : "Inactive";
		if (d.isGuest())
			membership = "Guest";
		return membership;

	}

	public int getBirthdayVisibility() {
		MonthDay bDate = MonthDay.fromDateFields(getMember().getBirthDate());

		return (bDate.equals(MonthDay.now())) ? View.VISIBLE : View.INVISIBLE;

	}

	public int getMedicalVisibility() {
		String medical = getMember().getMedicalConcerns();

		return (medical == null || medical.trim().isEmpty()) ? View.INVISIBLE : View.VISIBLE;

	}

	public String getJoinDate() {
		Member d = getMember();
		return DATEFORMAT.format(d.getCreatedAt());
	}

	public String getBirthdate() {
		Member d = getMember();
		return DATEFORMAT.format(d.getBirthDate());
	}

	public String getStatus() {
		Member d = getMember();
		return (d.getPFacility() == null) ? "OUT" : "IN";
	}

	public String getStatusInfoLabel() {
		return getStatus().equals("IN") ? minutesLeftLabel : lastCheckInLabel;
	}

	public String getStatusInfo() {
		Member d = getMember();
		if (getStatus().equals("IN"))
			return String.valueOf(d.getMinutesLeft());
		else
			return DATEFORMAT.format(d.getLastCheckIn());
	}

	public String getAge() {

		LocalDate bDate = LocalDate.fromDateFields(getMember().getBirthDate());
		Period age = new Period(bDate, LocalDate.now());

		int difference = age.getYears();
		String unit = YEAR_UNIT;

		if (difference < 1) {
			difference = age.getMonths();
			unit = MONTH_UNIT;
		}

		return String.format(Locale.getDefault(), "%d%s", difference, unit);

	}

	public int getMinutesLeft() {

		DateTime lastCheckIn = new DateTime(getMember().getLastCheckIn());

		int minutesPassed = Minutes.minutesBetween(lastCheckIn, getNow()).getMinutes();
		return getMember().getMinutesLeft().intValue() - minutesPassed;

	}

	public String getCheckinStats() {

		DateTime lastCheckIn = new DateTime(getMember().getLastCheckIn());
		Interval timeIn = new Interval(lastCheckIn, getNow());

		DateTimeFormatter dfmt = DateTimeFormat.forPattern("hh:mm a");
		PeriodFormatter pfmt = new PeriodFormatterBuilder().appendDays().appendSuffix("d").appendHours()
				.appendSuffix("hr").appendMinutes().appendSuffix("min").toFormatter();

		return new StringBuilder().append(timeIn.toPeriod().toString(pfmt)).append(" (")
				.append(lastCheckIn.toString(dfmt)).append(")").toString();

	}

	public String getCubbyListString() {

		List<Cubby> forChild = getUsing();

		DaycareHelpers.sortCubbyList(forChild);

		if (forChild.size() < 1)
			return "";

		StringBuilder toReturn = new StringBuilder("Cubby: ");
		for (Cubby c : forChild) {
			toReturn.append(c.getName()).append(", ");
		}

		String returning = toReturn.toString().substring(0, toReturn.length() - 2);

		return returning;
	}

	public Bitmap getMemberImage() {

		Bitmap image = getMember().getImage();

		return (image == null) ? BitmapFactory.decodeResource(getResources(), R.drawable.photo_not_available) : image;

	}

	public void setNow(DateTime now) {
		mNow.set(now);
	}

	public DateTime getNow() {
		return mNow.get();
	}

	public Member getMember() {

		return mMemberData.get().getMember();
	}

	public List<Cubby> getUsing() {
		return mMemberData.get().getUsing();

	}

	@Override
	public void bind(MemberAndCubbies dataSource) {

		mMemberData.set(dataSource);

	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		getHandler().post(this);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		getHandler().removeCallbacks(this);
	}

	@Override
	public void run() {
		setNow(new DateTime());
		postDelayed(this, REFRESH_RATE);
	}

}
