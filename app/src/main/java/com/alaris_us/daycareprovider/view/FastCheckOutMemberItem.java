package com.alaris_us.daycareprovider.view;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Minutes;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Authorization;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycaredata.util.Cancelable;
import com.alaris_us.daycareprovider.data.FastCheckOutMember;
import com.alaris_us.daycareprovider.data.MemberAndCubbies.DoneCb;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.BindingMode;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.LinearLayout;

public class FastCheckOutMemberItem extends LinearLayout implements BoundUi<FastCheckOutMember>, Runnable {

	private final TrackableField<FastCheckOutMember> mMemberData;
	private final TrackableField<DateTime> mNow;

	private final static int REFRESH_RATE = 60000;

	public FastCheckOutMemberItem(Context context) {

		// Android Boilerplate
		super(context);
		View.inflate(getContext(), R.layout.membernamecheckmark_item, this);

		// Init Fields
		mMemberData = new TrackableField<FastCheckOutMember>();
		mNow = new TrackableField<DateTime>();

		// Bind Views
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.ImageView_Photo), "ImageBitmap")), this,
				"MemberImage", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Name), "Text")), this,
				"MemberName", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Time), "Text")), this,
				"TimeUsed", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_ProgramAndRoom), "Text")), this,
				"MemberProgramAndRoom", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Usage), "Text")), this,
				"UsageString", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(findViewById(R.id.TextView_Incident), "Text")), this,
				"IncidentString", BindingMode.ONE_WAY);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Alpha")), this, "ItemAlpha", BindingMode.ONE_WAY);
		UiBinder.bind(this, R.id.ImageView_Checkmark, "Visibility", this, "CheckmarkVisibility");
	}

	public float getItemAlpha() {
		return getIsSelected() ? 1f : 0.5f;
	}

	public int getCheckmarkVisibility() {
		return getShowCheckmarkOnSelection() && getIsSelected() ? View.VISIBLE : View.GONE;
	}

	public String getMemberName() {

		Member d = getMember();

		return WordUtils.capitalizeFully(d.getFirstName() + " " + d.getLastName());

	}
	
	public String getMemberProgramAndRoom() {

		Member d = getMember();
		if (d.getInProgram() != null)
			return d.getInProgram().getName() + " - " + d.getPRoom().getName();
		else
			return d.getPRoom().getName();
	}
	
	public String getTimeUsed() {

		DateTime lastCheckIn = new DateTime(getMember().getLastCheckIn());
		Interval timeIn = new Interval(lastCheckIn, getNow());

		DateTimeFormatter dfmt = DateTimeFormat.forPattern("h:mm a");
		PeriodFormatter pfmt = new PeriodFormatterBuilder().appendDays().appendSuffix("d").appendHours()
				.appendSuffix("hr").appendMinutes().appendSuffix("min").toFormatter();

		return new StringBuilder().append(timeIn.toPeriod().toString(pfmt)).append(" (")
				.append(lastCheckIn.toString(dfmt)).append(")").toString();
	}
	
	public String getUsageString() {

		String returning = "";
		List<Cubby> cubbiesUsed = getUsing();
		List<Pager> pagersUsed =  getPagers();
		String estimatedPickup = getEstimatedPickup();

		if (cubbiesUsed.size() < 1 && pagersUsed.size() < 1 && estimatedPickup == null)
			returning = "";
		else if (cubbiesUsed.size() > 0){
			StringBuilder toReturn = new StringBuilder("Cubby: ");
			for (Cubby c : cubbiesUsed) {
				toReturn.append(c.getName()).append(", ");
			}
			returning = toReturn.toString().substring(0, toReturn.length() - 2);
		} else if (pagersUsed.size() > 0){
			StringBuilder toReturn = new StringBuilder("Pager: ");
			for (Pager p : pagersUsed) {
				toReturn.append(p.getName()).append(", ");
			}
			returning = toReturn.toString().substring(0, toReturn.length() - 2);
		} else if (!estimatedPickup.isEmpty()){
			StringBuilder toReturn = new StringBuilder("Est. Pickup: ");
			returning = toReturn.append(estimatedPickup).toString();
		}
		return returning;
	}
	
	public List<Cubby> getUsing() {
		return mMemberData.get().getUsing();
	}

	public List<Pager> getPagers() {
		return mMemberData.get().getPagers();
	}

	public String getEstimatedPickup() {
		return mMemberData.get().getMember().getEstimatedPickupTime();
	}
	
	public List<Incident> getIncidents() {
		return mMemberData.get().getIncidents();
	}
	
	public String getIncidentString(){
		String returning = "";
		List<Incident> incidents = getIncidents();
		
		StringBuilder toReturn = new StringBuilder("Reminder: ");
		if (incidents.size() > 0) {
			for (Incident i : incidents) {
				toReturn.append(i.getIncident()).append(", ");
			}
			returning = toReturn.toString().substring(0, toReturn.length() - 2);
		}
		return returning;
	}

	public Bitmap getMemberImage() {

		Bitmap image = getMember().getImage();

		return (image == null) ? BitmapFactory.decodeResource(getResources(), R.drawable.photo_not_available) : image;

	}
	
	public Member getMember() {

		return mMemberData.get().getMember();
	}

	public boolean getIsSelected() {
		return mMemberData.get().getIsSelected();
	}

	public boolean getShowCheckmarkOnSelection() {
		return mMemberData.get().getShowCheckmarkOnSelection();
	}

	public void setNow(DateTime now) {
		mNow.set(now);
	}

	public DateTime getNow() {
		return mNow.get();
	}
	
	@Override
	public void bind(FastCheckOutMember dataSource) {

		mMemberData.set(dataSource);

	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		getHandler().post(this);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		getHandler().removeCallbacks(this);
	}

	@Override
	public void run() {
		setNow(new DateTime());
		postDelayed(this, REFRESH_RATE);
	}

}
