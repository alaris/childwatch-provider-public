package com.alaris_us.daycareprovider.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.alaris_us.daycaredata.to.WorkoutArea;
import com.bindroid.BindingMode;
import com.bindroid.trackable.TrackableField;
import com.bindroid.ui.BoundUi;
import com.bindroid.ui.UiBinder;
import com.bindroid.ui.UiProperty;
import com.bindroid.utils.ReflectedProperty;

public class ChildPopupExpectedAreas extends TextView implements BoundUi<WorkoutArea> {

	private TrackableField<WorkoutArea> mData;

	public WorkoutArea getData() {
		return mData.get();
	}

	public ChildPopupExpectedAreas(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public ChildPopupExpectedAreas(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ChildPopupExpectedAreas(Context context) {
		super(context);
	}

	@Override
	public void bind(WorkoutArea dataSource) {
		mData = new TrackableField<WorkoutArea>(dataSource);
		UiBinder.bind(UiProperty.make(new ReflectedProperty(this, "Text")), this, "Data.WorkoutName",
				BindingMode.ONE_WAY);

	}

}
