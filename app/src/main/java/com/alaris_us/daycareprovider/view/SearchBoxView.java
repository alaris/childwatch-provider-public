package com.alaris_us.daycareprovider.view;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.SearchView;

import com.alaris_us.daycareprovider_dev.R;

public class SearchBoxView extends SearchView {
	public SearchBoxView(Context context) {
		super(context);
		init(context);
	}

	public SearchBoxView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		setBackgroundResource(R.drawable.rectangle_whitegrey);
		setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		int padding = R.dimen.searchbar_searchbox_padding;
		setPadding(padding, padding, padding, padding);

	}

}
