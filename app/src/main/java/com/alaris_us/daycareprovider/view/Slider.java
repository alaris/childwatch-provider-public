package com.alaris_us.daycareprovider.view;

import com.bindroid.trackable.TrackableBoolean;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.Switch;

public class Slider extends Switch {
	private TrackableBoolean m_checked = new TrackableBoolean(false);

	public Slider(Context context) {
		super(context);
		setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				setIsChecked(isChecked);
			}
		});
	}

	public Slider(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				setIsChecked(isChecked);
			}
		});
	}

	public Slider(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				setIsChecked(isChecked);
			}
		});
	}

	public boolean getIsChecked() {
		return m_checked.get();
	}

	public void setIsChecked(boolean checked) {
		m_checked.set(checked);
		setChecked(checked);
	}

}