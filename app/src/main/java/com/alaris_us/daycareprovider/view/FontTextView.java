package com.alaris_us.daycareprovider.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
import com.alaris_us.daycareprovider_dev.R;

public class FontTextView extends TextView {

	private static final String FONT_DIRECTORY = "fonts/";

	public FontTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init(attrs);

	}

	public FontTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

		init(attrs);

	}

	public FontTextView(Context context) {
		super(context);

		init(null);
	}

	private void init(AttributeSet attrs) {

		if (attrs != null) {

			TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FontTextView);

			String fontName = a.getString(R.styleable.FontTextView_fontName);

			if (fontName != null) {
				try {
					Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), FONT_DIRECTORY + fontName);
					setTypeface(myTypeface);
				} catch (Exception e) {
				}
			}
			a.recycle();

		}

	}

}
