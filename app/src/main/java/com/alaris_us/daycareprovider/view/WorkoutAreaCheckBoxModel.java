package com.alaris_us.daycareprovider.view;

import android.graphics.Bitmap;

import com.alaris_us.daycaredata.to.WorkoutArea;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableField;

public class WorkoutAreaCheckBoxModel {

	private TrackableField<WorkoutArea> m_workoutArea;
	private TrackableBoolean m_isSelected;

	public WorkoutAreaCheckBoxModel() {

		this(null, false);
	}

	public WorkoutAreaCheckBoxModel(WorkoutArea workoutArea, boolean isSelected) {

		m_workoutArea = new TrackableField<WorkoutArea>(workoutArea);
		m_isSelected = new TrackableBoolean(isSelected);

	}

	public WorkoutArea getWorkoutArea() {

		return m_workoutArea.get();
	}

	public void setWoutArea(WorkoutArea workoutArea) {

		m_workoutArea.set(workoutArea);
	}

	public boolean getSelected() {

		return m_isSelected.get();
	}

	public void setSelected(boolean selected) {

		m_isSelected.set(selected);
	}

	public void toggleSelected() {
		m_isSelected.set(!m_isSelected.get());
	}

	public String getWorkoutAreaTitle() {

		return m_workoutArea.get().getWorkoutName();
	}

	public Bitmap getWorkoutAreaImage() {

		return m_workoutArea.get().getWorkoutImage();
	}

}
