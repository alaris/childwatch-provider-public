package com.alaris_us.daycareprovider.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.alaris_us.daycareprovider_dev.R;

public class RoundedImageView extends ImageView {

	private static final ScaleType[] SCALE_TYPES = { ScaleType.MATRIX, ScaleType.FIT_XY, ScaleType.FIT_START,
			ScaleType.FIT_CENTER, ScaleType.FIT_END, ScaleType.CENTER, ScaleType.CENTER_CROP, ScaleType.CENTER_INSIDE };
	public static final float DEFAULT_RADIUS = 0f;
	public static final int DEFAULT_BORDER_WIDTH = 0;
	private float mRadius;
	private Drawable mDrawable;
	private ScaleType mScaleType;
	private int mBorderWidth;
	private ColorStateList mBorderColor = ColorStateList.valueOf(RoundedDrawable.DEFAULT_BORDER_COLOR);

	public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {

		super(context, attrs, defStyle);
		init(attrs);
	}

	public RoundedImageView(Context context, AttributeSet attrs) {

		super(context, attrs);
		init(attrs);
	}

	public RoundedImageView(Context context) {

		super(context);
		init(null);
	}

	private void init(AttributeSet attrs) {

		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.RoundedImageView);
		int index = a.getInt(R.styleable.RoundedImageView_android_scaleType, -1);
		if (index >= 0) {
			setScaleType(SCALE_TYPES[index]);
		} else {
			// default scaletype to FIT_CENTER
			setScaleType(ScaleType.FIT_CENTER);
		}
		mRadius = a.getDimensionPixelSize(R.styleable.RoundedImageView_radius, -1);
		mBorderWidth = a.getDimensionPixelSize(R.styleable.RoundedImageView_border_width, -1);
		// don't allow negative values for radius and border
		if (mRadius < 0) {
			mRadius = DEFAULT_RADIUS;
		}
		if (mBorderWidth < 0) {
			mBorderWidth = DEFAULT_BORDER_WIDTH;
		}
		mBorderColor = a.getColorStateList(R.styleable.RoundedImageView_border_color);
		if (mBorderColor == null) {
			mBorderColor = ColorStateList.valueOf(RoundedDrawable.DEFAULT_BORDER_COLOR);
		}
		updateDrawableAttrs();
		a.recycle();
	}

	private void updateAttrs(Drawable drawable) {

		if (drawable == null) {
			return;
		}
		if (drawable instanceof RoundedDrawable) {
			((RoundedDrawable) drawable).setScaleType(mScaleType).setCornerRadius(mRadius).setBorderWidth(mBorderWidth)
					.setBorderColor(mBorderColor);
		} else if (drawable instanceof LayerDrawable) {
			// loop through layers to and set drawable attrs
			LayerDrawable ld = ((LayerDrawable) drawable);
			for (int i = 0, layers = ld.getNumberOfLayers(); i < layers; i++) {
				updateAttrs(ld.getDrawable(i));
			}
		}
	}

	@Override
	protected void drawableStateChanged() {

		super.drawableStateChanged();
		invalidate();
	}

	/**
	 * Return the current scale type in use by this ImageView.
	 * 
	 * @attr ref android.R.styleable#ImageView_scaleType
	 * @see android.widget.ImageView.ScaleType
	 */
	@Override
	public ScaleType getScaleType() {

		return mScaleType;
	}

	/**
	 * Controls how the image should be resized or moved to match the size of
	 * this ImageView.
	 * 
	 * @param scaleType
	 *            The desired scaling mode.
	 * @attr ref android.R.styleable#ImageView_scaleType
	 */
	@Override
	public void setScaleType(ScaleType scaleType) {

		assert scaleType != null;
		if (mScaleType != scaleType) {
			mScaleType = scaleType;
			switch (scaleType) {
			case CENTER:
			case CENTER_CROP:
			case CENTER_INSIDE:
			case FIT_CENTER:
			case FIT_START:
			case FIT_END:
			case FIT_XY:
				super.setScaleType(ScaleType.FIT_XY);
				break;
			default:
				super.setScaleType(scaleType);
				break;
			}
			updateDrawableAttrs();
			invalidate();
		}
	}

	@Override
	public void setImageDrawable(Drawable drawable) {

		mDrawable = RoundedDrawable.fromDrawable(drawable);
		updateDrawableAttrs();
		super.setImageDrawable(mDrawable);
	}

	@Override
	public void setImageBitmap(Bitmap bm) {

		mDrawable = RoundedDrawable.fromBitmap(bm);
		updateDrawableAttrs();
		super.setImageDrawable(mDrawable);
	}

	private void updateDrawableAttrs() {

		updateAttrs(mDrawable);
	}
}
