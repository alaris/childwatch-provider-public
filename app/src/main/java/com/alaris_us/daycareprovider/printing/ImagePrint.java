/**
 * ImagePrint for printing
 * 
 * @author Brother Industries, Ltd.
 * @version 2.2
 */
package com.alaris_us.daycareprovider.printing;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;

import com.brother.ptouch.sdk.PrinterInfo.ErrorCode;

public class ImagePrint extends BasePrint {

	protected List<Bitmap> mImageFiles;

	public ImagePrint(Context context, MsgHandle mHandle, MsgDialog mDialog, String ip) {
		super(context, mHandle, mDialog, ip);
	}

	/** set print data */
	public void setFiles(List<Bitmap> images) {
		mImageFiles = images;
	}

	/** do the particular print */
	@Override
	protected void doPrint() {

		for (Bitmap b : mImageFiles) {
			mPrintResult = mPrinter.printImage(b);

			// if error, stop print next files
			if (mPrintResult.errorCode != ErrorCode.ERROR_NONE) {
				break;
			}
		}

	}
}