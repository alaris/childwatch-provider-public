package com.alaris_us.daycareprovider.models;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.MutableDateTime;

import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycareprovider.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.popups.DateTimePopup;
import com.alaris_us.daycareprovider.popups.DateTimePopup.DateTimePickerDialogListener;
import com.alaris_us.daycareprovider.view.CustomToast;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.TimePicker;

public class ReportingViewModel extends ViewModel {

	private class UpdateClickedListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			try {
				getPersistenceLayer().fetchReportingHistory(getStartDate(), getEndDate());
			} catch (Throwable e) {
				CustomToast
						.makeToast(v.getContext(), e.getMessage(), CustomToast.DURATION_LONG, CustomToast.STYLE_ERROR)
						.show();
			}
		}
	}

	private class StartTimeClickedListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			DateTimePopup dtp = new DateTimePopup(v.getContext());
			dtp.setDateTimePickerDialogListener(new StartDateChanged());
			dtp.show(v, getStartDate());
		}
	}

	private class EndTimeClickedListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			DateTimePopup dtp = new DateTimePopup(v.getContext());
			dtp.setDateTimePickerDialogListener(new EndDateChanged());
			dtp.show(v, getEndDate());
		}
	}

	private class StartDateChanged implements DateTimePickerDialogListener {

		@Override
		public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

			MutableDateTime mdt = new MutableDateTime(getStartDate());
			mdt.setDate(year, monthOfYear, dayOfMonth);
			setStartDate(mdt.toDateTime());
		}

		@Override
		public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

			MutableDateTime mdt = new MutableDateTime(getStartDate());
			mdt.setTime(hourOfDay, minute, 0, 0);
			setStartDate(mdt.toDateTime());
		}
	}

	private class EndDateChanged implements DateTimePickerDialogListener {

		@Override
		public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

			MutableDateTime mdt = new MutableDateTime(getEndDate());
			mdt.setDate(year, monthOfYear, dayOfMonth);
			setEndDate(mdt.toDateTime());
		}

		@Override
		public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

			MutableDateTime mdt = new MutableDateTime(getEndDate());
			mdt.setTime(hourOfDay, minute, 0, 0);
			setEndDate(mdt.toDateTime());
		}
	}

	private class ReportingHistoryPropertyChangeListener implements PropertyChangeListener {

		@Override
		public void propertyChange(PropertyChangeEvent event) {

			getReportingHistory().clear();
			getReportingHistory().addAll(getPersistenceData().getReportingHistory());
		}
	}

	public ReportingViewModel(PersistenceLayer persistenceLayer) {

		super(persistenceLayer);
	}

	public String getStartDateString() {

		return m_startDate.get().toString("MMM-dd-YYYY hh:mm a", Locale.getDefault()).toUpperCase(Locale.getDefault());
	}

	public DateTime getStartDate() {

		return m_startDate.get();
	}

	public void setStartDate(DateTime startDate) {

		m_startDate.set(startDate);
	}

	public String getEndDateString() {

		return m_endDate.get().toString("MMM-dd-YYYY hh:mm a", Locale.getDefault()).toUpperCase(Locale.getDefault());
	}

	public DateTime getEndDate() {

		return m_endDate.get();
	}

	public void setEndDate(DateTime endDate) {

		m_endDate.set(endDate);
	}

	@Override
	protected void initialize() {

		watchProperty(new ReportingHistoryPropertyChangeListener(), CHANGE_ID.REPORTING_HISTORY);
		m_startClickListener = new StartTimeClickedListener();
		m_endClickListener = new EndTimeClickedListener();
		m_updateClickListener = new UpdateClickedListener();
		m_reportingHistory = new TrackableField<TrackableCollection<Record>>(new TrackableCollection<Record>());
		m_startDate = new TrackableField<DateTime>(new LocalDate().toDateTimeAtStartOfDay());
		m_endDate = new TrackableField<DateTime>(new LocalDate().plusDays(1).toDateTimeAtStartOfDay());
	}

	public OnClickListener getStartTimeButtonListener() {

		return m_startClickListener;
	}

	public OnClickListener getEndTimeButtonListener() {

		return m_endClickListener;
	}

	public OnClickListener getUpdateButtonListener() {

		return m_updateClickListener;
	}

	public TrackableCollection<Record> getReportingHistory() {

		return m_reportingHistory.get();
	}

	private OnClickListener m_startClickListener;
	private OnClickListener m_endClickListener;
	private OnClickListener m_updateClickListener;
	private TrackableField<DateTime> m_startDate;
	private TrackableField<DateTime> m_endDate;
	private TrackableField<TrackableCollection<Record>> m_reportingHistory;
}
