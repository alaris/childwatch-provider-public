package com.alaris_us.daycareprovider.models;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycareprovider.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.view.WorkoutAreaCheckBox;
import com.alaris_us.daycareprovider.view.WorkoutAreaCheckBoxModel;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class SetupWorkoutAreasViewModel extends ViewModel {

	public SetupWorkoutAreasViewModel(PersistenceLayer persistenceLayer) {

		super(persistenceLayer);
	}

	private TrackableField<TrackableCollection<WorkoutAreaCheckBoxModel>> m_workoutModels;
	private TrackableField<OnItemClickListener> m_listviewListener;

	public TrackableCollection<WorkoutAreaCheckBoxModel> getWorkoutAreas() {

		return m_workoutModels.get();
	}

	public OnItemClickListener getListviewListener() {

		return m_listviewListener.get();
	}

	public void persistModel() {

		getPersistenceLayer().persistFacility();
	}

	public void fetchModel() {

		getPersistenceLayer().fetchSelectedWorkoutLocations();
		getPersistenceLayer().fetchAllWorkoutLocations();
	}

	@Override
	public void initialize() {

		// Initialize Fields
		m_workoutModels = new TrackableField<TrackableCollection<WorkoutAreaCheckBoxModel>>(
				new TrackableCollection<WorkoutAreaCheckBoxModel>());
		m_listviewListener = new TrackableField<OnItemClickListener>(new ListViewClicked());
		// Populate Data Field Watchers
		watchProperty(new WorkoutPropertyChangeListener(), CHANGE_ID.ALLWORKOUTAREAS);
		watchProperty(new WorkoutPropertyChangeListener(), CHANGE_ID.SELECTEDWORKOUTAREAS);
		initializeModelData();
	}

	private void initializeModelData() {

		List<WorkoutArea> allWorkoutAreas = getPersistenceData().getAllWorkoutAreas();
		List<WorkoutArea> selectedWorkoutAreas = getPersistenceData().getSelectedWorkoutAreas();
		m_workoutModels.get().clear();
		for (WorkoutArea area : allWorkoutAreas) {
			m_workoutModels.get().add(new WorkoutAreaCheckBoxModel(area, (selectedWorkoutAreas.contains(area))));
		}
	}

	private void selectWorkoutArea(WorkoutArea area) {

		getPersistenceLayer().addSelectedWorkoutArea(area);
	}

	private void deselectWorkoutArea(WorkoutArea area) {

		getPersistenceLayer().removeSelectedWorkoutArea(area);
	}

	private class WorkoutPropertyChangeListener implements PropertyChangeListener {

		// Made private class since it will respond the same way for two fields
		@Override
		public void propertyChange(PropertyChangeEvent event) {

			initializeModelData();
		}
	}

	private class ListViewClicked implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			WorkoutAreaCheckBoxModel wm = ((WorkoutAreaCheckBox) view).getWorkoutAreaCheckBoxModel();
			if (wm.getSelected()) {
				deselectWorkoutArea(wm.getWorkoutArea());
			} else {
				selectWorkoutArea(wm.getWorkoutArea());
			}
		}
	}
}
