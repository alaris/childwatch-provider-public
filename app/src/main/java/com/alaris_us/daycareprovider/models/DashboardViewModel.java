package com.alaris_us.daycareprovider.models;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.comparators.ComparatorChain;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.json.JSONObject;

import com.alaris_us.daycaredata.EmergencyNumber;
import com.alaris_us.daycaredata.PhoneNumber;
import com.alaris_us.daycaredata.dao.Records.EventType;
import com.alaris_us.daycaredata.to.Authorization;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.daycareprovider.comparators.MemberAgeComparator;
import com.alaris_us.daycareprovider.comparators.MemberFirstNameComparator;
import com.alaris_us.daycareprovider.comparators.MemberLastNameComparator;
import com.alaris_us.daycareprovider.comparators.MemberTimeLeftComparator;
import com.alaris_us.daycareprovider.data.CheckInCount;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.alaris_us.daycareprovider.fragments.Dashboard;
import com.alaris_us.daycareprovider.fragments.Dashboard.ChildListMode;
import com.alaris_us.daycareprovider.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.popups.ChildDetailsPopup;
import com.alaris_us.daycareprovider.popups.ChildDetailsPopup.ChildDetailsPopupCallback;
import com.alaris_us.daycareprovider.popups.SelectRoomPopup.SelectRoomPopupListener;
import com.alaris_us.daycareprovider.popups.StaffPopup;
import com.alaris_us.daycareprovider.printing.MsgDialog;
import com.alaris_us.daycareprovider.printing.MsgHandle;
import com.alaris_us.daycareprovider.printing.TemplatePrint;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider.view.ChildListTabHost;
import com.alaris_us.daycareprovider.view.CustomToast;
import com.alaris_us.daycareprovider.view.FilterTabs;
import com.alaris_us.daycareprovider.view.FilterTabs.FilterTab;
import com.alaris_us.daycareprovider.view.TabWithIcons.SortDirection;
import com.alaris_us.daycareprovider_dev.R;
import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BarRenderer;
//import com.androidplot.xy.BarRenderer.BarWidthStyle;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.SimpleXYSeries.ArrayFormat;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYRegionFormatter;
import com.androidplot.xy.XYSeriesFormatter;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TabHost;

public class DashboardViewModel extends ViewModel implements ChildDetailsPopupCallback, SelectRoomPopupListener {

	private static final int FETCH_DAYS_COUNT = 7;

	@SuppressLint("NewApi")
	public DashboardViewModel(PersistenceLayer persistenceLayer, Dashboard fragment) {

		super(persistenceLayer);
		this.fragment = fragment;

		String printerIP = getPersistenceLayer().getPersistenceData().getPrinterIP();
		String printerModel = getPersistenceLayer().getPersistenceData().getPrinterModel();
		String printerMacAddress = getPersistenceLayer().getPersistenceData().getPrinterMacAddress();
		List<Integer> labelKeys = new ArrayList<Integer>();
		labelKeys.add(fragment.getResources().getInteger(R.integer.CHILDTEMPLATE));
		labelKeys.add(fragment.getResources().getInteger(R.integer.SICKCHILDTEMPLATE));
		labelKeys.add(fragment.getResources().getInteger(R.integer.GUARDIANTEMPLATE));
		JSONObject labelOpts = persistenceLayer.getPersistenceData().getFacility().getLabelPrintingOpts();

		if ((null != printerIP && printerIP.trim().length() > 0) || (null != printerMacAddress && printerMacAddress.trim().length() > 0)) {
			mDialog = new MsgDialog(fragment.getActivity());
			mHandle = new MsgHandle(fragment.getActivity(), mDialog);
			mPrint = new TemplatePrint(fragment.getActivity(), mHandle, mDialog, printerIP, labelKeys, labelOpts, printerModel, printerMacAddress);
		}
	}

	@Override
	protected void initialize() {

		// Initialize Fields
		mCheckedInMembers = new TrackableCollection<MemberAndCubbies>();
		mMembersInRoom = new TrackableCollection<MemberAndCubbies>();
		m_inCenter = new TrackableInt();
		m_overAge = new TrackableInt();
		m_underAge = new TrackableInt();
		m_weightedCount = new TrackableInt();
		m_totalCheckIns = new TrackableInt();
		m_AMCheckIns = new TrackableInt();
		m_PMCheckIns = new TrackableInt();
		m_staffCount = new TrackableInt();
		m_TabHost = new TrackableField<TabHost>();
		m_Menu = new TrackableField<Menu>();
		mSelectedMembers = new TrackableCollection<MemberAndCubbies>();
		mChildListMode = ChildListMode.SINGLE;
		m_FilterTabs = new TrackableField<FilterTabs>();
		mRoomName = new TrackableField<String>("OVERVIEW");

		// Populate Data Field Watchers
		watchProperty(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {
				mCheckedInMembers.clear();
				mCheckedInMembers.addAll(getPersistenceData().getCheckedInMembers());
				calculateAndSetMemberCounts(mCheckedInMembers);

				filterChildrenByRoom(mRoomName.get());

				if (m_TabHost.get() != null) {
					int currTab = m_TabHost.get().getCurrentTab();
					SortDirection dir = ((ChildListTabHost) (m_TabHost.get())).getTab(currTab).getView()
							.getSortDirection();
					switch (m_TabHost.get().getCurrentTab()) {
					case 0:
						sortChildListByFullName(dir);
						break;
					case 1:
						sortChildListByFamily(dir);
						break;
					case 2:
						sortChildListByAge(dir);
						break;
					case 3:
						sortChildListByTimeLeft(dir);
						break;
					}
				}
			}
		}, CHANGE_ID.CHECKED_IN_MEMBERS);

		watchProperty(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent event) {
				parseAndSetCheckIns(mRoomName.get());
			}
		}, CHANGE_ID.TODAYS_SIGN_IN_RECORDS);

		watchProperty(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent event) {
				parseAndPlotCounts(mRoomName.get());
			}
		}, CHANGE_ID.CHECK_IN_COUNTS);
	}

	private class ChildDetailOnItemClickListener implements OnItemClickListener {
		ChildDetailsPopup popup;

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			final MemberAndCubbies child = mCheckedInMembers.get(position);

			if (mChildListMode.equals(ChildListMode.GROUP)) {
				mSelectedMembers.add(child);
				child.setSelected(!child.getSelected());
				child.setDimmed(!child.getSelected());
				if (areMembersSelected()) {
					m_Menu.get().findItem(R.id.action_move).setEnabled(true);
					m_Menu.get().findItem(R.id.action_checkout).setEnabled(true);
				} else {
					m_Menu.get().findItem(R.id.action_move).setEnabled(false);
					m_Menu.get().findItem(R.id.action_checkout).setEnabled(false);
				}
			} else {
				mContext = view.getContext();
				if (popup != null)
					popup.dismiss();

				popup = new ChildDetailsPopup(mContext, view, getPersistenceLayer(), child, DashboardViewModel.this,
						DashboardViewModel.this, mPrint);
				popup.show(view);
			}
		}
	}

	public OnItemClickListener getChildDetailsOnItemClickListener() {

		if (mChildDetailsOnItemClickListener == null)
			mChildDetailsOnItemClickListener = new ChildDetailOnItemClickListener();
		return mChildDetailsOnItemClickListener;
	}

	public int getInCenter() {

		return m_inCenter.get();
	}

	/*
	 * public String getInCenterTopText(){ return mInCenterTopText; } public String setInCenterTopText() {
	 *
	 * if (mRoomName != null) { if (!mRoomName.equals("OVERVIEW")){ mInCenterTopText = "In Room"; return
	 * mInCenterTopText; } else{ mInCenterTopText = "In Room"; return mInCenterTopText; } } else{ mInCenterTopText =
	 * "In Room"; return mInCenterTopText; } }
	 */

	public int getOverAge() {

		return m_overAge.get();
	}

	public int getUnderAge() {

		return m_underAge.get();
	}

	public int getWeightedCount() {

		return m_weightedCount.get();
	}

	public void setWeightedCount(int count) {

		m_weightedCount.set(count);
	}

	public int getTotalCheckIns() {

		return m_totalCheckIns.get();
	}

	public int getAMCheckIns() {

		return m_AMCheckIns.get();
	}

	public int getPMCheckIns() {

		return m_PMCheckIns.get();
	}

	public String getWeightedCountText() {
		if (getPersistenceData().getFacility().getRatioType() != null)
			return "Open Spots";
		else if (getPersistenceData().getFacility().getMaxCapacityType() != null)
			if (getPersistenceData().getFacility().getMaxCapacityType().equals("FACILITY"))
				return "Open Spots";
			else
				return "Count";
		else
			return "Count";
	}

	public int getStaffCount() {

		return m_staffCount.get();
	}

	public void setStaffCount(int count) {

		m_staffCount.set(count);
	}

	public OnClickListener getStaffOnClickListener() {
		return new StaffOnClickListener();
	}

	private class StaffOnClickListener implements OnClickListener {
		@Override
		public void onClick(final View view) {
			getPersistenceLayer().fetchFacilityStaff(new PersistenceLayerCallback<List<Staff>>() {
				@Override
				public void done(List<Staff> data, Exception e) {
					List<Room> facilityRooms = getPersistenceData().getAllFacilityRooms();
					(new StaffPopup(view.getContext(), getPersistenceLayer(), DashboardViewModel.this, data, facilityRooms)).show(view);
				}
			});
		}

	}

	public List<MemberAndCubbies> getCheckedInMembers() {

		return mCheckedInMembers;
	}

	public void setRoomName(String roomName) {
		mRoomName.set(roomName);
		filterChildrenByRoom(roomName);
		parseAndPlotCounts(roomName);
		parseAndSetCheckIns(roomName);
	}

	public String getRoomName() {
		return mRoomName.get();
	}

	public void filterChildrenByRoom(String roomName) {
		List<Room> facilityRooms = getPersistenceData().getAllFacilityRooms();
		String roomObjId = "";
		mMembersInRoom.clear();
		mCheckedInMembers.clear();
		if (roomName.equals("OVERVIEW")) {
			mCheckedInMembers.addAll(getPersistenceData().getCheckedInMembers());
			mMembersInRoom.addAll(mCheckedInMembers);
		} else {
			for (Room room : facilityRooms)
				if (room.getName().equals(roomName))
					roomObjId = room.getObjectId();

			for (MemberAndCubbies mc : getPersistenceData().getCheckedInMembers())
				if (mc.getMember().getPRoom() != null)
					if (mc.getMember().getPRoom().getObjectId().equals(roomObjId))
						mMembersInRoom.add(mc);

			mCheckedInMembers.addAll(mMembersInRoom);
		}
		calculateAndSetMemberCounts(mCheckedInMembers);

		for (FilterTab filterTab : m_FilterTabs.get().getTabsList()) {
			if (m_FilterTabs.get().getCurrentTab().equals(filterTab))
				filterTab.click();
		}
		if (m_TabHost.get().getCurrentTab() == 0)
			sortChildListByFullName(((ChildListTabHost) (m_TabHost.get())).getTab(0).getView().getSortDirection());
		else if (m_TabHost.get().getCurrentTab() == 1)
			sortChildListByFamily(((ChildListTabHost) (m_TabHost.get())).getTab(1).getView().getSortDirection());
		else if (m_TabHost.get().getCurrentTab() == 2)
			sortChildListByAge(((ChildListTabHost) (m_TabHost.get())).getTab(2).getView().getSortDirection());
		else if (m_TabHost.get().getCurrentTab() == 3)
			sortChildListByTimeLeft(((ChildListTabHost) (m_TabHost.get())).getTab(3).getView().getSortDirection());

	}

	private void calculateAndSetMemberCounts(List<MemberAndCubbies> members) {

		int total = 0, under = 0, over = 0;
		double weighted = 0.0;
		DateTime now = new DateTime();
		for (MemberAndCubbies m : members) {
			DateTime birthdate = new DateTime(m.getMember().getBirthDate());
			int ageInMonths = Months.monthsBetween(birthdate, now).getMonths();
			if (ageInMonths >= OVER_AGE_MONTHS) {
				over++;
				weighted++;
			} else {
				under++;
				weighted += OVER_AGE_MULTIPLIER;
			}
			total++;
		}

		if (getPersistenceData().getFacility().getRatioType() != null) {
			if (getPersistenceData().getFacility().getRatioType().equals("FACILITY"))
				weighted = getPersistenceData().getClockedInStaff().size() * getPersistenceData().getFacility().getRatio().intValue() - mCheckedInMembers.size();
			else if (getPersistenceData().getFacility().getRatioType().equals("FACILITYANDROOM")) {
				if (getPersistenceData().getFacility().getMaxCapacityType() != null) {
					if (getPersistenceData().getFacility().getMaxCapacityType().equals("FACILITY")) {
						if (mRoomName.get().equals("OVERVIEW")) {
							if (getPersistenceData().getClockedInStaff().size() * getPersistenceData().getFacility().getRatio().intValue() >
									getPersistenceData().getFacility().getMaxCapacity().intValue()) {
								weighted = getPersistenceData().getFacility().getMaxCapacity().intValue();
								for (Room r : getPersistenceData().getAllFacilityRooms()) {
									weighted = weighted - calculateCapacityUsingRoomMultipliers(r.getName());
								}
							} else {
								weighted = getPersistenceData().getClockedInStaff().size() * getPersistenceData().getFacility().getRatio().intValue();
								for (Room r : getPersistenceData().getAllFacilityRooms()) {
									weighted = weighted - calculateCapacityUsingRoomMultipliers(r.getName());
								}
							}
						} else {
							Room selectedRoom = null;
							for (Room room : getPersistenceData().getAllFacilityRooms()) {
								if (room.getName().equals(mRoomName.get()))
									selectedRoom = room;
							}
							if (getPersistenceData().getClockedInStaff().size() * getPersistenceData().getFacility().getRatio().intValue() >
									getPersistenceData().getFacility().getMaxCapacity().intValue()) {
								weighted = getPersistenceData().getFacility().getMaxCapacity().intValue();
							} else {
								weighted = getPersistenceData().getClockedInStaff().size() * getPersistenceData().getFacility().getRatio().intValue();
							}
							for (Room room : getPersistenceData().getAllFacilityRooms()) {
								weighted = weighted - calculateCapacityUsingRoomMultipliers(room.getName());
							}
							weighted = (int) (weighted/selectedRoom.getCapacityMultiplier().doubleValue());
						}
					}
				} else {
					Room selectedRoom = null;
					if (mRoomName.get().equals("OVERVIEW")) {
						weighted = getPersistenceData().getClockedInStaff().size() * getPersistenceData().getFacility().getRatio().intValue();
						for (Room r : getPersistenceData().getAllFacilityRooms()) {
							weighted = weighted - calculateCapacityUsingRoomMultipliers(r.getName());
						}
					} else {
						for (Room room : getPersistenceData().getAllFacilityRooms())
							if (room.getName().equals(mRoomName.get()))
								selectedRoom = room;
						weighted = getPersistenceData().getClockedInStaff().size() * getPersistenceData().getFacility().getRatio().intValue();
						for (Room room : getPersistenceData().getAllFacilityRooms()) {
							weighted = weighted - calculateCapacityUsingRoomMultipliers(room.getName());
						}
						weighted = (int) (weighted/selectedRoom.getCapacityMultiplier().doubleValue());
					}
				}
			} else {
				// ROOM ratioType
				if (mRoomName.get().equals("OVERVIEW")) {
					weighted = 0;
					for (Staff s : getPersistenceData().getClockedInStaff()) {
						if (s.getPRoom() != null)
							weighted = weighted + s.getPRoom().getRatio().intValue();
					}
					weighted = weighted - getPersistenceData().getCheckedInMembers().size();
				} else {
					weighted = 0;
					Room selectedRoom = null;
					for (Room room : getPersistenceData().getAllFacilityRooms())
						if (room.getName().equals(mRoomName.get()))
							selectedRoom = room;
					for (Staff s : getPersistenceData().getClockedInStaff()) {
						if (s.getPRoom().equals(selectedRoom))
							weighted = weighted + s.getPRoom().getRatio().intValue();
					}
					for (MemberAndCubbies mc : getPersistenceData().getCheckedInMembers()) {
						if (mc.getMember().getPRoom().equals(selectedRoom))
							weighted--;
					}
				}
			}
		} else if (getPersistenceData().getFacility().getMaxCapacityType() != null) {
			if (getPersistenceData().getFacility().getMaxCapacityType().equals("FACILITY")) {
				weighted = getPersistenceData().getFacility().getMaxCapacity().intValue() - getPersistenceData().getCheckedInMembers().size();
			}
		}
		m_inCenter.set(total);
		m_overAge.set(over);
		m_underAge.set(under);
		m_weightedCount.set((int) weighted);
	}

	public double calculateCapacityUsingRoomMultipliers(String roomName) {
		double capacity = 0;
		if (roomName.equals("OVERVIEW")) {
			for (Room r : getPersistenceData().getAllFacilityRooms()) {
				for (MemberAndCubbies mc : mCheckedInMembers) {
					if (mc.getMember().getPRoom().equals(r)) {
						capacity += r.getCapacityMultiplier().doubleValue();
					}
				}
			}
		} else {
			for (MemberAndCubbies mc : getPersistenceData().getCheckedInMembers()) {
				if (mc.getMember().getPRoom() != null)
					if (mc.getMember().getPRoom().getName().equals(roomName)) {
						capacity += mc.getMember().getPRoom().getCapacityMultiplier().doubleValue();
					}
			}
		}
		return capacity;
	}

	private void parseAndSetCheckIns(String roomName) {
		List<Record> recs = getPersistenceData().getTodaysSignInRecords();
		DateTime start = new LocalDate().toDateTimeAtStartOfDay();
		Facility facility = getPersistenceData().getFacility();
		DateTime pm;
		if (facility.getShiftCutoff().equals(0))
			pm = start.plusHours(12);
		else {
			int shiftCutoff = facility.getShiftCutoff().intValue();
			pm = start.plusMinutes(shiftCutoff);
		}

		int checkIns = 0, ams = 0, pms = 0, staff = 0;
		for (Record r : recs) {
			if ((roomName.equals("OVERVIEW") && !r.getEventType().equals(EventType.ROOM_CHANGE_IN))
					|| roomName.equals(r.getRoomName())) {
				DateTime recordTime = new DateTime(r.getCreatedAt());

				checkIns++;
				if (recordTime.isAfter(pm)) {
					pms++;
				} else {
					ams++;
				}
			}
		}
		getPersistenceLayer().fetchFacilityStaff(new PersistenceLayerCallback<List<Staff>>() {

			@Override
			public void done(List<Staff> data, Exception e) {

			}
		});
		if (roomName.equals("OVERVIEW"))
			staff = getPersistenceData().getClockedInStaff().size();
		else {
			if (facility.getRatioType() != null) {
				if (facility.getRatioType().equals("FACILITY") || facility.getRatioType().equals("FACILITYANDROOM"))
					staff = getPersistenceData().getClockedInStaff().size();
				else {
					for (Staff s : getPersistenceData().getClockedInStaff()) {
						if (s.getPRoom().getName().equals(mRoomName.get()))
							staff++;
					}
				}
			} else {
				staff = 0;
			}
		}
		m_totalCheckIns.set(checkIns);
		m_AMCheckIns.set(ams);
		m_PMCheckIns.set(pms);
		m_staffCount.set(staff);
	}

	private Map<String, SimpleXYSeries> roomData = new HashMap<String, SimpleXYSeries>();
	private SimpleXYSeries defaultSeries;

	private SimpleXYSeries getDefaultSeries() {
		if (defaultSeries == null) {
			List<Number> data = getFormattedPlotData(Arrays.asList(0, 0, 0, 0, 0, 0, 0));
			defaultSeries = new SimpleXYSeries(data, ArrayFormat.XY_VALS_INTERLEAVED, "CheckIn");
		}
		return defaultSeries;
	}

	private List<Number> getFormattedPlotData(List<Integer> counts) {
		List<Number> formattedPlotData = new ArrayList<Number>();
		DateTime start = new LocalDate().toDateTimeAtStartOfDay();
		for (int count : counts) {
			start = start.minusDays(1);
			Number time = start.getMillis() / 1000;
			formattedPlotData.add(time);
			formattedPlotData.add(count);
		}
		return formattedPlotData;
	}

	private void parseAndPlotCounts(String roomName) {
		SimpleXYSeries series = roomData.get(roomName);
		if (series == null) {
			List<Integer> counts = new ArrayList<Integer>();
			CheckInCount checkInCounts = getPersistenceData().getCheckInCounts();
			if (checkInCounts != null) {
				if (roomName.equals("OVERVIEW"))
					counts.addAll(checkInCounts.getTotalCount());
				else
					counts.addAll(checkInCounts.getRoomCount(roomName));
			}
			if (counts.size() > 0) {
				series = new SimpleXYSeries(getFormattedPlotData(counts), ArrayFormat.XY_VALS_INTERLEAVED, "CheckIn");
				roomData.put(roomName, series);
			} else {
				series = getDefaultSeries();
			}
		}
		if (m_xyPlot != null && mPlotFormatter != null) {
			m_xyPlot.clear();
			m_xyPlot.addSeries(series, mPlotFormatter);
			Number plotMax = LocalDate.now().minusDays(0).toDateTimeAtStartOfDay().getMillis() / 1000;
			Number plotMin = LocalDate.now().minusDays(8).toDateTimeAtStartOfDay().getMillis() / 1000;
			m_xyPlot.setDomainBoundaries(plotMin, BoundaryMode.FIXED, plotMax, BoundaryMode.FIXED);
			BarRenderer<?> renderer = (BarRenderer<?>) m_xyPlot.getRenderer(BarRenderer.class);
			renderer.setBarGroupWidth(BarRenderer.BarGroupWidthMode.FIXED_WIDTH, PixelUtils.dpToPix(60f));
			m_xyPlot.redraw();
		}
	}

	public void setXYPlot(XYPlot xyPlot) {
		m_xyPlot = xyPlot;
		parseAndPlotCounts(mRoomName.get());
	}

	public void setXYPlotFormatter(XYSeriesFormatter<XYRegionFormatter> formatter) {
		mPlotFormatter = formatter;
		parseAndPlotCounts(mRoomName.get());
	}

	private final Dashboard fragment;
	private XYPlot m_xyPlot;
	private XYSeriesFormatter<XYRegionFormatter> mPlotFormatter;
	private ChildDetailOnItemClickListener mChildDetailsOnItemClickListener;
	// TODO : NEED TO MAKE CLOUD CONFIGURABLE
	// ALSO MAKE THE facility cutoff age configurable
	private static int OVER_AGE_MONTHS = 12;
	private static int OVER_AGE_MULTIPLIER = 2;
	private TrackableInt m_inCenter;
	private TrackableInt m_underAge;
	private TrackableInt m_overAge;
	private TrackableInt m_weightedCount;
	private TrackableInt m_totalCheckIns;
	private TrackableInt m_AMCheckIns;
	private TrackableInt m_PMCheckIns;
	private TrackableInt m_staffCount;
	private TrackableCollection<MemberAndCubbies> mCheckedInMembers;
	private TrackableCollection<MemberAndCubbies> mMembersInRoom;
	private TrackableField<String> mRoomName;
	private TrackableField<TabHost> m_TabHost;
	private Context mContext;
	private TrackableCollection<MemberAndCubbies> mSelectedMembers;
	private TrackableField<Menu> m_Menu;
	private ChildListMode mChildListMode;
	private TrackableField<FilterTabs> m_FilterTabs;
	private MsgDialog mDialog;
	private MsgHandle mHandle;
	private TemplatePrint mPrint;

	@Override
	public void checkOut(final List<Member> children) {

		getPersistenceLayer().checkOutMembers(children, new PersistenceLayerCallback<List<Member>>() {
			@Override
			public void done(List<Member> data, Exception e) {
				//getPersistenceLayer().fetchCheckedInMembersAndCubbies();
				if (mChildListMode.equals(ChildListMode.GROUP)) {
					m_Menu.get().findItem(R.id.action_cancel).setVisible(false);
					m_Menu.get().findItem(R.id.action_selectall).setVisible(false);
					m_Menu.get().findItem(R.id.action_move).setVisible(false);
					m_Menu.get().findItem(R.id.action_checkout).setVisible(false);
					m_Menu.get().findItem(R.id.action_groupaction).setVisible(true);
					m_Menu.get().findItem(R.id.action_fastcheckout).setVisible(true);
					m_Menu.get().findItem(R.id.action_refresh).setVisible(true);
					mChildListMode = ChildListMode.SINGLE;
					for(MemberAndCubbies mc : getCheckedInMembers()){
						mc.setDimmed(false);
						mc.setSelected(false);
					}
					getSelectedMembersAndCubbies().clear();
				}

				String FILENAME = "offline_emergency_contacts.txt";
				String separator = System.getProperty("line.separator");
				List<Member> sortedData = new ArrayList<Member>();
				for (MemberAndCubbies mc: getCheckedInMembers()){
					sortedData.add(mc.getMember());
				}
				ComparatorChain<Member> chain = new ComparatorChain<Member>();
				chain.addComparator(new MemberFirstNameComparator(SortDirection.ASCENDING));
				chain.addComparator(new MemberLastNameComparator(SortDirection.ASCENDING));
				Collections.sort(sortedData, chain);

				FileOutputStream fos;
				try {
					fos = fragment.getActivity().openFileOutput(FILENAME, Context.MODE_PRIVATE);
					OutputStreamWriter osw = new OutputStreamWriter(fos);
					for (int i = 0; i < sortedData.size(); i++) {
						List<EmergencyNumber> emergencyList = sortedData.get(i).listEmergencyNumbers();
						String emergencyContacts = "";
						for (int j = 0; j < emergencyList.size(); j++){
							emergencyContacts = emergencyContacts + emergencyList.get(j).getName() + ": " + emergencyList.get(j).getPhone();
							if (j != emergencyList.size()-1)
								emergencyContacts = emergencyContacts + ", ";
						}
						List<PhoneNumber> checkedInByPhones = sortedData.get(i).getPCheckInBy().listPhoneNumbers();
						String checkedInByPhonesText = "";
						for (int k = 0; k < checkedInByPhones.size(); k++){
							checkedInByPhonesText = checkedInByPhonesText + checkedInByPhones.get(k).getNumber();
							if (k != checkedInByPhones.size() - 1)
								checkedInByPhonesText = checkedInByPhonesText +  ", ";
						}
						osw.append(sortedData.get(i).getFirstName() + " " + sortedData.get(i).getLastName() + " (Age: " + DaycareHelpers.getAgeInYears(sortedData.get(i).getBirthDate())
								+ ", Checked In By: " + sortedData.get(i).getPCheckInBy().getFirstName() + " " + sortedData.get(i).getPCheckInBy().getLastName()
								+ " " + checkedInByPhonesText + ")");
						osw.append(separator);
						osw.append("Emergency Contacts: " + emergencyContacts);
						osw.append(separator);
						osw.append(separator);
					}
					osw.close();
					fos.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}

		});
	}

	@Override
	public void addCubby(Member child) {

		getPersistenceLayer().reserveAdditionalMemberCubby(child, new PersistenceLayerCallback<List<Cubby>>() {

			@Override
			public void done(List<Cubby> data, Exception e) {
				getPersistenceLayer().fetchCheckedInMembersAndCubbies();
				if (e != null) {
				}
			}
		});
	}

	@Override
	public void addIncident(final Member child, String message) {
		getPersistenceLayer().addIncident(child, getPersistenceLayer().getPersistenceData().getFacility(), message,
				new PersistenceLayerCallback<Incident>() {

					@Override
					public void done(Incident data, Exception e) {
						getPersistenceLayer().fetchCheckedInMembersAndCubbies();
					}
				});
	}

	@Override
	public void sendSMS(final Member member, String message) {
		getPersistenceLayer().sendSMS(member, message, new PersistenceLayerCallback<String>() {
			@Override
			public void done(String data, Exception e) {
				if (data != null && data.equals("MESSAGE SENT")) {
					CustomToast.makeToast(mContext, "Text Message Was Sent Successfully", CustomToast.DURATION_LONG,
							CustomToast.STYLE_SUCCESS).show();
				} else {
					CustomToast.makeToast(mContext, "Text Message Was Not Sent", CustomToast.DURATION_LONG,
							CustomToast.STYLE_ERROR).show();
				}
			}
		});
	}

	@Override
	public void assignPager(final Pager pager, Member child, final Family family, final Member proxy) {
		getPersistenceLayer().fetchMembersByFamily(family, new PersistenceLayerCallback<List<Member>>() {
			@Override
			public void done(final List<Member> members, Exception e) {
				getPersistenceLayer().fetchAuthorizedMemberOrAuthorizedFor(proxy, true,
						new PersistenceLayerCallback<List<Authorization>>() {
							@Override
							public void done(List<Authorization> authorizations, Exception e) {
								final int AGE_CUTOFF = 13;
								members.addAll(extractAuthorizedFor(authorizations));

								List<Member> children = new ArrayList<Member>();
								for (Member member : members) {
									if (member.getPCheckInBy() != null
											&& member.getPCheckInBy().getObjectId().equals(proxy.getObjectId())
											&& !DaycareHelpers.isAdult(member.getBirthDate(), AGE_CUTOFF))
										children.add(member);
								}
								getPersistenceLayer().reservePagers(new ArrayList<Pager>(Arrays.asList(pager)),
										children, family, proxy, new PersistenceLayerCallback<List<Pager>>() {
											@Override
											public void done(List<Pager> data, Exception e) {
												getPersistenceLayer().fetchCheckedInMembersAndCubbies();
												CustomToast
														.makeToast(mContext, "Pager Was Assigned Successfully",
																CustomToast.DURATION_LONG, CustomToast.STYLE_SUCCESS)
														.show();
											}
										});
							}

							public List<Member> extractAuthorizedFor(List<Authorization> authorizations) {
								List<Member> members = new ArrayList<Member>();
								for (Authorization authorization : authorizations) {
									members.add(authorization.getAuthorizedFor());
								}
								return members;
							}
						});
			}
		});
	}

	@Override
	public void onRoomSelected(final Room selectedItem, final List<Member> forChild) {
		getPersistenceLayer().roomChange(forChild, selectedItem, new PersistenceLayerCallback<List<Member>>() {
			@Override
			public void done(List<Member> data, Exception e) {
				if (mChildListMode.equals(ChildListMode.GROUP)) {
					m_Menu.get().findItem(R.id.action_cancel).setVisible(false);
					m_Menu.get().findItem(R.id.action_selectall).setVisible(false);
					m_Menu.get().findItem(R.id.action_move).setVisible(false);
					m_Menu.get().findItem(R.id.action_checkout).setVisible(false);
					m_Menu.get().findItem(R.id.action_groupaction).setVisible(true);
					m_Menu.get().findItem(R.id.action_refresh).setVisible(true);
					m_Menu.get().findItem(R.id.action_fastcheckout).setVisible(true);
					mChildListMode = ChildListMode.SINGLE;
					getSelectedMembersAndCubbies().clear();
				}
				getPersistenceLayer().fetchCheckedInMembersAndCubbies();
				// getPersistenceLayer().fetchSignInHistory();
			}
		});
	}

	/*
	 * public OnTabChangeListener getOnTabChangedListener() {
	 *
	 * return new OnTabChangedListener(); }
	 *
	 * private class OnTabChangedListener implements OnTabChangeListener {
	 *
	 * @Override public void onTabChanged(String tabId) { if (tabId.equals( "Full Name Tab")) sortChildListByFullName();
	 * else if (tabId.equals( "Family Tab")) sortChildListByFamily(); else if (tabId.equals("Age Tab"))
	 * sortChildListByAge(); else if (tabId.equals("Time Left Tab")) sortChildListByTimeLeft(); } }
	 */

	public void sortChildListByFullName() {
		sortChildListByFullName(SortDirection.ASCENDING);
	}

	public void sortChildListByFullName(SortDirection sortDirection) {
		List<Member> checkedInOnlyMembers = new ArrayList<Member>();
		List<MemberAndCubbies> sortedCheckedInMembers = new ArrayList<MemberAndCubbies>();
		for (MemberAndCubbies mc : mCheckedInMembers)
			checkedInOnlyMembers.add(mc.getMember());
		ComparatorChain<Member> chain = new ComparatorChain<Member>();
		chain.addComparator(new MemberFirstNameComparator(sortDirection));
		chain.addComparator(new MemberLastNameComparator(sortDirection));
		Collections.sort(checkedInOnlyMembers, chain);
		for (Member m : checkedInOnlyMembers)
			for (MemberAndCubbies mAndC : mCheckedInMembers) {
				if (mAndC.getMember().equals(m))
					sortedCheckedInMembers.add(mAndC);
			}
		mCheckedInMembers.clear();
		mCheckedInMembers.addAll(sortedCheckedInMembers);
	}

	public void sortChildListByFamily() {
		sortChildListByFamily(SortDirection.ASCENDING);
	}

	public void sortChildListByFamily(SortDirection sortDirection) {
		List<Member> checkedInOnlyMembers = new ArrayList<Member>();
		List<MemberAndCubbies> sortedCheckedInMembers = new ArrayList<MemberAndCubbies>();
		for (MemberAndCubbies mc : mCheckedInMembers)
			checkedInOnlyMembers.add(mc.getMember());
		ComparatorChain<Member> chain = new ComparatorChain<Member>();
		chain.addComparator(new MemberLastNameComparator(sortDirection));
		chain.addComparator(new MemberFirstNameComparator(sortDirection));
		Collections.sort(checkedInOnlyMembers, chain);
		for (Member m : checkedInOnlyMembers)
			for (MemberAndCubbies mAndC : mCheckedInMembers) {
				if (mAndC.getMember().equals(m))
					sortedCheckedInMembers.add(mAndC);
			}
		mCheckedInMembers.clear();
		mCheckedInMembers.addAll(sortedCheckedInMembers);
	}

	public void sortChildListByAge() {
		sortChildListByAge(SortDirection.ASCENDING);
	}

	public void sortChildListByAge(SortDirection sortDirection) {
		List<Member> checkedInOnlyMembers = new ArrayList<Member>();
		List<MemberAndCubbies> sortedCheckedInMembers = new ArrayList<MemberAndCubbies>();
		for (MemberAndCubbies mc : mCheckedInMembers)
			checkedInOnlyMembers.add(mc.getMember());
		Collections.sort(checkedInOnlyMembers, new MemberAgeComparator(sortDirection));
		for (Member m : checkedInOnlyMembers)
			for (MemberAndCubbies mAndC : mCheckedInMembers) {
				if (mAndC.getMember().equals(m))
					sortedCheckedInMembers.add(mAndC);
			}
		mCheckedInMembers.clear();
		mCheckedInMembers.addAll(sortedCheckedInMembers);
	}

	public void sortChildListByTimeLeft() {
		sortChildListByTimeLeft(SortDirection.ASCENDING);
	}

	public void sortChildListByTimeLeft(SortDirection sortDirection) {
		List<Member> checkedInOnlyMembers = new ArrayList<Member>();
		List<MemberAndCubbies> sortedCheckedInMembers = new ArrayList<MemberAndCubbies>();
		for (MemberAndCubbies mc : mCheckedInMembers)
			checkedInOnlyMembers.add(mc.getMember());
		Collections.sort(checkedInOnlyMembers, new MemberTimeLeftComparator(sortDirection));
		for (Member m : checkedInOnlyMembers)
			for (MemberAndCubbies mAndC : mCheckedInMembers) {
				if (mAndC.getMember().equals(m))
					sortedCheckedInMembers.add(mAndC);
			}
		mCheckedInMembers.clear();
		mCheckedInMembers.addAll(sortedCheckedInMembers);
	}

	public void setTabHost(TabHost tabHost) {
		m_TabHost.set(tabHost);
	}

	public void setFilterTabs(FilterTabs filterTabs) {
		m_FilterTabs.set(filterTabs);
	}

	public void setMenu(Menu menu) {
		m_Menu.set(menu);
	}

	public ChildListMode getChildListMode() {
		return mChildListMode;
	}

	public void setChildListMode(ChildListMode mode) {
		mChildListMode = mode;
	}

	public String getSelectAllButtonText() {
		return mCheckedInMembers.size() > 0 && (mSelectedMembers.size() == mCheckedInMembers.size()) ? "SELECT NONE"
				: "SELECT ALL";
	}

	public void onSelectAllClick() {
		boolean state = !(mSelectedMembers.size() == mCheckedInMembers.size());
		for (MemberAndCubbies member : mCheckedInMembers) {
			selectMember(member, state);
		}
	}

	private void selectMember(MemberAndCubbies member, boolean select) {
		member.setSelected(select);
		member.setDimmed(!select);
		if (!select)
			mSelectedMembers.remove(member);
		else if (!mSelectedMembers.contains(member))
			mSelectedMembers.add(member);
	}

	public List<MemberAndCubbies> getSelectedMembersAndCubbies() {
		mSelectedMembers.clear();
		List<MemberAndCubbies> selectedMembers = new ArrayList<MemberAndCubbies>();
		for (MemberAndCubbies mAndC : mCheckedInMembers) {
			if (mAndC.getSelected())
				selectedMembers.add(mAndC);
		}
		mSelectedMembers.addAll(selectedMembers);
		return mSelectedMembers;
	}

	public boolean areMembersSelected() {
		getSelectedMembersAndCubbies();
		if (mSelectedMembers.isEmpty())
			return false;
		else
			return true;
	}

	public void filterChildListByAge(double minAge, double maxAge) {
		List<MemberAndCubbies> filteredCheckedInMembers = new ArrayList<MemberAndCubbies>();
		for (MemberAndCubbies mAndC : mMembersInRoom) {
			int age = DaycareHelpers.getAgeInYears(mAndC.getMember().getBirthDate());
			if (age >= minAge && age <= maxAge)
				filteredCheckedInMembers.add(mAndC);
		}
		mCheckedInMembers.clear();
		mCheckedInMembers.addAll(filteredCheckedInMembers);
	}

	public void resetAgeFilters() {
		mCheckedInMembers.clear();
		mCheckedInMembers.addAll(mMembersInRoom);
	}
}
