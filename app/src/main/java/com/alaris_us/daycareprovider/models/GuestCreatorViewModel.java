package com.alaris_us.daycareprovider.models;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.data.PhoneNumber;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.popups.DatePopup;
import com.alaris_us.daycareprovider.popups.DatePopup.DatePickerDialogListener;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider.view.CustomToast;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class GuestCreatorViewModel extends ViewModel {

	private static final int AGE_CUTOFF = 12;

	public GuestCreatorViewModel(PersistenceLayer persistenceLayer) {

		super(persistenceLayer);
	}

	public void setSelectedGender(String selectedGender) {

		mSelectedGender.set(selectedGender);
	}

	public String getSelectedGender() {

		return mSelectedGender.get();
	}

	public void setPhoneNumber(String pin) {

		mPhoneNumber.set(pin);
	}

	public String getPhoneNumber() {

		return mPhoneNumber.get();
	}

	public String getPIN() {

		PhoneNumber memberPhone = PhoneNumber.getNewInstance(getPhoneNumber());
		return (memberPhone == null) ? null : memberPhone.getDashedSeparatedNumberWithAreaCode();
	}

	public void setDaysAllowed(int daysAllowed) {

		mDaysAllowed.set(daysAllowed);
	}

	public int getDaysAllowed() {

		return mDaysAllowed.get();
	}

	public TrackableCollection<Member> getExisting() {

		return mExisting.get();
	}

	public TrackableCollection<Member> getToCreate() {

		return mToCreate.get();
	}

	public TrackableCollection<Member> getCreated() {

		return mCreated.get();
	}

	private void clearForm() {

		setFirstName(null);
		setLastName(null);
		setEmail(null);
		setBirthDate(null);
	}

	public OnClickListener getButtonDateOnClickListener() {

		return new OnClickListener() {

			@Override
			public void onClick(View v) {

				DatePopup dp = new DatePopup(v.getContext());
				dp.setDateTimePickerDialogListener(new BirthDateChanged());
				dp.show(v, getBirthDate());
			}
		};
	}

	public OnClickListener getButtonAddOnClickListener() {

		return new OnClickListener() {

			@Override
			public void onClick(View v) {

				String missingField = null;
				if (getFirstName() == null)
					missingField = "First Name";
				if (getLastName() == null)
					missingField = "Last Name";
				if (getEmail() == null)
					missingField = "Email";
				if (getBirthDate() == null)
					missingField = "Birth Date";
				if (getSelectedGender() == null)
					missingField = "Gender";
				if (missingField != null) {
					CustomToast.makeToast(v.getContext(), "Missing " + missingField, CustomToast.DURATION_LONG,
							CustomToast.STYLE_ERROR).show();
					return;
				}
				addMemberToCreate();
				clearForm();
			}
		};
	}

	private void addMemberToCreate() {

		List<String> pin = new ArrayList<String>();
		pin.add(getPIN());
		com.alaris_us.daycaredata.PhoneNumber phone = new com.alaris_us.daycaredata.PhoneNumber("HOME", pin.get(0));
		List<com.alaris_us.daycaredata.PhoneNumber> phones = new ArrayList<>();
		phones.add(phone);
		getToCreate().add(getPersistenceLayer().createMemberNoPerist("", "", getFirstName(), getLastName(),
				getBirthDate().toDate(), getSelectedGender(), getEmail(), pin, phones, getPersistenceData().getFacilityTimeLimit(), null, true, getDaysAllowed(),
				"", true));
	}

	private class BirthDateChanged implements DatePickerDialogListener {

		@Override
		public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

			MutableDateTime mdt = new MutableDateTime(getBirthDate());
			mdt.setDate(year, monthOfYear, dayOfMonth);
			setBirthDate(mdt.toDateTime());
		}
	}

	public OnCheckedChangeListener getGenderOnCheckedChangeListener() {

		return new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {

				switch (checkedId) {
				case R.id.RadioButton_Gender_Female:
					setSelectedGender("F");
					return;
				case R.id.RadioButton_Gender_Male:
					setSelectedGender("M");
					return;
				}
			}
		};
	}

	public String getFirstName() {

		return mFirstName.get();
	}

	public void setFirstName(String firstName) {

		mFirstName.set(firstName);
	}

	public String getLastName() {

		return mLastName.get();
	}

	public void setLastName(String lastName) {

		mLastName.set(lastName);
	}

	public String getEmail() {

		return mEmail.get();
	}

	public void setEmail(String email) {

		mEmail.set(email);
	}

	public DateTime getBirthDate() {

		return mBirthDate.get();
	}

	public String getBirthDateString() {

		return getBirthDate().toString("dd-MMM-YYYY");
	}

	public void setBirthDate(DateTime birthDate) {

		mBirthDate.set(birthDate);
	}

	public int getCurrentPage() {

		return mCurrentPage.get();
	}

	public void setCurrentPage(int currentPage) {

		mCurrentPage.set(currentPage);
	}

	public boolean registerGuests() {

		boolean adultFound = false;

		final List<Member> toCreate = getToCreate();
		Family family = null;
		List<Member> existing = getExisting();
		/*if (existing.isEmpty()) {
			for (Member m : toCreate) {
				if (DaycareHelpers.isAdult(m.getBirthDate(), AGE_CUTOFF))
					adultFound = true;
			}
		} else {
			adultFound = true;
		}

		if (!adultFound)
			return false;*/

		if (existing != null && existing.size() > 0) {
			family = existing.get(0).getPFamily();
		}

		getPersistenceLayer().registerMembers(toCreate, family, new PersistenceLayerCallback<List<Member>>() {

			@Override
			public void done(List<Member> data, Exception e) {

				if (e == null) {
					getCreated().clear();
					getCreated().addAll(data);
					getToCreate().clear();
					setCurrentPage(getCurrentPage() + 1);
				}
			}
		});
		return true;
	}

	public void setAddGuestToExisting(boolean addingToExisting) {
		mAddGuestToExisting = addingToExisting;
	}

	@Override
	protected void initialize() {

		mPhoneNumber = new TrackableField<String>();
		mExisting = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		mToCreate = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		mCreated = new TrackableField<TrackableCollection<Member>>(new TrackableCollection<Member>());
		mFirstName = new TrackableField<String>();
		mLastName = new TrackableField<String>();
		mEmail = new TrackableField<String>();
		mBirthDate = new TrackableField<DateTime>();
		mDaysAllowed = new TrackableInt(3);
		mSelectedGender = new TrackableField<String>();
		mCurrentPage = new TrackableInt();
		mAddGuestToExisting = false;
	}

	private TrackableField<String> mPhoneNumber;
	private TrackableField<TrackableCollection<Member>> mExisting;
	private TrackableField<TrackableCollection<Member>> mToCreate;
	private TrackableField<TrackableCollection<Member>> mCreated;
	private TrackableInt mDaysAllowed;
	private TrackableField<String> mFirstName;
	private TrackableField<String> mLastName;
	private TrackableField<String> mEmail;
	private TrackableField<DateTime> mBirthDate;
	private TrackableField<String> mSelectedGender;
	private TrackableInt mCurrentPage;
	private boolean mAddGuestToExisting;
}
