package com.alaris_us.daycareprovider.models;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.data.FastCheckOutMember;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

import android.view.View;

public class FastCheckoutViewModel extends ViewModel {
	private TrackableCollection<FastCheckOutMember> mAuthenticatedMember;
	private TrackableField<State> mCurrentState;
	private TrackableCollection<FastCheckOutMember> mChildren;
	private TrackableCollection<Member> mChildrenToCheckOut;
	private TrackableField<String> mBarcode;

	public FastCheckoutViewModel(PersistenceLayer persistenceLayer) {
		super(persistenceLayer);
	}

	@Override
	protected void initialize() {
		mAuthenticatedMember = new TrackableCollection<FastCheckOutMember>();
		mCurrentState = new TrackableField<State>(new AwaitingBarcodeState());
		mChildren = new TrackableCollection<FastCheckOutMember>();
		mChildrenToCheckOut = new TrackableCollection<Member>();
		mBarcode = new TrackableField<String>("");
	}

	public void lookupBarcode(String barcode) {
		if (mCurrentState.get() instanceof AwaitingBarcodeState) {
			mBarcode.set(barcode);
			mCurrentState.get().exitState();
			mCurrentState.set(new SearchingByBarcodeState());
			mCurrentState.get().enterState();
		}
	}

	public void onChildClick(int position) {
		FastCheckOutMember member = mChildren.get(position);
		selectMember(member, !member.getIsSelected());
	}

	private void selectMember(FastCheckOutMember member, boolean select) {
		member.setIsSelected(select);
		if (!select)
			mChildrenToCheckOut.remove(member.getMember());
		else if (!mChildrenToCheckOut.contains(member.getMember()))
			mChildrenToCheckOut.add(member.getMember());
	}

	public void onCancelClick() {
		mCurrentState.set(new AwaitingBarcodeState());
		mCurrentState.get().enterState();
	}

	public void onSelectAllClick() {
		boolean state = !(mChildrenToCheckOut.size() == mChildren.size());
		for (FastCheckOutMember member : mChildren) {
			selectMember(member, state);
		}
	}

	public boolean getCheckOutButtonEnabled() {
		return mChildrenToCheckOut.size() > 0;
	}

	public boolean getSelectAllButtonEnabled() {
		return mChildren.size() > 0;
	}

	public String getSelectAllButtonText() {
		return mChildrenToCheckOut.size() == mChildren.size() ? "SELECT NONE" : "SELECT ALL";
	}

	public void onCheckOutClick(PersistenceLayerCallback<List<Member>> callback) {
		//getPersistenceLayer().checkOutParent(mAuthenticatedMember.get(0).getMember());
		getPersistenceLayer().checkOutMembers(new ArrayList<Member>(mChildrenToCheckOut), callback);
	}

	public List<FastCheckOutMember> getAuthenticatedMember() {
		if (mCurrentState.get() instanceof DisplayMembersState)
			return mAuthenticatedMember;
		else
			return null;
	}

	public List<FastCheckOutMember> getChildren() {
		if (mCurrentState.get() instanceof DisplayMembersState)
			return mChildren;
		else
			return null;
	}

	public int getLeftInstructionsVisibility() {
		return mCurrentState.get() instanceof DisplayMembersState ? View.GONE : View.VISIBLE;
	}

	public int getRightInstructionsVisibility() {
		return mCurrentState.get() instanceof DisplayMembersState ? View.VISIBLE : View.GONE;
	}

	public void reset() {
		mCurrentState.get().exitState();
		mCurrentState.set(new AwaitingBarcodeState());
		mCurrentState.get().enterState();
	}
	
	public List<Member> getChildrenToCheckOut(){
		return mChildrenToCheckOut;
	}

	private abstract class State {
		public abstract void enterState();

		public void exitState() {
			mCurrentState.set(null);
		}

		public void exception(Exception e) {
			mCurrentState.set(null);
		}
	}

	private class AwaitingBarcodeState extends State {

		@Override
		public void enterState() {
			mAuthenticatedMember.clear();
			mChildren.clear();
			mChildrenToCheckOut.clear();
			mBarcode.set("");
		}
	}

	private class SearchingByBarcodeState extends State {
		@Override
		public void enterState() {
			getPersistenceLayer().fetchByBarcode(mBarcode.get(), new FetchByBarcodeCallback());
		}

		private class FetchByBarcodeCallback implements PersistenceLayerCallback<Member> {
			@Override
			public void done(Member member, Exception e) {
				if (e == null && member != null) {
					DaycareData dataSource = getPersistenceLayer().getDaycareDataSource();
					Facility facility = getPersistenceData().getFacility();
					boolean hasCubbies = getPersistenceData().getAllFacilityCubbies().isEmpty() ? false : true;
					boolean hasPagers = getPersistenceData().getAllFacilityPagers().isEmpty() ? false : true;
					mAuthenticatedMember.add(new FastCheckOutMember(dataSource, member, true, false, facility, hasCubbies, hasPagers));
					getPersistenceLayer().FetchMembersCheckedInBy(member, new FetchCheckedInByCallback());
				} else {
					getPersistenceLayer().readMember(mBarcode.get(), new FetchByObjectIdCallback());
				}
			}
		}

		private class FetchByObjectIdCallback implements PersistenceLayerCallback<Member> {
			@Override
			public void done(Member member, Exception e) {
				if (e == null && member != null) {
					DaycareData dataSource = getPersistenceLayer().getDaycareDataSource();
					Facility facility = getPersistenceData().getFacility();
					boolean hasCubbies = getPersistenceData().getAllFacilityCubbies().isEmpty() ? false : true;
					boolean hasPagers = getPersistenceData().getAllFacilityPagers().isEmpty() ? false : true;
					mAuthenticatedMember.add(new FastCheckOutMember(dataSource, member, true, false, facility, hasCubbies, hasPagers));
					getPersistenceLayer().FetchMembersCheckedInBy(member, new FetchCheckedInByCallback());
				} else {
					reset();
				}
			}
		}
		
		private class FetchCheckedInByCallback implements PersistenceLayerCallback<List<Member>> {
			@Override
			public void done(List<Member> members, Exception e) {
				DaycareData dataSource = getPersistenceLayer().getDaycareDataSource();
				Facility facility = getPersistenceData().getFacility();
				boolean hasCubbies = getPersistenceData().getAllFacilityCubbies().isEmpty() ? false : true;
				boolean hasPagers = getPersistenceData().getAllFacilityPagers().isEmpty() ? false : true;
				for (Member member : members) {
					mChildren.add(new FastCheckOutMember(dataSource, member, facility, hasCubbies, hasPagers));
				}
				mCurrentState.get().exitState();
				mCurrentState.set(new DisplayMembersState());
				mCurrentState.get().enterState();
			}
		}
	}

	private class DisplayMembersState extends State {

		@Override
		public void enterState() {

		}
	}

}
