package com.alaris_us.daycareprovider.models;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.MutableDateTime;

import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycareprovider.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.popups.DateTimePopup;
import com.alaris_us.daycareprovider.popups.DateTimePopup.DateTimePickerDialogListener;
import com.alaris_us.daycareprovider.popups.EditIncidentPopup;
import com.alaris_us.daycareprovider.popups.EditIncidentPopup.EditIncidentPopupCallback;
import com.alaris_us.daycareprovider.view.CustomToast;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class IncidentsViewModel extends ViewModel implements EditIncidentPopupCallback {

	private class UpdateClickedListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			try {
				getPersistenceLayer().fetchIncidentsHistory(getStartDate(), getEndDate());
			} catch (Throwable e) {
				CustomToast
						.makeToast(v.getContext(), e.getMessage(), CustomToast.DURATION_LONG, CustomToast.STYLE_ERROR)
						.show();
			}
		}
	}

	private class StartTimeClickedListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			DateTimePopup dtp = new DateTimePopup(v.getContext());
			dtp.setDateTimePickerDialogListener(new StartDateChanged());
			dtp.show(v, getStartDate());
		}
	}

	private class EndTimeClickedListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			DateTimePopup dtp = new DateTimePopup(v.getContext());
			dtp.setDateTimePickerDialogListener(new EndDateChanged());
			dtp.show(v, getEndDate());
		}
	}

	private class StartDateChanged implements DateTimePickerDialogListener {

		@Override
		public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

			MutableDateTime mdt = new MutableDateTime(getStartDate());
			mdt.setDate(year, monthOfYear, dayOfMonth);
			setStartDate(mdt.toDateTime());
		}

		@Override
		public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

			MutableDateTime mdt = new MutableDateTime(getStartDate());
			mdt.setTime(hourOfDay, minute, 0, 0);
			setStartDate(mdt.toDateTime());
		}
	}

	private class EndDateChanged implements DateTimePickerDialogListener {

		@Override
		public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

			MutableDateTime mdt = new MutableDateTime(getEndDate());
			mdt.setDate(year, monthOfYear, dayOfMonth);
			setEndDate(mdt.toDateTime());
		}

		@Override
		public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

			MutableDateTime mdt = new MutableDateTime(getEndDate());
			mdt.setTime(hourOfDay, minute, 0, 0);
			setEndDate(mdt.toDateTime());
		}
	}

	private class IncidentHistoryPropertyChangeListener implements PropertyChangeListener {

		@Override
		public void propertyChange(PropertyChangeEvent event) {

			getIncidentHistory().clear();
			getIncidentHistory().addAll(getPersistenceData().getIncidentHistory());
		}
	}

	public IncidentsViewModel(PersistenceLayer persistenceLayer) {

		super(persistenceLayer);
	}

	public String getStartDateString() {

		return m_startDate.get().toString("MMM-dd-YYYY hh:mm a", Locale.getDefault()).toUpperCase(Locale.getDefault());
	}

	public DateTime getStartDate() {

		return m_startDate.get();
	}

	public void setStartDate(DateTime startDate) {

		m_startDate.set(startDate);
	}

	public String getEndDateString() {

		return m_endDate.get().toString("MMM-dd-YYYY hh:mm a", Locale.getDefault()).toUpperCase(Locale.getDefault());
	}

	public DateTime getEndDate() {

		return m_endDate.get();
	}

	public void setEndDate(DateTime endDate) {

		m_endDate.set(endDate);
	}

	@Override
	protected void initialize() {

		watchProperty(new IncidentHistoryPropertyChangeListener(), CHANGE_ID.INCIDENT_HISTORY);
		m_startClickListener = new StartTimeClickedListener();
		m_endClickListener = new EndTimeClickedListener();
		m_updateClickListener = new UpdateClickedListener();
		m_IncidentHistory = new TrackableField<TrackableCollection<Incident>>(new TrackableCollection<Incident>());
		m_startDate = new TrackableField<DateTime>(new LocalDate().toDateTimeAtStartOfDay());
		m_endDate = new TrackableField<DateTime>(new LocalDate().plusDays(1).toDateTimeAtStartOfDay());
		getPersistenceLayer().fetchIncidentsHistory(getStartDate(), getEndDate());
	}

	public OnClickListener getStartTimeButtonListener() {

		return m_startClickListener;
	}

	public OnClickListener getEndTimeButtonListener() {

		return m_endClickListener;
	}

	public OnClickListener getUpdateButtonListener() {

		return m_updateClickListener;
	}

	public TrackableCollection<Incident> getIncidentHistory() {

		return m_IncidentHistory.get();
	}

	private class EditIncidentOnItemClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {

			final Incident incident = m_IncidentHistory.get().get(position);
			(new EditIncidentPopup(view.getContext(), view, incident, IncidentsViewModel.this)).show(view);
		}
	}

	public OnItemClickListener getEditIncidentOnItemClickListener() {

		return new EditIncidentOnItemClickListener();
	}

	@Override
	public void updateIncident(final Incident incident) {
		getPersistenceLayer().updateIncident(incident);
		getIncidentHistory().clear();
		getPersistenceLayer().fetchIncidentsHistory(getStartDate(), getEndDate());
	}

	private OnClickListener m_startClickListener;
	private OnClickListener m_endClickListener;
	private OnClickListener m_updateClickListener;
	private TrackableField<DateTime> m_startDate;
	private TrackableField<DateTime> m_endDate;
	private TrackableField<TrackableCollection<Incident>> m_IncidentHistory;

}
