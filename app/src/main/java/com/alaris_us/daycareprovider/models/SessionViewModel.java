package com.alaris_us.daycareprovider.models;

import com.alaris_us.daycareprovider.persistence.PersistenceLayer;

public class SessionViewModel extends ViewModel {

	public SessionViewModel(PersistenceLayer persistenceLayer) {

		super(persistenceLayer);
	}

	@Override
	protected void initialize() {

	}
}
