package com.alaris_us.daycareprovider.models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.alaris_us.daycaredata.EmergencyNumber;
import com.alaris_us.daycaredata.PhoneNumber;
import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Authorization;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.SelfCheckIn;
import com.alaris_us.daycaredata.to.ValidMembership;
import com.alaris_us.daycareprovider.data.AdmissionAndFacility;
import com.alaris_us.daycareprovider.data.AuthorizedMember;
import com.alaris_us.daycareprovider.data.HerokuJob;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.popups.GuestConversionPopup;
import com.alaris_us.daycareprovider.popups.GuestConversionPopup.SelectGuestTypePopupListener;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider.utils.DaycareHelpers.YesNoDialogCallback;
import com.alaris_us.daycareprovider.utils.ImageFunctions;
import com.alaris_us.daycareprovider.view.CustomToast;
import com.alaris_us.daycareprovider.view.SearchParameterDropDown;
import com.alaris_us.daycareprovider_dev.R;
import com.bindroid.trackable.TrackableBoolean;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableFloat;
import com.bindroid.trackable.TrackableInt;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;
import android.view.KeyEvent;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import org.json.JSONException;

public class SearchAndEditFamilyViewModel extends ViewModel {
	public static int SEARCHRESULTPAGE = 0;
	public static int DISPLAYFAMILYPAGE = 1;
	public static int MEMBERPHOTOPAGE = 2;
	public static int FAMILYSETTINGS = 3;

	public List<MemberAndCubbies> getSearchResults() {
		return mSearchResults.get();
	}

	public List<MemberAndCubbies> getChildResults() {
		return mChildren.get();
	}

	public List<MemberAndCubbies> getAdultResults() {
		return mAdults.get();
	}
	
	public List<Facility> getAssocFacilities(){
		return getPersistenceLayer().getPersistenceData().getAllAssocFacilities();
	}
	
	public Facility getFacility(){
		return getPersistenceLayer().getPersistenceData().getFacility();
	}
	
	public int getAuthHeadingVisibility(){
		if (mSelectedMember.get().getAuthorizedMembers().isEmpty() && mSelectedMember.get().getAuthorizedFors().isEmpty())
			return View.GONE;
		else
			return View.VISIBLE;
	}
	public List<AuthorizedMember> getAuthorizations() {
		List<AuthorizedMember> auths = new ArrayList<AuthorizedMember>();
		if (!DaycareHelpers.isAdult(mSelectedMember.get().getMember().getBirthDate(), 12)){
			if (mSelectedMember.get().getAuthorizedMembers() != null && !mSelectedMember.get().getAuthorizedMembers().isEmpty()){
				for (Authorization auth : mSelectedMember.get().getAuthorizedMembers()){
					AuthorizedMember authMem = new AuthorizedMember(auth.getAuthorizedMember(), false);
					auths.add(authMem);
				}
			}
		}
		else {
			if (mSelectedMember.get().getAuthorizedFors() != null && !mSelectedMember.get().getAuthorizedFors().isEmpty()){
				for (Authorization auth : mSelectedMember.get().getAuthorizedFors()){
					AuthorizedMember authFor = new AuthorizedMember(auth.getAuthorizedFor(), false);
					auths.add(authFor);
				}
			}
		}
		return auths;
	}

	public List<String> getSearchParameterList() {
		return mSearchParameterList.get();
	}

	public String getSearchBoxText() {
		return mSearchBoxText.get();
	}

	public void setSearchBoxText(String searchText) {
		mSearchBoxText.set(searchText);
	}

	public String getFamilyName() {
		return mFamilyName.get();
	}

	public Family getFamily() {
		return mFamily;
	}
	
	public List<AdmissionAndFacility> getFamilyAdmissions() {
		return mFamilyAdmissions;
	}
	
	public void setFamilyAdmissions(List<AdmissionAndFacility> admitAndFacility) {
		mFamilyAdmissions.addAll(admitAndFacility);
	}
	
	public Admission createFamilyAdmission() {
		return getPersistenceLayer().getNewEmptyAdmission();
	}
	
	public void creditAdmissions(Member member, Facility facility, int amountToCredit, PersistenceLayerCallback cb) {
		getPersistenceLayer().creditAdmissions(member, facility, amountToCredit, cb);
	}
	
	public void deleteAdmissions(List<Admission> admissions) {
		getPersistenceLayer().deleteAdmissions(admissions, null);
	}
	
	public boolean getIsEditable() {
		return (mCurrentState.get() instanceof EditMemberInfoState);
	}

	public Member getSelectedMember() {
		return mSelectedMember.get().getMember();
	}

	public Member getMemberForm() {
		return mMemberForms.get().get(mSelectedMember.get().getMember());
	}

	public String getDateOfBirth() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return DATEFORMAT.format(mMemberForms.get().get(mSelectedMember.get().getMember()).getBirthDate());
		else
			return DATEFORMAT.format(mSelectedMember.get().getMember().getBirthDate());
	}

	public String getHomeNumber() {
		List<PhoneNumber> phones = new ArrayList<PhoneNumber>();
		String homePhone = "";
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			phones = mMemberForms.get().get(mSelectedMember.get().getMember()).listPhoneNumbers();
			for (PhoneNumber phone : phones) {
				if (phone.getType().equals("HOME"))
					homePhone = phone.getNumber();
			}
			return homePhone.length() > 0 ? homePhone : "";
		} else {
			phones = mSelectedMember.get().getMember().listPhoneNumbers();
			for (PhoneNumber phone : phones) {
				if (phone.getType().equals("HOME"))
					homePhone = phone.getNumber();
			}
			return homePhone.length() > 0 ? homePhone : "";
		}
	}
	
	public void setHomeNumber(String homeNum) {
		List<PhoneNumber> phones = mMemberForms.get().get(mSelectedMember.get().getMember()).listPhoneNumbers();
		boolean hasHomePhone = false;
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			for (PhoneNumber phone : phones){
				if (phone.getType().equals("HOME")){
					hasHomePhone = true;
					break;
				}
			}
			if (!hasHomePhone) {
				PhoneNumber newHomePhone = new PhoneNumber();
				if (homeNum.length() == 10) {
					newHomePhone.setType("HOME");
					newHomePhone.setNumber(
							homeNum.substring(0, 3) + "-" + homeNum.substring(3, 6) + "-" + homeNum.substring(6, 10));
					mMemberForms.get().get(mSelectedMember.get().getMember()).addPhoneNumber(newHomePhone);
				} else if (homeNum.length() == 12) {
					newHomePhone.setType("HOME");
					newHomePhone.setNumber(homeNum);
					mMemberForms.get().get(mSelectedMember.get().getMember()).addPhoneNumber(newHomePhone);
				}
			} else {
				for (PhoneNumber p : phones) {
					if (p.getType().equals("HOME"))
						p.setNumber(homeNum);
				}
				mMemberForms.get().get(mSelectedMember.get().getMember()).setPhoneNumbers(phones);
				if (homeNum.isEmpty()) {
					for (PhoneNumber p : phones) {
						if (p.getType().equals("HOME")) {
							mMemberForms.get().get(mSelectedMember.get().getMember()).removePhoneNumber(p);
						}
					}
				}
			}
		}
	}
	
	public String getWorkNumber() {
		List<PhoneNumber> phones = new ArrayList<PhoneNumber>();
		String workPhone = "";
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			phones = mMemberForms.get().get(mSelectedMember.get().getMember()).listPhoneNumbers();
			for (PhoneNumber phone : phones) {
				if (phone.getType().equals("WORK"))
					workPhone = phone.getNumber();
			}
			return workPhone.length() > 0 ? workPhone : "";
		} else {
			phones = mSelectedMember.get().getMember().listPhoneNumbers();
			for (PhoneNumber phone : phones) {
				if (phone.getType().equals("WORK"))
					workPhone = phone.getNumber();
			}
			return workPhone.length() > 0 ? workPhone : "";
		}
	}
	
	public void setWorkNumber(String workNum) {
		List<PhoneNumber> phones = mMemberForms.get().get(mSelectedMember.get().getMember()).listPhoneNumbers();
		boolean hasWorkPhone = false;
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			for (PhoneNumber phone : phones){
				if (phone.getType().equals("WORK")){
					hasWorkPhone = true;
					break;
				}
			}
			if (!hasWorkPhone) {
				PhoneNumber newWorkPhone = new PhoneNumber();
				if (workNum.length() == 10) {
					newWorkPhone.setType("WORK");
					newWorkPhone.setNumber(
							workNum.substring(0, 3) + "-" + workNum.substring(3, 6) + "-" + workNum.substring(6, 10));
					mMemberForms.get().get(mSelectedMember.get().getMember()).addPhoneNumber(newWorkPhone);
				} else if (workNum.length() == 12) {
					newWorkPhone.setType("WORK");
					newWorkPhone.setNumber(workNum);
					mMemberForms.get().get(mSelectedMember.get().getMember()).addPhoneNumber(newWorkPhone);
				}
			} else {
				for (PhoneNumber p : phones) {
					if (p.getType().equals("WORK"))
						p.setNumber(workNum);
				}
				mMemberForms.get().get(mSelectedMember.get().getMember()).setPhoneNumbers(phones);
				if (workNum.isEmpty()) {
					for (PhoneNumber p : phones) {
						if (p.getType().equals("WORK")) {
							mMemberForms.get().get(mSelectedMember.get().getMember()).removePhoneNumber(p);
						}
					}
				}
			}
		}
	}

	public String getLastCheckIn() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return DATEFORMAT.format(mMemberForms.get().get(mSelectedMember.get().getMember()).getLastCheckIn());
		else
			return DATEFORMAT.format(mSelectedMember.get().getMember().getLastCheckIn());
	}

	public String getStatus() {
		Member member;
		if (mCurrentState.get() instanceof EditMemberInfoState)
			member = mMemberForms.get().get(mSelectedMember.get().getMember());
		else
			member = mSelectedMember.get().getMember();
		if (member.isGuest())
			return String.valueOf(member.getGuestDaysLeft() + " remaining");
		else
			return member.isActive() ? "Active" : "Inactive";
	}

	public void setStatus(String status) {
		if (mMemberForms.get().get(mSelectedMember.get().getMember()).isGuest()) {
			mMemberForms.get().get(mSelectedMember.get().getMember())
					.setGuestDaysLeft(Integer.valueOf(status.replace("remaining", "").trim()));
		} else
			mMemberForms.get().get(mSelectedMember.get().getMember()).setActive(status.equals("Active"));
	}

	public boolean getIsStatusEnabled() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mSelectedMember.get().getMember().isGuest();
		else
			return mMemberForms.get().get(mSelectedMember.get().getMember()).isGuest();
	}

	public String getMembership() {
		Member member;
		if (mCurrentState.get() instanceof EditMemberInfoState)
			member = mMemberForms.get().get(mSelectedMember.get().getMember());
		else
			member = mSelectedMember.get().getMember();
		return member.getMemberID();
	}
	
	public String getMembershipType() {
		Member member;
		if (mCurrentState.get() instanceof EditMemberInfoState)
			member = mMemberForms.get().get(mSelectedMember.get().getMember());
		else
			member = mSelectedMember.get().getMember();
		return member.getMembershipType();
	}

	public int getStatusLabel() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mMemberForms.get().get(mSelectedMember.get().getMember()).isGuest() ? mStatusLabelGuest
					: mStatusLabel;
		else
			return mSelectedMember.get().getMember().isGuest() ? mStatusLabelGuest : mStatusLabel;
	}

	public int getMinutesLeft() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mMemberForms.get().get(mSelectedMember.get().getMember()).getMinutesLeft().intValue();
		else
			return mSelectedMember.get().getMember().getMinutesLeft().intValue();
	}

	public void setMinutesLeft(String min) {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			mMemberForms.get().get(mSelectedMember.get().getMember())
					.setMinutesLeft(Integer.valueOf(min.replace("min remaining", "").trim()));
		else
			mSelectedMember.get().getMember().setMinutesLeft(Integer.valueOf(min));
	}

	public int getWeeklyMinutesLeft() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mMemberForms.get().get(mSelectedMember.get().getMember()).getWeeklyMinutesLeft().intValue();
		else
			return mSelectedMember.get().getMember().getWeeklyMinutesLeft().intValue();
	}

	public void setWeeklyMinutesLeft(String min) {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			mMemberForms.get().get(mSelectedMember.get().getMember())
					.setWeeklyMinutesLeft(Integer.valueOf(min.replace("min remaining", "").trim()));
		else
			mSelectedMember.get().getMember().setWeeklyMinutesLeft(Integer.valueOf(min));
	}
	
	public String getService() {
		Member member;
		if (mCurrentState.get() instanceof EditMemberInfoState)
			member = mMemberForms.get().get(mSelectedMember.get().getMember());
		else
			member = mSelectedMember.get().getMember();
		return member.getService();
	}

	public String getSMSNumber() {
		List<PhoneNumber> phones = new ArrayList<PhoneNumber>();
		String smsNumber = "";
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			phones = mMemberForms.get().get(mSelectedMember.get().getMember()).listPhoneNumbers();
		} else {
			phones = mSelectedMember.get().getMember().listPhoneNumbers();
		}
		
		for (PhoneNumber p : phones) {
			if (p.getType().equals("SMS") || p.getType().equals("CELL"))
				smsNumber = p.getNumber();
		}
		return smsNumber;
	}

	public void setSMSNumber(String smsNum) {
		List<PhoneNumber> phones = mMemberForms.get().get(mSelectedMember.get().getMember()).listPhoneNumbers();
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			boolean smsFound = false;
			if (!smsNum.isEmpty()) {
				for (PhoneNumber p : phones) {
					if (p.getType().equals("SMS") || p.getType().equals("CELL")){
						p.setNumber(smsNum);
						smsFound = true;
					}
				}
				if (!smsFound) {
					PhoneNumber newSMS = new PhoneNumber();
					if (smsNum.length() == 10) {
						newSMS.setType("SMS");
						newSMS.setNumber(
								smsNum.substring(0, 3) + "-" + smsNum.substring(3, 6) + "-" + smsNum.substring(6, 10));
						mMemberForms.get().get(mSelectedMember.get().getMember()).addPhoneNumber(newSMS);
					} else if (smsNum.length() == 12) {
						newSMS.setType("SMS");
						newSMS.setNumber(smsNum);
						mMemberForms.get().get(mSelectedMember.get().getMember()).addPhoneNumber(newSMS);
					}
				} else {
					mMemberForms.get().get(mSelectedMember.get().getMember()).setPhoneNumbers(phones);
				}
			} else {
				for (PhoneNumber p : phones) {
					if (p.getType().equals("SMS") || p.getType().equals("CELL")) {
						mMemberForms.get().get(mSelectedMember.get().getMember()).removePhoneNumber(p);
						mMemberForms.get().get(mSelectedMember.get().getMember()).setUsesSMS(false);
					}
				}
			}
		}
	}

	public boolean getPottyTrained() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mMemberForms.get().get(mSelectedMember.get().getMember()).getIsPottyTrained() ? true : false;
		else
			return mSelectedMember.get().getMember().getIsPottyTrained() ? true : false;
	}

	public void setPottyTrained(boolean isPottyTrained) {
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			mMemberForms.get().get(mSelectedMember.get().getMember()).setIsPottyTrained(isPottyTrained);
		} else
			mSelectedMember.get().getMember().setIsPottyTrained(isPottyTrained);
	}

	public boolean getUsesChildWatch() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mMemberForms.get().get(mSelectedMember.get().getMember()).getUsesChildWatch() ? true : false;
		else
			return mSelectedMember.get().getMember().getUsesChildWatch() ? true : false;
	}

	public void setUsesChildWatch(boolean usesChildWatch) {
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			mMemberForms.get().get(mSelectedMember.get().getMember()).setUsesChildWatch(usesChildWatch);
		} else
			mSelectedMember.get().getMember().setUsesChildWatch(usesChildWatch);
	}
	
	public boolean getDiaper() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mMemberForms.get().get(mSelectedMember.get().getMember()).getAllowsDiaperChange() ? true : false;
		else
			return mSelectedMember.get().getMember().getAllowsDiaperChange() ? true : false;
	}

	public void setDiaper(boolean allowsDiaperChange) {
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			mMemberForms.get().get(mSelectedMember.get().getMember()).setAllowsDiaperChange(allowsDiaperChange);
		} else
			mSelectedMember.get().getMember().setAllowsDiaperChange(allowsDiaperChange);
	}

	public boolean getFeeding() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mMemberForms.get().get(mSelectedMember.get().getMember()).getAllowsFeeding()? true : false;
		else
			return mSelectedMember.get().getMember().getAllowsFeeding() ? true : false;
	}

	public void setFeeding(boolean allowsFeeding) {
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			mMemberForms.get().get(mSelectedMember.get().getMember()).setAllowsFeeding(allowsFeeding);
		} else
			mSelectedMember.get().getMember().setAllowsFeeding(allowsFeeding);
	}
	
	public boolean getEnrichment() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mMemberForms.get().get(mSelectedMember.get().getMember()).getAllowsEnrichment()? true : false;
		else
			return mSelectedMember.get().getMember().getAllowsEnrichment() ? true : false;
	}

	public void setEnrichment(boolean allowsEnrichment) {
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			mMemberForms.get().get(mSelectedMember.get().getMember()).setAllowsEnrichment(allowsEnrichment);
		} else
			mSelectedMember.get().getMember().setAllowsEnrichment(allowsEnrichment);
	}
	
	public boolean getHasForm() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mMemberForms.get().get(mSelectedMember.get().getMember()).getHasForm()? true : false;
		else
			return mSelectedMember.get().getMember().getHasForm() ? true : false;
	}

	public void setHasForm(boolean hasForm) {
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			mMemberForms.get().get(mSelectedMember.get().getMember()).setHasForm(hasForm);
		} else
			mSelectedMember.get().getMember().setHasForm(hasForm);
	}
	
	public boolean getUsesSMS() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			return mMemberForms.get().get(mSelectedMember.get().getMember()).getUsesSMS() ? true : false;
		else
			return mSelectedMember.get().getMember().getUsesSMS() ? true : false;
	}

	public void setUsesSMS(boolean usesSMS) {
		if (mCurrentState.get() instanceof EditMemberInfoState) {
			if (getSMSNumber().isEmpty())
				mMemberForms.get().get(mSelectedMember.get().getMember()).setUsesSMS(false);
			else
				mMemberForms.get().get(mSelectedMember.get().getMember()).setUsesSMS(usesSMS);
		} else
			mSelectedMember.get().getMember().setUsesSMS(usesSMS);
	}

    public boolean getCanSelfCheckIn() {
        if (mCurrentState.get() instanceof EditMemberInfoState)
        	return mSelfCheckInForms.get().get(mSelectedMember.get().getMember()) == null ? false : true;
        else
			return mSelectedMember.get().getSelfCheckIn() == null ? false : true;
    }

    public void setCanSelfCheckIn(boolean canSelfCheckIn) {
        if (mCurrentState.get() instanceof EditMemberInfoState) {
			if (canSelfCheckIn) {
				mSelfCheckInForms.get().put(mSelectedMember.get().getMember(), getPersistenceLayer().getNewEmptySelfCheckIn());
			} else {
				mSelfCheckInForms.get().put(mSelectedMember.get().getMember(), null);
			}
        } else {
			if (canSelfCheckIn) {
				mSelectedMember.get().setSelfCheckIn(getPersistenceLayer().getNewEmptySelfCheckIn());
			} else {
				mSelectedMember.get().setSelfCheckIn(null);
			}
		}
    }

	public int getSaveOrEditButtonRes() {
		return (mCurrentState.get() instanceof EditMemberInfoState) ? mSaveButtonRes : mEditButtonRes;
	}

	public List<EmergencyNumber> getEmergencyContacts() {
		if (mCurrentState.get() instanceof EditMemberInfoState)
			// return
			// mMemberForms.get().get(mSelectedMember.get().getMember()).listEmergencyNumbers();
			return mEmergencyContacts.get().get(mSelectedMember.get().getMember());
		else
			return mSelectedMember.get().getMember().listEmergencyNumbers();
	}

	public int getSearchBoxInputType() {
		if (mSearchParameter.get().equals(SEARCH_PARAMETER_DELEGATE.UNIT_ID))
			return EditorInfo.TYPE_CLASS_NUMBER;
		else
			return EditorInfo.TYPE_TEXT_FLAG_CAP_WORDS | EditorInfo.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
	}

	public int getDiscardButtonVisibility() {
		return (mCurrentState.get() instanceof EditMemberInfoState) ? View.VISIBLE : View.GONE;
	}

	public int getGuestConversionButtonVisibility() {
		return (mSelectedMember.get().getMember().isGuest()) ? View.INVISIBLE : View.INVISIBLE;
	}

	public OnClickListener getSetForAllOnClickListener() {
		return new SetForAllOnClickListener();
	}

	public OnClickListener getDiscardChangesOnClickListener() {
		return new DiscardChangesOnClickListener();
	}

	public class DiscardChangesOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (mCurrentState.get() instanceof EditMemberInfoState) {
				mCurrentState.set(new DisplayingMemberInfoState());
				mCurrentState.get().enterState();
			}
		}
	}

	public OnClickListener getGuestConversionOnClickListener() {
		return new GuestConversionOnClickListener();
	}

	public class GuestConversionOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			getPersistenceLayer().fetchMembershipsForFacility(getPersistenceData().getFacility(),
					new FecthMembershipsForFacility(v));

		}
	}

	public class GuestTypeSelectedListener implements SelectGuestTypePopupListener {

		@Override
		public void onGuestTypeSelectedListener(ValidMembership selectedMembershipType, Member member) {
			member.setGuest(false);
			member.setGuestDaysLeft(0);
			member.setMembershipType(selectedMembershipType.getDescriptor());
			member.setIsValidMembership(true);
			getPersistenceLayer().updateMember(member);

		}

	}

	public OnItemClickListener getFamilyListOnItemClickListener() {
		return new FamilyListOnItemClickListener();
	}

	public OnItemClickListener getEditMemberOnItemClickListener() {
		return new EditMemberOnItemClickListener();
	}

	public class EditMemberOnItemClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			if (parent.getId() == R.id.listview_parents) {
				mParentIsSelected.set(true);
				mChildrenIsSelected.set(false);
				mSelectedMember.set(mAdults.get().get(position));
			} else {
				mChildrenIsSelected.set(true);
				mParentIsSelected.set(false);
				mSelectedMember.set(mChildren.get().get(position));
			}
			for (MemberAndCubbies member : getAdultResults())
				member.setSelected(member.equals(mSelectedMember.get()));
			for (MemberAndCubbies member : getChildResults())
				member.setSelected(member.equals(mSelectedMember.get()));
			mKeyboardCallback.get().hide(0);
		}
	}

	public class SetForAllOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			// List<EmergencyNumber> nums =
			// mMemberForms.get().get(mSelectedMember.get().getMember()).listEmergencyNumbers();
			List<EmergencyNumber> nums = mEmergencyContacts.get().get(mSelectedMember.get().getMember());
			/*
			 * for (MemberAndCubbies mAndC : getChildResults()) { mMemberForms.get
			 * ().get(mAndC.getMember()).setEmergencyNumbers(nums); }
			 */
			for (MemberAndCubbies mAndC : getChildResults())
				mEmergencyContacts.get().put(mAndC.getMember(), nums);
			mEmergencyContacts.updateTrackers();
			mMemberForms.updateTrackers();
		}
	}

	public OnCheckedChangeListener getPottyCheckedListener() {
		return new PottyCheckedChangeListener();
	}

	public class PottyCheckedChangeListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if (isChecked)
				mMemberForms.get().get(mSelectedMember.get().getMember()).setIsPottyTrained(true);
			else
				mMemberForms.get().get(mSelectedMember.get().getMember()).setIsPottyTrained(false);
		}
	}

	public class FamilyListOnItemClickListener implements OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
			showFamily(position);
		}
	}

	private void showFamily(int position) {
		if (mCurrentState.get() instanceof SearchingMembersState)
			mCurrentState.get().exitState();
		final int AGE_CUTOFF = 12;
		mMember = mSearchResults.get().get(position).getMember();
		getPersistenceLayer().fetchFamilyByMember(mMember, new PersistenceLayerCallback<Family>() {
			@Override
			public void done(Family data, Exception e) {
				mFamily = data;
				mFamilyName.set("THE " + data.getName() + " FAMILY");
			}
		});
		getPersistenceLayer().fetchMembersByFamily(mMember.getPFamily(), new PersistenceLayerCallback<List<Member>>() {
			@Override
			public void done(List<Member> data, Exception e) {
				List<MemberAndCubbies> childResults = getChildResults();
				List<MemberAndCubbies> adultResults = getAdultResults();
				childResults.clear();
				adultResults.clear();
				MemberAndCubbies selectedMandC = null;
				boolean hasCubbies = !getPersistenceData().getAllFacilityCubbies().isEmpty();
				boolean hasPagers = !getPersistenceData().getAllFacilityPagers().isEmpty();
				for (Member m : data) {
					MemberAndCubbies mAndC = new MemberAndCubbies(getPersistenceLayer().getDaycareDataSource(), m,
							getPersistenceData().getFacility(), hasCubbies, hasPagers);
					if (DaycareHelpers.isAdult(m.getBirthDate(), AGE_CUTOFF)) {
						adultResults.add(mAndC);                                                                                                                                                                   
					} else {
						childResults.add(mAndC);
					}
					if (mAndC.getMember().getObjectId().equals(mMember.getObjectId())) {
						mAndC.setSelected(true);
						selectedMandC = mAndC;
					}
				}
				mSelectedMember.set(selectedMandC);
				mCurrentState.set(new DisplayingMemberInfoState());
				mCurrentState.get().enterState();
			}
		});
	}

	public OnItemSelectedListener getSearchParameterOnItemSelectedListener() {
		return new SearchParameterOnItemSelectedListener();
	}

	public SEARCH_PARAMETER_DELEGATE getSearchParameter() {
		return mSearchParameter.get();
	}

	public OnClickListener getEditOnClickListener() {
		return new EditOnClickListener();
	}

	public class EditOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (mCurrentState.get() instanceof DisplayingMemberInfoState) {
				mCurrentState.set(new EditMemberInfoState());
				mCurrentState.get().enterState();
			} else {
				mCurrentState.get().exitState();
			}
		}
	}

	public void setSearchParameter(SEARCH_PARAMETER_DELEGATE criteria) {
		mSearchParameter.set(criteria);
	}

	public SearchAndEditFamilyViewModel(PersistenceLayer persistenceLayer) {
		super(persistenceLayer);
	}

	public float getParentAlpha() {
		return (mParentIsSelected.get() == false) ? (float) 0.5 : 1;
	}

	public float getChildrenAlpha() {
		return (mChildrenIsSelected.get() == false) ? (float) 0.5 : 1;
	}

	@Override
	protected void initialize() {
		// Data configuration
		mSearchResults = new TrackableField<TrackableCollection<MemberAndCubbies>>(
				new TrackableCollection<MemberAndCubbies>());
		mChildren = new TrackableField<TrackableCollection<MemberAndCubbies>>(
				new TrackableCollection<MemberAndCubbies>());
		mAdults = new TrackableField<TrackableCollection<MemberAndCubbies>>(
				new TrackableCollection<MemberAndCubbies>());
		mSearchParameterList = new TrackableField<TrackableCollection<String>>(new TrackableCollection<String>());
		for (SEARCH_PARAMETER_DELEGATE f : SEARCH_PARAMETER_DELEGATE.values()) {
			if (f.isShownInMenu())
				getSearchParameterList().add(f.getValue());
		}
		mSearchBoxText = new TrackableField<String>();
		mFamilyName = new TrackableField<String>();
		mSearchParameter = new TrackableField<SEARCH_PARAMETER_DELEGATE>();
		mCurrentPage = new TrackableInt(0);
		mSelectedMember = new TrackableField<MemberAndCubbies>();
		mKeyboardCallback = new TrackableField<OnKeyboardCallback>();
		mParentIsSelected = new TrackableBoolean();
		mChildrenIsSelected = new TrackableBoolean();
		mMemberForms = new TrackableField<HashMap<Member, Member>>(new HashMap<Member, Member>());
		mEmergencyContacts = new TrackableField<HashMap<Member, List<EmergencyNumber>>>(
				new HashMap<Member, List<EmergencyNumber>>());

		mFaceInFocus = new TrackableBoolean();
		mCurrentState = new TrackableField<State>(new SearchingMembersState());
		mCameraDisplay = new TrackableField<TextureView>();
		mPhotoFrameRect = new TrackableField<Rect>(new Rect());
		mMemberPhoto = new TrackableField<Bitmap>();
		mScreenDensity = new TrackableFloat();
		mInstructionsText = new TrackableField<String>();
		mFamilyAdmissions = new TrackableCollection<AdmissionAndFacility>();
		mSelfCheckInForms = new TrackableField<HashMap<Member, SelfCheckIn>>(new HashMap<Member, SelfCheckIn>());
	}

	public OnClickListener getSearchOnClickListener() {
		return new SearchOnClickListener();
	}

	public OnClickListener getMemberPhotoOnClickListener() {
		return new MemberPhotoOnClickListener();
	}

	public void setCurrentPage(int page) {
		if (getCurrentPage() != page)
			mCurrentPage.set(page);
	}

	public void setKeyboardCallback(OnKeyboardCallback callback) {
		mKeyboardCallback.set(callback);
	}

	public int getCurrentPage() {
		return mCurrentPage.get();
	}

	public OnEditorActionListener getSearchOnActionListener() {
		return new SearchOnActionListener();
	}

	public class SearchOnActionListener implements OnEditorActionListener {
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			if (actionId == EditorInfo.IME_ACTION_SEARCH) {
				v.clearFocus();
				mKeyboardCallback.get().hide(0);
				SEARCH_PARAMETER_DELEGATE delegate = getSearchParameter();
				if (delegate == null)
					return false;
				delegate.query(getSearchBoxText(), getPersistenceLayer(), new SearchResultsCallback());
				return true;
			}
			return false;
		}
	}

	public class SearchParameterOnItemSelectedListener implements OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			setSearchParameter(SEARCH_PARAMETER_DELEGATE.getEnum(((SearchParameterDropDown) view).getData()));
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			setSearchParameter(null);
		}
	}

	public class SearchOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			mKeyboardCallback.get().hide(0);
			query(getSearchBoxText());
		}
	}

	public class MemberPhotoOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (mCurrentState.get() instanceof EditMemberInfoState) {
				// mCurrentState.get().exitState();
				mCurrentState.set(new AwaitingPhotoState());
				mCurrentState.get().enterState();
			}
		}
	}

	public void query(String criteria) {
		// bad way to do this
		// coming from onclick
		// wmShowSingleFamilyResults = false;
		query(criteria, getSearchParameter());
	}

	public void query(String criteria, SEARCH_PARAMETER_DELEGATE delegate) {
		// bad way to do this
		// coming from intent
		// TODOmShowSingleFamilyResults = false;
		if (delegate == null)
			return;
		delegate.query(criteria, getPersistenceLayer(), new SearchResultsCallback());
	}

	class SearchResultsCallback implements PersistenceLayerCallback<List<Member>> {
		@Override
		public void done(List<Member> data, Exception e) {
			List<MemberAndCubbies> toSave = getSearchResults();
			toSave.clear();
			boolean hasCubbies = !getPersistenceData().getAllFacilityCubbies().isEmpty();
			boolean hasPagers = !getPersistenceData().getAllFacilityPagers().isEmpty();
			for (Member m : data) {
				toSave.add(new MemberAndCubbies(getPersistenceLayer().getDaycareDataSource(), m,
						getPersistenceData().getFacility(), hasCubbies, hasPagers));
			}
			if (toSave.size() == 1) { // && mShowSingleFamilyResults
				showFamily(0);
			}
		}
	}

	private TrackableBoolean mFaceInFocus;
	private TrackableField<TrackableCollection<MemberAndCubbies>> mSearchResults;
	private TrackableField<TrackableCollection<MemberAndCubbies>> mChildren;
	private TrackableField<TrackableCollection<MemberAndCubbies>> mAdults;
	private TrackableField<String> mFamilyName;
	private TrackableBoolean mParentIsSelected;
	private TrackableBoolean mChildrenIsSelected;
	private final SimpleDateFormat DATEFORMAT = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
	private final int mMembershipLabel = R.string.member_label_membership;
	private final int mMembershipLabelGuest = R.string.member_label_membership_guest;
	private final int mStatusLabel = R.string.member_label_status;
	private final int mStatusLabelGuest = R.string.member_label_status_guest;
	private final int mEditButtonRes = R.drawable.edit_icon;
	private final int mSaveButtonRes = R.drawable.icon_save;
	// Action Bar and ViewPager
	private TrackableInt mCurrentPage;
	// Initial Search Strings
	private TrackableField<String> mSearchBoxText;
	private TrackableField<TrackableCollection<String>> mSearchParameterList;
	private TrackableField<SEARCH_PARAMETER_DELEGATE> mSearchParameter;
	private TrackableField<MemberAndCubbies> mSelectedMember;
	private TrackableField<OnKeyboardCallback> mKeyboardCallback;
	private TrackableField<HashMap<Member, Member>> mMemberForms;
	private TrackableField<HashMap<Member, List<EmergencyNumber>>> mEmergencyContacts;
	private Family mFamily;
	private TrackableCollection<AdmissionAndFacility> mFamilyAdmissions;
	private TrackableField<HashMap<Member, SelfCheckIn>> mSelfCheckInForms;

	private final int PHOTOWIDTH = 320;
	private final int PHOTOHEIGHT = 200;
	private TrackableField<Rect> mPhotoFrameRect;
	private TrackableField<TextureView> mCameraDisplay;
	private TrackableField<Bitmap> mMemberPhoto;
	private TrackableFloat mScreenDensity;
	private TrackableField<String> mInstructionsText;
	private TrackableField<State> mCurrentState;
	private Member mMember;

	public enum SEARCH_PARAMETER_DELEGATE {
		LAST_NAME("Last Name") {
			@Override
			public void query(String criteria, PersistenceLayer pl, PersistenceLayerCallback<List<Member>> cb) {
				pl.fetchByLastName(criteria, cb);
			}

			@Override
			public boolean isShownInMenu() {
				return true;
			}

			@Override
			public int getSearchBoxHintReference() {
				return R.string.searchbar_searchboxhint_lastname;
			}
		},
		FULL_NAME("Full Name") {
			@Override
			public void query(String criteria, PersistenceLayer pl, PersistenceLayerCallback<List<Member>> cb) {
				pl.fetchByFullName(criteria, cb);
			}

			@Override
			public boolean isShownInMenu() {
				return true;
			}

			@Override
			public int getSearchBoxHintReference() {
				return R.string.searchbar_searchboxhint_fullname;
			}
		},
		UNIT_ID("Unit Id") {
			@Override
			public void query(String criteria, PersistenceLayer pl, PersistenceLayerCallback<List<Member>> cb) {
				pl.fetchByUnitId(criteria, cb);
			}

			@Override
			public boolean isShownInMenu() {
				return true;
			}

			@Override
			public int getSearchBoxHintReference() {
				return R.string.searchbar_searchboxhint_unitid;
			}
		},
		MEMBER_ID("Member Id") {
			@Override
			public void query(String criteria, PersistenceLayer pl, final PersistenceLayerCallback<List<Member>> cb) {
				pl.fetchByMemberId(criteria, new PersistenceLayerCallback<Member>() {
					@Override
					public void done(Member data, Exception e) {
						List<Member> toReturn = new ArrayList<Member>();
						toReturn.add(data);
						cb.done(toReturn, e);
					}
				});
			}

			@Override
			public boolean isShownInMenu() {
				return true;
			}

			@Override
			public int getSearchBoxHintReference() {
				return R.string.searchbar_searchboxhint_memberid;
			}
		},
		OBJECT_ID("Object Id") {
			@Override
			public void query(String criteria, PersistenceLayer pl, final PersistenceLayerCallback<List<Member>> cb) {
				pl.readMember(criteria, new PersistenceLayerCallback<Member>() {
					@Override
					public void done(Member data, Exception e) {
						List<Member> toReturn = new ArrayList<Member>();
						toReturn.add(data);
						cb.done(toReturn, e);
					}
				});
			}

			@Override
			public boolean isShownInMenu() {
				return false;
			}

			@Override
			public int getSearchBoxHintReference() {
				return R.string.searchbar_searchboxhint_objectid;
			}
		};
		private String value;

		SEARCH_PARAMETER_DELEGATE(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		@Override
		public String toString() {
			return this.getValue();
		}

		public static SEARCH_PARAMETER_DELEGATE getEnum(String value) {
			for (SEARCH_PARAMETER_DELEGATE v : values())
				if (v.getValue().equalsIgnoreCase(value))
					return v;
			throw new IllegalArgumentException();
		}

		public abstract void query(String criteria, PersistenceLayer pl, PersistenceLayerCallback<List<Member>> cb);

		public abstract boolean isShownInMenu();

		public abstract int getSearchBoxHintReference();
	}

	public State getCurrentState() {
		return mCurrentState.get();
	}

	public int getTakePhotoVisibility() {
		return mCurrentState.get() instanceof AwaitingPhotoState ? View.VISIBLE : View.INVISIBLE;
	}

	public int getTryAgainVisibility() {
		return mCurrentState.get() instanceof ConfirmPhotoState ? View.VISIBLE : View.INVISIBLE;
	}

	public int getUsePhotoVisibility() {
		return mCurrentState.get() instanceof ConfirmPhotoState ? View.VISIBLE : View.INVISIBLE;
	}

	public int getSwitchCameraVisibility() {
		return mCurrentState.get() instanceof AwaitingPhotoState ? View.VISIBLE : View.INVISIBLE;
	}

	public int getInstructionsVisibility() {
		return getCurrentState() instanceof AwaitingPhotoState ? View.VISIBLE : View.INVISIBLE;
	}

	public String getInstructionsText() {
		return mInstructionsText.get();
	}

	public void setScreenDensity(float density) {
		mScreenDensity.set(density);
	}

	public int getFaceFrameVisibility() {
		return (mCurrentState.get() instanceof ConfirmPhotoState) ? View.INVISIBLE : View.VISIBLE;
	}

	public Rect getPhotoSize() {
		return mPhotoFrameRect.get();
	}

	public Bitmap getNewMemberPhoto() {
		return mMemberPhoto.get();
	}

	public String getMemberName() {
		Member member;
		if (mCurrentState.get() instanceof EditMemberInfoState)
			member = mMemberForms.get().get(mSelectedMember.get().getMember());
		else
			member = mSelectedMember.get().getMember();
		return member.getFirstName() + " " + member.getLastName();
	}

	public TextureView getCameraDisplay() {
		return mCameraDisplay.get();
	}

	public void setCameraDisplay(TextureView view) {
		// view.setSurfaceTextureListener(new CameraSurfaceTextureListener());
		mCameraDisplay.set(view);
	}

	public int getDimVisibility() {
		return mCurrentState.get() instanceof ConfirmPhotoState ? View.VISIBLE : View.INVISIBLE;
	}

	public int getPhotoNameVisibility() {
		return mCurrentState.get() instanceof ConfirmPhotoState ? View.VISIBLE : View.INVISIBLE;
	}

	public boolean getIsConfirmPhotoState() {
		return mCurrentState.get() instanceof ConfirmPhotoState;
	}

	public OnClickListener getEditFamilyBackButtonListener() {
		return new EditFamilyBackButtonListener();
	}

	public class EditFamilyBackButtonListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			if (mCurrentState.get() instanceof EditMemberInfoState) {
				Resources res = v.getContext().getResources();
				String message = res.getString(R.string.popup_unsavedchanges_body_text);
				String cancelBtn = res.getString(R.string.popup_unsavedchanges_cancel_text);
				String yesBtn = res.getString(R.string.popup_unsavedchanges_yes_text);
				DaycareHelpers.createYesNoDialog(v.getContext(), message, yesBtn, cancelBtn, new YesNoDialogCallback() {
					@Override
					public void yesClicked() {
						setCurrentPage(SEARCHRESULTPAGE);
					}

					@Override
					public void noClicked() {
					}
				});
			} else if (mCurrentState.get() instanceof DisplayingMemberInfoState) {
				setCurrentPage(SEARCHRESULTPAGE);
			}
		}
	}
	
	public OnClickListener getFamilySettingsOnClickListener() {
		return new FamilySettingsOnClickListener();
	}

	public class FamilySettingsOnClickListener implements OnClickListener {
		@Override
		public void onClick(View v) {
			/*getPersistenceLayer().fetchFamilyAdmissions(mFamily, new PersistenceLayerCallback<List<Admission>>() {
				@Override
				public void done(final List<Admission> admissions, Exception e) {
					//mFamilyAdmissions = admissions;
					getPersistenceLayer().fetchAssocFacilities(new PersistenceLayerCallback<List<Facility>>() {
						@Override
						public void done(List<Facility> facilities, Exception e) {
							mFamilyAdmissions.clear();
							for (Admission admit : admissions) {
								mFamilyAdmissions.add(new AdmissionAndFacilities(admit, false, facilities));
							}
							setCurrentPage(FAMILYSETTINGS);
							mCurrentState.set(new FamilySettingState());
							mCurrentState.get().enterState();
						}
					});
				}
			});*/
			getPersistenceLayer().fetchAssocFacilities(new PersistenceLayerCallback<List<Facility>>() {
				@Override
				public void done(List<Facility> facilities, Exception e) {
					mFamilyAdmissions.clear();
					getPersistenceLayer().fetchFamilyAdmissionBalance(mSelectedMember.get().getMember().getObjectId(), new PersistenceLayerCallback<HashMap<String, Object>>() {
						@Override
						public void done(HashMap<String, Object> map, Exception e) {
							Log.i("Balance", map.get("balance").toString());
							mFamilyAdmissions.add(new AdmissionAndFacility(map.get("balance").toString(), false, getFacility()));
							setCurrentPage(FAMILYSETTINGS);
							mCurrentState.set(new FamilySettingState());
							mCurrentState.get().enterState();
						}
					});
				}
			});
		}
	}
	
	public int getFamilySettingsVisibility() {
		return (getPersistenceData().getFacilityUsesAdmissions()) ? View.VISIBLE : View.INVISIBLE;
	}

	public class FecthMembershipsForFacility implements PersistenceLayerCallback<List<ValidMembership>> {
		private View mView;

		FecthMembershipsForFacility(View v) {
			mView = v;
		}

		@Override
		public void done(List<ValidMembership> data, Exception e) {
			GuestConversionPopup gc = new GuestConversionPopup(mView.getContext(), data,
					mSelectedMember.get().getMember());
			gc.setGuestTypeSelectedListener(new GuestTypeSelectedListener());
			gc.show(mView);
		}

	}

	public void exit() {
		if (mCurrentState.get() instanceof AwaitingPhotoState || mCurrentState.get() instanceof ConfirmPhotoState) {
			setCurrentPage(DISPLAYFAMILYPAGE);
			mCurrentState.set(new EditMemberInfoState());
		} else if (mCurrentState.get() instanceof FamilySettingState) {
			setCurrentPage(DISPLAYFAMILYPAGE);
			mCurrentState.set(new DisplayingMemberInfoState());
		}
	}

	abstract class State {
		public abstract void enterState();

		public void exitState() {
			mCurrentState = null;
		}

		public void exception(Exception e) {
			mCurrentState = null;
		}
	}

	private class SearchingMembersState extends State {
		@Override
		public void enterState() {
			Log.w("SearchingMembersState", "EnterState");
			setCurrentPage(SEARCHRESULTPAGE);
		}

		@Override
		public void exitState() {
			Log.w("SearchingMembersState", "ExitState");
			// setCurrentPage(getCurrentPage() + 1);
			// mIsEditable.set(false);
			// mHideKeyboard.set(!mHideKeyboard.get());
			// setCurrentPage(DISPLAYFAMILYPAGE);
		}

		@Override
		public void exception(Exception e) {
			Log.w("SearchingMembersState", "Exception: " + e.getMessage());
		}
	}

	private class DisplayingMemberInfoState extends State {
		@Override
		public void enterState() {
			Log.w("DisplayingMemberInfoState", "EnterState");
			setCurrentPage(DISPLAYFAMILYPAGE);
			mKeyboardCallback.get().hide(500);
		}

		@Override
		public void exitState() {
			Log.w("DisplayingMemberInfoState", "ExitState");
		}

		@Override
		public void exception(Exception e) {
			Log.w("DisplayingMemberInfoState", "Exception: " + e.getMessage());
		}
	}

	private class EditMemberInfoState extends State {
		private void makeMemberForms(List<MemberAndCubbies> members) {
			for (MemberAndCubbies mAndC : members) {
				Member form = getPersistenceLayer().getNewEmptyMember();
				copyMember(mAndC.getMember(), form);
				if (form.listEmergencyNumbers() == null)
					form.setEmergencyNumbers(new ArrayList<EmergencyNumber>());
				while (form.listEmergencyNumbers().size() < 6) {
					form.addEmergencyNumber(new EmergencyNumber());
				}
				mEmergencyContacts.get().put(mAndC.getMember(), form.listEmergencyNumbers());
				mMemberForms.get().put(mAndC.getMember(), form);
			}
		}

		private void makeSelfCheckInForms(List<MemberAndCubbies> members) {
			for (MemberAndCubbies mAndC : members) {
				mSelfCheckInForms.get().put(mAndC.getMember(), mAndC.getSelfCheckIn());
			}
		}
		@Override
		public void enterState() {
			Log.w("EditMemberInfoState", "EnterState");
			/*
			 * mMemberForm.set(getPersistenceLayer().getNewEmptyMember()); List<EmergencyNumber> nums =
			 * mSelectedMember.get().getMember().listEmergencyNumbers(); if (nums == null || nums.isEmpty()) nums = new
			 * ArrayList<EmergencyNumber>(); while (nums.size() < 2) { nums.add(new EmergencyNumber()); }
			 * mEmergencyContacts.set(new TrackableCollection<EmergencyNumber>(nums));
			 */
			// copy all
			// copyMember(mSelectedMember.get().getMember(), mMemberForm.get());
			makeMemberForms(getAdultResults());
			makeMemberForms(getChildResults());

			makeSelfCheckInForms(getAdultResults());
			makeSelfCheckInForms(getChildResults());
			/*
			 * //Fill null emergency nums with new empty ones while (mMemberForm.get().listEmergencyNumbers().size() <
			 * 2) { mMemberForm.get().listEmergencyNumbers().add(new EmergencyNumber()); }
			 */
			mEmergencyContacts.updateTrackers();
			mMemberForms.updateTrackers();
			mSelfCheckInForms.updateTrackers();
			mChildren.updateTrackers();
			mAdults.updateTrackers();
		}

		@Override
		public void exitState() {
			Log.w("EditMemberInfoState", "ExitState");
			// copy all
			/*
			 * copyMember(mMemberForm.get(), mSelectedMember.get().getMember()); mSelectedMember.updateTrackers(); //
			 * save to cloud getPersistenceLayer ().updateMember(mSelectedMember.get().getMember());
			 */
			for (final MemberAndCubbies mAndC : getAdultResults()) {
				copyMember(mMemberForms.get().get(mAndC.getMember()), mAndC.getMember());
				mAndC.getMember()
						.setEmergencyNumbers(extractNonEmptys(mEmergencyContacts.get().get(mAndC.getMember())));
				mAndC.getMember().setUserPINs(updateUserPins(mMemberForms.get().get(mAndC.getMember()).listPhoneNumbers()));
				if (mSelfCheckInForms.get().get(mAndC.getMember()) != null) {
					SelfCheckIn selfCheckIn = mSelfCheckInForms.get().get(mAndC.getMember());
					selfCheckIn.setPMember(mAndC.getMember());
					mAndC.setSelfCheckIn(selfCheckIn);
					getPersistenceLayer().createSelfCheckIn(mAndC.getMember(), selfCheckIn, new PersistenceLayerCallback<SelfCheckIn>() {
						@Override
						public void done(SelfCheckIn data, Exception e) {
						}
					});
				} else {
					mAndC.setSelfCheckIn(null);
					getPersistenceLayer().deleteSelfCheckIn(mAndC.getMember(), new PersistenceLayerCallback<SelfCheckIn>() {
						@Override
						public void done(SelfCheckIn data, Exception e) {
						}
					});
				}
			}
			for (final MemberAndCubbies mAndC : getChildResults()) {
				copyMember(mMemberForms.get().get(mAndC.getMember()), mAndC.getMember());
				if (mAndC.getMember().isGuest()) {
					if (mAndC.getMember().getGuestDaysLeft().intValue() > 0)
						mAndC.getMember().setIsValidMembership(true);
					else
						mAndC.getMember().setIsValidMembership(false);
				}
				mAndC.getMember()
						.setEmergencyNumbers(extractNonEmptys(mEmergencyContacts.get().get(mAndC.getMember())));
				if (mSelfCheckInForms.get().get(mAndC.getMember()) != null) {
					SelfCheckIn selfCheckIn = mSelfCheckInForms.get().get(mAndC.getMember());
					selfCheckIn.setPMember(mAndC.getMember());
					mAndC.setSelfCheckIn(selfCheckIn);
					getPersistenceLayer().createSelfCheckIn(mAndC.getMember(), selfCheckIn, new PersistenceLayerCallback<SelfCheckIn>() {
						@Override
						public void done(SelfCheckIn data, Exception e) {
						}
					});
				} else {
					mAndC.setSelfCheckIn(null);
					getPersistenceLayer().deleteSelfCheckIn(mAndC.getMember(), new PersistenceLayerCallback<SelfCheckIn>() {
						@Override
						public void done(SelfCheckIn data, Exception e) {
						}
					});
				}
			}
			getPersistenceLayer().updateMembers(new ArrayList<Member>(mMemberForms.get().keySet()));
			mSelectedMember.updateTrackers();
			mKeyboardCallback.get().hide(0);
			mChildren.updateTrackers();
			mAdults.updateTrackers();
			mSearchResults.updateTrackers();
			mCurrentState.set(new DisplayingMemberInfoState());
			mCurrentState.get().enterState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("EditMemberInfoState", "Exception: " + e.getMessage());
		}

		private List<EmergencyNumber> extractNonEmptys(List<EmergencyNumber> numbers) {
			List<EmergencyNumber> temp = new ArrayList<EmergencyNumber>();
			for (EmergencyNumber num : numbers) {
				if (!num.getName().isEmpty() || !num.getPhone().isEmpty())
					temp.add(num);
			}
			return temp;
		}
		
		private List<String> updateUserPins(List<PhoneNumber> numbers) {
			/*String pinInQuestion = "";
			for (PhoneNumber num : numbers) {
				if (num.getType().equals("Home") || num.getType().equals("Work") || num.getType().equals("SMS")) {
					boolean pinExists = false;
					pinInQuestion = num.getNumber();
					for (String pin : userPins) {
						if (pinInQuestion.equals(pin))
							pinExists = true;
					}
					if (!pinExists)
						userPins.add(pinInQuestion);
				}
			}*/
			List<String> updatedUserPins = new ArrayList<String>();
			for (PhoneNumber num : numbers) {
				if (num.getType().equals("HOME") || num.getType().equals("WORK") || num.getType().equals("CELL") || num.getType().equals("SMS")) {
					updatedUserPins.add(num.getNumber());
				}
			}
			return updatedUserPins;
		}

		private void copyMember(Member src, Member target) {
			target.setFirstName(src.getFirstName().toUpperCase(Locale.US));
			target.setLastName(src.getLastName().toUpperCase(Locale.US));
			target.setBirthDate(src.getBirthDate());
			target.setEmail(src.getEmail());
			target.setMemberID(src.getMemberID());
			target.setGender(src.getGender().toUpperCase(Locale.US));
			target.setGuest(src.isGuest());
			target.setActive(src.isActive());
			target.setGuestDaysLeft(src.getGuestDaysLeft());
			target.setUserPINs(src.listUserPINs());
			target.setBarcode(src.getBarcode());
			target.setLastCheckIn(src.getLastCheckIn());
			target.setMedicalConcerns(src.getMedicalConcerns());
			target.setMinutesLeft(src.getMinutesLeft());
			target.setImage(src.getImage());
			target.setEmergencyNumbers(src.listEmergencyNumbers());
			target.setPhoneNumbers(src.listPhoneNumbers());
			target.setUsesSMS(src.getUsesSMS());
			target.setNotes(src.getNotes());
			target.setIsPottyTrained(src.getIsPottyTrained());
			target.setWeeklyMinutesLeft(src.getWeeklyMinutesLeft());
			target.setUsesChildWatch(src.getUsesChildWatch());
			target.setMembershipType(src.getMembershipType());
			target.setService(src.getService());
			if (src.getAllowsDiaperChange() != null)
				target.setAllowsDiaperChange(src.getAllowsDiaperChange());
			if (src.getAllowsFeeding() != null)
				target.setAllowsFeeding(src.getAllowsFeeding());
			if (src.getAllowsEnrichment() != null)
				target.setAllowsEnrichment(src.getAllowsEnrichment());
			if (src.getHasForm() != null)
				target.setHasForm(src.getHasForm());
			if (src.getCanCheckSelfIn() != null)
	            target.setCanCheckSelfIn(src.getCanCheckSelfIn());

			/*
			 * List<EmergencyNumber> nums = new ArrayList<EmergencyNumber>(); for (EmergencyNumber num :
			 * src.listEmergencyNumbers()) if (!num.getName().isEmpty() || !num.getPhone().isEmpty()) nums.add(num);
			 * target.setEmergencyNumbers(nums);
			 */
		}
	}

	private class AwaitingPhotoState extends State {
		@Override
		public void enterState() {
			Log.w("AwaitingPhotoState", "EnterState");
			mInstructionsText.set("Move Face into Purple Box");
			setCurrentPage(MEMBERPHOTOPAGE);
			mMemberPhoto.set(null);
		}

		@Override
		public void exitState() {
			Log.w("AwaitingPhotoState", "ExitState");
			mCurrentState.set(new ConfirmPhotoState());
			mCurrentState.get().enterState();
		}

		@Override
		public void exception(Exception e) {
			Log.w("AwaitingPhotoState", "Exception: " + e.getMessage());
			mCurrentState.set(new DisplayingMemberInfoState());
			mCurrentState.get().enterState();
		}
	}

	private class ConfirmPhotoState extends State {
		@Override
		public void enterState() {
			Log.w("ConfirmPhotoState", "EnterState");
			// cameraDestroy();
		}

		@Override
		public void exitState() {
			Log.w("ConfirmPhotoState", "ExitState");
			mMemberForms.get().get(mSelectedMember.get().getMember())
					.setImage(ImageFunctions.scaleBitmap(mMemberPhoto.get(), PHOTOWIDTH, PHOTOHEIGHT));
			mMemberForms.updateTrackers();
			setCurrentPage(DISPLAYFAMILYPAGE);
			mCurrentState.set(new EditMemberInfoState());
		}

		@Override
		public void exception(Exception e) {
			Log.w("ConfirmPhotoState", "Exception: " + e.getMessage());
			mCurrentState.set(new DisplayingMemberInfoState());
			mCurrentState.get().enterState();
		}
	}

	private class FamilySettingState extends State {
		@Override
		public void enterState() {
			Log.w("FamilySettingState", "EnterState");
		}

		@Override
		public void exitState() {
			Log.w("FamilySettingState", "ExitState");
			//TODO save family admissions here
		}

		@Override
		public void exception(Exception e) {
			Log.w("FamilySettingState", "Exception: " + e.getMessage());
		}
	}
	
	public void photoTaken(Bitmap photo, boolean isFrontCamera) {
		mMemberPhoto.set(photo);
		mCurrentState.get().exitState();
	}

	public void faceInFocus() {
		mFaceInFocus.set(true);
	}

	public void faceNotInFocus() {
		mFaceInFocus.set(false);
	}

	public void tryAgain() {
		if (mCurrentState.get() instanceof ConfirmPhotoState) {
			mCurrentState.set(new AwaitingPhotoState());
			mCurrentState.get().enterState();
		}
	}

	public void usePhoto() {
		if (mCurrentState.get() instanceof ConfirmPhotoState) {
			mCurrentState.get().exitState();
		}
	}

	public interface OnKeyboardCallback {
		public void show(long delay);

		public void hide(long delay);
	}

	public OnClickListener getUpdateButtonOnClickListener() {
		return new UpdateButtonOnClickListener();
	}

	public class UpdateButtonOnClickListener implements OnClickListener {
		@Override
		public void onClick(final View v) {
			String message = "Update this family from the Cloud?";
			DaycareHelpers.createYesNoDialog(v.getContext(), message, new YesNoDialogCallback() {

				@Override
				public void yesClicked() {
					// TODO implement to a list of memberIds when sync server is
					// ready
					/*
					 * for (Member m : mFamilyMembers) { familyMemberIds.put(m.getMemberID()); }
					 */
					List<Member> members = new ArrayList<Member>();
					//members.add(mMember);
					members.add(getAdultResults().get(0).getMember()); // Can only use a parent memberId for syncing
					getPersistenceLayer().syncFamilyData(members, new PersistenceLayerCallback<HerokuJob>() {
						@Override
						public void done(HerokuJob data, Exception e) {
							if (e != null) {
								CustomToast.makeToast(v.getContext(), "Members Were Not Updated",
										CustomToast.DURATION_LONG, CustomToast.STYLE_ERROR).show();
							} else {
								CustomToast.makeToast(v.getContext(), "Members Are Updated", CustomToast.DURATION_LONG,
										CustomToast.STYLE_SUCCESS).show();
							}
						}
					});
				}

				@Override
				public void noClicked() {

				}
			});
		}
	}

	public int getSelfCheckInVisibility() {
		if (getFacility().getSelfCheckInOpts() != null) {
			try {
				if (getFacility().getSelfCheckInOpts().getString("type").equals("prompted")) {
					return View.VISIBLE;
				} else {
					return View.GONE;
				}
			} catch (JSONException e) {
				e.printStackTrace();
				return View.GONE;
			}
		} else {
			return View.GONE;
		}
	}
}
