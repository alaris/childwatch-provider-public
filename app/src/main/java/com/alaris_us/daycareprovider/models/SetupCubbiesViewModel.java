package com.alaris_us.daycareprovider.models;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycareprovider.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;
import com.bindroid.trackable.TrackableInt;

public class SetupCubbiesViewModel extends ViewModel {

	public SetupCubbiesViewModel(PersistenceLayer persistenceLayer) {

		super(persistenceLayer);
	}

	class SaveCubbiesOnClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {

			persistModel();
		}
	}

	@Override
	protected void initialize() {

		// Initialize Fields
		mCubbyColumns = new TrackableInt();
		mCubbyList = new TrackableField<TrackableCollection<Cubby>>(new TrackableCollection<Cubby>());
		/* CubbyList */watchProperty(new PropertyChangeListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void propertyChange(PropertyChangeEvent event) {

				setCubbyList((List<Cubby>) event.getNewValue());
			}
		}, CHANGE_ID.CUBBY_LIST);
		/* CubbyColumnCount */watchProperty(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {

				setCubbyColumns((int) event.getNewValue());
			}
		}, CHANGE_ID.CUBBY_COLUMNS);
		initializeModelData();
	}

	private void initializeModelData() {

		setCubbyColumns(getPersistenceData().getFacilityCubbyColumns());
		setCubbyList(getPersistenceData().getAllFacilityCubbies());
	}

	private void persistModel() {

		getPersistenceData().setFacilityCubbyColumns(getCubbyColumns());
		getPersistenceLayer().persistFacility();
		getPersistenceLayer().persistAllFacilityCubbies(getCubbyList());
	}

	public int getCubbyColumns() {

		int curVal = mCubbyColumns.get();
		return (curVal < 1) ? 1 : curVal;
	}

	public void setCubbyColumns(int cubbyColumns) {

		mCubbyColumns.set(cubbyColumns);
	}

	public int getSeekbarCubbyColumns() {

		return getCubbyColumns() - 1;
	}

	public void setSeekbarCubbyColumns(int cubbyColumns) {

		setCubbyColumns(cubbyColumns + 1);
	}

	public int getCubbyCount() {

		return getCubbyList().size();
	}

	public void setCubbyCount(int cubbyCount) {

		int currentCount = getCubbyCount();
		int difference = cubbyCount - currentCount;
		if (difference > 0) { /* Adding cubbies */

			for (int i = currentCount; i < currentCount + difference; i++) {
				Cubby toAdd = getPersistenceLayer().getEmptyCubby();
				toAdd.setName(String.format("%02d", (i + 1)));
				getCubbyList().add(toAdd);
			}

		} else if (difference < 0) { /* Removing cubbies */
			for (int i = currentCount; i > currentCount + difference; i--) {
				getCubbyList().remove(i - 1);
			}
		}
	}

	public List<Cubby> getCubbyList() {

		return mCubbyList.get();
	}

	public void setCubbyList(List<Cubby> cubbyList) {

		getCubbyList().clear();
		getCubbyList().addAll(cubbyList);
	}

	public OnClickListener getSaveCubbiesOnClickListener() {

		return new SaveCubbiesOnClickListener();
	}

	private TrackableInt mCubbyColumns;
	private TrackableField<TrackableCollection<Cubby>> mCubbyList;
}
