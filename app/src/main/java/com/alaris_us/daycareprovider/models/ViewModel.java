package com.alaris_us.daycareprovider.models;

import java.beans.PropertyChangeListener;

import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;

public abstract class ViewModel {

	private PersistenceLayer mPersistenceLayer;
	private PersistenceData mPersistenceData;

	protected ViewModel(PersistenceLayer persistenceLayer) {

		mPersistenceLayer = persistenceLayer;
		mPersistenceData = persistenceLayer.getPersistenceData();
		initialize();
	}

	protected abstract void initialize();

	protected void watchProperty(PropertyChangeListener listener, CHANGE_ID id) {

		mPersistenceData.addPropertyChangeListener(listener, id);
	}

	protected void watchAll(PropertyChangeListener listener) {

		mPersistenceData.addPropertyChangeListener(listener);
	}

	protected PersistenceLayer getPersistenceLayer() {

		return mPersistenceLayer;
	}

	protected PersistenceData getPersistenceData() {

		return mPersistenceData;
	}

	public interface ViewModelHost<T extends ViewModel> {

		public T getModel();
	}
}
