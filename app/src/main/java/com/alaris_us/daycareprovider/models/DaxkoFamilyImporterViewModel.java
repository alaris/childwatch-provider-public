package com.alaris_us.daycareprovider.models;

import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.data.PhoneNumber;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;
import com.alaris_us.daycareprovider.utils.DaycareHelpers.YesNoDialogCallback;
import com.alaris_us.daycareprovider.view.CustomToast;
import com.bindroid.trackable.TrackableCollection;
import com.bindroid.trackable.TrackableField;

public class DaxkoFamilyImporterViewModel extends ViewModel {

	private static final int AGE_CUTOFF = 12;

	public String getphoneNumber() {

		return mPhoneNumber.get();
	}

	public void setPhoneNumber(String phoneNumber) {

		this.mPhoneNumber.set(phoneNumber);
	}

	public TrackableCollection<Member> getRegisterableMembers() {

		return mRegisterable;
	}

	public OnClickListener getSearchButtonOnClickListener() {

		return new SearchButtonClicked();
	}

	private class SearchButtonClicked implements OnClickListener {

		@Override
		public void onClick(final View v) {

			PhoneNumber pin = PhoneNumber.getNewInstance(getphoneNumber());
			if (pin == null) {
				CustomToast.makeToast(v.getContext(), "Invalid Phone Number", CustomToast.DURATION_LONG,
						CustomToast.STYLE_ERROR).show();
				return;
			}
			mAvailableMembers.clear();
			mRegisteredMembers.clear();
			mAvailableYMCAMembers.clear();
			mAvailableDaycareMembers.clear();
			mRegisterable.clear();
			mLastPin = pin.getDashedSeparatedNumberWithAreaCode();
			getPersistenceLayer().fetchFamilyByPin(mLastPin, new PersistenceLayerCallback<List<Member>>() {

				@Override
				public void done(List<Member> data, Exception e) {

					if (data == null)
						return;
					mRegisteredMembers.addAll(data);
					for (Member mem : data) {
						if (!mem.getUsesChildWatch())
							mAvailableDaycareMembers.add(mem);
					}
				}
			});
			getPersistenceLayer().fetchDaxkoFamilyMembers(pin.getAreaCode(), pin.getDashSeparatedNumber(),
					new PersistenceLayerCallback<List<com.alaris_us.externaldata.to.Member>>() {

						@Override
						public void done(List<com.alaris_us.externaldata.to.Member> data, Exception e) {

							if (data == null) {
								Toast.makeText(v.getContext(), "No Members Found", Toast.LENGTH_LONG).show();
								return;
							}
							for (com.alaris_us.externaldata.to.Member dma : data) {
								mAvailableMembers.add(dma);
							}
							findUnregisteredYMCAMembers();
							for (com.alaris_us.externaldata.to.Member ma : mAvailableYMCAMembers)
								createMember(ma);
							mRegisterable.addAll(mAvailableDaycareMembers);
							if (mRegisterable.isEmpty())
								Toast.makeText(v.getContext(), "No Members Found", Toast.LENGTH_LONG).show();
						}
					});
		}
	}

	public OnItemClickListener getMemberListOnItemClickListener() {

		return new MemberListOnItemClickListener();
	}

	private class MemberListOnItemClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

			final Member toAdd = getRegisterableMembers().get(position);
			String message = "Do you want to add " + toAdd.getFirstName() + " " + toAdd.getLastName() + "?";
			DaycareHelpers.createYesNoDialog(view.getContext(), message, new YesNoDialogCallback() {

				@Override
				public void yesClicked() {

					registerMember(toAdd);
				}

				@Override
				public void noClicked() {

				}
			});
		}
	}

	private void registerMember(Member toAdd) {
		/*
		 * Family attachedTo = getFirstMembersPFamily();
		 * 
		 * final Member toRegister =
		 * getPersistenceLayer().createMemberNoPerist(toAdd.getMemberID(),
		 * toAdd.getUnitID(), toAdd.getFirstName(), toAdd.getLastName(),
		 * toAdd.getBirthDate(), toAdd.getGender(), toAdd.getEmail(), mLastPin,
		 * null, attachedTo, !toAdd.isActive(), null);
		 */
		boolean alreadyExists = false;
		for (Member member : mAvailableDaycareMembers)
			if (toAdd.getMemberID().equals(member.getMemberID())) {
				alreadyExists = true;
				break;
			}

		if (alreadyExists) {
			toAdd.setUsesChildWatch(true);
			getPersistenceLayer().updateMember(toAdd);
		} else {
			toAdd.setUsesChildWatch(true);
			// toAdd.setActive(true);
			// toAdd.setIsValidMembership(true);
			toAdd.setMembershipType(mMembershipType);
			getPersistenceLayer().updateMember(toAdd);
			// getPersistenceLayer().setupFamilyAssociation(toAdd, mAttachedTo);

			getPersistenceLayer().registerMember(toAdd, mAttachedTo, new PersistenceLayerCallback<Member>() {

				@Override
				public void done(Member data, Exception e) {

					/*
					 * if (data != null) { mRegisteredMembers.add(data); }
					 * filterMemberArrays();
					 */
				}
			});
		}
		mRegisterable.remove(toAdd);
	}

	/*
	 * private void filterMemberArrays() {
	 * 
	 * boolean hasRegisteredAdults = hasRegisteredAdults();
	 * 
	 * mMembers.clear(); mRegisterable.get().clear(); for (MemberAdditional
	 * daxmem : mAvailableMembers) { MemberAdditional foundMember = null; Member
	 * foundDaycareMember = null; for (Member m : mRegisteredMembers) { if
	 * (daxmem.getMemberId().equals(m.getMemberID())) { foundMember = daxmem;
	 * foundDaycareMember = m; } } if (foundMember == null) { if
	 * (hasRegisteredAdults) mRegisterable.get().add(daxmem); else if
	 * (DaycareHelpers.isAdult(daxmem.getDateOfBirth(), AGE_CUTOFF))
	 * mRegisterable.get().add(daxmem); } else {
	 * mMembers.add(foundDaycareMember); } } }
	 */

	private void findUnregisteredYMCAMembers() {
		boolean hasRegisteredAdults = hasRegisteredAdults();
		mAvailableYMCAMembers.clear();
		for (com.alaris_us.externaldata.to.Member daxmem : mAvailableMembers) {
			com.alaris_us.externaldata.to.Member foundMember = null;
			for (Member m : mRegisteredMembers) {
				if (daxmem.getMemberId().equals(m.getMemberID())) {
					foundMember = daxmem;
				}
			}
			if (foundMember == null) {
				if (hasRegisteredAdults)
					mAvailableYMCAMembers.add(daxmem);
				else {
					if (DaycareHelpers.isAdult(daxmem.getBirthDate(), AGE_CUTOFF))
						mAvailableYMCAMembers.add(daxmem);
				}
			}
		}
	}

	public void createMember(com.alaris_us.externaldata.to.Member memAdd) {
		mAttachedTo = getFirstMembersPFamily();
		mMembershipType = getMembershipType();
		String email = "";

		if (memAdd.getEmail() != null)
			email = memAdd.getEmail().toString();

		List<String> userPins = new ArrayList<String>();
		List<com.alaris_us.daycaredata.PhoneNumber> phones = new ArrayList<com.alaris_us.daycaredata.PhoneNumber>();

		for (com.alaris_us.externaldata.data.PhoneNumber num : memAdd.getPhones()) {
			userPins.add(num.getDashedSeparatedNumberWithAreaCode());
			com.alaris_us.daycaredata.PhoneNumber p = new com.alaris_us.daycaredata.PhoneNumber(num.getType(),
					num.getDashSeparatedNumber());
			phones.add(p);
		}
		if (!userPins.contains(mLastPin))
			userPins.add(mLastPin);

		final Member toRegister = getPersistenceLayer().createMemberNoPerist(memAdd.getMemberId(), memAdd.getUnitID(),
				memAdd.getFirstName(), memAdd.getLastName(), memAdd.getBirthDate(), memAdd.getGender(), email, userPins,
				phones, null, mAttachedTo, false, null, memAdd.getBarcode().toString(), memAdd.isActive());
		mRegisterable.add(toRegister);
	}

	@Override
	protected void initialize() {

		mRegisteredMembers = new ArrayList<Member>();
		mAvailableMembers = new ArrayList<com.alaris_us.externaldata.to.Member>();
		mPhoneNumber = new TrackableField<String>();
		mRegisterable = new TrackableCollection<Member>();
		mAvailableYMCAMembers = new ArrayList<com.alaris_us.externaldata.to.Member>();
		mAvailableDaycareMembers = new ArrayList<Member>();
		mAttachedTo = null;
		mMembershipType = "";
	}

	private boolean hasRegisteredAdults() {

		for (Member m : mRegisteredMembers) {

			if (DaycareHelpers.isAdult(m.getBirthDate(), AGE_CUTOFF))
				return true;
		}

		return false;
	}

	private Family getFirstMembersPFamily() {

		if (mRegisteredMembers.size() == 0) {
			return null;
		}

		return mRegisteredMembers.get(0).getPFamily();

	}

	private String getMembershipType() {
		if (mRegisteredMembers.size() == 0)
			return null;

		return mRegisteredMembers.get(0).getMembershipType();
	}

	public DaxkoFamilyImporterViewModel(PersistenceLayer persistenceLayer) {

		super(persistenceLayer);
	}

	private List<Member> mRegisteredMembers;
	private List<com.alaris_us.externaldata.to.Member> mAvailableMembers;
	private List<Member> mAvailableDaycareMembers;
	private List<com.alaris_us.externaldata.to.Member> mAvailableYMCAMembers;
	private TrackableField<String> mPhoneNumber;
	private TrackableCollection<Member> mRegisterable;
	private String mLastPin;
	private Family mAttachedTo;
	private String mMembershipType;
}
