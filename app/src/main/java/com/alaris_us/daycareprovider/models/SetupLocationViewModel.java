package com.alaris_us.daycareprovider.models;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import android.graphics.Bitmap;
import android.view.View;
import android.view.View.OnClickListener;

import com.alaris_us.daycareprovider.persistence.PersistenceData.CHANGE_ID;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.bindroid.trackable.TrackableField;

public class SetupLocationViewModel extends ViewModel {

	public SetupLocationViewModel(PersistenceLayer persistenceLayer) {

		super(persistenceLayer);
	}

	public Bitmap getBitmapHomeScreen() {

		return bitmapHomeScreen.get();
	}

	public void setBitmapHomeScreen(Bitmap bitmapHomeScreen) {

		this.bitmapHomeScreen.set(bitmapHomeScreen);
	}

	public String getCity() {

		return city.get();
	}

	public void setCity(String city) {

		this.city.set(city);
	}

	public String getState() {

		return state.get();
	}

	public void setState(String state) {

		this.state.set(state);
	}

	public String getCityState() {

		return getCity() + ", " + getState();
	}

	public String getYmcaName() {

		return ymcaName.get();
	}

	public void setYmcaName(String ymcaName) {

		this.ymcaName.set(ymcaName);
	}

	public OnClickListener getSaveInformationOnClickListener() {

		return new SaveInformation();
	}

	private class SaveInformation implements OnClickListener {

		@Override
		public void onClick(View v) {

			persistModel();
		}
	}

	@Override
	protected void initialize() {

		// Initialize Fields
		city = new TrackableField<String>();
		state = new TrackableField<String>();
		ymcaName = new TrackableField<String>();
		bitmapHomeScreen = new TrackableField<Bitmap>();
		// Add listeners
		/* City */watchProperty(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {

				setCity((String) event.getNewValue());
			}
		}, CHANGE_ID.FACILITY_CITY);
		/* Bitmap */watchProperty(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {

				setBitmapHomeScreen((Bitmap) event.getNewValue());
			}
		}, CHANGE_ID.FACILITY_IMAGE);
		/* State */watchProperty(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {

				setState((String) event.getNewValue());
			}
		}, CHANGE_ID.FACILITY_STATE);
		/* YMCA Name */watchProperty(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {

				setYmcaName((String) event.getNewValue());
			}
		}, CHANGE_ID.FACILITY_YMCANAME);
		/* Whole Facility */watchProperty(new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent event) {

				initializeModelData();
			}
		}, CHANGE_ID.FACILITY);
		initializeModelData();
	}

	private void initializeModelData() {

		setBitmapHomeScreen(getPersistenceData().getFacilityImage());
		setCity(getPersistenceData().getFacilityCity());
		setState(getPersistenceData().getFacilityState());
		setYmcaName(getPersistenceData().getYmcaName());
	}

	private void persistModel() {

		getPersistenceData().setFacilityCity(getCity());
		getPersistenceData().setFacilityImage(getBitmapHomeScreen());
		getPersistenceData().setFacilityState(getState());
		getPersistenceData().setYmcaName(getYmcaName());
		getPersistenceLayer().persistFacility();
	}

	private TrackableField<Bitmap> bitmapHomeScreen;
	private TrackableField<String> city;
	private TrackableField<String> state;
	private TrackableField<String> ymcaName;
}
