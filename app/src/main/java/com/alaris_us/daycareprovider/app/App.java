package com.alaris_us.daycareprovider.app;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender.Method;
import org.acra.sender.HttpSender.Type;

import com.alaris_us.daycaredata.util.ConnectivityUtility;
import com.alaris_us.daycaredata.util.InternetConnectivityUtility;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer;
import com.alaris_us.daycareprovider.utils.AutoUpdateApk;
import com.alaris_us.daycareprovider_dev.R;

import android.app.Application;

@ReportsCrashes(formKey = "", httpMethod = Method.PUT, reportType = Type.JSON, formUri = "http://alaris.iriscouch.com/acra-myapp/_design/acra-storage/_update/report", formUriBasicAuthLogin = "ymcaKiosk", formUriBasicAuthPassword = "joystick")
public class App extends Application {

	// Client application
	private static final String CLIENT_PACKAGE = "com.example.daycareclient";

	public PersistenceLayer getPersistenceLayer() {

		if (mPersistenceLayer == null) {
			mPersistenceLayer = new PersistenceLayer(this, getConnectivityUtility());
		}
		return mPersistenceLayer;
	}

	public ConnectivityUtility getConnectivityUtility() {
		if (mConnectivityManager == null) {
			mConnectivityManager = new InternetConnectivityUtility(this);
		}
		return mConnectivityManager;
	}

	public String getClientPackage() {

		return CLIENT_PACKAGE;
	}

	public String getAppPassword() {

		return getString(R.string.PARSE_CLIENT_KEY);

	}

	public boolean isErrorReportEnabled() {

		return getResources().getBoolean(R.bool.IS_ERROR_REPORT_ENABLED);
	}

	public boolean isUpdateEnabled() {

		return getResources().getBoolean(R.bool.IS_UPDATE_ENABLED);

	}

	public boolean isStartupUpdateEnabled() {

		return getResources().getBoolean(R.bool.IS_STARTUP_UPDATE_ENABLED);
	}

	public void checkForUpdates() {

		if (isStartupUpdateEnabled())
			mAUA.checkUpdatesManually();
	}

	@Override
	public void onCreate() {

		super.onCreate();
		if (isErrorReportEnabled())
			ACRA.init(this);
		if (isUpdateEnabled())
			mAUA = new AutoUpdateApk(this);
	}

	private PersistenceLayer mPersistenceLayer;
	private ConnectivityUtility mConnectivityManager;
	private AutoUpdateApk mAUA;
}
