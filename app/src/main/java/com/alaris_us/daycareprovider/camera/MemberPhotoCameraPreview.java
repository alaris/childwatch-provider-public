package com.alaris_us.daycareprovider.camera;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.util.Log;
import android.view.View;

public class MemberPhotoCameraPreview extends CameraPreview implements Camera.PreviewCallback, FaceDetectionListener {
	private FaceOverlayView mFaceOverlay;
	private boolean mFaceDetectionStarted;
	private FaceInViewFinder mFaceInViewFinderListener;
	private View mViewFinder;
	private Rect mViewFinderRect;

	public MemberPhotoCameraPreview(Context context, CameraManager manager) {
		super(context, manager);
		mViewFinderRect = new Rect();
		mFaceOverlay = new FaceOverlayView(context);
		addView(mFaceOverlay, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	}

	public interface FaceInViewFinder {
		public void faceAccepted();

		public void faceNotAccepted();
	}

	public void setViewFinderRect(View viewFinder) {
		mViewFinder = viewFinder;
	}

	@Override
	public void startPreview() {
		Camera theCamera = getCamera();
		if (null == theCamera)
			return;
		mFaceOverlay.setFaceRects(null);
		theCamera.setDisplayOrientation(180);
		theCamera.startPreview();
		if (!mFaceDetectionStarted) {
			mFaceDetectionStarted = true;
			theCamera.setFaceDetectionListener(this);
			if (theCamera.getParameters().getMaxNumDetectedFaces() > 0)
				theCamera.startFaceDetection();
		}
	}

	@Override
	public void stopPreview() {
		Camera theCamera = getCamera();
		if (null == theCamera)
			return;
		if (mFaceDetectionStarted) {
			mFaceDetectionStarted = false;
			theCamera.stopFaceDetection();
			theCamera.setFaceDetectionListener(null);
		}
		theCamera.stopPreview();
	}

	@Override
	public void pictureTaken() {
		mFaceDetectionStarted = false;
	}

	private RectF[] calculateDrawRects(Face[] faces) {
		Log.i("FACES", "" + faces.length);
		RectF[] drawRects = new RectF[faces.length];
		Matrix drawMatrix = new Matrix();
		boolean facesMirrored = (CameraHelpers.CAMERA_FRONT == getCameraDirection()) ? true : false;
		CameraHelpers.prepareMatrix(drawMatrix, facesMirrored, getWidth(), getHeight());
		for (int faceIndex = 0; faceIndex < faces.length; faceIndex++) {
			drawRects[faceIndex] = new RectF(faces[faceIndex].rect);
			drawMatrix.mapRect(drawRects[faceIndex]);
		}
		return drawRects;
	}

	public void setFaceInViewFinderListener(FaceInViewFinder listener) {
		mFaceInViewFinderListener = listener;
	}

	@Override
	public void onFaceDetection(Face[] faces, Camera camera) {
		RectF[] faceRects = calculateDrawRects(faces);
		Rect roundedRect = new Rect();
		boolean faceInViewFinder = false;
		if (null != mViewFinder) {
			mViewFinderRect.set(mViewFinder.getLeft(), mViewFinder.getTop(), mViewFinder.getRight(),
					mViewFinder.getBottom());
		}
		mFaceOverlay.setFaceRects(faceRects);
		for (RectF thisRect : faceRects) {
			thisRect.round(roundedRect);
			if (mViewFinderRect.contains(roundedRect))
				faceInViewFinder = true;
		}
		if (mFaceInViewFinderListener != null) {
			if (faceInViewFinder){
				mFaceInViewFinderListener.faceAccepted();
				Log.i("CAMERA", "ACCEPTED");
			}
			else {
				mFaceInViewFinderListener.faceNotAccepted();
				Log.i("CAMERA", "NOT ACCEPTED");
			}
		}
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {

	}
}