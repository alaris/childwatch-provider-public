package com.alaris_us.daycareprovider.camera;

import com.alaris_us.daycareprovider.camera.CameraHelpers.CameraDirection;
import com.alaris_us.daycareprovider.utils.LogUtils;

import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.AsyncTask;

public class CameraManager {

	private Camera mCamera;
	private AsyncTask<?, ?, ?> mRunningTask;
	private CameraManagerCallback mCallback;
	private CameraDirection mCameraDirection;

	public CameraManager() {
		mCameraDirection = CameraDirection.NOTSPECIFIED;
	}

	public interface CameraManagerCallback {

		public void onCameraOpened(Camera opened);

	}

	public synchronized Camera getCamera() {
		return mCamera;
	}

	public synchronized void openCamera(CameraDirection direction, CameraManagerCallback callback) {

		LogUtils.LOGD("BarcodeCameraPreview", "openCamera()");

		if (null != mRunningTask) {
			return;
		}

		mCallback = callback;

		if (direction == mCameraDirection) {
			mCallback.onCameraOpened(mCamera);
		} else {
			mCameraDirection = direction;
			mRunningTask = new OpenCameraTask();
			((OpenCameraTask) mRunningTask).execute(direction);
		}

	}

	public synchronized CameraDirection getCameraDirection() {
		return mCameraDirection;
	}

	public synchronized boolean isFrontCamera() {
		return getCameraDirection() == CameraDirection.FRONT;
	}

	public synchronized void closeCamera() {

		LogUtils.LOGD("BarcodeCameraPreview", "closeCamera()");

		if (null != mRunningTask) {
			cancelOutstandingTask();
		}

		if (null != mCamera) {
			mCamera.release();
		}
		mCamera = null;
		mCameraDirection = CameraDirection.NOTSPECIFIED;
		mRunningTask = null;

	}

	public synchronized void switchCamera() {
		CameraDirection prevDirection = getCameraDirection();
		CameraDirection newDirection = prevDirection == CameraDirection.BACK ? CameraDirection.FRONT
				: CameraDirection.BACK;
		closeCamera();
		openCamera(newDirection, mCallback);
	}

	public synchronized void takePicture(PictureCallback callback) {
		mCamera.takePicture(null, null, callback);
	}

	private synchronized void cancelOutstandingTask() {
		if (mRunningTask != null) {
			if (mRunningTask.getStatus() != AsyncTask.Status.FINISHED) {
				mRunningTask.cancel(true);
			}
			mRunningTask = null;
		}
	}

	private final class OpenCameraTask extends AsyncTask<CameraDirection, Void, Camera> {

		@Override
		protected void onPostExecute(Camera result) {
			super.onPostExecute(result);

			mCamera = result;
			if (null != mCallback) {
				mCallback.onCameraOpened(mCamera);
			}
		}

		@Override
		protected Camera doInBackground(CameraDirection... params) {
			return CameraHelpers.openCamera(params[0]);
		}

	}

}
