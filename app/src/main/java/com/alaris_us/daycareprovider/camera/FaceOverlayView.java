package com.alaris_us.daycareprovider.camera;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;

public class FaceOverlayView extends View {
	private Paint mPaint;
	private RectF[] mFaces;

	public FaceOverlayView(Context context) {
		super(context);
		initialize();
	}

	private void initialize() {
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setColor(Color.GREEN);
		mPaint.setAlpha(128);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeWidth(5);
	}

	public void setFaceRects(RectF[] faces) {
		mFaces = faces;
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (null == mFaces)
			return;
		if (mFaces.length > 0) {
			canvas.save();

			for (RectF face : mFaces) {
				canvas.drawRect(face, mPaint);
			}
			canvas.restore();
		}
	}
}