package com.alaris_us.daycareprovider.camera;

import static com.alaris_us.daycareprovider.utils.LogUtils.LOGD;
import static com.alaris_us.daycareprovider.utils.LogUtils.LOGE;

import java.io.IOException;
import java.util.List;

import com.alaris_us.daycareprovider.camera.CameraHelpers.CameraDirection;
import com.alaris_us.daycareprovider.camera.CameraManager.CameraManagerCallback;
import com.alaris_us.daycareprovider.utils.LogUtils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

public abstract class CameraPreview extends ViewGroup implements SurfaceHolder.Callback, CameraManagerCallback {
	private SurfaceView mSurfaceView;
	private SurfaceHolder mHolder;
	protected List<Size> mSupportedPreviewSizes;
	private final String TAG = LogUtils.makeLogTag(this.getClass());
	private Size mPreviewSize;
	protected CameraManager mCManager;

	public CameraPreview(Context context, CameraManager manager) {
		super(context);
		mSurfaceView = new SurfaceView(context);
		addView(mSurfaceView);
		mHolder = mSurfaceView.getHolder();
		mHolder.addCallback(this);
		mCManager = manager;
	}

	public void surfaceCreated(SurfaceHolder holder) {
		LOGD(TAG, "surfaceCreated()");
		if (null == getCamera()) {
			mCManager.openCamera(CameraDirection.BACK, this);
			return;
		}

	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		LOGD(TAG, "surfaceDestroyed()");
		if (null == getCamera())
			return;
		stopPreview();
		mCManager.closeCamera();
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		LOGD(TAG, "surfaceChanged()");
		if (null == getCamera())
			return;
		requestLayout();
		startPreview();
	}

	public Camera getCamera() {
		return mCManager.getCamera();
	}

	public void switchCamera() {
		stopPreview();
		mCManager.switchCamera();
	}

	public boolean isFrontCamera() {
		return mCManager.isFrontCamera();
	}

	public Size getPreviewSize() {
		return mPreviewSize;
	}

	public int getCameraDirection() {
		return mCManager.getCameraDirection().getDirection();
	}

	public void stopPreviewAndFreeCamera() {
		LOGD(TAG, "stopPreviewAndFreeCamera()");
		if (null == getCamera()) {
			return;
		}
		stopPreview();
		mCManager.closeCamera();
	}

	public void takePicture(PictureCallback callback) {
		mCManager.takePicture(callback);
		pictureTaken();
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		LOGD(TAG, "onLayout()");
		if (changed && getChildCount() > 0) {
			final int width = r - l;
			final int height = b - t;
			int previewWidth = width;
			int previewHeight = height;
			if (mPreviewSize != null) {
				previewWidth = mPreviewSize.width;
				previewHeight = mPreviewSize.height;
			}
			for (int childIndex = 0; childIndex < getChildCount(); childIndex++) {
				View child = getChildAt(childIndex);
				// Center the child SurfaceView within the parent.
				if (width * previewHeight > height * previewWidth) {
					final int scaledChildWidth = previewWidth * height / previewHeight;
					child.layout((width - scaledChildWidth) / 2, 0, (width + scaledChildWidth) / 2, height);
				} else {
					final int scaledChildHeight = previewHeight * width / previewWidth;
					child.layout(0, (height - scaledChildHeight) / 2, width, (height + scaledChildHeight) / 2);
				}
			}
		}
	}

	@Override
	public void onCameraOpened(Camera opened) {

		try {
			mSupportedPreviewSizes = getCamera().getParameters().getSupportedPreviewSizes();
			requestLayout();
            Camera.Parameters parameters = opened.getParameters();
            if (getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH) && getCameraDirection() == 0)
            	parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            opened.setParameters(parameters);
			opened.setPreviewDisplay(mHolder);
			startPreview();
		} catch (IOException e) {
			LOGE(TAG, "Error setting camera preview: " + e.getLocalizedMessage());
		}

	}

	protected abstract void stopPreview();

	protected abstract void startPreview();

	protected abstract void pictureTaken();
}