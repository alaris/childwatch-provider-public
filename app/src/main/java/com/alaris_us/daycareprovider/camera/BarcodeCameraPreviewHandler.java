/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
<<<<<<< HEAD
 *      http://www.apache.org/licenses/LICENSE-2.	0
=======
 *      http://www.apache.org/licenses/LICENSE-2.0
>>>>>>> ACP-106-offline-for-provider
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.alaris_us.daycareprovider.camera;

import java.util.Collection;
import java.util.Map;

import com.alaris_us.daycareprovider_dev.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * This class handles all the messaging which comprises the state machine for
 * capture.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
public final class BarcodeCameraPreviewHandler extends Handler {

	private final BarcodeCameraPreview mBarcodePreview;

	private final DecodeThread mDecodeThread;
	private State state;

	private enum State {
		PREVIEW, SUCCESS, DONE
	}

	BarcodeCameraPreviewHandler(BarcodeCameraPreview barcodePreview, Collection<BarcodeFormat> decodeFormats,
			Map<DecodeHintType, ?> baseHints, String characterSet) {
		mBarcodePreview = barcodePreview;
		mDecodeThread = new DecodeThread(barcodePreview, decodeFormats, baseHints, characterSet);
		mDecodeThread.start();
		state = State.SUCCESS;

		// Start ourselves capturing previews and decoding.
		// cameraManager.startPreview();
	}

	@Override
	public void handleMessage(Message message) {
		switch (message.what) {
		case R.id.restart_preview:
			restartDecode();
			break;
		case R.id.decode_succeeded:
			state = State.SUCCESS;
			Bundle bundle = message.getData();
			Bitmap barcode = null;
			float scaleFactor = 1.0f;
			if (bundle != null) {
				byte[] compressedBitmap = bundle.getByteArray(DecodeThread.BARCODE_BITMAP);
				if (compressedBitmap != null) {
					barcode = BitmapFactory.decodeByteArray(compressedBitmap, 0, compressedBitmap.length, null);
					// Mutable copy:
					barcode = barcode.copy(Bitmap.Config.ARGB_8888, true);
				}
				scaleFactor = bundle.getFloat(DecodeThread.BARCODE_SCALED_FACTOR);
			}
			mBarcodePreview.handleDecode((Result) message.obj, barcode, scaleFactor);
			break;
		case R.id.decode_failed:
			// We're decoding as fast as possible, so when one decode fails,
			// start another.
			state = State.PREVIEW;
			mBarcodePreview.requestPreviewFrame(mDecodeThread.getHandler(), R.id.decode);
			break;
		}
	}

	public void quitSynchronously() {
		state = State.DONE;
		Message quit = Message.obtain(mDecodeThread.getHandler(), R.id.quit);
		quit.sendToTarget();
		try {
			// Wait at most half a second; should be enough time, and onPause()
			// will timeout quickly
			mDecodeThread.join(500L);
		} catch (InterruptedException e) {
			// continue
		}

		// Be absolutely sure we don't send any queued up messages
		removeMessages(R.id.decode_succeeded);
		removeMessages(R.id.decode_failed);
	}

	private void restartDecode() {
		if (state == State.SUCCESS) {
			state = State.PREVIEW;
			mBarcodePreview.requestPreviewFrame(mDecodeThread.getHandler(), R.id.decode);
		}
	}

}
