package com.alaris_us.daycareprovider.persistence.commands;

import java.util.Arrays;
import java.util.List;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class ReserveMemberCubby extends DaycareDataCommand<List<Cubby>> {

	private final Member mMember;
	private final Facility mFacility;

	public ReserveMemberCubby(Facility facility, Member member, PersistenceLayerCallback<List<Cubby>> callback) {

		super(callback);
		mMember = member;
		mFacility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getFamiliesDAO().listCheckedInFamilyMembers(mMember, mFacility,
				new DaycareOperationComplete<List<Member>>() {

					@Override
					public void operationComplete(final List<Member> familyMembers, DaycareDataException e) {
						List<Cubby> availableCubbies = ((PersistenceData) getPersistentModel()).getAvailableCubbies();
						if (availableCubbies.size() < 1) {
							ReserveMemberCubby.this.operationComplete(null,
									new DaycareDataException("No cubbies available"));
						} else {
							getDataSource().getCubbiesDAO().reserveCubbies(Arrays.asList(availableCubbies.get(0)), familyMembers, mFacility,
									mMember.getPFamily(), ReserveMemberCubby.this);
						}
					}
				});
	}
}
