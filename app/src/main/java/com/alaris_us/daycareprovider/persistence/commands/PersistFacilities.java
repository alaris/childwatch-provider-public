package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class PersistFacilities extends DaycareDataCommand<List<Facility>> {

	private final List<Facility> mToPersist;

	public PersistFacilities(Facility toPersist, PersistenceLayerCallback<List<Facility>> callback) {

		super(callback);
		mToPersist = new ArrayList<Facility>();
		mToPersist.add(toPersist);

	}

	public PersistFacilities(List<Facility> toPersist, PersistenceLayerCallback<List<Facility>> callback) {

		super(callback);
		mToPersist = toPersist;

	}

	@Override
	public void executeSource() {

		getDataSource().getFacilitiesDAO().updateAll(mToPersist, this);

	}
}
