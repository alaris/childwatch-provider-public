package com.alaris_us.daycareprovider.persistence;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.daycaredata.to.User;
import com.alaris_us.daycaredata.to.ValidMembership;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycareprovider.data.CheckInCount;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;

import android.graphics.Bitmap;

public class PersistenceData {

	private User m_user;
	private Facility m_facility;
	private List<WorkoutArea> m_allWorkoutAreas;
	private List<WorkoutArea> m_selectedWorkoutAreas;
	private List<MemberAndCubbies> m_checkedInMembers;
	private List<Record> m_todaysSignInRecords;
	private List<Record> m_reportingHistory;
	private List<Cubby> m_cubbies;
	private List<Pager> m_pagers;
	private List<MemberAndCubbies> m_childrenResults;
	private List<MemberAndCubbies> m_parentResults;
	private List<Incident> m_incidentHistory;
	private List<Room> m_rooms;
	private List<MemberAndCubbies> m_checkedInMembersWithRoom;
	private List<ValidMembership> m_validMemberships;
	private List<Pager> m_availablePagers;
	private List<Cubby> m_availableCubbies;
	private String m_printerIP;
	private String m_printerModel;
	private String m_printerMacAddress;
	private CheckInCount m_checkInCounts;
    private List<Staff> m_staff;
    private List<Staff> m_clockedInStaff;
    private List<Facility> m_assocFacilities;
    private HashMap<Admission, Number> m_AdmissionUsage;
    private List<String> m_messages;
    private boolean m_usesAdmissions;

	// Change Fields
	public static enum CHANGE_ID {
		DAXKO_AUTH, FACILITY, FACILITY_IMAGE, ALLWORKOUTAREAS, SELECTEDWORKOUTAREAS, FACILITY_CITY, FACILITY_STATE, FACILITY_YMCANAME, USER, CHECKED_IN_MEMBERS, TODAYS_SIGN_IN_RECORDS, REPORTING_HISTORY, SEARCHNAME, SEARCHUNIT, CUBBY_RESERVED_USER_LIST, CUBBY_LIST, CUBBY_COLUMNS, CHILDREN_RESULTS, PARENT_RESULTS, INCIDENT_HISTORY, ROOM_LIST, PAGER_LIST, CHECKED_IN_MEMBERS_WITH_ROOM, VALIDMEMBERSHIPS, AVAILABLE_PAGERS, AVAILABLE_CUBBIES, CHECK_IN_COUNTS, STAFF_LIST, CLOCKEDIN_STAFF, ASSOC_FACILITIES, ADMISSION_USAGE, SMS_MESSAGES, USES_ADMISSIONS
	}

	// Listeners
	private PropertyChangeSupport m_propertyChangeSupport;

	public PersistenceData() {

		m_propertyChangeSupport = new PropertyChangeSupport(this);
	}

	public List<Cubby> getAllFacilityCubbies() {

		if (m_cubbies == null)
			m_cubbies = new ArrayList<Cubby>();
		return m_cubbies;
	}

	public void setAllFacilityPagers(List<Pager> allFacilityPagers) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.PAGER_LIST.name(), null, m_pagers = allFacilityPagers);
	}

	public List<Pager> getAllFacilityPagers() {

		if (m_pagers == null)
			m_pagers = new ArrayList<Pager>();
		return m_pagers;
	}

	public void setAllFacilityCubbies(List<Cubby> allFacilityCubbies) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.CUBBY_LIST.name(), null, m_cubbies = allFacilityCubbies);
	}

	public int getFacilityCubbyColumns() {

		return (m_facility == null) ? 0 : m_facility.getCubbyColumns().intValue();
	}

	public void setFacilityCubbyColumns(int count) {

		Number curValue = m_facility.getCubbyColumns();
		m_facility.setCubbyColumns(count);
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.CUBBY_COLUMNS.name(), curValue, curValue.intValue());
	}

	public List<Room> getAllFacilityRooms() {

		if (m_rooms == null)
			m_rooms = new ArrayList<Room>();
		return m_rooms;
	}
	
	public void setAllFacilityRooms(List<Room> allFacilityRooms) {

		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.ROOM_LIST.name(), null, m_rooms = allFacilityRooms);
	}

	public List<Facility> getAllAssocFacilities() {

		if (m_assocFacilities == null)
			m_assocFacilities = new ArrayList<Facility>();
		return m_assocFacilities;
	}

	public void setAllAssocFacilities(List<Facility> allAssocFacilities) {

		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.ASSOC_FACILITIES.name(), null, m_assocFacilities = allAssocFacilities);
	}
	
    public List<Staff> getAllFacilityStaff() {

        if (m_staff == null)
            m_staff = new ArrayList<Staff>();
        return m_staff;
    }

    public void setAllFacilityStaff(List<Staff> allFacilityStaff) {

        m_propertyChangeSupport.firePropertyChange(CHANGE_ID.STAFF_LIST.name(), null, m_staff = allFacilityStaff);
    }
    
    public List<Staff> getClockedInStaff() {

        if (m_clockedInStaff == null)
        	m_clockedInStaff = new ArrayList<Staff>();
        return m_clockedInStaff;
    }

    public void setClockedInStaff(List<Staff> clockedInStaff) {

        m_propertyChangeSupport.firePropertyChange(CHANGE_ID.CLOCKEDIN_STAFF.name(), null, m_clockedInStaff = clockedInStaff);
    }

	public void setFacility(Facility facility) {

		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.FACILITY.name(), m_facility, m_facility = facility);
	}

	public void setFacilityImage(Bitmap image) {

		Bitmap curValue = m_facility.getImageFile();
		m_facility.setImageFile(image);
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.FACILITY_IMAGE.name(), curValue, image);
	}

	public void setFacilityCity(String city) {

		String curValue = m_facility.getCity();
		m_facility.setCity(city);
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.FACILITY_CITY.name(), curValue, city);
	}

	public void setFacilityState(String state) {

		String curValue = m_facility.getUSState();
		m_facility.setUSState(state);
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.FACILITY_STATE.name(), curValue, state);
	}

	public void setYmcaName(String ymcaName) {

		String curValue = m_facility.getYMCAName();
		m_facility.setYMCAName(ymcaName);
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.FACILITY_YMCANAME.name(), curValue, ymcaName);
	}

	public Facility getFacility() {

		return m_facility;
	}

	public void setUser(User user) {

		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.USER.name(), m_user, m_user = user);
		m_printerIP = user.getPrinterIP();
		m_printerModel = user.getPrinterModel();
		m_printerMacAddress = user.getPrinterMacAddress();
	}

	public User getUser() {

		return m_user;
	}

	public String getPrinterIP() {
		return m_printerIP;
	}

	public String getPrinterModel() {
		return m_printerModel;
	}
	
	public String getPrinterMacAddress() {
		return m_printerMacAddress;
	}
	
	public String getFacilityCity() {

		return (m_facility == null) ? null : m_facility.getCity();
	}

	public String getFacilityState() {

		return (m_facility == null) ? null : m_facility.getUSState();
	}

	public String getYmcaName() {

		return (m_facility == null) ? null : m_facility.getYMCAName();
	}

	public Bitmap getFacilityImage() {

		return (m_facility == null) ? null : m_facility.getImageFile();
	}

	public int getFacilityTimeLimit() {
		return m_facility.getTimeLimit().intValue();
	}

	public List<ValidMembership> getvalidMembershipsForFacility() {
		return (m_validMemberships == null) ? new ArrayList<ValidMembership>() : m_validMemberships;
	}

	public void setvalidMembershipsForFacility(List<ValidMembership> validMemberships) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.VALIDMEMBERSHIPS.name(), null,
				m_validMemberships = validMemberships);
	}

	public List<WorkoutArea> getAllWorkoutAreas() {

		return (m_allWorkoutAreas == null) ? new ArrayList<WorkoutArea>() : m_allWorkoutAreas;
	}

	public void setAllWorkoutAreas(List<WorkoutArea> allWorkoutAreas) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.ALLWORKOUTAREAS.name(), null,
				m_allWorkoutAreas = allWorkoutAreas);
	}

	public List<WorkoutArea> getSelectedWorkoutAreas() {

		return (m_selectedWorkoutAreas == null) ? new ArrayList<WorkoutArea>() : m_selectedWorkoutAreas;
	}

	public void setSelectedWorkoutAreas(List<WorkoutArea> selectedWorkoutAreas) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.SELECTEDWORKOUTAREAS.name(), null,
				m_selectedWorkoutAreas = selectedWorkoutAreas);
	}

	public List<MemberAndCubbies> getCheckedInMembers() {

		return (m_checkedInMembers == null) ? new ArrayList<MemberAndCubbies>() : m_checkedInMembers;
	}

	public void setCheckedInMembers(List<MemberAndCubbies> checkedInMembers) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.CHECKED_IN_MEMBERS.name(), null,
				m_checkedInMembers = checkedInMembers);
	}

	public List<MemberAndCubbies> getCheckedInMembersWithRoom() {

		return (m_checkedInMembersWithRoom == null) ? new ArrayList<MemberAndCubbies>() : m_checkedInMembersWithRoom;
	}

	public void setCheckedInMembersWithRoom(List<MemberAndCubbies> checkedInMembersWithRoom) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.CHECKED_IN_MEMBERS_WITH_ROOM.name(), null,
				m_checkedInMembersWithRoom = checkedInMembersWithRoom);
	}

	public List<MemberAndCubbies> getChildrenResults() {

		return (m_childrenResults == null) ? new ArrayList<MemberAndCubbies>() : m_childrenResults;
	}

	public void setChildrenResults(List<MemberAndCubbies> childrenResults) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.CHILDREN_RESULTS.name(), null,
				m_childrenResults = childrenResults);
	}

	public List<MemberAndCubbies> getParentResults() {

		return (m_parentResults == null) ? new ArrayList<MemberAndCubbies>() : m_parentResults;
	}

	public void setParentResults(List<MemberAndCubbies> parentResults) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.PARENT_RESULTS.name(), null,
				m_parentResults = parentResults);
	}

	public List<Record> getTodaysSignInRecords() {

		return (m_todaysSignInRecords == null) ? new ArrayList<Record>() : m_todaysSignInRecords;
	}

	public void setTodaysSignInRecords(List<Record> todaysSignInRecords) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.TODAYS_SIGN_IN_RECORDS.name(), null,
				m_todaysSignInRecords = todaysSignInRecords);
	}
	
	public CheckInCount getCheckInCounts() {

		return m_checkInCounts;
	}

	public void setCheckInCounts(CheckInCount checkInCounts) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.CHECK_IN_COUNTS.name(), null,
				m_checkInCounts = checkInCounts);
	}

	public List<Record> getReportingHistory() {

		return (m_reportingHistory == null) ? new ArrayList<Record>() : m_reportingHistory;
	}

	public void setReportingHistory(List<Record> reportingHistory) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.REPORTING_HISTORY.name(), null,
				m_reportingHistory = reportingHistory);
	}

	public List<Incident> getIncidentHistory() {

		return (m_incidentHistory == null) ? new ArrayList<Incident>() : m_incidentHistory;
	}

	public void setIncidentHistory(List<Incident> incidentHistory) {

		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.INCIDENT_HISTORY.name(), null,
				m_incidentHistory = incidentHistory);
	}

	public void setAvailablePagers(List<Pager> pagers) {
		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.AVAILABLE_PAGERS.name(), null, m_availablePagers = pagers);
	}

	public List<Pager> getAvailablePagers() {
		return (m_availablePagers == null) ? new ArrayList<Pager>() : m_availablePagers;
	}
	
	public void setAvailableCubbies(List<Cubby> cubbies) {
		// Set original value to null to force property change event for list
		// with same reference
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.AVAILABLE_CUBBIES.name(), null, m_availableCubbies = cubbies);
	}

	public List<Cubby> getAvailableCubbies() {
		return (m_availableCubbies == null) ? new ArrayList<Cubby>() : m_availableCubbies;
	}

	public void setAdmissionUsage(HashMap<Admission, Number> admitUsage) {
		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.ADMISSION_USAGE.name(), null, m_AdmissionUsage = admitUsage);
	}

	public HashMap<Admission, Number> getAdmissionUsage() {
		return (m_AdmissionUsage == null) ? new HashMap<Admission, Number>() : m_AdmissionUsage;
	}

	public List<String> getSMSMessages() {
		return m_messages;
	}

	public void setSMSMessages(List<String> messages) {

		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.SMS_MESSAGES.name(), null, m_messages = messages);
	}

	public boolean getFacilityUsesAdmissions() {
		return m_usesAdmissions;
	}

	public void setFacilityUsesAdmissions(boolean usesAdmissions) {

		m_propertyChangeSupport.firePropertyChange(CHANGE_ID.USES_ADMISSIONS.name(), null, m_usesAdmissions = usesAdmissions);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {

		m_propertyChangeSupport.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(PropertyChangeListener listener, CHANGE_ID propertyName) {

		m_propertyChangeSupport.addPropertyChangeListener(propertyName.name(), listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {

		m_propertyChangeSupport.removePropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(CHANGE_ID propertyName, PropertyChangeListener listener) {

		m_propertyChangeSupport.removePropertyChangeListener(propertyName.name(), listener);
	}
}
