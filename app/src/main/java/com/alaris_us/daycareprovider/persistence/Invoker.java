package com.alaris_us.daycareprovider.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.alaris_us.daycareprovider.persistence.Command.CommandStatusListener;

public class Invoker implements CommandStatusListener {

	public interface InvokerStatusListener {

		public void started();

		public void finished();

		public void exception(Exception e);
	}

	private final Object mDataSource;
	private final Object mModel;
	private final List<Command<?>> mCommandlist;
	private final InvokerStatusListener mListener;

	public Invoker(Object dataSource, Object model, InvokerStatusListener listener) {

		mCommandlist = Collections.synchronizedList(new ArrayList<Command<?>>());
		mModel = model;
		mDataSource = dataSource;
		mListener = listener;
	}

	public void invoke(Command<?> command) {

		command.setListener(this);
		command.setDataSource(mDataSource);
		command.setPersistentModel(mModel);
		mCommandlist.add(command);
		command.execute();
	};

	public void starting(Command<?> command) {

		if (mListener != null)
			mListener.started();
	}

	public void finished(Command<?> command) {

		mCommandlist.remove(command);
		if (mCommandlist.size() > 0)
			return;
		if (mListener != null)
			mListener.finished();
	}

	@Override
	public void valueChanged(Command<?> command) {

		// Placeholder
	}

	public void exception(Command<?> command, Exception e) {

		if (mListener != null)
			mListener.exception(e);
	}

	public int getSize() {

		return mCommandlist.size();
	}
}
