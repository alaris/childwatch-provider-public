package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycareprovider.persistence.DaxkoCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class DaxkoLookupByUnitID extends DaxkoCommand<List<com.alaris_us.externaldata.to.Member>> {

	private final String mUnitId;

	public DaxkoLookupByUnitID(String unitId,
			PersistenceLayerCallback<List<com.alaris_us.externaldata.to.Member>> callback) {

		super(callback);
		mUnitId = unitId;

	}

	@Override
	public void executeSource() {

		getDataSource().findMembers(mUnitId, this);

	}
}