package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchMembersByUnitID extends DaycareDataCommand<List<Member>> {

	String mUnitID;

	public FetchMembersByUnitID(String unitID, PersistenceLayerCallback<List<Member>> callback) {

		super(callback);
		mUnitID = unitID;
	}

	@Override
	public void executeSource() {

		getDataSource().getMembersDAO().listMembersByUnitID(mUnitID, this);
	}
}
