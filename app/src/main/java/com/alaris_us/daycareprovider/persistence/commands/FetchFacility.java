package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.User;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchFacility extends DaycareDataCommand<Facility> {

	private final User mUser;

	public FetchFacility(User user) {

		this(user, null);
	}

	protected FetchFacility(User user, PersistenceLayerCallback<Facility> callback) {
		super(callback);
		mUser = user;
	}

	@Override
	public void executeSource() {

		getDataSource().getUsersDAO().getFacility(mUser, this);

	}

	@Override
	public void handleResponse(Facility data) {

		((PersistenceData) getPersistentModel()).setFacility(data);

	}

}
