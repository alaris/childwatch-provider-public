package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.comparators.ComparatorChain;

import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.comparators.MemberAgeComparator;
import com.alaris_us.daycareprovider.comparators.MemberFirstNameComparator;
import com.alaris_us.daycareprovider.comparators.MemberLastNameComparator;
import com.alaris_us.daycareprovider.comparators.MemberTimeLeftComparator;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.view.TabWithIcons.SortDirection;

public class FetchCheckedInMembersIncludingCubbies extends DaycareDataCommand<List<MemberAndCubbies>> {

	final Facility m_facility;
	final List<Member> m_memberList;
	private SortDirection sortDirection;
	private String tabName = " ";
	private boolean hasPagers;
	private boolean hasCubbies;

	public FetchCheckedInMembersIncludingCubbies(Facility facility) {

		this(facility, null, SortDirection.DESCENDING, " ");
	}
	
	public FetchCheckedInMembersIncludingCubbies(Facility facility, PersistenceLayerCallback<List<MemberAndCubbies>> callback) {

		this(facility, callback, SortDirection.DESCENDING, " ");
	}

	public FetchCheckedInMembersIncludingCubbies(Facility facility, SortDirection sortDirection, String tabName) {

		this(facility, null, sortDirection, tabName);
	}
	
	public FetchCheckedInMembersIncludingCubbies(Facility facility, SortDirection sortDirection, String tabName, PersistenceLayerCallback<List<MemberAndCubbies>> callback) {

		this(facility, callback, sortDirection, tabName);
	}

	protected FetchCheckedInMembersIncludingCubbies(Facility facility,
			PersistenceLayerCallback<List<MemberAndCubbies>> callback, SortDirection sortDirection, String tabName) {

		super(callback);
		m_facility = facility;
		m_memberList = new ArrayList<Member>();
		this.sortDirection = sortDirection;
		this.tabName = tabName;
	}

	@Override
	public void executeSource() {
		hasPagers = !((PersistenceData) getPersistentModel()).getAllFacilityPagers().isEmpty();
		hasCubbies = !((PersistenceData) getPersistentModel()).getAllFacilityCubbies().isEmpty();

		List<MemberAndCubbies> oldList = ((PersistenceData) getPersistentModel()).getCheckedInMembers();
		if (oldList != null)
			for (MemberAndCubbies t : oldList)
				t.cancel();
		getDataSource().getFacilitiesDAO().listCheckedInMembers(m_facility,
				new DaycareOperationComplete<List<Member>>() {
					@Override
					public void operationComplete(List<Member> data, DaycareDataException e) {
						if (e != null || data == null || data.isEmpty()) {
							FetchCheckedInMembersIncludingCubbies.this
									.operationComplete(new ArrayList<MemberAndCubbies>(), e);
							return;
						}
						m_memberList.addAll(data);

						ComparatorChain<Member> chain = new ComparatorChain<Member>();

						switch (tabName) {
						case " ":
							chain.addComparator(new MemberFirstNameComparator(sortDirection));
							chain.addComparator(new MemberLastNameComparator(sortDirection));
							Collections.sort(m_memberList, chain);
							break;
						case "Name":
							chain.addComparator(new MemberFirstNameComparator(sortDirection));
							chain.addComparator(new MemberLastNameComparator(sortDirection));
							Collections.sort(m_memberList, chain);
							break;
						case "Family":
							chain.addComparator(new MemberLastNameComparator(sortDirection));
							chain.addComparator(new MemberFirstNameComparator(sortDirection));
							Collections.sort(m_memberList, chain);
							break;
						case "Age":
							Collections.sort(m_memberList, new MemberAgeComparator(sortDirection));
							break;
						case "Time":
							Collections.sort(m_memberList, new MemberTimeLeftComparator(sortDirection));
							break;
						default:
							break;
						}

						DaycareData datasource = getDataSource();
						List<MemberAndCubbies> list = new ArrayList<MemberAndCubbies>();
						for (Member member : m_memberList)
							list.add(new MemberAndCubbies(datasource, member, m_facility, hasCubbies, hasPagers));

						FetchCheckedInMembersIncludingCubbies.this.operationComplete(list, null);
					}
				});
	}

	@Override
	public void handleResponse(List<MemberAndCubbies> data) {
		((PersistenceData) getPersistentModel()).setCheckedInMembers(data);
	}

}
