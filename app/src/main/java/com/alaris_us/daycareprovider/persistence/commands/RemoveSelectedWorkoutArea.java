package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class RemoveSelectedWorkoutArea extends DaycareDataCommand<Facility> {

	private final WorkoutArea mWorkoutArea;
	private final Facility mFacility;

	public RemoveSelectedWorkoutArea(Facility facility, WorkoutArea area) {
		this(facility, area, null);
	}

	public RemoveSelectedWorkoutArea(Facility facility, WorkoutArea area, PersistenceLayerCallback<Facility> callback) {
		super(callback);
		mFacility = facility;
		mWorkoutArea = area;
	}

	@Override
	public void executeSource() {

		getDataSource().getFacilitiesDAO().removeSelectedWorkoutArea(mFacility, mWorkoutArea, this);

	}

	@Override
	public void handleResponse(Facility data) {

		// TODO: remove casts
		List<WorkoutArea> current = ((PersistenceData) getPersistentModel()).getSelectedWorkoutAreas();
		current.remove(mWorkoutArea);
		((PersistenceData) getPersistentModel()).setSelectedWorkoutAreas(current);

	}

}
