package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;

public class CheckOutParent extends DaycareDataCommand<Member> {

	private final Member m_parent;
	private final List<WorkoutArea> m_workoutAreas;

	public CheckOutParent(Member parent, List<WorkoutArea> workoutAreas) {
		// super(dataSource, model);
		m_parent = parent;
		m_workoutAreas = workoutAreas;
	}

	@Override
	public void executeSource() {
		getDataSource().getMembersDAO().removeWorkoutAreasNoPersist(m_parent, m_workoutAreas);
		// if (m_parent.getWasOverridden())
		m_parent.setWasOverridden(false);
		getDataSource().getMembersDAO().update(m_parent, this);
	}

	@Override
	public void handleResponse(Member member) {
	}

}
