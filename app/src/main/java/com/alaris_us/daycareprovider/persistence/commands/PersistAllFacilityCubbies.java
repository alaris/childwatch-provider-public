package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class PersistAllFacilityCubbies extends DaycareDataCommand<List<Cubby>> {

	private final Facility mFacility;
	private final List<String> mToPersist;

	public PersistAllFacilityCubbies(Facility facility, List<Cubby> toPersistList) {

		this(facility, toPersistList, null);
	}

	public PersistAllFacilityCubbies(Facility facility, List<Cubby> toPersistList,
			PersistenceLayerCallback<List<Cubby>> callback) {

		super(callback);
		mFacility = facility;
		mToPersist = new ArrayList<String>();
		for (Cubby c : toPersistList) {
			mToPersist.add(c.getName());
		}
	}

	@Override
	public void executeSource() {

		final List<Cubby> current = ((PersistenceData) getPersistentModel()).getAllFacilityCubbies();
		getDataSource().getCubbiesDAO().removeCubbies(current, new DaycareOperationComplete<List<Cubby>>() {

			@Override
			public void operationComplete(List<Cubby> data, DaycareDataException e) {

				if (e != null) {
					operationComplete(data, e);
				}
				getDataSource().getCubbiesDAO().addCubbies(mToPersist, mFacility, PersistAllFacilityCubbies.this);
			}
		});
	}

	@Override
	protected void handleResponse(List<Cubby> data) {

		((PersistenceData) getPersistentModel()).setAllFacilityCubbies(data);
	}
}
