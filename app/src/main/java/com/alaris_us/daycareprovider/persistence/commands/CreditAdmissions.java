package com.alaris_us.daycareprovider.persistence.commands;

import java.util.HashMap;
import java.util.List;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.AdmissionBalance;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class CreditAdmissions extends DaycareDataCommand<AdmissionBalance> {

	private final Member mMember;
	private final Facility mFacility;
	private final int mAmountToCredit;

	public CreditAdmissions(Member member, Facility facility, int amountToCredit) {
		this(member, facility, amountToCredit, null);
	}

	public CreditAdmissions(Member member, Facility facility, int amountToCredit, PersistenceLayerCallback<AdmissionBalance> callback) {
		super(callback);
		mMember = member;
		mFacility = facility;
		mAmountToCredit = amountToCredit;
	}

	@Override
	public void executeSource() {
		getDataSource().getAdmissionsDAO().creditAdmission(mMember.getObjectId(), mFacility.getObjectId(), mAmountToCredit, this);
	}
}
