package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class UpdateStaff extends DaycareDataCommand<List<Staff>> {

	private final List<Staff> mStaff;

	public UpdateStaff(List<Staff> staff) {
		this(staff, null);
	}

	public UpdateStaff(List<Staff> staff, PersistenceLayerCallback<List<Staff>> callback) {
		super(callback);
		mStaff = staff;
	}

	@Override
	public void executeSource() {
		getDataSource().getStaffDAO().updateAll(mStaff, this);
	}
}
