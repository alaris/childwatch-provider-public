package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.SelfCheckIn;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

import java.util.List;

public class DeleteSelfCheckIn extends DaycareDataCommand<SelfCheckIn> {

	private final Member mMember;

	public DeleteSelfCheckIn(Member member) {

		this(member, null);
	}

	public DeleteSelfCheckIn(Member member, PersistenceLayerCallback<SelfCheckIn> callback) {

		super(callback);
		mMember = member;
	};

	@Override
	public void executeSource() {
		getDataSource().getSelfCheckInsDAO().getSelfCheckInByMember(mMember, new GetSelfCheckInByMemberCommandListener(this));
	}

	private class GetSelfCheckInByMemberCommandListener implements DaycareOperationComplete<SelfCheckIn> {
		private DeleteSelfCheckIn m_listener;

		public GetSelfCheckInByMemberCommandListener(DeleteSelfCheckIn listener) {
			m_listener = listener;
		}

		@Override
		public void operationComplete(SelfCheckIn arg0, DaycareDataException arg1) {
			if (arg0 != null) {
				getDataSource().getSelfCheckInsDAO().delete(arg0, m_listener);
			} else {
				m_listener.operationComplete(null, arg1);
			}
		}
	}
}
