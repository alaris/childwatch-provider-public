package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;

public class FetchAvailablePagers extends DaycareDataCommand<List<Pager>> {

	Facility m_facility;

	public FetchAvailablePagers(Facility facility) {

		this(facility, null);
	}

	public FetchAvailablePagers(Facility facility, PersistenceLayerCallback<List<Pager>> callback) {
		super(callback);
		m_facility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getPagersDAO().listAvailablePagers(m_facility, this);

	}

	@Override
	public void handleResponse(List<Pager> data) {
		DaycareHelpers.sortPagerList(data);
		((PersistenceData) getPersistentModel()).setAvailablePagers(data);
	}
}