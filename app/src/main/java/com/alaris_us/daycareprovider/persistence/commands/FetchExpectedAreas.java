package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchExpectedAreas extends DaycareDataCommand<List<WorkoutArea>> {

	private final Member mMember;

	public FetchExpectedAreas(Member member, PersistenceLayerCallback<List<WorkoutArea>> callback) {

		super(callback);
		mMember = member;
	}

	@Override
	public void executeSource() {

		getDataSource().getMembersDAO().listExpectedAreas(mMember, this);

	}

}
