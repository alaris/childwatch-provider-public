package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;

public class FetchAllFacilitiesForAssoc extends DaycareDataCommand<List<Facility>> {

	public FetchAllFacilitiesForAssoc() {

		this(null);
	}

	public FetchAllFacilitiesForAssoc(PersistenceLayerCallback<List<Facility>> callback) {

		super(callback);
	}

	@Override
	public void executeSource() {

		getDataSource().getFacilitiesDAO().listAllFacilities(this);
	}

	@Override
	protected void handleResponse(List<Facility> data) {

		DaycareHelpers.sortFacilitiesList(data);
		((PersistenceData) getPersistentModel()).setAllAssocFacilities(data);
	}
}
