package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class ReservePager extends DaycareDataCommand<List<Pager>> {

	private final Member m_child;
	private final Pager m_pager;
	private final Facility m_facility;
	private final Family m_family;
	private final Member m_proxy;

	public ReservePager(Pager pager, Member child, Family family, Facility facility, Member proxy) {
		this(pager, child, family, facility, proxy, null);
	};

	public ReservePager(Pager pager, Member child, Family family, Facility facility, Member proxy,
			PersistenceLayerCallback<List<Pager>> callback) {
		super(callback);
		m_pager = pager;
		m_child = child;
		m_family = family;
		m_facility = facility;
		m_proxy = proxy;
	}

	@Override
	public void executeSource() {
		getDataSource().getPagersDAO().reservePagers(m_pager, m_child, m_facility, m_family, m_proxy, this);
	}

	@Override
	public void handleResponse(List<Pager> pagers) {

	}

}