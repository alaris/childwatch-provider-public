package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class FetchFamilyAdmissionBalance extends DaycareDataCommand<HashMap<String, Object>> {

	private final String mMemberObjectId;

	public FetchFamilyAdmissionBalance(String memberId) {

		this(memberId, null);
	}

	public FetchFamilyAdmissionBalance(String memberObjectId, PersistenceLayerCallback<HashMap<String, Object>> callback) {

		super(callback);
		mMemberObjectId =  memberObjectId;
	}

	@Override
	public void executeSource() {

		getDataSource().getAdmissionsDAO().fetchFamilyAdmissionBalance(mMemberObjectId, this);
	}
}
