package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;
import java.util.Locale;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchMembersByLastName extends DaycareDataCommand<List<Member>> {

	String mName;

	public FetchMembersByLastName(String name, PersistenceLayerCallback<List<Member>> callback) {

		super(callback);
		mName = name.toUpperCase(Locale.US);
	}

	@Override
	public void executeSource() {

		getDataSource().getMembersDAO().listMembersByLastName(mName, this);
	}
}
