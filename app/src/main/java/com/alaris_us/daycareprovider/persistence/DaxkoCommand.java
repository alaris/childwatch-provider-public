package com.alaris_us.daycareprovider.persistence;

import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.externaldata.ExternalData;
import com.alaris_us.externaldata.ExternalDataOperationComplete;
import com.alaris_us.externaldata.exception.ExternalDataException;

public abstract class DaxkoCommand<T> extends Command<T> implements ExternalDataOperationComplete<T> {

	public DaxkoCommand() {
		super(null);
	}

	public DaxkoCommand(PersistenceLayerCallback<T> callback) {
		super(callback);
	}

	@Override
	public void DataReceived(T data, ExternalDataException e) {

		reportException(e);

		if (e == null) {
			setResult(data);
			handleResponse(data);
			statusChanged();
		}
		statusUpdateComplete();

	}

	@Override
	protected ExternalData getDataSource() {
		return (ExternalData) super.getDataSource();
	}

}
