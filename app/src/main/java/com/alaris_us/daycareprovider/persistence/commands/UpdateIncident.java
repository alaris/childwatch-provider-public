package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class UpdateIncident extends DaycareDataCommand<Incident> {

	private final Incident mIncident;

	public UpdateIncident(Incident incident) {

		this(incident, null);
	}

	public UpdateIncident(Incident incident, PersistenceLayerCallback<Incident> callback) {

		super(callback);
		mIncident = incident;
	};

	@Override
	public void executeSource() {

		getDataSource().getIncidentsDAO().update(mIncident, this);
	}
}
