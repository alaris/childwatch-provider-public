package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class DeleteAdmissions extends DaycareDataCommand<List<Admission>> {

	private final List<Admission> mAdmissions;

	public DeleteAdmissions(List<Admission> admissions) {
		this(admissions, null);
	}

	public DeleteAdmissions(List<Admission> admissions, PersistenceLayerCallback<List<Admission>> callback) {
		super(callback);
		mAdmissions = admissions;
	}

	@Override
	public void executeSource() {
		getDataSource().getAdmissionsDAO().deleteAll(mAdmissions, this);
	}
}
