package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;

public class FetchAllFacilityStaff extends DaycareDataCommand<List<Staff>> {

	private final Facility mFacility;

	public FetchAllFacilityStaff(Facility facility) {

		this(facility, null);
	}

	public FetchAllFacilityStaff(Facility facility, PersistenceLayerCallback<List<Staff>> callback) {

		super(callback);
		mFacility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getStaffDAO().listAllFacilityStaff(mFacility, this);
	}

	@Override
	protected void handleResponse(List<Staff> data) {

		DaycareHelpers.sortStaffList(data);
		List<Staff> clockedInStaff = new ArrayList<>();
		for (Staff s : data) {
			if (s.getIsClockedIn())
				clockedInStaff.add(s);
		}
		((PersistenceData) getPersistentModel()).setClockedInStaff(clockedInStaff);
		((PersistenceData) getPersistentModel()).setAllFacilityStaff(data);
	}
}
