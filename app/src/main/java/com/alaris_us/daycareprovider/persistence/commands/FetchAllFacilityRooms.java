package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;

public class FetchAllFacilityRooms extends DaycareDataCommand<List<Room>> {

	private final Facility mFacility;

	public FetchAllFacilityRooms(Facility facility) {

		this(facility, null);
	}

	public FetchAllFacilityRooms(Facility facility, PersistenceLayerCallback<List<Room>> callback) {

		super(callback);
		mFacility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getRoomsDAO().listAllFacilityRooms(mFacility, this);
	}

	@Override
	protected void handleResponse(List<Room> data) {

		DaycareHelpers.sortRoomsList(data);
		((PersistenceData) getPersistentModel()).setAllFacilityRooms(data);
	}
}
