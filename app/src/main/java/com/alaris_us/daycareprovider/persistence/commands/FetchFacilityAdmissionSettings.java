package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.AdmissionSetting;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

import java.util.Collections;
import java.util.List;

public class FetchFacilityAdmissionSettings extends DaycareDataCommand<List<AdmissionSetting>> {

	private final Facility mFacility;

	public FetchFacilityAdmissionSettings(Facility facility) {

		this(facility, null);
	}

	public FetchFacilityAdmissionSettings(Facility facility, PersistenceLayerCallback<List<AdmissionSetting>> callback) {

		super(callback);
		mFacility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getAdmissionSettingsDAO().fetchAdmissionSettingsForFacility(mFacility, this);
	}

	@Override
	protected void handleResponse(List<AdmissionSetting> data) {
		if (data.isEmpty())
			((PersistenceData) getPersistentModel()).setFacilityUsesAdmissions(false);
		else
			((PersistenceData) getPersistentModel()).setFacilityUsesAdmissions(true);
	}
}
