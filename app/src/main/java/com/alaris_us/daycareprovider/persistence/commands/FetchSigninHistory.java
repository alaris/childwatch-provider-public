package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycareprovider.data.CheckInCount;
import com.alaris_us.daycareprovider.persistence.HerokuCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchSigninHistory extends HerokuCommand<CheckInCount> {

	private final Facility mFacility;
	private final List<Room> mRooms;

	public FetchSigninHistory(Facility facility, List<Room> rooms) {
		this(facility, rooms, null);
	}

	public FetchSigninHistory(Facility facility, List<Room> rooms,
			PersistenceLayerCallback<CheckInCount> callback) {
		super(callback);
		mFacility = facility;
		mRooms = rooms;
	}

	@Override
	public void executeSource() {
		List<String> roomNames = new ArrayList<String>();
		for (Room room : mRooms)
			roomNames.add(room.getName());
		
		getDataSource().getHerokuCommands().fetchCheckInCounts(mFacility.getYMCAName(), roomNames, this);
	}

	@Override
	public void handleResponse(CheckInCount data) {
		((PersistenceData) getPersistentModel()).setCheckInCounts(data);
	}
}
