package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.dao.Incidents.IncidentType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.comparators.IncidentDateComparator;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchIncidentHistory extends DaycareDataCommand<List<Incident>> {

	private final DateTime mStartDate, mEndDate;
	private final Facility mFacility;
	private final IncidentType mIncidentType;

	public FetchIncidentHistory(Facility facility, DateTime start, DateTime end, IncidentType incidentType) {

		this(facility, start, end, incidentType, null);
	}

	public FetchIncidentHistory(Facility facility, DateTime start, DateTime end, IncidentType incidentType,
			PersistenceLayerCallback<List<Incident>> callback) {

		super(callback);
		mFacility = facility;
		mStartDate = start;
		mEndDate = end;
		mIncidentType = incidentType;
	}

	@Override
	public void executeSource() {

		getDataSource().getFacilitiesDAO().listIncidents(mFacility, mIncidentType, mStartDate.toDate(),
				mEndDate.toDate(), new IncidentsCallback(this));

	}

	@Override
	public void handleResponse(List<Incident> data) {

		Collections.sort(data, new IncidentDateComparator());
		((PersistenceData) getPersistentModel()).setIncidentHistory(data);

	}

	private class IncidentsCallback implements DaycareOperationComplete<List<Incident>> {
		private FetchIncidentHistory mListener;

		public IncidentsCallback(FetchIncidentHistory listener) {
			mListener = listener;
		}

		@Override
		public void operationComplete(List<Incident> data, DaycareDataException e) {
			Set<Member> members = new HashSet<Member>();
			for (Incident incident : data) {
				members.add(incident.getPMember());
			}
			getDataSource().getMembersDAO().fetchAllObjects(new ArrayList<Member>(members),
					new MembersCallback(mListener, data));
		}
	}

	private class MembersCallback implements DaycareOperationComplete<List<Member>> {
		private FetchIncidentHistory mListener;
		private List<Incident> mIncidents;

		public MembersCallback(FetchIncidentHistory listener, List<Incident> incident) {
			mListener = listener;
			mIncidents = incident;
		}

		@Override
		public void operationComplete(List<Member> data, DaycareDataException e) {
			mListener.operationComplete(mIncidents, e);
		}
	}
}
