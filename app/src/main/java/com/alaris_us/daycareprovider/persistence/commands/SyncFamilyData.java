package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.data.HerokuJob;
import com.alaris_us.daycareprovider.persistence.HerokuCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class SyncFamilyData extends HerokuCommand<HerokuJob> {

	private final List<Member> mMembers;

	public SyncFamilyData(List<Member> members) {
		this(members, null);
	}

	public SyncFamilyData(List<Member> members, PersistenceLayerCallback<HerokuJob> callback) {
		super(callback);
		mMembers = members;
	}

	@Override
	public void executeSource() {
		List<String> memberIds = new ArrayList<String>();
		for (Member member : mMembers)
			memberIds.add(member.getMemberID());

		getDataSource().getHerokuCommands().syncFamilyData(memberIds, this);
	}

	@Override
	public void handleResponse(HerokuJob data) {
		
	}
}
