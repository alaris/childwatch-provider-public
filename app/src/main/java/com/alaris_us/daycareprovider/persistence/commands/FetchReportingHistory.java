package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import org.joda.time.DateTime;

import com.alaris_us.daycaredata.dao.Records.EventType;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;

public class FetchReportingHistory extends DaycareDataCommand<List<Record>> {

	private final DateTime mStartDate, mEndDate;
	private final Member mProxy;
	private final Facility mFacility;
	private final List<EventType> mEventType;
	private final SignInType mSignInType;

	public FetchReportingHistory(Facility facility, DateTime start, DateTime end, Member proxy, List<EventType> eventType,
			SignInType signInType) {

		this(facility, start, end, proxy, eventType, signInType, null);
	}

	public FetchReportingHistory(Facility facility, DateTime start, DateTime end, Member proxy, List<EventType> eventType,
			SignInType signInType, PersistenceLayerCallback<List<Record>> callback) {

		super(callback);
		mFacility = facility;
		mStartDate = start;
		mEndDate = end;
		mProxy = proxy;
		mEventType = eventType;
		mSignInType = signInType;
	}

	@Override
	public void executeSource() {

		getDataSource().getFacilitiesDAO().listRecords(mFacility, mEventType, mProxy, mSignInType, mStartDate.toDate(),
				mEndDate.toDate(), this);

	}

	@Override
	public void handleResponse(List<Record> data) {

		DaycareHelpers.sortRecordsList(data);
		((PersistenceData) getPersistentModel()).setReportingHistory(data);

	}

}
