package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.ValidMembership;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchAllMembershipsForFacility extends DaycareDataCommand<List<ValidMembership>> {

	private final Facility mFacility;

	public FetchAllMembershipsForFacility(Facility facility) {

		this(facility, null);
	}

	public FetchAllMembershipsForFacility(Facility facility, PersistenceLayerCallback<List<ValidMembership>> callback) {

		super(callback);
		mFacility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getValidMembershipsDAO().listAllMembershipsForFacility(mFacility, this);
	}

	@Override
	protected void handleResponse(List<ValidMembership> data) {

		((PersistenceData) getPersistentModel()).setvalidMembershipsForFacility(data);
	}
}
