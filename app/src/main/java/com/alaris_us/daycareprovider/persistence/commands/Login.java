package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.User;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class Login extends DaycareDataCommand<User> {

	private final String mUsername, mPassword;

	public Login(String username, String password) {

		this(username, password, null);
	};

	public Login(String username, String password, PersistenceLayerCallback<User> callback) {

		super(callback);
		mUsername = username;
		mPassword = password;
	}

	@Override
	public void executeSource() {

		getDataSource().login(mUsername, mPassword, this);

	}

	@Override
	public void handleResponse(User data) {

		((PersistenceData) getPersistentModel()).setUser(data);

	}

}
