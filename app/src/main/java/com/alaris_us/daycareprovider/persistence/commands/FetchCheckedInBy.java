package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchCheckedInBy extends DaycareDataCommand<Member> {

	private final Member mChild;

	public FetchCheckedInBy(Member child, PersistenceLayerCallback<Member> callback) {

		super(callback);
		mChild = child;
	}

	@Override
	public void executeSource() {

		getDataSource().getMembersDAO().getCheckedInBy(mChild, this);

	}

}
