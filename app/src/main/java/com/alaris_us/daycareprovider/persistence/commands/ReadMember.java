package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class ReadMember extends DaycareDataCommand<Member> {

	String mObjectId;

	public ReadMember(String objectId, PersistenceLayerCallback<Member> callback) {

		super(callback);
		mObjectId = objectId;
	}

	@Override
	public void executeSource() {

		getDataSource().getMembersDAO().read(mObjectId, this);
	}
}
