package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchAllWorkoutAreas extends DaycareDataCommand<List<WorkoutArea>> {

	public FetchAllWorkoutAreas() {

		this(null);
	}

	protected FetchAllWorkoutAreas(PersistenceLayerCallback<List<WorkoutArea>> callback) {

		super(callback);
	}

	@Override
	public void executeSource() {

		getDataSource().getWorkoutAreasDAO().listAllWorkoutAreas(this);

	}

	@Override
	public void handleResponse(List<WorkoutArea> data) {

		((PersistenceData) getPersistentModel()).setAllWorkoutAreas(data);

	}

}
