package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchMembersByFamilyObject extends DaycareDataCommand<List<Member>> {

	private final Family mFamily;

	public FetchMembersByFamilyObject(Family family, PersistenceLayerCallback<List<Member>> callback) {

		super(callback);
		mFamily = family;
	}

	@Override
	public void executeSource() {

		getDataSource().getFamiliesDAO().listFamilyMembers(mFamily, this);

	}

}
