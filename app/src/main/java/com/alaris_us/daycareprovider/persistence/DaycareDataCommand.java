package com.alaris_us.daycareprovider.persistence;

import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public abstract class DaycareDataCommand<T> extends Command<T> implements DaycareOperationComplete<T> {

	public DaycareDataCommand() {
		super(null);
	}

	public DaycareDataCommand(PersistenceLayerCallback<T> callback) {
		super(callback);
	}

	@Override
	public void operationComplete(T data, DaycareDataException e) {

		reportException(e);

		if (e == null) {
			setResult(data);
			handleResponse(data);
			statusChanged();
		}
		statusUpdateComplete();
	}

	@Override
	protected DaycareData getDataSource() {
		return (DaycareData) super.getDataSource();
	}

}
