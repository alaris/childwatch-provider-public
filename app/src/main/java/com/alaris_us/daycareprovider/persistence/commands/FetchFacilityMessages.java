package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FetchFacilityMessages extends DaycareDataCommand<List<String>> {

	private final Facility mFacility;

	public FetchFacilityMessages(Facility facility) {

		this(facility, null);
	}

	public FetchFacilityMessages(Facility facility, PersistenceLayerCallback<List<String>> callback) {

		super(callback);
		mFacility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getFacilitiesDAO().fetchFacilityMessages(mFacility, this);
	}

	@Override
	protected void handleResponse(List<String> data) {
		Collections.sort(data);
		data.add("Other");
		((PersistenceData) getPersistentModel()).setSMSMessages(data);
	}
}
