package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class ReservePagers extends DaycareDataCommand<List<Pager>> {

	private final List<Member> m_children;
	private final List<Pager> m_pagers;
	private final Facility m_facility;
	private final Family m_family;
	private final Member m_proxy;

	public ReservePagers(List<Pager> pagers, List<Member> children, Family family, Facility facility, Member proxy) {
		this(pagers, children, family, facility, proxy, null);
	};

	public ReservePagers(List<Pager> pagers, List<Member> children, Family family, Facility facility, Member proxy,
			PersistenceLayerCallback<List<Pager>> callback) {
		super(callback);
		m_pagers = pagers;
		m_children = children;
		m_family = family;
		m_facility = facility;
		m_proxy = proxy;
	}

	@Override
	public void executeSource() {
		getDataSource().getPagersDAO().reservePagers(m_pagers, m_children, m_facility, m_family, m_proxy, this);
	}

	@Override
	public void handleResponse(List<Pager> pagers) {

	}

}