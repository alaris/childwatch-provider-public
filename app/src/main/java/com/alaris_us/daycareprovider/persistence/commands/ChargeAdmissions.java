package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChargeAdmissions extends DaycareDataCommand<List<HashMap<String, Object>>> {

	private final List<Member> mMembers;
	private final Facility mFacility;
	private final boolean mShouldChargeAccount;
	private final Member mMemberToCharge;

	public ChargeAdmissions(List<Member> members, Facility facility, boolean shouldChargeAccount, Member memberToCharge) {

		this(members, facility, shouldChargeAccount, memberToCharge, null);
	}

	public ChargeAdmissions(List<Member> members, Facility facility, boolean shouldChargeAccount, Member memberToCharge,
							PersistenceLayerCallback<List<HashMap<String, Object>>> callback) {

		super(callback);
		mMembers = members;
		mFacility = facility;
		mShouldChargeAccount = shouldChargeAccount;
		mMemberToCharge = memberToCharge;
	}

	@Override
	public void executeSource() {
		List<String> memberIds = new ArrayList<>();
		for (Member m : mMembers) {
			memberIds.add(m.getObjectId());
		}
		String memberToCharge = (mMemberToCharge == null) ? "" : mMemberToCharge.getObjectId();
		getDataSource().getAdmissionsDAO().chargeAdmission(memberIds, mFacility.getObjectId(), mShouldChargeAccount, memberToCharge, this);
	}
}
