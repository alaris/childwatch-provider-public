package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class UpdateMember extends DaycareDataCommand<Member> {

	private final Member mMember;

	public UpdateMember(Member member) {
		this(member, null);
	}

	public UpdateMember(Member member, PersistenceLayerCallback<Member> callback) {
		super(callback);
		mMember = member;
	}

	@Override
	public void executeSource() {
		getDataSource().getMembersDAO().update(mMember, this);
	}
}
