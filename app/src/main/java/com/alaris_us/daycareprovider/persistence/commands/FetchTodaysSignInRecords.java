package com.alaris_us.daycareprovider.persistence.commands;

import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import com.alaris_us.daycaredata.dao.Records.EventType;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchTodaysSignInRecords extends DaycareDataCommand<List<Record>> {

	private final DateTime mStartDate, mEndDate;
	private final Facility mFacility;
	private final List<EventType> mEventTypes;

	public FetchTodaysSignInRecords(Facility facility) {

		this(facility, null);
	}

	public FetchTodaysSignInRecords(Facility facility, PersistenceLayerCallback<List<Record>> callback) {

		super(callback);
		mFacility = facility;
		mStartDate = new LocalDate().toDateTimeAtStartOfDay();
		mEndDate = mStartDate.plusHours(24);
		mEventTypes = Arrays.asList(EventType.CHECK_IN, EventType.ROOM_CHANGE_IN);
		
	}

	@Override
	public void executeSource() {
		getDataSource().getFacilitiesDAO().listRecords(mFacility, mEventTypes, null, (SignInType)null, mStartDate.toDate(), mEndDate.toDate(), this);
	}

	@Override
	public void handleResponse(List<Record> data) {

		((PersistenceData) getPersistentModel()).setTodaysSignInRecords(data);

	}

}