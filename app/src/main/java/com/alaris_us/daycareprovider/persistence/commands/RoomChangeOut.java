package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class RoomChangeOut extends DaycareDataCommand<Member> {

	private final Member mToCheckout;
	private final Facility mAtLocation;

	public RoomChangeOut(Member toCheckout, Facility atLocation, PersistenceLayerCallback<Member> callback) {

		super(callback);
		mToCheckout = toCheckout;
		mAtLocation = atLocation;
	};

	@Override
	public void executeSource() {
		// Save Record
		getDataSource().getRecordsDAO().saveRoomChangeOutRecord(mToCheckout, mAtLocation, null, SignInType.OTHER,
				mToCheckout.getPRoom() == null ? "" : mToCheckout.getPRoom().getName(),
				new DaycareOperationComplete<Record>() {

					@Override
					public void operationComplete(Record data, DaycareDataException e) {

						if (e != null) {
							RoomChangeOut.this.operationComplete(null, e);
						} else {
							RoomChangeOut.this.operationComplete(mToCheckout, null);
						}
					}
				});
	}
}
