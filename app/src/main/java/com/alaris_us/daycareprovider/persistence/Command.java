package com.alaris_us.daycareprovider.persistence;

import com.alaris_us.daycaredata.util.ConnectivityUtility;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public abstract class Command<T> {

	public interface CommandStatusListener {

		public void starting(Command<?> command);

		public void finished(Command<?> command);

		public void valueChanged(Command<?> command);

		public void exception(Command<?> command, Exception e);
	}

	private Object mDataSource;
	private Object mModel;
	private ConnectivityUtility mConnectivityUtility;

	private final PersistenceLayerCallback<T> mCallback;
	private CommandStatusListener mListener;

	private T mData;
	private Exception mException;

	protected Command() {
		this(null);
	}

	protected Command(PersistenceLayerCallback<T> callback) {

		mCallback = callback;

	}

	public void setDataSource(Object dataSource) {

		mDataSource = dataSource;

	}

	protected Object getDataSource() {
		return mDataSource;
	}

	public void setPersistentModel(Object model) {

		mModel = model;

	}

	protected Object getPersistentModel() {
		return mModel;
	}

	public T getResult() {

		return mData;
	}
	
	public void setConnectivityUtility(ConnectivityUtility connectivityUtility){
		mConnectivityUtility = connectivityUtility;
	}
	
	protected ConnectivityUtility getConnectivityUtility(){
		return mConnectivityUtility;
	}

	protected void setResult(T data) {

		mData = data;

	}

	public void setListener(CommandStatusListener lis) {

		mListener = (lis);

	}

	public final void execute() {

		statusUpdateStart();
		executeSource();
	}

	public abstract void executeSource();

	protected void handleResponse(T data) {

	}

	public void reportException(Exception e) {

		if (e == null)
			return;

		mException = e;

		if (mListener != null) {
			mListener.exception(this, e);
		}

	}

	protected void statusChanged() {

		if (mListener != null) {
			mListener.valueChanged(this);
		}

	}

	protected void statusUpdateStart() {

		if (mListener != null) {
			mListener.starting(this);
		}

	}

	protected void statusUpdateComplete() {

		if (mListener != null) {
			mListener.finished(this);
		}

		if (mCallback != null)
			mCallback.done(mData, mException);

	}
}
