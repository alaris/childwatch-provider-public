package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class SendSMS extends DaycareDataCommand<String> {

	private final Member mMember;
	private final String mMessage;

	public SendSMS(Member member, String message) {

		this(member, message, null);
	}

	public SendSMS(Member member, String message, PersistenceLayerCallback<String> callback) {

		super(callback);
		mMember = member;
		mMessage = message;
	};

	@Override
	public void executeSource() {

		getDataSource().getMembersDAO().sendSMS(mMember, mMessage, this);
	}
}
