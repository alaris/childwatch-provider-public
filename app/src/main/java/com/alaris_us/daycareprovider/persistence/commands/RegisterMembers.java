package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.dao.FamilyAssociations.FamilyType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.FamilyAssociation;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class RegisterMembers extends DaycareDataCommand<List<Member>> {

	private final List<Member> mMembersToRegister;
	private Family mBelongsTo;
	private Family mNewFam;

	public RegisterMembers(Member memberToRegister, Family belongsTo, PersistenceLayerCallback<List<Member>> callback) {

		super(callback);
		mMembersToRegister = new ArrayList<Member>();
		mMembersToRegister.add(memberToRegister);
		mBelongsTo = belongsTo;
	};

	public RegisterMembers(List<Member> membersToRegister, Family belongsTo,
			PersistenceLayerCallback<List<Member>> callback) {

		super(callback);
		mMembersToRegister = membersToRegister;
		mBelongsTo = belongsTo;
	};

	@Override
	public void executeSource() {

		if (mBelongsTo == null) {
			/* Create Family */
			Family newFam = getDataSource().getFamiliesDAO().getEmptyTO();
			newFam.setName(findCommonLastName(mMembersToRegister).toUpperCase(Locale.US));
			newFam.setEmergencySkips(3);
			getDataSource().getFamiliesDAO().update(newFam, new DaycareOperationComplete<Family>() {

				@Override
				public void operationComplete(Family data, DaycareDataException e) {

					mNewFam = data;
					List<FamilyAssociation> familyAssociations = new ArrayList<FamilyAssociation>();
					for (Member member : mMembersToRegister) {
						mBelongsTo = data;
						FamilyAssociation fa = getDataSource().getFamilyAssociationsDAO().getEmptyTO();
						fa.setPMember(member);
						fa.setPFamily(mNewFam);
						fa.setFamilyType(FamilyType.PRIMARY);
						familyAssociations.add(fa);
					}

					getDataSource().getFamilyAssociationsDAO().createAll(familyAssociations,
							new SetUpFamilyAssociationCommandListener());
				}
			});
		} else {
			List<FamilyAssociation> familyAssociations = new ArrayList<FamilyAssociation>();
			for (Member member : mMembersToRegister) {
				FamilyAssociation fa = getDataSource().getFamilyAssociationsDAO().getEmptyTO();
				fa.setPMember(member);
				fa.setPFamily(mBelongsTo);
				fa.setFamilyType(FamilyType.PRIMARY);
				familyAssociations.add(fa);
			}
			getDataSource().getFamilyAssociationsDAO().createAll(familyAssociations,
					new SetUpFamilyAssociationToExistingFamilyCommandListener());

		}
	}

	private String findCommonLastName(List<Member> members) {

		String commonLastName = "";
		Map<String, Integer> map = new HashMap<String, Integer>();
		int count = 1;
		int max = 1;
		for (Member member : members) {
			String lastname = member.getLastName();
			if (map.containsKey(lastname)) {
				count = map.get(lastname) + 1;
				if (count > max)
					max = count;
				map.put(lastname, count);
			} else {
				map.put(lastname, 1);
			}
		}
		for (Entry<String, Integer> entry : map.entrySet()) {
			if (max == entry.getValue()) {
				commonLastName = entry.getKey();
				break;
			}
		}
		return commonLastName;
	}

	private class SetUpFamilyAssociationCommandListener implements DaycareOperationComplete<List<FamilyAssociation>> {

		public SetUpFamilyAssociationCommandListener() {
		}

		@Override
		public void operationComplete(List<FamilyAssociation> arg0, DaycareDataException arg1) {
			for (Member m : mMembersToRegister) {
				m.setPFamily(mNewFam);
			}
			getDataSource().getMembersDAO().updateAll(mMembersToRegister, RegisterMembers.this);
		}
	}

	private class SetUpFamilyAssociationToExistingFamilyCommandListener
			implements DaycareOperationComplete<List<FamilyAssociation>> {

		public SetUpFamilyAssociationToExistingFamilyCommandListener() {
		}

		@Override
		public void operationComplete(List<FamilyAssociation> arg0, DaycareDataException arg1) {
			for (Member m : mMembersToRegister) {
				m.setPFamily(mBelongsTo);
				// m.setActive(true);
				// m.setIsValidMembership(true);
			}
			getDataSource().getMembersDAO().updateAll(mMembersToRegister, RegisterMembers.this);
		}
	}
}
