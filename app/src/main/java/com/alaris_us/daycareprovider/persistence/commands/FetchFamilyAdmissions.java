package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchFamilyAdmissions extends DaycareDataCommand<List<Admission>> {

	private final Family mFamily;

	public FetchFamilyAdmissions(Family family) {

		this(family, null);
	}

	public FetchFamilyAdmissions(Family family, PersistenceLayerCallback<List<Admission>> callback) {

		super(callback);
		mFamily = family;
	}

	@Override
	public void executeSource() {

		getDataSource().getAdmissionsDAO().fetchFamilyAdmissions(mFamily, this);
	}
}
