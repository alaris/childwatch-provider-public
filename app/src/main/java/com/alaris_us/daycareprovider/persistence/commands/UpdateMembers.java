package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class UpdateMembers extends DaycareDataCommand<List<Member>> {

	private final List<Member> mMembers;

	public UpdateMembers(List<Member> members) {
		this(members, null);
	}

	public UpdateMembers(List<Member> members, PersistenceLayerCallback<List<Member>> callback) {
		super(callback);
		mMembers = members;
	}

	@Override
	public void executeSource() {
		getDataSource().getMembersDAO().updateAll(mMembers, this);
	}
}
