package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;

public class FetchAvailableCubbies extends DaycareDataCommand<List<Cubby>> {

	Facility m_facility;

	public FetchAvailableCubbies(Facility facility) {

		this(facility, null);
	}

	public FetchAvailableCubbies(Facility facility, PersistenceLayerCallback<List<Cubby>> callback) {
		super(callback);
		m_facility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getCubbiesDAO().listAvailableCubbies(m_facility, this);

	}

	@Override
	public void handleResponse(List<Cubby> data) {
		DaycareHelpers.sortCubbyList(data);
		((PersistenceData) getPersistentModel()).setAvailableCubbies(data);
	}
}