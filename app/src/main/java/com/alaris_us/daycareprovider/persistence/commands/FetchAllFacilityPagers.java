package com.alaris_us.daycareprovider.persistence.commands;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.comparators.ComparatorChain;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycareprovider.comparators.PagerNameComparator;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchAllFacilityPagers extends DaycareDataCommand<List<Pager>> {

	private final Facility mFacility;

	public FetchAllFacilityPagers(Facility facility) {

		this(facility, null);
	}

	public FetchAllFacilityPagers(Facility facility, PersistenceLayerCallback<List<Pager>> callback) {

		super(callback);
		mFacility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getPagersDAO().listAllFacilityPagers(mFacility, this);
	}

	@Override
	protected void handleResponse(List<Pager> data) {

		ComparatorChain<Pager> chain = new ComparatorChain<Pager>();
		chain.addComparator(new PagerNameComparator());
		Collections.sort(data, chain);

		((PersistenceData) getPersistentModel()).setAllFacilityPagers(data);
	}
}
