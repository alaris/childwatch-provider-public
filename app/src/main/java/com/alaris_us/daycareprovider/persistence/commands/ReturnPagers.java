package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class ReturnPagers extends DaycareDataCommand<List<Pager>> {

	private final List<Member> m_children;

	public ReturnPagers(List<Member> children) {
		this(children, null);
	};

	public ReturnPagers(List<Member> children, PersistenceLayerCallback<List<Pager>> callback) {
		super(callback);
		m_children = children;
	}

	@Override
	public void executeSource() {
		getDataSource().getPagersDAO().returnPagers(m_children, this);
	}

	@Override
	public void handleResponse(List<Pager> pagers) {

	}

}