package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;

import android.os.Handler;
import android.util.Log;

public class CheckOutUser extends DaycareDataCommand<Member> {

	private final Member mToCheckout;
	private final Facility mAtLocation;
	private final List<WorkoutArea> mWorkoutAreas;
	private final Member mParent;

	public CheckOutUser(Member toCheckout, Facility atLocation, List<WorkoutArea> workoutAreas,
			PersistenceLayerCallback<Member> callback) {

		super(callback);
		mToCheckout = toCheckout;
		mAtLocation = atLocation;
		mWorkoutAreas = workoutAreas;
		mParent = mToCheckout.getPCheckInBy();
	}

	@Override
	public void executeSource() {

		// Calculate time remaining
		final int minutesUsed = DaycareHelpers.getMinutesToNow(mToCheckout.getLastCheckIn());
		final int minutesLeft = mToCheckout.getMinutesLeft().intValue() - minutesUsed;
		mToCheckout.setMinutesLeft(minutesLeft);

		// Calculate weekly time remaining
		if (mAtLocation.getWeeklyTimeLimit() != null && mAtLocation.getWeeklyTimeLimit().intValue() != 0) {
			if (mToCheckout.getInProgram() != null) {
				if (mToCheckout.getInProgram().getUsesWeeklyMinutes()) {
					final int weeklyMinutesLeft = mToCheckout.getWeeklyMinutesLeft().intValue() - minutesUsed;
					mToCheckout.setWeeklyMinutesLeft(weeklyMinutesLeft);
				} 
			} else {
				final int weeklyMinutesLeft = mToCheckout.getWeeklyMinutesLeft().intValue() - minutesUsed;
				mToCheckout.setWeeklyMinutesLeft(weeklyMinutesLeft);
			}
		}

		getDataSource().getMembersDAO().checkOutMember(mToCheckout, new CheckoutMemberCb());

	}

	private class ReturnCubbiesCb implements DaycareOperationComplete<List<Cubby>> {

		@Override
		public void operationComplete(List<Cubby> arg0, DaycareDataException arg1) {
			Log.e(this.getClass().getSimpleName(), "DONE");
		}

	}

	private class ReturnPagersCb implements DaycareOperationComplete<List<Pager>> {

		@Override
		public void operationComplete(List<Pager> arg0, DaycareDataException arg1) {
			Log.e(this.getClass().getSimpleName(), "DONE");
		}

	}

	private class FindIncidentsCb implements DaycareOperationComplete<List<Incident>> {

		@Override
		public void operationComplete(List<Incident> data, DaycareDataException e) {
			Log.e(this.getClass().getSimpleName(), "DONE");
			if (data != null && !data.isEmpty()) {
				for (Incident incident : data)
					incident.setAcknowledged(true);
				getDataSource().getIncidentsDAO().updateAll(data, new UpdateIncidentsCb());
			}
		}

	}

	private class UpdateIncidentsCb implements DaycareOperationComplete<List<Incident>> {

		@Override
		public void operationComplete(List<Incident> arg0, DaycareDataException arg1) {
			Log.e(this.getClass().getSimpleName(), "DONE");
		}

	}

	private class CheckoutMemberCb implements DaycareOperationComplete<Member> {

		@Override
		public void operationComplete(Member arg0, DaycareDataException e) {
			Log.e(this.getClass().getSimpleName(), "DONE");
			CheckOutUser.this.operationComplete(arg0, e);

			if (e == null) {
				//Delay all requests by a second so operationComplete can happen and close the popup before they fire
				Handler h = new Handler();
				h.postDelayed(new Runnable() {
					@Override
					public void run() {
						getDataSource().getRecordsDAO().saveCheckOutRecord(mToCheckout, mAtLocation, mToCheckout.getPCheckInBy(),
								SignInType.OTHER,
								mToCheckout.getPRoom() == null ? "" : mToCheckout.getPRoom().getName(),
								new SaveCheckoutRecordCb());
						
						// if (m_parent.getWasOverridden())
						mParent.setWasOverridden(false);
						getDataSource().getMembersDAO().removeWorkoutAreasNoPersist(mParent, mWorkoutAreas);
						getDataSource().getMembersDAO().update(mParent, new CheckoutParentCb());
						
						getDataSource().getCubbiesDAO().returnCubbies(mToCheckout, new ReturnCubbiesCb());

						getDataSource().getPagersDAO().returnPagers(mToCheckout, new ReturnPagersCb());

						getDataSource().getIncidentsDAO().findIncidents(mToCheckout, new FindIncidentsCb());

					}
				}, 1000);
			}

		}

	}

	private class SaveCheckoutRecordCb implements DaycareOperationComplete<Record> {

		@Override
		public void operationComplete(Record arg0, DaycareDataException arg1) {
			Log.e(this.getClass().getSimpleName(), "DONE");
		}

	}

	private class CheckoutParentCb implements DaycareOperationComplete<Member> {
		@Override
		public void operationComplete(Member arg0, DaycareDataException e) {
			Log.e(this.getClass().getSimpleName(), "DONE");
		}
	}

}
