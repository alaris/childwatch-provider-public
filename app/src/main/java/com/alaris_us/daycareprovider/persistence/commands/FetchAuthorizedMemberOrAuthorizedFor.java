package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Authorization;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchAuthorizedMemberOrAuthorizedFor extends DaycareDataCommand<List<Authorization>> {

	private final Member mMember;
	private final boolean mIsAdult;

	public FetchAuthorizedMemberOrAuthorizedFor(Member member, boolean isAdult) {

		this(member, isAdult, null);
	}

	public FetchAuthorizedMemberOrAuthorizedFor(Member member, boolean isAdult,
			PersistenceLayerCallback<List<Authorization>> callback) {
		super(callback);
		mMember = member;
		mIsAdult = isAdult;
	}

	@Override
	public void executeSource() {
		if (mIsAdult)
			getDataSource().getAuthorizationsDAO().fetchAuthorizedChildren(mMember, this);
		else
			getDataSource().getAuthorizationsDAO().fetchAuthorizedMember(mMember, this);
	}

}
