package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchMembersCheckedInBy extends DaycareDataCommand<List<Member>> {

	Member mMember;

	public FetchMembersCheckedInBy(Member member, PersistenceLayerCallback<List<Member>> callback) {

		super(callback);
		mMember = member;
	}

	@Override
	public void executeSource() {

		getDataSource().getMembersDAO().listMembersCheckedInBy(mMember, this);
	}
}
