package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.User;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class RegisterTablet extends DaycareDataCommand<User> {

	private final String mUsername, mPassword;

	public RegisterTablet(String username, String password) {

		this(username, password, null);
	};

	public RegisterTablet(String username, String password, PersistenceLayerCallback<User> callback) {

		super(callback);
		mUsername = username;
		mPassword = password;
	}

	@Override
	public void executeSource() {

		getDataSource().getUsersDAO().registerUser(mUsername, mPassword, this);

	}

	@Override
	public void handleResponse(User data) {

		((PersistenceData) getPersistentModel()).setUser(data);

	}

}
