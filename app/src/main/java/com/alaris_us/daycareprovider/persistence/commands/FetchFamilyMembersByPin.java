package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchFamilyMembersByPin extends DaycareDataCommand<List<Member>> {

	private final String mPin;

	public FetchFamilyMembersByPin(String pin, PersistenceLayerCallback<List<Member>> callback) {

		super(callback);
		mPin = pin;
	}

	@Override
	public void executeSource() {

		final List<Member> familyList = new ArrayList<Member>();
		getDataSource().getMembersDAO().listMembersByPin(mPin, new DaycareOperationComplete<List<Member>>() {

			@Override
			public void operationComplete(List<Member> data, DaycareDataException e) {

				reportException(e);
				if (data.size() > 0) {
					// Pin found. Get first member to retrieve children.
					// Relation identical for all parents.
					familyList.addAll(data);
					getDataSource().getFamiliesDAO().listFamilyMembers(data.get(0),
							new DaycareOperationComplete<List<Member>>() {

						@Override
						public void operationComplete(List<Member> data, DaycareDataException e) {

							reportException(e);
							familyList.clear();
							familyList.addAll(data);
							setResult(familyList);
							statusUpdateComplete();
						}
					});
				} else {
					// Pin unused. Return empty list
					setResult(familyList);
					statusUpdateComplete();
				}
			}
		});
	}
}
