package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycareprovider.persistence.DaxkoCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.externaldata.data.PhoneNumber;

public class DaxkoLookupByPhone extends DaxkoCommand<List<com.alaris_us.externaldata.to.Member>> {

	private final String mAreaCode;
	private final String mNumber;

	public DaxkoLookupByPhone(String areaCode, String number,
			PersistenceLayerCallback<List<com.alaris_us.externaldata.to.Member>> callback) {

		super(callback);
		mAreaCode = areaCode;
		mNumber = number;

	}

	@Override
	public void executeSource() {

		PhoneNumber pin = PhoneNumber.getNewInstance(mAreaCode + mNumber);
		getDataSource().findMembers(pin, this);

	}

}