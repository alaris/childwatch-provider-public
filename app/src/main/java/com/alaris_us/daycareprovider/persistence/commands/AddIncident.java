package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class AddIncident extends DaycareDataCommand<Incident> {

	private final Member mMember;
	private final Facility mFacility;
	private final String mMessage;

	public AddIncident(Member member, Facility facility, String message) {

		this(member, facility, message, null);
	}

	public AddIncident(Member member, Facility facility, String message, PersistenceLayerCallback<Incident> callback) {

		super(callback);
		mMember = member;
		mFacility = facility;
		mMessage = message;
	};

	@Override
	public void executeSource() {
		getDataSource().getIncidentsDAO().addIncident(mMember, mFacility, mMessage, this);
	}
}
