package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class CheckInUser extends DaycareDataCommand<Member> {

	private final Member mToCheckout;
	private final Facility mAtLocation;
	private final boolean mIsSoftCheckIn;

	public CheckInUser(Member toCheckout, Facility atLocation, boolean isSoftCheckIn,
			PersistenceLayerCallback<Member> callback) {

		super(callback);
		mToCheckout = toCheckout;
		mAtLocation = atLocation;
		mIsSoftCheckIn = isSoftCheckIn;
	};

	@Override
	public void executeSource() {

		if (mIsSoftCheckIn) {
			// Save Record
			getDataSource().getRecordsDAO().saveCheckInEvent(mToCheckout, mAtLocation, mToCheckout.getPCheckInBy(),
					SignInType.OTHER, null, null, new DaycareOperationComplete<Record>() {

						@Override
						public void operationComplete(Record data, DaycareDataException e) {

							if (e != null) {
								CheckInUser.this.operationComplete(null, e);
							} else {
								CheckInUser.this.operationComplete(mToCheckout, null);
							}
						}
					});
		}
	}
}
