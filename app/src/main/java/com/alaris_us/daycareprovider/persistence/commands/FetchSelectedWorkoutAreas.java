package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchSelectedWorkoutAreas extends DaycareDataCommand<List<WorkoutArea>> {

	private final Facility mFacility;

	public FetchSelectedWorkoutAreas(Facility facility) {

		this(facility, null);
	}

	protected FetchSelectedWorkoutAreas(Facility facility, PersistenceLayerCallback<List<WorkoutArea>> callback) {

		super(callback);
		mFacility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getFacilitiesDAO().listSelectedWorkoutAreas(mFacility, this);

	}

	@Override
	public void handleResponse(List<WorkoutArea> data) {

		// TODO: Remove cast
		((PersistenceData) getPersistentModel()).setSelectedWorkoutAreas(data);

	}

}
