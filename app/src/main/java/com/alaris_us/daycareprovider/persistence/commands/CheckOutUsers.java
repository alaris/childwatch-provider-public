package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Program;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;
import com.alaris_us.daycareprovider.utils.DaycareHelpers;

import android.os.Handler;
import android.util.Log;

public class CheckOutUsers extends DaycareDataCommand<List<Member>> {

	private final List<Member> mToCheckout;
	private final Facility mAtLocation;
	private final List<WorkoutArea> mWorkoutAreas;
	private final List<Member> mParents;
	private final Map<Member, Room> mRooms;
	private List<MemberAndCubbies> mCheckedInMembers;
	private final Map<Member, Program> mPrograms;
	private Admission mFamilyAdmision;
	private Family mFamilyToCompare;
	private HashMap<Admission, Number> mAdmissionUsage;

	public CheckOutUsers(List<Member> toCheckout, Facility atLocation, List<WorkoutArea> workoutAreas, HashMap<Admission, Number> admissionUsage,
			PersistenceLayerCallback<List<Member>> callback) {
		super(callback);
		mToCheckout = toCheckout;
		mAtLocation = atLocation;
		mWorkoutAreas = workoutAreas;
		mParents = new ArrayList<Member>();
		mRooms = new HashMap<Member, Room>();
		mPrograms = new HashMap<Member, Program>();
		for (Member member : mToCheckout) {
			mParents.add(member.getPCheckInBy());
			mRooms.put(member, member.getPRoom());
			mPrograms.put(member, member.getInProgram());
		}
		mCheckedInMembers = new ArrayList<MemberAndCubbies>();
		mAdmissionUsage = admissionUsage;
	}

	public CheckOutUsers(List<Member> toCheckout, Facility atLocation, List<WorkoutArea> workoutAreas,
						 PersistenceLayerCallback<List<Member>> callback) {
		super(callback);
		mToCheckout = toCheckout;
		mAtLocation = atLocation;
		mWorkoutAreas = workoutAreas;
		mParents = new ArrayList<Member>();
		mRooms = new HashMap<Member, Room>();
		mPrograms = new HashMap<Member, Program>();
		for (Member member : mToCheckout) {
			mParents.add(member.getPCheckInBy());
			mRooms.put(member, member.getPRoom());
			mPrograms.put(member, member.getInProgram());
		}
		mCheckedInMembers = new ArrayList<MemberAndCubbies>();
	}

	@Override
	public void executeSource() {
		List<Family> uniqueFamilies = new ArrayList<>();
		List<Admission> admissionsToSave = new ArrayList<>();

		for (Member member : mToCheckout) {
			if (!uniqueFamilies.contains(member.getPFamily()))
				uniqueFamilies.add(member.getPFamily());
		}

		for (Member member : mToCheckout) {
			// Calculate time remaining
			final int minutesUsed = DaycareHelpers.getMinutesToNow(member.getLastCheckIn());
			final int minutesLeft = member.getMinutesLeft().intValue() - minutesUsed;
			member.setMinutesLeft(minutesLeft);

			// Calculate weekly time remaining
			if (mAtLocation.getWeeklyTimeLimit() != null && mAtLocation.getWeeklyTimeLimit().intValue() != 0) {
				if (member.getInProgram() != null) {
					if (member.getInProgram().getUsesWeeklyMinutes()) {
						final int weeklyMinutesLeft = member.getWeeklyMinutesLeft().intValue() - minutesUsed;
						member.setWeeklyMinutesLeft(weeklyMinutesLeft);
					}
				} else {
					final int weeklyMinutesLeft = member.getWeeklyMinutesLeft().intValue() - minutesUsed;
					member.setWeeklyMinutesLeft(weeklyMinutesLeft);
				}
			}
			
			//Calculate DailyMax time
			if (member.getInProgram() != null) {
				if (member.getInProgram().getUsesDailyMax()) {
					int dailyTimeUsed = member.getDailyMax().intValue() - minutesUsed;
					member.setDailyMax(dailyTimeUsed);
				}
			}
		}

		// Only remove workout locations of parents if all checked in kids
		// have/are being checked out
		mCheckedInMembers = ((PersistenceData) getPersistentModel()).getCheckedInMembers();
		for (Member parent : mParents) {
			int totalKidsCheckedIn = 0;
			int kidsToCheckOut = 0;
			for (MemberAndCubbies mc : mCheckedInMembers) {
				if (mc.getMember().getPCheckInBy().equals(parent)) {
					totalKidsCheckedIn++;
				}
			}
			for (Member m : mToCheckout) {
				if (m.getPCheckInBy().equals(parent))
					kidsToCheckOut++;
			}

			if (totalKidsCheckedIn == kidsToCheckOut) {
				parent.setWasOverridden(false);
				getDataSource().getMembersDAO().removeWorkoutAreasNoPersist(parent, mWorkoutAreas);
			}
		}

		getDataSource().getMembersDAO().checkOutMembers(mToCheckout, new CheckoutMemberCb());

	}

	@Override
	protected void handleResponse(List<Member> data) {
		List<MemberAndCubbies> checkedInMembers = new ArrayList<MemberAndCubbies>(
				((PersistenceData) getPersistentModel()).getCheckedInMembers());
		for (Member m : data) {
			Iterator<MemberAndCubbies> it = checkedInMembers.iterator();
			while (it.hasNext()) {
				MemberAndCubbies mem = it.next();
				if (mem.getMember().equals(m)) {
					it.remove();
					break;
				}
			}
		}
		((PersistenceData) getPersistentModel()).setCheckedInMembers(checkedInMembers);

		super.handleResponse(data);
	}

	private class ReturnCubbiesCb implements DaycareOperationComplete<List<Cubby>> {

		@Override
		public void operationComplete(List<Cubby> arg0, DaycareDataException arg1) {
			Log.e(this.getClass().getSimpleName(), "DONE");
		}

	}

	private class ReturnPagersCb implements DaycareOperationComplete<List<Pager>> {

		@Override
		public void operationComplete(List<Pager> arg0, DaycareDataException arg1) {
			Log.e(this.getClass().getSimpleName(), "DONE");
		}

	}

	private class FindIncidentsCb implements DaycareOperationComplete<List<Incident>> {

		@Override
		public void operationComplete(List<Incident> data, DaycareDataException e) {
			Log.e(this.getClass().getSimpleName(), "DONE");
			if (data != null && !data.isEmpty()) {
				for (Incident incident : data)
					incident.setAcknowledged(true);
				getDataSource().getIncidentsDAO().updateAll(data, new UpdateIncidentsCb());
			}
		}

	}

	private class UpdateIncidentsCb implements DaycareOperationComplete<List<Incident>> {

		@Override
		public void operationComplete(List<Incident> arg0, DaycareDataException arg1) {
			Log.e(this.getClass().getSimpleName(), "DONE");
		}

	}

	private class CheckoutMemberCb implements DaycareOperationComplete<List<Member>> {

		@Override
		public void operationComplete(List<Member> arg0, DaycareDataException e) {
			Log.e(this.getClass().getSimpleName(), "DONE");
			CheckOutUsers.this.operationComplete(arg0, e);

			if (e == null) {
				// Delay all requests by a second so operationComplete can
				// happen and close the popup before they fire
				Handler h = new Handler();
				h.postDelayed(new Runnable() {
					@Override
					public void run() {
						List<Record> records = new ArrayList<Record>();
						for (Member member : mToCheckout) {
							records.add(getDataSource().getRecordsDAO().createCheckOutRecordNoPersist(member,
									mAtLocation, member.getPCheckInBy(), SignInType.OTHER,
									mRooms.get(member) == null ? "" : mRooms.get(member).getName(), null,
									mPrograms.get(member)));
						}

						getDataSource().getRecordsDAO().updateAll(records, new SaveCheckoutRecordsCb());

						getDataSource().getMembersDAO().updateAll(mParents, new CheckoutParentCb());

						getDataSource().getCubbiesDAO().returnCubbies(mToCheckout, new ReturnCubbiesCb());

						getDataSource().getPagersDAO().returnPagers(mToCheckout, new ReturnPagersCb());

						getDataSource().getIncidentsDAO().findIncidents(mToCheckout, new FindIncidentsCb());

					}
				}, 1000);
			}

		}

	}

	private class SaveCheckoutRecordsCb implements DaycareOperationComplete<List<Record>> {

		@Override
		public void operationComplete(List<Record> arg0, DaycareDataException arg1) {
			Log.e(this.getClass().getSimpleName(), "DONE");
		}

	}

	private class CheckoutParentCb implements DaycareOperationComplete<List<Member>> {
		@Override
		public void operationComplete(List<Member> arg0, DaycareDataException e) {
			Log.e(this.getClass().getSimpleName(), "DONE");
		}
	}

	private class FacilityFamilyAdmissionCb implements DaycareOperationComplete<List<Admission>> {
		@Override
		public void operationComplete(List<Admission> arg0, DaycareDataException arg1) {
			mFamilyAdmision = arg0.get(0);
		}
	}
}
