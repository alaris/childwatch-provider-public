package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchReservedCubbiesByMember extends DaycareDataCommand<List<Cubby>> {
	Facility m_facility;
	Member m_member;

	public FetchReservedCubbiesByMember(Member member, Facility facility) {
		this(member, facility, null);
	}

	public FetchReservedCubbiesByMember(Member member, Facility facility,
			final PersistenceLayerCallback<List<Cubby>> callback) {
		super(callback);
		m_facility = facility;
		m_member = member;
	}

	@Override
	public void executeSource() {
		getDataSource().getCubbiesDAO().listReservedCubbies(m_facility, m_member, this);
	}
}