package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchMemberByMemberID extends DaycareDataCommand<Member> {

	String mMemberId;

	public FetchMemberByMemberID(String memberId, PersistenceLayerCallback<Member> callback) {

		super(callback);
		mMemberId = memberId;
	}

	@Override
	public void executeSource() {

		getDataSource().getMembersDAO().getMemberByMemberId(mMemberId, this);
	}
}
