package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchFacilityFamilyAdmissions extends DaycareDataCommand<List<Admission>> {

	private final Family mFamily;
	private final Facility mFacility;

	public FetchFacilityFamilyAdmissions(Family family, Facility facility) {

		this(family, facility, null);
	}

	public FetchFacilityFamilyAdmissions(Family family, Facility facility, PersistenceLayerCallback<List<Admission>> callback) {

		super(callback);
		mFamily = family;
		mFacility = facility;
	}

	@Override
	public void executeSource() {

		if (mFacility.getUsesAssocWideAdmissions())
			getDataSource().getAdmissionsDAO().fetchFamilyAdmissions(mFamily, this);
		else
			getDataSource().getAdmissionsDAO().fetchFacilityFamilyAdmissions(mFamily, mFacility, this);
	}
}
