package com.alaris_us.daycareprovider.persistence;

import com.alaris_us.daycareprovider.heroku.HerokuData;
import com.alaris_us.daycareprovider.heroku.HerokuRESTException;
import com.alaris_us.daycareprovider.heroku.HerokuRESTOperationComplete;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public abstract class HerokuCommand<T> extends Command<T> implements HerokuRESTOperationComplete<T> {

	public HerokuCommand() {
		super(null);
	}

	public HerokuCommand(PersistenceLayerCallback<T> callback) {
		super(callback);
	}
	
	@Override
	public void operationComplete(T data, HerokuRESTException e) {

		reportException(e);

		if (e == null) {
			setResult(data);
			handleResponse(data);
			statusChanged();
		}
		statusUpdateComplete();
	}

	@Override
	protected HerokuData getDataSource() {
		return (HerokuData) super.getDataSource();
	}

}
