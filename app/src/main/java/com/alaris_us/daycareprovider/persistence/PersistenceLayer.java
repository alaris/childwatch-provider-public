package com.alaris_us.daycareprovider.persistence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import com.alaris_us.daycaredata.DaycareData;
import com.alaris_us.daycaredata.ParseData;
import com.alaris_us.daycaredata.dao.Incidents.IncidentType;
import com.alaris_us.daycaredata.dao.Records.EventType;
import com.alaris_us.daycaredata.to.Admission;
import com.alaris_us.daycaredata.to.AdmissionBalance;
import com.alaris_us.daycaredata.to.Authorization;
import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.FamilyAssociation;
import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycaredata.to.SelfCheckIn;
import com.alaris_us.daycaredata.to.Staff;
import com.alaris_us.daycaredata.to.User;
import com.alaris_us.daycaredata.to.ValidMembership;
import com.alaris_us.daycaredata.to.WorkoutArea;
import com.alaris_us.daycaredata.util.ConnectivityUtility;
import com.alaris_us.daycareprovider.data.CheckInCount;
import com.alaris_us.daycareprovider.data.HerokuJob;
import com.alaris_us.daycareprovider.data.MemberAndCubbies;
import com.alaris_us.daycareprovider.heroku.HerokuData;
import com.alaris_us.daycareprovider.persistence.Invoker.InvokerStatusListener;
import com.alaris_us.daycareprovider.persistence.commands.AddIncident;
import com.alaris_us.daycareprovider.persistence.commands.AddSelectedWorkoutArea;
import com.alaris_us.daycareprovider.persistence.commands.ChargeAdmissions;
import com.alaris_us.daycareprovider.persistence.commands.CheckInUser;
import com.alaris_us.daycareprovider.persistence.commands.CheckOutParent;
import com.alaris_us.daycareprovider.persistence.commands.CheckOutUser;
import com.alaris_us.daycareprovider.persistence.commands.CheckOutUsers;
import com.alaris_us.daycareprovider.persistence.commands.CreateSelfCheckIn;
import com.alaris_us.daycareprovider.persistence.commands.DaxkoLookupByPhone;
import com.alaris_us.daycareprovider.persistence.commands.DaxkoLookupByUnitID;
import com.alaris_us.daycareprovider.persistence.commands.DeleteAdmissions;
import com.alaris_us.daycareprovider.persistence.commands.DeleteSelfCheckIn;
import com.alaris_us.daycareprovider.persistence.commands.FetchAllFacilitiesForAssoc;
import com.alaris_us.daycareprovider.persistence.commands.FetchAllFacilityCubbies;
import com.alaris_us.daycareprovider.persistence.commands.FetchAllFacilityPagers;
import com.alaris_us.daycareprovider.persistence.commands.FetchAllFacilityRooms;
import com.alaris_us.daycareprovider.persistence.commands.FetchAllFacilityStaff;
import com.alaris_us.daycareprovider.persistence.commands.FetchAllMembershipsForFacility;
import com.alaris_us.daycareprovider.persistence.commands.FetchAllWorkoutAreas;
import com.alaris_us.daycareprovider.persistence.commands.FetchAuthorizedMemberOrAuthorizedFor;
import com.alaris_us.daycareprovider.persistence.commands.FetchAvailableCubbies;
import com.alaris_us.daycareprovider.persistence.commands.FetchAvailablePagers;
import com.alaris_us.daycareprovider.persistence.commands.FetchCheckedInBy;
import com.alaris_us.daycareprovider.persistence.commands.FetchCheckedInMembersIncludingCubbies;
import com.alaris_us.daycareprovider.persistence.commands.FetchExpectedAreas;
import com.alaris_us.daycareprovider.persistence.commands.FetchFacility;
import com.alaris_us.daycareprovider.persistence.commands.FetchFacilityAdmissionSettings;
import com.alaris_us.daycareprovider.persistence.commands.FetchFacilityFamilyAdmissions;
import com.alaris_us.daycareprovider.persistence.commands.FetchFacilityMessages;
import com.alaris_us.daycareprovider.persistence.commands.FetchFamilyAdmissionBalance;
import com.alaris_us.daycareprovider.persistence.commands.FetchFamilyAdmissions;
import com.alaris_us.daycareprovider.persistence.commands.FetchFamilyByMember;
import com.alaris_us.daycareprovider.persistence.commands.FetchFamilyMembersByPin;
import com.alaris_us.daycareprovider.persistence.commands.FetchIncidentHistory;
import com.alaris_us.daycareprovider.persistence.commands.FetchIncidentsByMember;
import com.alaris_us.daycareprovider.persistence.commands.FetchMemberByBarcode;
import com.alaris_us.daycareprovider.persistence.commands.FetchMemberByMemberID;
import com.alaris_us.daycareprovider.persistence.commands.FetchMemberPager;
import com.alaris_us.daycareprovider.persistence.commands.FetchMembersByFamilyObject;
import com.alaris_us.daycareprovider.persistence.commands.FetchMembersByLastName;
import com.alaris_us.daycareprovider.persistence.commands.FetchMembersByUnitID;
import com.alaris_us.daycareprovider.persistence.commands.FetchMembersCheckedInBy;
import com.alaris_us.daycareprovider.persistence.commands.FetchReportingHistory;
import com.alaris_us.daycareprovider.persistence.commands.FetchReservedCubbiesByMember;
import com.alaris_us.daycareprovider.persistence.commands.FetchReservedPagersByMember;
import com.alaris_us.daycareprovider.persistence.commands.FetchSelectedWorkoutAreas;
import com.alaris_us.daycareprovider.persistence.commands.FetchSigninHistory;
import com.alaris_us.daycareprovider.persistence.commands.FetchTodaysSignInRecords;
import com.alaris_us.daycareprovider.persistence.commands.Login;
import com.alaris_us.daycareprovider.persistence.commands.PersistAllFacilityCubbies;
import com.alaris_us.daycareprovider.persistence.commands.PersistFacilities;
import com.alaris_us.daycareprovider.persistence.commands.ReadMember;
import com.alaris_us.daycareprovider.persistence.commands.RegisterMembers;
import com.alaris_us.daycareprovider.persistence.commands.RegisterTablet;
import com.alaris_us.daycareprovider.persistence.commands.RemoveSelectedWorkoutArea;
import com.alaris_us.daycareprovider.persistence.commands.ReserveMemberCubby;
import com.alaris_us.daycareprovider.persistence.commands.ReservePager;
import com.alaris_us.daycareprovider.persistence.commands.ReservePagers;
import com.alaris_us.daycareprovider.persistence.commands.ReturnPagers;
import com.alaris_us.daycareprovider.persistence.commands.RoomChange;
import com.alaris_us.daycareprovider.persistence.commands.RoomChangeIn;
import com.alaris_us.daycareprovider.persistence.commands.RoomChangeOut;
import com.alaris_us.daycareprovider.persistence.commands.SendSMS;
import com.alaris_us.daycareprovider.persistence.commands.SetupFamilyAssociation;
import com.alaris_us.daycareprovider.persistence.commands.SyncFamilyData;
import com.alaris_us.daycareprovider.persistence.commands.CreditAdmissions;
import com.alaris_us.daycareprovider.persistence.commands.UpdateIncident;
import com.alaris_us.daycareprovider.persistence.commands.UpdateMember;
import com.alaris_us.daycareprovider.persistence.commands.UpdateMembers;
import com.alaris_us.daycareprovider.persistence.commands.UpdateStaff;
import com.alaris_us.daycareprovider.view.TabWithIcons.SortDirection;
import com.alaris_us.daycareprovider_dev.R;
import com.alaris_us.externaldata.ExternalData;
import com.alaris_us.externaldata.ExternalDataOperationComplete;
import com.alaris_us.externaldata.data.PhoneNumber;
import com.alaris_us.externaldata.daxko.DaxkoExternalData;

import android.content.Context;
import android.provider.Settings.Secure;

public class PersistenceLayer {

	private static final int ADULT_AGE_CUTOFF = 12;

	public interface PersistenceLayerListener {

		public void updating();

		public void updated();

		public void exception(Exception e);
	}

	public interface PersistenceLayerCallback<T> {

		public void done(T data, Exception e);
	}

	class ListenerRelay implements InvokerStatusListener {

		@Override
		public void started() {

			for (PersistenceLayerListener pl : m_listeners) {
				if (pl != null)
					pl.updating();
			}
		}

		@Override
		public void finished() {

			if ((mDaxkoCommandInvoker.getSize() > 0) || (mDaycareCommandInvoker.getSize() > 0))
				return;
			for (PersistenceLayerListener pl : m_listeners) {
				if (pl != null)
					pl.updated();
			}
		}

		@Override
		public void exception(Exception e) {

			for (PersistenceLayerListener pl : m_listeners) {
				if (pl != null)
					pl.exception(e);
			}
		}
	}

	/*
	 * public void initializeDaxko(String clientId) {
	 * 
	 * mExternalDataSource.initialize(mDaxkoUsername, mDaxkoPassword, clientId, true, true); }
	 */

	public PersistenceData getPersistenceData() {

		return mPersistenceData;
	}

	public String getAndroidId() {

		return mAndroidID;
	}

	public void addPersistanceLayerListener(PersistenceLayerListener lis) {

		m_listeners.add(lis);
	}

	public void removePersistanceLayerListener(PersistenceLayerListener lis) {

		m_listeners.remove(lis);
	}

	public Member getNewEmptyMember() {
		return mDaycareDataSource.getMembersDAO().getEmptyTO();
	}

	public Room getNewEmptyRoom() {
		return mDaycareDataSource.getRoomsDAO().getEmptyTO();
	}
	
	public Admission getNewEmptyAdmission() {
		return mDaycareDataSource.getAdmissionsDAO().getEmptyTO();
	}

	public SelfCheckIn getNewEmptySelfCheckIn() {
		return mDaycareDataSource.getSelfCheckInsDAO().getEmptyTO();
	}

	public void updateMember(Member member) {
		mDaycareCommandInvoker.invoke(new UpdateMember(member));
	}

	public void updateMember(Member member, PersistenceLayerCallback<Member> callback) {
		mDaycareCommandInvoker.invoke(new UpdateMember(member, callback));
	}

	public void updateMembers(List<Member> members) {
		mDaycareCommandInvoker.invoke(new UpdateMembers(members));
	}

	public void updateMembers(List<Member> members, PersistenceLayerCallback<List<Member>> callback) {
		mDaycareCommandInvoker.invoke(new UpdateMembers(members, callback));
	}
	
	public void updateStaff(List<Staff> staff) {
		List<Staff> clockedInStaff = new ArrayList<>();
		for (Staff s : staff) {
			if (s.getIsClockedIn())
				clockedInStaff.add(s);
		}
		getPersistenceData().setClockedInStaff(clockedInStaff);
		mDaycareCommandInvoker.invoke(new UpdateStaff(staff));
	}

	public void login(String appPass) {

		mDaycareCommandInvoker.invoke(new Login(mAndroidID, appPass));
	}

	public void registerUser(String appPass) {

		registerUser(mAndroidID, appPass);
	}

	public void registerUser(String username, String password) {

		mDaycareCommandInvoker.invoke(new RegisterTablet(username, password));
	}

	public void fetchByLastName(String lastName, PersistenceLayerCallback<List<Member>> callback) {

		mDaycareCommandInvoker.invoke(new FetchMembersByLastName(lastName, callback));
	}

	public void fetchByFullName(String fullName, final PersistenceLayerCallback<List<Member>> callback) {

		final String[] names = StringUtils.split(StringUtils.upperCase(fullName), " ", 2);

		// TODO Must setup error reporting
		if (names.length != 2) {
			callback.done(new ArrayList<Member>(), null);
			return;
			// callback.done(
			// null,
			// new Exception(
			// "Must provide a first name and last name seperated by a space"));
		}

		mDaycareCommandInvoker
				.invoke(new FetchMembersByLastName(names[1], new PersistenceLayerCallback<List<Member>>() {

					@Override
					public void done(List<Member> data, Exception e) {

						if (e != null) {
							callback.done(null, e);
						}

						List<Member> filteredMembers = new ArrayList<Member>();

						for (Member m : data) {
							if (m.getFirstName().equals(names[0])) {
								filteredMembers.add(m);
							}
						}

						callback.done(filteredMembers, e);
					}
				}));
	}

	public void FetchMembersCheckedInBy(Member member, PersistenceLayerCallback<List<Member>> callback) {
		mDaycareCommandInvoker.invoke(new FetchMembersCheckedInBy(member, callback));
	}

	public void fetchByMemberId(String memberId, PersistenceLayerCallback<Member> callback) {

		mDaycareCommandInvoker.invoke(new FetchMemberByMemberID(memberId, callback));
	}

	public void fetchByBarcode(String barcode, PersistenceLayerCallback<Member> callback) {

		mDaycareCommandInvoker.invoke(new FetchMemberByBarcode(barcode, callback));
	}

	public void readMember(String objectId, PersistenceLayerCallback<Member> callback) {

		mDaycareCommandInvoker.invoke(new ReadMember(objectId, callback));
	}

	public void fetchByUnitId(String unitId, PersistenceLayerCallback<List<Member>> callback) {

		mDaycareCommandInvoker.invoke(new FetchMembersByUnitID(unitId, callback));
	}

	public void fetchMembersByFamily(Family family, PersistenceLayerCallback<List<Member>> callback) {

		mDaycareCommandInvoker.invoke(new FetchMembersByFamilyObject(family, callback));
	}

	public void fetchFamilyByMember(Member member, PersistenceLayerCallback<Family> callback) {

		mDaycareCommandInvoker.invoke(new FetchFamilyByMember(member, callback));
	}

	public void fetchFacility() {

		User user = mPersistenceData.getUser();
		fetchFacility(user);
	}

	public int fetchFacilityTimeLimit() {

		return mPersistenceData.getFacilityTimeLimit();
	}

	public void fetchFacility(User user) {

		mDaycareCommandInvoker.invoke(new FetchFacility(user));
	}

	public void fetchFacilityPagers() {

		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchAllFacilityPagers(facility));
	}

	public void fetchFacilityCubbies() {

		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchAllFacilityCubbies(facility));
	}

	public void fetchFacilityRooms() {

		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchAllFacilityRooms(facility));
	}

    public void fetchFacilityStaff() {

        Facility facility = mPersistenceData.getFacility();
        mDaycareCommandInvoker.invoke(new FetchAllFacilityStaff(facility));
    }
    
    public void fetchFacilityStaff(PersistenceLayerCallback<List<Staff>> callback) {

        Facility facility = mPersistenceData.getFacility();
        mDaycareCommandInvoker.invoke(new FetchAllFacilityStaff(facility, callback));
    }

    /*public void fetchClockedInFacilityStaff() {

        Facility facility = mPersistenceData.getFacility();
        mDaycareCommandInvoker.invoke(new FetchAllFacilityStaff(facility));
    }*/

	public void fetchSMSMessages() {

		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchFacilityMessages(facility));
	}

	public void fetchAdmissionSettingsForFacility() {

		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchFacilityAdmissionSettings(facility));
	}

	public void fetchAllWorkoutLocations() {

		mDaycareCommandInvoker.invoke(new FetchAllWorkoutAreas());
	}

	public void fetchSelectedWorkoutLocations() {

		Facility facility = mPersistenceData.getFacility();
		fetchSelectedWorkoutLocations(facility);
	}

	public void fetchSelectedWorkoutLocations(Facility facility) {

		mDaycareCommandInvoker.invoke(new FetchSelectedWorkoutAreas(facility));
	}

	public void fetchCheckedInMembersAndCubbies() {

		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchCheckedInMembersIncludingCubbies(facility));
	}
	
	public void fetchCheckedInMembersAndCubbies(PersistenceLayerCallback<List<MemberAndCubbies>> cb) {

		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchCheckedInMembersIncludingCubbies(facility, cb));
	}

	public void fetchCheckedInMembersAndCubbies(SortDirection sortDirection, String tabName) {
		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchCheckedInMembersIncludingCubbies(facility, sortDirection, tabName));
	}
	
	public void fetchCheckedInMembersAndCubbies(SortDirection sortDirection, String tabName, PersistenceLayerCallback<List<MemberAndCubbies>> cb) {
		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchCheckedInMembersIncludingCubbies(facility, sortDirection, tabName, cb));
	}

	public void fetchCheckedInMembersByRoom(
			String roomObjId/* , PersistenceLayerCallback<List<List<Cubby>>>callback */) {
	}

	public void persistFacility() {

		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new PersistFacilities(facility, null));
	}

	public void addSelectedWorkoutArea(WorkoutArea area) {

		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new AddSelectedWorkoutArea(facility, area));
	}

	public void removeSelectedWorkoutArea(WorkoutArea area) {

		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new RemoveSelectedWorkoutArea(facility, area));
	}
	
	public void syncFamilyData(List<Member> members, PersistenceLayerCallback<HerokuJob> cb){
		mHerokuCommandInvoker.invoke(new SyncFamilyData(members, cb));
	}

	public void fetchSignInHistory() {
		fetchSignInHistory(null);
	}

	public void fetchSignInHistory(PersistenceLayerCallback<CheckInCount> cb) {

		Facility facility = mPersistenceData.getFacility();
		List<Room> rooms = mPersistenceData.getAllFacilityRooms();
		mHerokuCommandInvoker.invoke(new FetchSigninHistory(facility, rooms, cb));
	}
	
	public void fetchTodaysSignInRecords() {
		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchTodaysSignInRecords(facility));
	}
	
	public void fetchReportingHistory(DateTime startDate, DateTime endDate) throws IllegalArgumentException {
		if (startDate.isAfter(endDate))
			throw new IllegalArgumentException("Start Date Can't be Greater than End Date!");
		Facility facility = mPersistenceData.getFacility();
		List<EventType> eventTypes = new ArrayList<>();
		eventTypes.add(EventType.CHECK_IN);
		eventTypes.add(EventType.CHECK_OUT);
		mDaycareCommandInvoker.invoke(new FetchReportingHistory(facility, startDate, endDate, null, eventTypes, null));
	}

	public void fetchIncidentsHistory(DateTime startDate, DateTime endDate) throws IllegalArgumentException {

		if (startDate.isAfter(endDate))
			throw new IllegalArgumentException("Start Date Can't be Greater than End Date!");
		Facility facility = mPersistenceData.getFacility();
		mDaycareCommandInvoker
				.invoke(new FetchIncidentHistory(facility, startDate, endDate, IncidentType.DEFAULT, null));
	}

	// TODO: parallelize and join multiple callbacks
	public void fetchCheckedInBy(Member child, PersistenceLayerCallback<Member> callback) {

		mDaycareCommandInvoker.invoke(new FetchCheckedInBy(child, callback));
	}

	public void fetchExpectedAreas(Member member, PersistenceLayerCallback<List<WorkoutArea>> callback) {

		mDaycareCommandInvoker.invoke(new FetchExpectedAreas(member, callback));
	}

	public void fetchAllFacilityCubbies(final PersistenceLayerCallback<List<Cubby>> callback) {

		final Facility here = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new FetchAllFacilityCubbies(here, callback));
	}

	public void persistAllFacilityCubbies(List<Cubby> toPersist) {

		final Facility here = mPersistenceData.getFacility();
		mDaycareCommandInvoker.invoke(new PersistAllFacilityCubbies(here, toPersist));
	}

	public void fetchDaxkoFamilyMembers(String areaCode, String number,
			final PersistenceLayerCallback<List<com.alaris_us.externaldata.to.Member>> callback) {

		mDaxkoCommandInvoker.invoke(new DaxkoLookupByPhone(areaCode, number,
				new PersistenceLayerCallback<List<com.alaris_us.externaldata.to.Member>>() {

					@Override
					public void done(List<com.alaris_us.externaldata.to.Member> data, Exception e) {

						String unitToSearch;
						if (data != null && data.size() > 0) {
							unitToSearch = data.get(0).getUnitID();
							mDaxkoCommandInvoker.invoke(new DaxkoLookupByUnitID(unitToSearch, callback));
						} else {
							callback.done(null, null);
						}
					}

				}));
	}

	public void fetchFamilyByPin(String pin, final PersistenceLayerCallback<List<Member>> callback) {

		mDaycareCommandInvoker.invoke(new FetchFamilyMembersByPin(pin, callback));
	}

	public Member createMemberNoPerist(String memberId, String unitID, String firstName, String lastName,
			Date birthDate, String gender, String email, List<String> userPins,
			List<com.alaris_us.daycaredata.PhoneNumber> phones, Number minutesLeft, Family family, boolean isGuest,
			Number guestDaysLeft, String barcode, boolean isActive) {

		final Member member = getDaycareDataSource().getMembersDAO().getEmptyTO();
		member.setMemberID(memberId);
		member.setUnitID(unitID);
		member.setFirstName(firstName.toUpperCase(Locale.ENGLISH));
		member.setLastName(lastName.toUpperCase(Locale.ENGLISH));
		Calendar cal = Calendar.getInstance();
		cal.set(2000, Calendar.JANUARY, 1);
		member.setLastCheckIn(cal.getTime());
		member.setGender(gender);
		member.setBirthDate(birthDate);
		member.setGuest(false);
		member.setGuestDaysLeft(guestDaysLeft);
		member.setEmail(email);
		member.setIsCheckedIn(false);
		member.setPFamily(family);
		member.setUsesChildWatch(true);
		member.setBarcode(barcode);
		member.setUsesSMS(false);
		/*if (DaycareHelpers.isAdult(birthDate, ADULT_AGE_CUTOFF)) {
			// Adults have pins
			member.setUserPINs(userPins);
			if (phones != null)
				member.setPhoneNumbers(phones);
		} else {
			// Kids have minutes
			member.setMinutesLeft(fetchFacilityTimeLimit());
		}*/
		member.setUserPINs(userPins);
		if (phones != null)
			member.setPhoneNumbers(phones);
		member.setMinutesLeft(fetchFacilityTimeLimit());
		if (isGuest) {
			member.setActive(true);
			member.setMembershipType("Guest");
			member.setIsValidMembership(true);
			member.setGuest(true);
		}
		if (isActive) {
			member.setActive(true);
			member.setIsValidMembership(true);
		} else {
			member.setActive(false);
			member.setIsValidMembership(false);
		}
		return member;
	}

	public Cubby getEmptyCubby() {

		return getDaycareDataSource().getCubbiesDAO().getEmptyTO();
	}

	public void reserveAdditionalMemberCubby(Member using, PersistenceLayerCallback<List<Cubby>> cb) {

		Facility atLocation = getPersistenceData().getFacility();
		mDaycareCommandInvoker.invoke(new ReserveMemberCubby(atLocation, using, cb));
	}

	public void registerMember(Member toRegister, Family belongsTo, final PersistenceLayerCallback<Member> callback) {

		mDaycareCommandInvoker
				.invoke(new RegisterMembers(toRegister, belongsTo, new PersistenceLayerCallback<List<Member>>() {

					@Override
					public void done(List<Member> data, Exception e) {

						if (e == null && data.size() > 0) {
							callback.done(data.get(0), e);
						} else {
							callback.done(null, e);
						}
					}
				}));
	}

	public void registerMembers(List<Member> toRegister, Family belongsTo,
			final PersistenceLayerCallback<List<Member>> callback) {

		mDaycareCommandInvoker.invoke(new RegisterMembers(toRegister, belongsTo, callback));
	}

	public void checkInMember(Member toCheckout, boolean isSoftCheckIn,
			final PersistenceLayerCallback<Member> callback) {

		Facility atLocation = getPersistenceData().getFacility();
		mDaycareCommandInvoker.invoke(new CheckInUser(toCheckout, atLocation, isSoftCheckIn, callback));
	}

	public void checkOutMember(Member toCheckout, final PersistenceLayerCallback<Member> callback) {

		Facility atLocation = getPersistenceData().getFacility();
		List<WorkoutArea> workoutAreas = getPersistenceData().getAllWorkoutAreas();
		mDaycareCommandInvoker.invoke(new CheckOutUser(toCheckout, atLocation, workoutAreas, callback));
	}

	public void checkOutMembers(List<Member> toCheckout, final PersistenceLayerCallback<List<Member>> callback) {
		Facility atLocation = getPersistenceData().getFacility();
		List<WorkoutArea> workoutAreas = getPersistenceData().getAllWorkoutAreas();
		HashMap<Admission, Number> admissionUsage = getPersistenceData().getAdmissionUsage();
		mDaycareCommandInvoker.invoke(new CheckOutUsers(toCheckout, atLocation, workoutAreas, admissionUsage, callback));
	}

	public void roomChangeIn(Member toCheckout, final PersistenceLayerCallback<Member> callback) {

		Facility atLocation = getPersistenceData().getFacility();
		mDaycareCommandInvoker.invoke(new RoomChangeIn(toCheckout, atLocation, callback));
	}

	public void roomChangeOut(Member toCheckout, final PersistenceLayerCallback<Member> callback) {

		Facility atLocation = getPersistenceData().getFacility();
		mDaycareCommandInvoker.invoke(new RoomChangeOut(toCheckout, atLocation, callback));
	}

	public void roomChange(Member toCheckout, Room newRoom, final PersistenceLayerCallback<List<Member>> callback) {
		roomChange(Arrays.asList(toCheckout), newRoom, callback);
	}

	public void roomChange(List<Member> toCheckout, Room newRoom,
			final PersistenceLayerCallback<List<Member>> callback) {
		Facility atLocation = getPersistenceData().getFacility();
		mDaycareCommandInvoker.invoke(new RoomChange(toCheckout, newRoom, atLocation, callback));
	}

	public void checkOutParent(Member parent) {

		mDaycareCommandInvoker.invoke(new CheckOutParent(parent, getPersistenceData().getAllWorkoutAreas()));
	}

	public void addIncident(Member member, Facility facility, String message,
			PersistenceLayerCallback<Incident> callback) {

		mDaycareCommandInvoker.invoke(new AddIncident(member, facility, message, callback));
	}

	public void updateIncident(Incident incident) {

		mDaycareCommandInvoker.invoke(new UpdateIncident(incident));
	}

	public DaycareData getDaycareDataSource() {

		return mDaycareDataSource;
	}

	public ExternalData getExternalDataSource() {

		return mExternalDataSource;
	}
	
	public HerokuData getHerokuDataSource(){
		return mHerokuDataSource;
	}

	public void findMembersByPhone(String mAreaCode, String mNumber, String type,
			ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>> cb) {
		PhoneNumber pin = PhoneNumber.getNewInstance(mAreaCode + mNumber);
		mExternalDataSource.findMembers(pin,
				(ExternalDataOperationComplete<List<com.alaris_us.externaldata.to.Member>>) cb);
	}

	/*
	 * public void getUnitMembers(String mUnitId, ExternalDataOperationComplete<UnitInfo> cb) {
	 * mExternalDataSource.getUnitMembers(mUnitId, (ExternalDataOperationComplete<UnitInfo>) cb); }
	 */

	public void sendSMS(Member member, String message, PersistenceLayerCallback<String> callback) {

		mDaycareCommandInvoker.invoke(new SendSMS(member, message, callback));
	}

	public void setupFamilyAssociation(Member member, Family family) {
		setupFamilyAssociation(member, family, null);
	}

	public void setupFamilyAssociation(Member member, Family family,
			PersistenceLayerCallback<FamilyAssociation> callback) {
		mDaycareCommandInvoker.invoke(new SetupFamilyAssociation(member, family, callback));
	}

	public void fetchMembershipsForFacility(Facility facility,
			PersistenceLayerCallback<List<ValidMembership>> callback) {

		mDaycareCommandInvoker.invoke(new FetchAllMembershipsForFacility(facility, callback));
	}

	public void fetchAuthorizedMemberOrAuthorizedFor(Member member, boolean isAdult,
			PersistenceLayerCallback<List<Authorization>> callback) {
		mDaycareCommandInvoker.invoke(new FetchAuthorizedMemberOrAuthorizedFor(member, isAdult, callback));
	}

	public void fetchAvailableCubbies(PersistenceLayerCallback<List<Cubby>> callback) {
		Facility facility = mPersistenceData.getFacility();
		fetchAvailableCubbies(facility, callback);
	}

	public void fetchAvailableCubbies(Facility facility, PersistenceLayerCallback<List<Cubby>> callback) {
		mDaycareCommandInvoker.invoke(new FetchAvailableCubbies(facility, callback));
	}

	public void fetchAvailablePagers(PersistenceLayerCallback<List<Pager>> callback) {
		Facility facility = mPersistenceData.getFacility();
		fetchAvailablePagers(facility, callback);
	}

	public void fetchAvailablePagers(Facility facility, PersistenceLayerCallback<List<Pager>> callback) {
		mDaycareCommandInvoker.invoke(new FetchAvailablePagers(facility, callback));
	}

	public void fetchReservedPagers(Member usedBy, PersistenceLayerCallback<List<Pager>> callback) {
		Facility facility = mPersistenceData.getFacility();
		fetchReservedPagers(facility, usedBy, callback);
	}

	public void fetchReservedPagers(Facility facility, Member usedBy, PersistenceLayerCallback<List<Pager>> callback) {
		mDaycareCommandInvoker.invoke(new FetchMemberPager(facility, usedBy, callback));
	}

	public void reservePager(Pager pager, Member child, Family family, Member proxy,
			PersistenceLayerCallback<List<Pager>> callback) {
		Facility facility = mPersistenceData.getFacility();
		reservePager(pager, child, family, facility, proxy, callback);
	}

	public void reservePager(Pager pager, Member child, Family family, Facility facility, Member proxy,
			PersistenceLayerCallback<List<Pager>> callback) {
		mDaycareCommandInvoker.invoke(new ReservePager(pager, child, family, facility, proxy, callback));
	}

	public void reservePagers(List<Pager> pagers, List<Member> children, Family family, Member proxy,
			PersistenceLayerCallback<List<Pager>> callback) {
		Facility facility = mPersistenceData.getFacility();
		reservePagers(pagers, children, family, facility, proxy, callback);
	}

	public void reservePagers(List<Pager> pagers, List<Member> children, Family family, Facility facility, Member proxy,
			PersistenceLayerCallback<List<Pager>> callback) {
		mDaycareCommandInvoker.invoke(new ReservePagers(pagers, children, family, facility, proxy, callback));
	}

	public void returnPagers(List<Member> children, PersistenceLayerCallback<List<Pager>> callback) {
		mDaycareCommandInvoker.invoke(new ReturnPagers(children, callback));
	}

	public void fetchReservedCubbiesByMember(Member member) {
		fetchReservedCubbiesByMember(member, getPersistenceData().getFacility());
	}

	public void fetchReservedCubbiesByMember(Member member, PersistenceLayerCallback<List<Cubby>> callback) {
		fetchReservedCubbiesByMember(member, getPersistenceData().getFacility(), callback);
	}

	public void fetchReservedCubbiesByMember(Member member, Facility facility) {
		fetchReservedCubbiesByMember(member, facility, null);
	}

	public void fetchReservedCubbiesByMember(Member member, Facility facility,
			PersistenceLayerCallback<List<Cubby>> callback) {
		mDaycareCommandInvoker.invoke(new FetchReservedCubbiesByMember(member, facility, callback));
	}

	public void fetchReservedPagersByMember(Member member) {
		fetchReservedPagersByMember(member, getPersistenceData().getFacility());
	}

	public void fetchReservedPagersByMember(Member member, PersistenceLayerCallback<List<Pager>> callback) {
		fetchReservedPagersByMember(member, getPersistenceData().getFacility(), callback);
	}

	public void fetchReservedPagersByMember(Member member, Facility facility) {
		fetchReservedPagersByMember(member, facility, null);
	}

	public void fetchReservedPagersByMember(Member member, Facility facility,
			PersistenceLayerCallback<List<Pager>> callback) {
		mDaycareCommandInvoker.invoke(new FetchReservedPagersByMember(member, facility, callback));
	}

	public void fetchIncidentsByMember(Member member) {
		fetchIncidentsByMember(member, null);
	}

	public void fetchIncidentsByMember(Member member, PersistenceLayerCallback<List<Incident>> callback) {
		mDaycareCommandInvoker.invoke(new FetchIncidentsByMember(member, callback));
	}
	
	public void fetchFamilyAdmissions(Family family, PersistenceLayerCallback<List<Admission>> callback) {
		mDaycareCommandInvoker.invoke(new FetchFamilyAdmissions(family, callback));
	}
	
	public void fetchFacilityFamilyAdmissions(Family family, Facility facility, PersistenceLayerCallback<List<Admission>> callback) {
		mDaycareCommandInvoker.invoke(new FetchFacilityFamilyAdmissions(family, facility, callback));
	}

    public void fetchFamilyAdmissionBalance(String memberObjectId, PersistenceLayerCallback<HashMap<String, Object>> callback) {
        mDaycareCommandInvoker.invoke(new FetchFamilyAdmissionBalance(memberObjectId, callback));
    }

	public void chargeAdmissions(List<Member> members, Facility facility, boolean shouldChargeAccount, Member memberToCharge, PersistenceLayerCallback<List<HashMap<String, Object>>> callback) {
		mDaycareCommandInvoker.invoke(new ChargeAdmissions(members, facility, shouldChargeAccount, memberToCharge, callback));
	}

	public void fetchAssocFacilities(PersistenceLayerCallback<List<Facility>> callback) {
		mDaycareCommandInvoker.invoke(new FetchAllFacilitiesForAssoc(callback));
	}
	
	public void creditAdmissions(Member member, Facility facility, int amountToCredit, PersistenceLayerCallback<AdmissionBalance> callback) {
		mDaycareCommandInvoker.invoke(new CreditAdmissions(member, facility, amountToCredit, callback));
	}
	
	public void deleteAdmissions(List<Admission> admissions, PersistenceLayerCallback<List<Admission>> callback) {
		mDaycareCommandInvoker.invoke(new DeleteAdmissions(admissions, callback));
	}

	public void createSelfCheckIn(Member member, SelfCheckIn selfCheckIn, PersistenceLayerCallback<SelfCheckIn> callback) {
		mDaycareCommandInvoker.invoke(new CreateSelfCheckIn(member, selfCheckIn, callback));
	}

	public void deleteSelfCheckIn(Member member, PersistenceLayerCallback<SelfCheckIn> callback) {
		mDaycareCommandInvoker.invoke(new DeleteSelfCheckIn(member, callback));
	}

	public ConnectivityUtility getConnectivityUtility() {
		return mConnectivityUtility;
	}

	public PersistenceLayer(Context context, ConnectivityUtility connectivityUtility) {
		super();
		mConnectivityUtility = connectivityUtility;

		mAssociationCode = context.getString(R.string.ASSOCIATION_CODE);
		if (mAssociationCode.equals(context.getString(R.string.ASSOCIATION_CODE_ATL)))
			mHerokuBaseUrl = context.getString(R.string.HEROKU_ENDPOINT_ATL);
		else
			mHerokuBaseUrl = context.getString(R.string.HEROKU_ENDPOINT);

		// Internal Database Configuration
		String parseAppId = context.getString(R.string.PARSE_APPLICATION_ID);
		String parseClientKey = context.getString(R.string.PARSE_CLIENT_KEY);
		String parseServerURL = context.getString(R.string.PARSE_SERVER_URL);
		//boolean usesOfflineMode = context.getResources().getBoolean(R.bool.IS_OFFLINE_MODE_ENABLED);

		mDaycareDataSource = new ParseData(context, parseAppId, parseClientKey, parseServerURL, false);

		// External Database Integration
		String daxkoUsername = context.getString(R.string.DAXKO_USER_NAME);
		String daxkoPassword = context.getString(R.string.DAXKO_PASSWORD);
		String daxkoClientId = context.getString(R.string.DAXKO_CLIENTID);
		String daxkoEndPoint = context.getString(R.string.DAXKO_ENDPOINT);
		Boolean daxkoIsDetailedLog = context.getResources().getBoolean(R.bool.DAXKO_DETAILED_LOG);
		mExternalDataSource = new DaxkoExternalData(daxkoUsername, daxkoPassword, daxkoEndPoint, daxkoClientId,
				daxkoIsDetailedLog);

		// Get Android ID which is used for logging into parse
		mAndroidID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);

		// Initialize internal structure
		mListenerRelay = new ListenerRelay();
		mPersistenceData = new PersistenceData();

		// Initialize invoker queues
		mDaycareCommandInvoker = new Invoker(getDaycareDataSource(), mPersistenceData, mListenerRelay);
		mDaxkoCommandInvoker = new Invoker(getExternalDataSource(), mPersistenceData, mListenerRelay);
		//mHerokuCommandInvoker = new Invoker(getHerokuDataSource(), mPersistenceData, mListenerRelay);

	}

	public void initializeHerokuDataSource() {
		if (mAssociationCode.equals("MSF") || mAssociationCode.equals("CFF") || mAssociationCode.equals("EOS")) {
			String clubNumber = getPersistenceData().getFacility().getYmcaClientDataId();
			mHerokuDataSource = new HerokuData(mHerokuBaseUrl, mAssociationCode, clubNumber, getConnectivityUtility());
		}
		else
			mHerokuDataSource = new HerokuData(mHerokuBaseUrl, mAssociationCode, getConnectivityUtility());
		
		mHerokuCommandInvoker = new Invoker(getHerokuDataSource(), mPersistenceData, mListenerRelay);
	}
	
	private ListenerRelay mListenerRelay;
	private final PersistenceData mPersistenceData;
	private Invoker mDaycareCommandInvoker;
	private Invoker mDaxkoCommandInvoker;
	private Invoker mHerokuCommandInvoker;
	private String mAndroidID;
	private DaycareData mDaycareDataSource;
	private List<PersistenceLayerListener> m_listeners = new ArrayList<PersistenceLayerListener>();
	private ExternalData mExternalDataSource;
	private HerokuData mHerokuDataSource;
	private ConnectivityUtility mConnectivityUtility;
	private String mAssociationCode;
	private String mHerokuBaseUrl;
}
