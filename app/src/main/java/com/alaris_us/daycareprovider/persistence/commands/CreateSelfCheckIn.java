package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.SelfCheckIn;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

import java.util.List;

public class CreateSelfCheckIn extends DaycareDataCommand<SelfCheckIn> {

	private final Member mMember;
	private final SelfCheckIn mSelfCheckIn;

	public CreateSelfCheckIn(Member member, SelfCheckIn selfCheckIn) {

		this(member, selfCheckIn, null);
	}

	public CreateSelfCheckIn(Member member, SelfCheckIn selfCheckIn, PersistenceLayerCallback<SelfCheckIn> callback) {

		super(callback);
		mMember = member;
		mSelfCheckIn = selfCheckIn;
	};

	@Override
	public void executeSource() {
		getDataSource().getSelfCheckInsDAO().getSelfCheckInByMember(mMember, new GetSelfCheckInByMemberCommandListener(this));
	}

	@Override
	protected void handleResponse(SelfCheckIn data) {
		super.handleResponse(data);
	}

	private class GetSelfCheckInByMemberCommandListener implements DaycareOperationComplete<SelfCheckIn> {
		private CreateSelfCheckIn m_listener;

		public GetSelfCheckInByMemberCommandListener(CreateSelfCheckIn listener){
			m_listener = listener;
		}

		@Override
		public void operationComplete(SelfCheckIn arg0, DaycareDataException arg1) {
			if (arg0 == null) {
				getDataSource().getSelfCheckInsDAO().create(mSelfCheckIn, m_listener);
			} else {
				m_listener.operationComplete(null, arg1);
			}
		}
	}
}
