package com.alaris_us.daycareprovider.persistence.commands;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.comparators.ComparatorChain;

import com.alaris_us.daycaredata.to.Cubby;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycareprovider.comparators.CubbyNameComparator;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchAllFacilityCubbies extends DaycareDataCommand<List<Cubby>> {

	private final Facility mFacility;

	public FetchAllFacilityCubbies(Facility facility) {

		this(facility, null);
	}

	public FetchAllFacilityCubbies(Facility facility, PersistenceLayerCallback<List<Cubby>> callback) {

		super(callback);
		mFacility = facility;
	}

	@Override
	public void executeSource() {

		getDataSource().getCubbiesDAO().listAllFacilityCubbies(mFacility, this);
	}

	@Override
	protected void handleResponse(List<Cubby> data) {

		ComparatorChain<Cubby> chain = new ComparatorChain<Cubby>();
		chain.addComparator(new CubbyNameComparator());
		Collections.sort(data, chain);

		((PersistenceData) getPersistentModel()).setAllFacilityCubbies(data);
	}
}
