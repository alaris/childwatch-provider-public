package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.dao.FamilyAssociations.FamilyType;
import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.FamilyAssociation;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class SetupFamilyAssociation extends DaycareDataCommand<FamilyAssociation> {

	private Family m_family;
	private Member m_member;

	public SetupFamilyAssociation(Member members, Family family, PersistenceLayerCallback<FamilyAssociation> callback) {

		super(callback);
		m_member = members;
		m_family = family;
	}

	@Override
	public void executeSource() {

		FamilyAssociation fa = getDataSource().getFamilyAssociationsDAO().getEmptyTO();
		fa.setPMember(m_member);
		fa.setPFamily(m_family);
		fa.setFamilyType(FamilyType.PRIMARY);

		getDataSource().getFamilyAssociationsDAO().create(fa, this);
	}
}
