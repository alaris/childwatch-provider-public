package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchReservedPagersByMember extends DaycareDataCommand<List<Pager>> {
	Facility m_facility;
	Member m_member;

	public FetchReservedPagersByMember(Member member, Facility facility) {
		this(member, facility, null);
	}

	public FetchReservedPagersByMember(Member member, Facility facility,
			final PersistenceLayerCallback<List<Pager>> callback) {
		super(callback);
		m_facility = facility;
		m_member = member;
	}

	@Override
	public void executeSource() {
		getDataSource().getPagersDAO().listReservedPagers(m_facility, m_member, this);
	}
}