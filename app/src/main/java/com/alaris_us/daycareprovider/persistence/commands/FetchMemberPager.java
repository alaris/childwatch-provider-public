package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.List;

import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Pager;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchMemberPager extends DaycareDataCommand<List<List<Pager>>> {
	Facility m_facility;
	Member m_member;

	public FetchMemberPager(Facility facility, Member member) {
		this(facility, member, null);
	}

	public FetchMemberPager(Facility facility, Member member, final PersistenceLayerCallback<List<Pager>> callback) {
		super(new PersistenceLayerCallback<List<List<Pager>>>() {
			@Override
			public void done(List<List<Pager>> data, Exception e) {
				List<Pager> pagers = new ArrayList<Pager>();
				if (data.size() > 0) {
					pagers.addAll(data.get(0));
				}
				callback.done(pagers, e);
			}
		});
		m_facility = facility;
		m_member = member;
	}

	@Override
	public void executeSource() {
		List<Member> list = new ArrayList<Member>();
		list.add(m_member);
		getDataSource().getPagersDAO().listReservedPagers(m_facility, list, this);
	}

	@Override
	public void handleResponse(List<List<Pager>> data) {

	}
}