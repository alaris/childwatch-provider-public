package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchMemberByBarcode extends DaycareDataCommand<Member> {

	String mBarcode;

	public FetchMemberByBarcode(String memberId, PersistenceLayerCallback<Member> callback) {

		super(callback);
		mBarcode = memberId;
	}

	@Override
	public void executeSource() {

		getDataSource().getMembersDAO().getMemberByBarcode(mBarcode, this);
	}
}
