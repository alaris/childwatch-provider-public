package com.alaris_us.daycareprovider.persistence.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alaris_us.daycaredata.DaycareOperationComplete;
import com.alaris_us.daycaredata.dao.Records.SignInType;
import com.alaris_us.daycaredata.exceptions.DaycareDataException;
import com.alaris_us.daycaredata.to.Facility;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycaredata.to.Record;
import com.alaris_us.daycaredata.to.Room;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceData;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

import android.os.Handler;
import android.util.Log;

public class RoomChange extends DaycareDataCommand<List<Member>> {

	private final List<Member> mToCheckout;
	private final Facility mAtLocation;
	private final Room mNewRoom;
	private final Map<Member, Room> mOldRooms;

	public RoomChange(List<Member> toCheckout, Room newRoom, Facility atLocation, PersistenceLayerCallback<List<Member>> callback) {
		super(callback);
		mToCheckout = toCheckout;
		mAtLocation = atLocation;
		mNewRoom = newRoom;
		mOldRooms = new HashMap<Member, Room>();
		for(Member mem : mToCheckout)
			mOldRooms.put(mem, mem.getPRoom());
	}

	@Override
	public void executeSource() {
		for(Member mem : mToCheckout)
			mem.setPRoom(mNewRoom);
		getDataSource().getMembersDAO().updateAll(mToCheckout, new UpdateMembersCb());
	}

	private class UpdateMembersCb implements DaycareOperationComplete<List<Member>> {

		@Override
		public void operationComplete(List<Member> data, DaycareDataException e) {
			Log.d(this.getClass().getSimpleName(), "DONE");
			RoomChange.this.operationComplete(data, e);
			
			Handler h = new Handler();
			h.postDelayed(new Runnable() {
				@Override
				public void run() {
					List<Record> records = new ArrayList<Record>();
					List<Record> recordsHistory = ((PersistenceData) getPersistentModel()).getTodaysSignInRecords();
					for(Member mem : mToCheckout){
						Record outRecord = getDataSource().getRecordsDAO().createRoomChangeOutRecordNoPersist(mem,
								mAtLocation, mem.getPCheckInBy(), SignInType.OTHER, mOldRooms.get(mem) == null ? "" : mOldRooms.get(mem).getName());
						Record inRecord = getDataSource().getRecordsDAO().createRoomChangeInRecordNoPersist(mem,
								mAtLocation, mem.getPCheckInBy(), SignInType.OTHER, mNewRoom == null ? "" : mNewRoom.getName());

						records.add(outRecord);
						records.add(inRecord);
						recordsHistory.add(inRecord);
					}

					((PersistenceData) getPersistentModel()).setTodaysSignInRecords(recordsHistory);

					getDataSource().getRecordsDAO().updateAll(records, new SaveRecordsCb());
				}
			}, 1000);
		}
	}

	private class SaveRecordsCb implements DaycareOperationComplete<List<Record>> {

		@Override
		public void operationComplete(List<Record> data, DaycareDataException e) {
			Log.d(this.getClass().getSimpleName(), "DONE");
		}
	}

}
