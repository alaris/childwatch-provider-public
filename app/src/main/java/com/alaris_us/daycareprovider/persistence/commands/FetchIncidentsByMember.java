package com.alaris_us.daycareprovider.persistence.commands;

import java.util.List;

import com.alaris_us.daycaredata.to.Incident;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchIncidentsByMember extends DaycareDataCommand<List<Incident>> {
	Member m_member;

	public FetchIncidentsByMember(Member member) {
		this(member, null);
	}

	public FetchIncidentsByMember(Member member, final PersistenceLayerCallback<List<Incident>> callback) {
		super(callback);
		m_member = member;
	}

	@Override
	public void executeSource() {
		getDataSource().getIncidentsDAO().findIncidents(m_member, this);
	}
}