package com.alaris_us.daycareprovider.persistence.commands;

import com.alaris_us.daycaredata.to.Family;
import com.alaris_us.daycaredata.to.Member;
import com.alaris_us.daycareprovider.persistence.DaycareDataCommand;
import com.alaris_us.daycareprovider.persistence.PersistenceLayer.PersistenceLayerCallback;

public class FetchFamilyByMember extends DaycareDataCommand<Family> {

	private final Member mMember;

	public FetchFamilyByMember(Member member) {

		this(member, null);
	}

	public FetchFamilyByMember(Member member, PersistenceLayerCallback<Family> callback) {
		super(callback);
		mMember = member;
	}

	@Override
	public void executeSource() {

		getDataSource().getFamiliesDAO().getFamily(mMember, this);

	}

}
