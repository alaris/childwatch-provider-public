# Childwatch Provider for Gradle + Android Studio  

Childwatch Provider application updated to build with Gradle so that new development can continue on Android Studio.  Clone this repostory on your local machine, open the childwatch-provider folder in  Android Studio and you should be all set.

## Workflow

This project follows the [gitflow workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) design for branching and merging.

## Version Information

We will be using the [AndroidAutoVersion gradle plugin](https://github.com/alexfu/androidautoversion) for tracking version numbers in this project.
The version file is located in the project directory.  For this project it is childwatch-provider/app/version

The version file contains the major, minor and patch number.  It should be kept current in bitbucket for new builds.

### Versioning Guidelines

We will use the semversion guidelines for deciding whether to bump the major, minor or patch version of a release.  Those guidelines are [here](http://semver.org/).

### Bumping Versions

To bump a version, a gradle command will need to be executed in the current branch of the code being worked on.  The three ways to bump version are:

./gradlew bumpPatch
./gradlew bumpMinor
./gradlew bumpMajor

Run the applicable commands from the project directory and check the updated version file into source control.

## Testing With Espresso

This project uses Espresso to run UI tests that test the application while running on a device or emulator.  One example of an instrumentation test is given in the [SmokeTest.java](app/src/androidTest/java/com/alaris_us/daycareprovider/systemtests/SmokeTest.java) file.

It runs the test with the Android JUnit Runner and denotes that it will be a large test (meaning that it will access many resources, for more information check [this blog post](https://testing.googleblog.com/2010/12/test-sizes.html))  

~~~java
@RunWith(AndroidJUnit4ClassRunner.class)
@LargeTest
~~~

Then declares the test and makes the DaycareProviderKiosk activity begin for every test by making it a rule:  

~~~java
public class SmokeTest {

    @Rule
    public ActivityTestRule<DaycareProviderKiosk> kiosk =
            new ActivityTestRule<>(DaycareProviderKiosk.class);
~~~

Finally it performs Test(s):

~~~java
    @Test
    public void dashboardDisplaysOnStartup() {
        ...
    }

    @Test
    ...
~~~  

The tests can be run manually from Android Studio by making a run configuration with the module app and the Deployment Target being a connected device.

## Project To Do's
  
- Move all hard dependencies to gradle references.
- Refactor project to use more recent dependency versions.
- Build apk to deploy to repository.
- Test more features with Espresso.
- Externalize configuration values.
- Make an auto executable espresso test to be used with CI/CD.
